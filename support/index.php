<?php
session_start();
//Variables indicant le chemin
define('PATH', './');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<script src="<?php print PATH ?>js/browserDetect.js"></script>
		<script type="text/javascript">
			window.onload = function () {
				document.getElementById("browserInfos").innerHTML = 'Vous utilisez actuellement <strong>' + BrowserDetect.browser + ' ' + BrowserDetect.version + '</strong> sous <strong>' + BrowserDetect.OS + ' ' + BrowserDetect.OStype + '</strong>';
			};
		</script>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div class="jumbotron">
				<!--div style="width: 95%;text-align: right;">
					<a alt="FR" href="<?php print 'http://' . $_SERVER[HTTP_HOST] . $_SERVER['PHP_SELF'] . '?lang=fr-FR'; ?>" target="_top"><?php print localize('lang_french'); ?></a> / <a alt="EN" href="<?php print 'http://' . $_SERVER[HTTP_HOST] . $_SERVER['PHP_SELF'] . '?lang=en-EN'; ?>" target="_top"><?php print localize('lang_english'); ?></a>
				</div-->
				<div class="container">
					<h1><?php print localize('welcome_support'); ?></h1>
					<p><?php print localize('index_selectcat'); ?></p>
				</div>
				<div class="menu_panel" style="margin: 10px;">
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<a  href="pages/<?php print $_SESSION["lang"] ?>/helpdesk.php">
									<div class="caption">
										<div><span class="glyphicon glyphicon-resize-small" aria-hidden="true" style="font-size: 40px;"></span></div>
										<div><h3><?php print localize('helpdesk'); ?></h3></div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<a  href="pages/<?php print $_SESSION["lang"] ?>/conf.php">
									<div class="caption">
										<div><span class="glyphicon glyphicon-blackboard" aria-hidden="true" style="font-size: 40px;"></span></div>
										<div><h3><?php print localize('conf'); ?></h3></div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<div><span class="glyphicon glyphicon-wrench" aria-hidden="true" style="font-size: 40px;"></span></div>
								<div><h3>Outils de diagnostic</h3></div>
								<div>
									<a href="pages/<?php print $_SESSION["lang"] ?>/testenv.php">Testez votre environnement</a><br/>
									<a target="_blank" href="http://www.whatbrowser.org/intl/fr/">Vérifier que votre navigateur est à jour</a><br/>
								</div>
								<div>
									<center>
										<table>
											<tr>
												<td><img src="<?php print PATH ?>images/info.png"/></td>
												<td><p id="browserInfos" style="margin:auto;font-size: small;"></p></td>
											</tr>
										</table>
									</center>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<div class="caption">
									<div><span class="glyphicon glyphicon-hdd" aria-hidden="true" style="font-size: 40px;"></span></div>
									<div><h3>Installation des lecteurs</h3></div>
									<div>
										<a href="pages/<?php print $_SESSION["lang"] ?>/readers/omnikey.php">OmniKey 5321</a><br/>
										<a href="pages/<?php print $_SESSION["lang"] ?>/readers/scm.php">SCM SCL010 / SCL011</a><br/>
										<a href="pages/<?php print $_SESSION["lang"] ?>/readers/cloud.php">Identive CLOUD 4700F</a><br/>
										<a href="pages/<?php print $_SESSION["lang"] ?>/readers/accesso.php">Accesso</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<div class="caption">
									<div><span class="glyphicon glyphicon-file" aria-hidden="true" style="font-size: 40px;"></span></div>
									<div><h3>Applications</h3></div>
									<div>
										<a  href="pages/<?php print $_SESSION["lang"] ?>/jbadger/jbadger.php">Application de lecture de cartes (JBadger)</a><br/>
										<a  href="pages/<?php print $_SESSION["lang"] ?>/jbadger/applet.php">Applet de lecture Java</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<div class="caption">
									<div><span class="glyphicon glyphicon-question-sign" aria-hidden="true" style="font-size: 40px;"></span></div>
									<div><h3>Pré-requis techniques</h3></div>
									<div>
										<a  href="pages/<?php print $_SESSION["lang"] ?>/prereq.php"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> PC / <span class="glyphicon glyphicon-apple" aria-hidden="true"></span> MacOS</a><br/>
										<a  href="pages/<?php print $_SESSION["lang"] ?>/macos.php"><span class="glyphicon glyphicon-apple" aria-hidden="true"></span> Dépannage Mac OS</a><br/>
										<a  href="pages/<?php print $_SESSION["lang"] ?>/prereq_android.php"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> Tablettes / téléphones Android</a><br/>
										<a  href="pages/<?php print $_SESSION["lang"] ?>/prereq_tpe.php"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> Terminaux de paiement</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<div class="caption">
									<div><span class="glyphicon glyphicon-book" aria-hidden="true" style="font-size: 40px;"></span></div>
									<div><h3>Documentation</h3></div>
									<div>
										<a  href="pages/<?php print $_SESSION["lang"] ?>/postal.php"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Modèles Courrier Postal</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail">
								<a  href="pages/<?php print $_SESSION["lang"] ?>/issues/index.php">
									<div class="caption">
										<div><span class="glyphicon glyphicon-random" aria-hidden="true" style="font-size: 40px;"></span></div>
										<div><h3>Network / connection issues (En)</h3></div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </body>
</html>
