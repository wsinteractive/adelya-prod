<title>Adelya - Support</title>
<meta charset="utf-8">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?php print PATH ?>css/support.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php print PATH ?>css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="<?php print PATH ?>css/bootstrap-theme.min.css">
<!-- Jquery -->
<script src="<?php print PATH ?>js/jquery-2.2.0.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php print PATH ?>js/bootstrap.min.js"></script>