<div id="google_translate_element"></div><script type="text/javascript">
	function googleTranslateElementInit() {
		new google.translate.TranslateElement({pageLanguage: 'fr', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
	}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Affichage Mobile (Logo + Bouton de collapse -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#support-navbar" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php print PATH . 'index.php'; ?>"><img src="<?php print PATH ?>images/adelya-logo.png" style="height:100%"/></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="support-navbar">
			<ul class="nav navbar-nav">
				<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/helpdesk.php'; ?>"><span class="glyphicon glyphicon-resize-small" aria-hidden="true"></span> <?php print localize('helpdesk'); ?></a></li>
				<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/conf.php'; ?>"><span class="glyphicon glyphicon-blackboard" aria-hidden="true"></span> <?php print localize('conf'); ?></a></li>
				<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/testenv.php'; ?>"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span> Testez votre environnement</a></li>				
				<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/issues/index.php'; ?>"><span class="glyphicon glyphicon-random" aria-hidden="true"></span> Network / connection issues (En)</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Installation & Documentation<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li class="dropdown-header">Installation des lecteurs</li>
						<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/omnikey.php'; ?>"><span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> OmniKey 5321</a></li>
						<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/scm.php'; ?>"><span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> SCM SCL010 / SCL011</a></li>
						<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/cloud.php'; ?>"><span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> Identive CLOUD 4700F</a></li>
						<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/accesso.php'; ?>"><span class="glyphicon glyphicon-hdd" aria-hidden="true"></span> Accesso</a> </li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Applications</li>
						<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/jbadger/jbadger.php'; ?>"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Application de lecture de cartes (JBadger)</a></li>
						<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/jbadger/applet.php'; ?>"><span class="glyphicon glyphicon-file" aria-hidden="true"></span> Applet de lecture Java</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Pré-requis techniques</li>
						<li><a  href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/prereq.php'; ?>"><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> PC / <span class="glyphicon glyphicon-apple" aria-hidden="true"></span> MacOS</a></li>
						<li><a  href="<?php print PATH . 'pages/' . $_SESSION["lang"] . '/macos.php'; ?>"><span class="glyphicon glyphicon-apple" aria-hidden="true"></span> Dépannage Mac OS</a></li>
						<li><a  href="<?php print PATH . 'pages/' . $_SESSION["lang"] . '/prereq_android.php'; ?>"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span> Tablettes / téléphones Android</a></li>
						<li><a  href="<?php print PATH . 'pages/' . $_SESSION["lang"] . '/prereq_tpe.php'; ?>"><span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span> Terminaux de paiement</a></li>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Documentation</li>
						<li><a  href="<?php print PATH . 'pages/' . $_SESSION["lang"] . '/postal.php'; ?>"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Modèles Courrier Postal</a></li>
					</ul>
				</li>
			</ul>
			<!--ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php print localize('language'); ?><span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a alt="FR" href="<?php print 'http://' . $_SERVER[HTTP_HOST] . $_SERVER['PHP_SELF'] . '?lang=fr-FR'; ?>" target="_top"><?php print localize('lang_french'); ?></a></li>
						<li><a alt="EN" href="<?php print 'http://' . $_SERVER[HTTP_HOST] . $_SERVER['PHP_SELF'] . '?lang=en-EN'; ?>" target="_top"><?php print localize('lang_english'); ?></a></li>
					</ul>
				</li>
			</ul-->
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>