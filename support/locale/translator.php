<?php

if (!isset($_SESSION["lang"])) {
	$_SESSION["lang"] = "fr-FR";
}

if (isset($_GET['lang'])) {
	$_SESSION["lang"] = $_GET["lang"];
}

//Fonction de traduction
function localize($phrase) {
	/* Static keyword is used to ensure the file is loaded only once */
	static $translations = NULL;
	$language = $_SESSION["lang"];
	/* If no instance of $translations has occured load the language file */
	if (is_null($translations)) {
		$lang_file = PATH . 'locale/lang/' . $language . '.json';
		if (!file_exists($lang_file)) {
			$lang_file = PATH . 'locale/lang/' . 'fr-FR.json';
		}
		$lang_file_content = file_get_contents($lang_file);
		/* Load the language file as a JSON object and transform it into an associative array */
		$translations = json_decode($lang_file_content, true);
	}
	return $translations[$phrase];
}

?>