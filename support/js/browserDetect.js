var BrowserDetect = {
		init: function () {
				this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
				this.version = this.searchVersion(navigator.userAgent)
						|| this.searchVersion(navigator.appVersion)
						|| "an unknown version";
				this.OS = this.searchString(this.dataOS) || "an unknown OS";
				this.OStype = this.searchOSType(this.OS, navigator.userAgent);
		},
		searchString: function (data) {
				for (var i=0;i<data.length;i++)	{
						var dataString = data[i].string;
						var dataProp = data[i].prop;
						this.versionSearchString = data[i].versionSearch || data[i].identity;
						if (dataString) {
								if (dataString.indexOf(data[i].subString) != -1)
										return data[i].identity;
						}
						else if (dataProp)
								return data[i].identity;
				}
		},
		searchVersion: function (dataString) {
				var index = dataString.indexOf(this.versionSearchString);
				if (index == -1) return;
				return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
		},
		searchOSType: function (os, userAgent) {
			if (os.indexOf("Windows") === 0) {									
				if (userAgent.indexOf("WOW64") != -1 || userAgent.indexOf("Win64") != -1) {
					return "64bits";
				} else {
					return "32bits";
				}
			} else if (os === "Linux") {									
				if (userAgent.indexOf("i686") != -1) {
					return "32bits";
				} else if (userAgent.indexOf("x86_64") != -1) {
					return "64bits";
				} else {
					return "";
				}
			} else {
				return "";
			}
		},
		dataBrowser: [
				{
						string: navigator.userAgent,
						subString: "Edge",
						identity: "Edge"
				},
				{
						string: navigator.userAgent,
						subString: "Chrome",
						identity: "Chrome"
				},
				{ 	string: navigator.userAgent,
						subString: "OmniWeb",
						versionSearch: "OmniWeb/",
						identity: "OmniWeb"
				},
				{
						string: navigator.vendor,
						subString: "Apple",
						identity: "Safari",
						versionSearch: "Version"
				},
				{
						prop: window.opera,
						identity: "Opera",
						versionSearch: "Version"
				},
				{
						string: navigator.vendor,
						subString: "iCab",
						identity: "iCab"
				},
				{
						string: navigator.vendor,
						subString: "KDE",
						identity: "Konqueror"
				},
				{
						string: navigator.userAgent,
						subString: "Firefox",
						identity: "Firefox"
				},
				{
						string: navigator.vendor,
						subString: "Camino",
						identity: "Camino"
				},
				{		// for newer Netscapes (6+)
						string: navigator.userAgent,
						subString: "Netscape",
						identity: "Netscape"
				},
				{
						string: navigator.userAgent,
						subString: "MSIE",
						identity: "Internet Explorer",
						versionSearch: "MSIE"
				},				
				{
						string: navigator.userAgent,
						subString: "Trident/7.0",
						identity: "Internet Explorer",
						versionSearch: "rv"
				},
				{
						string: navigator.userAgent,
						subString: "Gecko",
						identity: "Mozilla",
						versionSearch: "rv"
				},
				{ 		// for older Netscapes (4-)
						string: navigator.userAgent,
						subString: "Mozilla",
						identity: "Netscape",
						versionSearch: "Mozilla"
				}
		],
		dataOS : [
				///////////////// Windows ///////////////////////////
				{
						string: navigator.userAgent,
						subString: "Windows NT 5.0",
						identity: "Windows 2000"
				},
				{
						string: navigator.userAgent,
						subString: "Windows NT 5.1",
						identity: "Windows XP"
				},
				{
						string: navigator.userAgent,
						subString: "Windows NT 6.0",
						identity: "Windows Vista"
				},
				{
						string: navigator.userAgent,
						subString: "Windows NT 6.1",
						identity: "Windows 7"
				},
				{
						string: navigator.userAgent,
						subString: "Windows NT 6.2",
						identity: "Windows 8"
				},
				{
						string: navigator.userAgent,
						subString: "Windows NT 6.3",
						identity: "Windows 8.1"
				},
				{
						string: navigator.userAgent,
						subString: "Windows NT 10.0",
						identity: "Windows 10"
				},
				{
						string: navigator.platform,
						subString: "Win",
						identity: "Windows"
				},
				///////////////// MAC OS ///////////////////////////
				{
						string: navigator.userAgent,
						subString: "Mac OS X 10_6",
						identity: "Mac OS 10.6"
				},
				{
						string: navigator.userAgent,
						subString: "Mac OS X 10_7",
						identity: "Mac OS 10.7"
				},
				{
						string: navigator.userAgent,
						subString: "Mac OS X 10_8",
						identity: "Mac OS 10.8"
				},
				{
						string: navigator.userAgent,
						subString: "Mac OS X 10_9",
						identity: "Mac OS 10.9"
				},
				{
						string: navigator.userAgent,
						subString: "Mac OS X 10_10",
						identity: "Mac OS 10.10"
				},
				{
						string: navigator.platform,
						subString: "Mac",
						identity: "Mac OS"
				},
				///////////////// MOBILES ///////////////////////////
				{
						string: navigator.userAgent,
						subString: "iPhone",
						identity: "iPhone/iPod"
				},
				{
						string: navigator.userAgent,
						subString: "iPad",
						identity: "iPad"
				},
				{
						string: navigator.userAgent,
						subString: "Android",
						identity: "Android"
				},
				///////////////// LINUX ///////////////////////////
				{
						string: navigator.userAgent,
						subString: "Ubuntu",
						identity: "Linux Ubuntu"
				},
				{
						string: navigator.userAgent,
						subString: "Debian",
						identity: "Linux Debian"
				},
				{
						string: navigator.platform,
						subString: "Linux",
						identity: "Linux"
				}
		]

};					

BrowserDetect.init();