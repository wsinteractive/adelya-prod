<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div id="doc-integ" >
				<br/>

				<h1>Loyalty Operator pour Android</h1>
				<div class="bloc" id="subnav" >
					<table class="menu">
						<tr>
							<td>
								<a href="#presentation">Présentation</a><br/>
								<a href="#prerequis">Prérequis</a><br/>
								<a href="#installation">Installation</a><br/>
								<a href="#utilisation">Utilisation du lecteur de carte</a><br/>						
							</td>
						</tr>
					</table>

				</div>

				<div class="bloc">
					<a name="presentation"></a><h2>Présentation</h2>
					<p>
						Pour les tablettes et smartphones Android, Adelya met à disposition l'application Loyalty Operator sur le 
						<a href="https://play.google.com/store/apps/details?id=com.adelya.android.loyaltyoperator" target="_blank">Google Play Store</a>.<br/>
						<br/>
						Cette application permet d'utiliser les fonctionnalités de Loyalty Operator Card (Loyalty Operator Manager non encore disponible) sur tablette.
						Certains lecteurs peuvent être utilisés avec l'application, par exemple les lecteurs SCM SCL011 et CLOUD 4700F, comme décris au paragraphe
						<a href="#utilisation">Utilisation du lecteur de carte</a>.<br/>
					</p>
				</div>

				<div class="bloc">
					<a name="prerequis"></a><h2>Pré-requis</h2>
					<p>Afin de pouvoir utiliser l'application correctement, la tablette ou le smartphone doit respecter les critères suivants :					
					<ul> 
						<li> <b>Version d'Android:</b> minimum 4.0</li> 
						<li> <b>Taille d'écran:</b> minimum 4"3 et une résolution d'au moins 800px x 480px. Cela correspond par exemple à un téléphone Samsung Galaxy SII.</li> 
						<li> <b>USB:</b> afin d'utiliser un lecteur de cartes, un port USB OTG ou micro-USB OTG est nécessaire. Dans le cas d'un port micro-USB OTG, un adaptateur USB/Micro-USB est nécessaire.</li> 
					</ul> 
					<br/>
					Il est important de noter que le comportement des tablettes avec les lecteurs NFC branchés en USB peut varier en fonction des modèles de tablettes, et un test préalable permet de s'assurer que le lecteur fonctionne.
					</p>
				</div>


				<div class="bloc">
					<a name="installation"></a><h2>Installation</h2>
					<p>
						L'application Loyalty Operator est disponible sur le Play Store de Google en recherchant "Loyalty Operator", ou bien en utilisant le lien suivant:
						<a href="https://play.google.com/store/apps/details?id=com.adelya.android.loyaltyoperator" target="_blank">Loyalty Operator Android</a>
					</p>
					<p>
						Aucune installation n'est nécessaire pour l'utilisation du lecteur sans contact. Au branchement du lecteur, le système Android
						propose directement d'utiliser Loyalty Operator avec le lecteur.
					</p>
				</div>


				<div class="bloc">
					<a name="utilisation"></a><h2>Utilisation du lecteur de carte</h2>
					<p>
						Afin d'être utilisé sur un système Android, le lecteur utilisé doit être compatible "CCID". L'application Loyalty Operator reconnaitra alors
						directement le lecteur au branchement.<br/>
						Parmi les lecteurs que nous proposons, les lecteurs suivants sont compatibles :		
					<ul> 
						<li>
							<a href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/scm.php'; ?>">Lecteur SCM SCL010 / SCL011</a>
						</li>						
						<li>
							<a href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/cloud.php'; ?>">Lecteur Identive 4700 F</a>
						</li>
					</ul> 
					</p>

					<p>
						Les lecteurs suivants ne sont quant à eux malheureusement pas supportés :
					<ul> 
						<li>
							<a href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/omnikey.php'; ?>">Lecteur OmniKey 5321</a>
						</li>
						<li>
							<a href="<?php print PATH . 'pages/'. $_SESSION["lang"] .'/readers/accesso.php'; ?>">Lecteur Accesso</a>
						</li>
					</ul> 
					</p>
				</div>
			</div>
		</div>
	</body>
</html>