<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<script type="text/javascript">
			var BrowserDetect = {
				init: function () {
					this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
					this.version = this.searchVersion(navigator.userAgent)
							|| this.searchVersion(navigator.appVersion)
							|| "an unknown version";
					this.OS = this.searchString(this.dataOS) || "an unknown OS";
				},
				searchString: function (data) {
					for (var i = 0; i < data.length; i++) {
						var dataString = data[i].string;
						var dataProp = data[i].prop;
						this.versionSearchString = data[i].versionSearch || data[i].identity;
						if (dataString) {
							if (dataString.indexOf(data[i].subString) != -1)
								return data[i].identity;
						} else if (dataProp)
							return data[i].identity;
					}
				},
				searchVersion: function (dataString) {
					var index = dataString.indexOf(this.versionSearchString);
					if (index == -1)
						return;
					return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
				},
				dataBrowser: [
					{
						string: navigator.userAgent,
						subString: "Chrome",
						identity: "Chrome"
					},
					{string: navigator.userAgent,
						subString: "OmniWeb",
						versionSearch: "OmniWeb/",
						identity: "OmniWeb"
					},
					{
						string: navigator.vendor,
						subString: "Apple",
						identity: "Safari",
						versionSearch: "Version"
					},
					{
						prop: window.opera,
						identity: "Opera",
						versionSearch: "Version"
					},
					{
						string: navigator.vendor,
						subString: "iCab",
						identity: "iCab"
					},
					{
						string: navigator.vendor,
						subString: "KDE",
						identity: "Konqueror"
					},
					{
						string: navigator.userAgent,
						subString: "Firefox",
						identity: "Firefox"
					},
					{
						string: navigator.vendor,
						subString: "Camino",
						identity: "Camino"
					},
					{// for newer Netscapes (6+)
						string: navigator.userAgent,
						subString: "Netscape",
						identity: "Netscape"
					},
					{
						string: navigator.userAgent,
						subString: "MSIE",
						identity: "Explorer",
						versionSearch: "MSIE"
					},
					{
						string: navigator.userAgent,
						subString: "Gecko",
						identity: "Mozilla",
						versionSearch: "rv"
					},
					{// for older Netscapes (4-)
						string: navigator.userAgent,
						subString: "Mozilla",
						identity: "Netscape",
						versionSearch: "Mozilla"
					}
				],
				dataOS: [
					{
						string: navigator.platform,
						subString: "Win",
						identity: "Windows"
					},
					{
						string: navigator.platform,
						subString: "Mac",
						identity: "Mac"
					},
					{
						string: navigator.userAgent,
						subString: "iPhone",
						identity: "iPhone/iPod"
					},
					{
						string: navigator.platform,
						subString: "Linux",
						identity: "Linux"
					}
				]

			};
			BrowserDetect.init();
			function OS() {
				if (BrowserDetect.OS == "Windows") {
					window.document.getElementById("winos").style.display = "block";
				} else if (BrowserDetect.OS == "Mac") {
					window.document.getElementById("macos").style.display = "block";
				}
			}
		</script>
    </head>
    <body onload="OS();">
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div class="jumbotron">
				<div class="container">
					<p>Afin de participer à la conférence, télécharger et exécuter le programme suivant</p>
					<p>
					<div style="position:relative; width:234px; height:60px;">
						<a href="https://go.teamviewer.com/w85h7j5" style="text-decoration:none;">
							<img src="<?php print PATH ?>images/telechargement.png" alt="Télécharger TeamViewer" title="Télécharger TeamViewer" border="0" width="234" height="60" />
							<span style="position:absolute; top:30.5px; left:60px; display:block; cursor:pointer; color:White; font-family:Arial; font-size:11px; line-height:1.2em; font-weight:bold; text-align:center; width:169px;">
								Télécharger TeamViewer
							</span>
						</a>
					</div>
					</p>
					<div id="winos" style="display: none;">  
						</br>
						</br>
						</br>
						<div style="position:relative; width:234px; height:60px;">
							Si le lien ci-dessus ne fonctionne pas, vous pouvez télécharger TeamViewer <a href="<?php print PATH ?>files/TeamViewerQJ.exe" style="text-decoration:none;">ICI</a> ou <a href="<?php print PATH ?>files/TeamViewerQJ_fr-idcw85h7j5.exe" style="text-decoration:none;">ICI</a>
						</div>	
					</div>
					<div id="macos" style="display: none;"> 
						</br>
						</br>
						</br>
						<div style="position:relative; width:234px; height:60px;">

						</div>
					</div> 
				</div>
			</div>
		</div>
	</body>
</html>