<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div id="doc-integ" >
				<h1>Prérequis techniques du poste de travail PC / MacOS</h1>
				<div class="bloc" id="subnav" >
					<table class="menu">
						<tr>
							<td>
								<a href="#web">Service en ligne</a><br/>
								<a href="#jbadger">Application de lecture de cartes (JBadger)</a><br/>
								<a href="#applet">Applet de lecture Java</a><br/>		
							</td>
						</tr>
					</table>

				</div>

				<div class="bloc">
					<a name="web"></a><h2>Service en ligne</h2>

					<p>La plateforme Adelya est accessible via un navigateur Internet. Vous devez donc disposer d'un ou plusieurs PC sous Windows, Mac OS ou Linux.						
						Vous devez également utiliser un navigateur récent, pour profiter d'un rendu optimal et profiter de toutes les fonctionnalités:
					<ul>
						<li><b>Chrome 10</b> ou plus</li>
						<li><b>Safari 7</b></li>
						<li><b>Firefox 3.6</b> ou plus</li>
						<li><b>Internet Explorer 9</b> minimum</li>
						<li><b>Edge</b> (sans lecture de cartes)</li>
					</ul>
					</p>
                                        <p><b>Important : Une connexion internet haut débit est requise pour faire fonctionner le programme de lecture de carte et accèder au service Adelya</b></p>
					<p><b>Attention :</b> si vous utilisez un lecteur de cartes, des versions plus récentes de ces navigateurs seront nécessaires. Plus d'information dans la rubrique concernant
					l'application de lecture de cartes.</p>
					
					<p>Merci de vérifier que vous avez accès sur votre navigateur à l'adresse suivante : <a href="https://asp.adelya.com/loyaltyoperator/">https://asp.adelya.com/loyaltyoperator</a>. dans le cas contraire, merci de vérifier qu'aucun parefeu ou antivirus ne vous empêche l'accès.</p>
					<p>Le service Loyalty Operator utilise un CDN (réseau de distribution de contenu) afin d'accélérer le chargement de ses pages. A ce jour nous utilisons les services de <a href="http://www.maxcdn.com">MaxCDN</a> la liste des blocs d'IP des serveurs distribuant le contenu est <a href="https://www.maxcdn.com/one/tutorial/ip-blocks/">consultable ici</a>. Il est possible que vous ayez à modifier vos règles de firewall pour autoriser ces blocs.</p>

					<div id="legal">
						(<span style="font-family: Tahoma, Verdana, Segoe, sans-serif; font-size: 11px; line-height: 15px; white-space: normal;">
							<em style="font-family: Tahoma,Verdana,Segoe,sans-serif;">Windows est une marque d&eacute;pos&eacute;e de &nbsp;Microsoft Corporation aux Etats Unis et dans d'autres pays, <span style="border-collapse: separate; color: #000000; font-family: 'Times New Roman'; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; font-size: medium;"><span style="color: #333333; font-family: 'Lucida Grande','Lucida Sans Unicode',Helvetica,Arial,Verdana,sans-serif; font-size: 12px; line-height: 18px;">Mac OS <span style="border-collapse: separate; color: #000000; font-family: 'Times New Roman'; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; font-size: medium;"><span style="font-family: Tahoma,Verdana,Segoe,sans-serif; font-size: 11px; line-height: 15px;"><em style="font-family: Tahoma,Verdana,Segoe,sans-serif;">est une marque d&eacute;pos&eacute;e de &nbsp;</em></span></span>Apple Inc.<span style="border-collapse: separate; color: #000000; font-family: 'Times New Roman'; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; font-size: medium;"><span style="font-family: Tahoma,Verdana,Segoe,sans-serif; font-size: 11px; line-height: 15px;"><em style="font-family: Tahoma,Verdana,Segoe,sans-serif;"> aux Etats Unis et dans d'autres pays</em></span></span></span></span>)</em></span>
					</div>
				</div>
					<br/>

				<div class="bloc">
					<a name="jbadger"></a><h2>Application de lecture de cartes (JBadger)</h2>

					<p>Afin de lire les cartes de fidélité et faire le lien avec le service Loyalty Operator, vous devez installer
						notre application de lecture de cartes:</p>
						<a href="jbadger/jbadger.php">Application de lecture de cartes (JBadger)</a>
					<br/>
				</div>
<br/>

				<div class="bloc">
					<a name="applet"></a><h2>Applet de lecture Java</h2>

					<p>La lecture de cartes via une applet intégrée au navigateur n'est plus conseillée. Néanmoins si vous avez besoin d'informations concernant cet applet,
					vous pouvez consulter la page suivante:</p>
					<a href="jbadger/applet.php">Applet de lecture Java</a>
					<br/>
				</div>
					<br/>
			</div>
		</div>
	</body>
</html>