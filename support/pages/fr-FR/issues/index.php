<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
		<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
		<script src="js/modernizr.js"></script> <!-- Modernizr -->
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<header>
				<h1>Adelya :: Technical Information and Incident Reports</h1>
			</header>

			<section id="cd-timeline" class="cd-container">


				<!-- new section ? -->
				<!-- 
				<div class="cd-timeline-block">
				 <div class="cd-timeline-img cd-picture">
					<img src="img/alert.svg" alt="alerte">
				 </div>
				 <div class="cd-timeline-content">
					 <h2>Title</h2>
					 <p></p>
					 <a href="#0" class="cd-read-more">Read more</a>
					 <span class="cd-date">Jan 14</span>
				  </div> 
				 </div> 
				-->
				<div class="cd-timeline-block">
				 <div class="cd-timeline-img cd-picture">
					<img src="img/alert.svg" alt="alerte">
				 </div>
				 <div class="cd-timeline-content">
					 <h2>DB Synchronisation Error</h2>
					 <h3>11:58 - 12:55</h3>
					 <p>Replication stop unexpectidly at 11:58 leading to disynchronisation between db servers. read/write operation was ok, but readonly operation experienced delay.</p>
					 <p>All traffic has been re-routed to RW servers, to brings back everything to a functionnal state, technical issue is under investigation</p>
					 <span class="cd-date">June 29th</span>
				  </div> 
				 </div> 
				 
				<div class="cd-timeline-block">
					<div class="cd-timeline-img">
						<img src="img/alert.svg" alt="alerte">
					</div>
					<div class="cd-timeline-content">
						<h2>Blocking SQL statistics request</h2>
						<h3>8:45 - 9:38 (GMT+1)</h3>
						<p>Unexpected sql query used for data statistics lock the RW db server</p>                 
						<p>A sql query doing global statistics at 4:00 AM was running at 8:45 am. So far no explanation why, but it leads to table lock and so no oher transaction could be started.<p>				 
							<span class="cd-date">Nov 6th</span>
					</div> 
				</div>

				<div class="cd-timeline-block">
					<div class="cd-timeline-img">
						<img src="img/alert.svg" alt="alerte">
					</div>
					<div class="cd-timeline-content">
						<h2>OS error, service reboot</h2>
						<h3>10:47 - 11h18 (GMT+2)</h3>
						<p>Secondary server reboot à 10h47 and Primary server at 11h18</p>                 
						<p>Our hosting provider change some routing rules. This lead to an error returned on the monitoring url and the supervision deamon decides to reboot the webserver. New supervision rules are under definition. <p>
						<p>Downtime was less than 5 minutes on each server</p>
						<span class="cd-date">Oct. 21th</span>
					</div> 
				</div>

				<div class="cd-timeline-block">
					<div class="cd-timeline-img">
						<img src="img/alert.svg" alt="alerte">
					</div>
					<div class="cd-timeline-content">
						<h2>seconday serveur down.</h2>
						<h3>16:30 - 17h20 (GMT+2)</h3>
						<p>Secondary server is down</p>                 
						<p>Network appliance deffect in datacenter. Main system is up (you should not use asp2.adelya.com in your url...)<p>
							<span class="cd-date">Oct. 7th</span>
					</div> 
				</div> 

				<div class="cd-timeline-block">
					<div class="cd-timeline-img">
						<img src="img/alert.svg" alt="alerte">
					</div>
					<div class="cd-timeline-content">
						<h2>Planned Maintenance</h2>
						<h3>5:00 - 5:30(GMT+2)</h3>
						<p>Frontend loadbalancer update. Expect 5 to 10 minutes downtime.</p>                 
						<p>status : Planned<p>
							<span class="cd-date">Sept 28th</span>
					</div> 
				</div> 

				<div class="cd-timeline-block">
					<div class="cd-timeline-img">
						<img src="img/alert.svg" alt="alerte">
					</div>
					<div class="cd-timeline-content">
						<h2>Servers Unreachables : DDos on Datacenter </h2>
						<h3>9:33 - 10:26 (GMT+2)</h3>
						<p>The primary datacenter was under a DDos attack (<a href="http://travaux.nfrance.com/index.php?do=details&task_id=201">reported here</a>), this lead to mains production and integration servers to be unreachable then very slow to respond.</p>                 
						<p>status : solved<p>
							<span class="cd-date">Sept 17th</span>
					</div> 
				</div> 

				<div class="cd-timeline-block">
					<div class="cd-timeline-img">
						<img src="img/info.svg" alt="info">
					</div>
					<div class="cd-timeline-content">
						<h2>Servers Connection Timeout - BGP errors</h2>
						<h3>14:00 (GMT+2)</h3>
						<p>Error made in Global routing table by Telekom Malaysia impact L3 network  this leads to global internet navigation issues</p>                 

						<a href="http://www.nextinpact.com/news/95399-internet-partie-sites-inaccessibles-a-cause-dun-probleme-routage.htm" class="cd-read-more">Read more</a>

						<span class="cd-date">June 12th</span>
					</div> 
				</div>



                <div class="cd-timeline-block">
					<div class="cd-timeline-img">
						<img src="img/alert.svg" alt="alerte">
					</div>
					<div class="cd-timeline-content">
						<h2>SMTP servers Unreachable</h2>
						<h3>10:12 - 12:10 (GMT+2)</h3>
						<p>Our emailing infrastructure has been slow to answer to connection, hence any loyalty/marketing rule that tries to trigger emails immediatelly wait until timeout and eventually reach max open connection and block.</p>                 
						<p>status : solved<p>
						<p>An alert system has been activated to detect such errors earlier</p>                 
						<span class="cd-date">May 5th</span>
					</div> 
				</div> 

			</section> <!-- cd-timeline -->
		</div>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="js/main.js"></script> <!-- Resource jQuery -->
	</body>
</html>