<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link rel="stylesheet" href="<?php print PATH ?>css/wl.css">
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div id="doc-integ" >
				<h1>Applet de lecture de cartes</h1>
				<div class="bloc" id="subnav" >
					<table class="menu">
						<tr>
							<td>
								<a href="#endapplet">Fin du support de l'applet Java pour les sytèmes récents</a><br/>
								<a href="#presentation">Présentation de l'applet Java</a><br/>
								<a href="#installation">Installation / Mise à jour de Java</a><br/>
								<a href="#macos">Compatibilité MacOS</a><br/>
								<a href="#alertes">Explications des alertes de sécurité</a><br/>	
								<a href="#backapplet">Utiliser de nouveau l'applet après utilisation de l'application de lecture de cartes</a><br/>				
							</td>
						</tr>
					</table>
				</div>

				<!-- ----------------------------------  -->
				<div class="bloc">
					<a name="endapplet"></a><h2>Fin du support de l'applet Java pour les sytèmes récents</h2>
					<p><big>La lecture des cartes de fidélité est possible directement dans votre navigateur Internet grâce à l'applet Java intégrée à Loyalty Operator.<br/>
						<br/>
						Nous faisons évoluer la lecture de cartes, et nous vous conseillons dorénavant d'installer notre application dédiée. Vous bénéficierez des avantages suivants:<br/>
						<ul>
							<li>Simplification de l'utilisation de Loyalty Operator, en évitant notamment les nombreuses mises à jour Java</li>
							<li>Compatible avec l'évolution des navigateurs Internet (qui bloquent de plus en plus l'utilisation de Java)</li>
							<li>Mêmes fonctionnalités que l'applet Java</li>
							<li>Compatible Windows, Linux et MacOS (sous réserve de respect des versions minimum demandées).</li>
						</ul>
						<br/>
						Vous trouverez toutes les informations ainsi que les téléchargements en suivant ce lien: <a href="jbadger.php">Application de lecture de cartes (JBadger)</a>
					</big>
					</p>
					<p>
					<big>
						Pour les systèmes anciens (notamment les postes utilisant <b>Internet Explorer version 9 ou moins</b>), l'utilisation de l'applet 
						est toujours posssible et vous trouverez les informations nécessaires ci-desosus dans cette page.
					</big>
					</p>
					<p>
						<br/>Plus d'information (technique) concernant la fin du support de Java sous chrome:<br/>
					<ul>
						<li><a target="_blank" href="https://java.com/fr/download/faq/chrome.xml">Utilisation de Java avec le navigateur Chrome</a></li>
						<li><a target="_blank" href="https://www.chromium.org/developers/npapi-deprecation">NPAPI deprecation: developer guide</a></li>
					</ul>
					</p>
				</div>

				<!-- ----------------------------------  -->
				<div class="bloc">
					<a name="presentation"></a><h2>Présentation de l'applet Java</h2>
					<p>L'applet Java est une application de lecture de cartes de fidélité intégrée au site Loyalty Operator. Elle est activée
						lors de l'accès au site en dehors de l'application Adelya (via un navigateur web comme Internet Explorer). Elle se présente
						sous la forme d'une icône en bas de page.</p>
					<br/>
					<center>
						<img src="<?php print PATH ?>images/appli.png" alt="Aperçu de l'application de lecture de cartes"/><br/>
						<i>Aperçu de l'application</i><br/>
					</center>
					<br/>
					<h4>Etat du lecteur de carte</h4>
					<p>L'icone de l'application indique l'état du lecteur de cartes.</p>
					<center>
						<table border="1">
							<tr>
								<td width="250"><center><img src="<?php print PATH ?>images/readerOK.png"/><br/>Un lecteur est connecté et reconnu</center></td>
							<td width="250"><center><img src="<?php print PATH ?>images/readerKO.png"/><br/>Aucun lecteur n'est connecté</center></td>
							<td width="250"><center><img src="<?php print PATH ?>images/readerUknwn.png"/><br/>Un lecteur non reconnu est connecté. L'application peut ne pas fonctionner.</center></td>
							</tr>
						</table>
						<br>
						<table border="1">
							<tr>
								<td width="250"><center><img src="<?php print PATH ?>images/error.png"/><br/>Une erreur est survenue: une autre page LoyaltyOperator est peut-être ouverte.</center></td>
							<td width="250"><center><img src="<?php print PATH ?>images/javaWarning.png"/><br/>Java n'est pas installé ou nécessite une mise à jour.</center></td>
							</tr>
						</table>
					</center>
					<br/><br/>
					<h4>Utilisation de Java</h4>
					<p>L'application nécessite la présence de Java sur votre ordinateur. Java est déjà installé sur la plupart des ordinateurs.
						Dans le cas contraire, une installation peut-être nécessaire. <br>
						Référez-vous au paragraphe 'Installation de Java' si cela est nécessaire.
					</p>
				</div>


				<!-- ----------------------------------  -->
				<div class="bloc">
					<a name="installation"></a><h2>Installation / Mise à jour de Java</h2>
					<p>Si vous disposez d'un navigateur récent et à jour, le module Java nécessaire pour
						la lecture de cartes sous Loyalty Operator est déjà installé. Dans le cas contraire une installation est nécessaire.
						Il peut cependant être nécessaire de réaliser une mise à jour.</p>

					<p>En fonction de votre navigateur, l'installation de Java peut vous être proposée automatiquement.</p>

					<ul>
						<li><a href="#javaWeb"> Accès direct à la page de téléchargement Java</a></li><br/>
						<li><a href="#javaIE"> Internet Explorer (toutes versions), Firefox</a></li><br/>
						<li><a href="#javaChrome"> Chrome</a></li><br/>
						<li><a href="#javaSafari"> Safari</a></li><br/>
					</ul>

					<b>ATTENTION: Un redémarrage de votre poste peut-être nécessaire à la fin de l'installation, si votre navigteur ne reconnait toujours pas Java.</b>
					<br/>
					<br/>

					<!--------------- Page web Java --------------------->
					<a name="javaWeb"></a><h4>Accès direct à la page de téléchargement Java</h4>
					<p>Le site de téléchargement de Java est accessible à cette adresse: <a href="http://www.java.com/fr/download/">
							http://www.java.com/fr/download/</a>. </p>
					<center>
						<img src="<?php print PATH ?>images/telechargeJava.png" alt="Site Java" border="1"/><br/>
						<i>Site de téléchargement Java</i><br/>
					</center>
					<br/>
					Cliquez sur "Téléchargement gratuit de Java" pour lancer le téléchargement
					du programme d'installation.
					Le fichier téléchargé (en .exe) doit être exécuté. Le programme d'installation s'ouvre alors. Une fois l'installation
					complétée, relancez LoyaltyOperator pour utiliser l'application de lecture de cartes.<br/>
					<br/>
					<center>
						<img src="<?php print PATH ?>images/telechargeIe9.png" alt="Téléchargement"/><br/>
						<i>Téléchargement sous Internet Explorer 9</i><br/>
						<br/>
						<img src="<?php print PATH ?>images/installJava.png" alt="Install Java"/><br/>
						<i>Installation de Java</i><br/>
					</center>
					<br/>

					<!------------- Internet Explorer ------------->
					<a name="javaIE"></a><h4>Internet Explorer (toutes versions), Firefox</h4>

					<p>Si une installation de Java est nécessaire, l'icône <img src="<?php print PATH ?>images/javaWarning.png"/> est affichée. Un clic sur cette
						icône conduit sur la page de téléchargement de Java.<br>
						<a href="#javaWeb">Page web Java</a></p>
					<br/>
					<br/>

					<!---------------- Chrome ---------------->
					<a name="javaChrome"></a><h4>Chrome</h4>
					<p>
						Pour Chrome, merci d'utiliser l'application de lecture de cartes: <a href="jbadger.php">Application de lecture de cartes</a>
					</p>

					<!----------------- Safari -------------------->
					<a name="javaSafari"></a><h4>Safari</h4>

					<p>En cas d'absence de Java, Safari propose la redirection sur une page de téléchargement. Rendez-vous sur cette page et téléchargez
						la version de Java adaptée à votre système.
						<a href="#javaWeb">Page web Java</a></p>
					<br/>

					<center>
						<img src="<?php print PATH ?>images/safari.png" alt="Java Safari"/><br/>
						<i>Demande d'installation Java.</i><br/>
					</center>
					<br>
				</div>


				<!-- ---------------------------------- -->
				<div class="bloc">
					<a name="macos"></a><h2>Compatibilité Mac OS</h2>
					<p><b>Java 7</b> présente un bug sous MacOS empêchant la reconnaissance des lecteurs de cartes.<br/>
						<b>Java 8</b> ne présente pas ce problème de reconnaissance.
					</p>
					<p>
						Si votre version de OS X est ancienne (avant 10.7), et que vous ne pouvez pas installer Java 8, il est possible de revenir à <b>Java 6</b>. Apple founi un descriptif pour réaliser cette opération:<br>
						<a target="_blank" href="http://support.apple.com/kb/HT5559?viewlocale=en_US">http://support.apple.com/kb/HT5559?viewlocale=en_US</a>
					</p>
					<br/>
				</div>

				<!-- ---------------------------------- -->
				<div class="bloc">
					<a name="alertes"></a><h2>Explications des alertes de sécurité</h2>
					<p>L'exécution des applets Java est mantenant liée à l'autorisation de l'utilisateur. 2 niveaux d'autorisations sont à valider:<br/>
						<br/>
						<b>- Autorisation du navigateur</b><br/>		
						Pour chaque site Internet, les navigateurs demandent l'autorisation d'exécuter des applications Java.<br/><br/>
						En sélectionnant "Toujours exécuter pour ce site", l'autorisation sera mémorisée et l'alerte n'apparaitra plus.
						<br/>
					<center>
						<img src="<?php print PATH ?>images/alertChromeJava.png" alt="Java sous Chrome"/><br/>
						<i>Exemple sous Chrome, autorisation Java.</i><br/>
					</center>
					<b>- Autorisation Java</b><br/>
					En plus du navigateur, l'environnement Java demande l'autorisation d'exécuter l'applet Adelya.<br/><br/>
					L'applet Adelya est signée (vous êtes assurés d'être en présence d'une application Adelya et non frauduleuse). Vous pouvez ainsi controler
					qu'il s'agit de l'application "Lecture de cartes" produite par "Adelya SAS".<br/>
					En sélectionnant "Toujours faire confiance à cet éditeur", l'autorisation sera mémorisée et l'alerte n'apparaitra plus.<br/>
					<br/>
					<center>
						<img src="<?php print PATH ?>images/javaAuthAdelya.png" alt="Java authorisation applet Adelya"/><br/>
						<i>Fenêtre d'autorisation Java.</i><br/>
					</center>
					<br/>
					</p>		
					<p>Les navigateurs récents imposent maintenant d'utiliser une version de Java à jour, sans quoi l'exécution des applets peut être bloquée.
						Par exemple sous Chrome, une alerte informe l'utilisateur que Java n'est pas à jour. 2 cas sont alors possibles:<br/>
						<br/>
						<b>- Exécuter cette fois</b><br/>			
						Permet d'exécuter l'applet sur la page en cours, mais l'alerte sera de nouveau visible après un changement de page.<br/>
						<br/>
						<b>- Mettre à jour</b><br/>	
						Redirige sur le site de mise à jour Java pour télécharger la dernière version.<br/>
						<br/>
					<center>
						<img src="<?php print PATH ?>images/alertOutdatedChrome.png" alt="Java à mettre à jour sous Chrome"/><br/>
						<i>Mise à jour nécessaire.</i><br/>
					</center>
					</p>
					<br/>
				</div>
				
				<!-- ---------------------------------- -->
				<div class="bloc">
					<a name="backapplet"></a><h2>Utiliser de nouveau l'applet après utilisation de l'application de lecture de cartes</h2>
					<p>
						Si vous devez utiliser l'applet Java à la place de l'application externe de lecture de cartes (Jbadger), pour cause par exemple
						d'utilisation d'une architecture Terminal Server, vous serez peut-être amené à réactiver l'applet.<br/>
						En effet, l'application Jbadger désactive l'applet dans votre navigateur, puisque celle-ci n'est alors plus nécessaire.<br/>
						<br/>
						Si cela vous arrive et que l'applet ne se lance pas (icone lecteur rouge), vous pouvez saisir dans la console de votre navigateur:<br/>
						<br/>
						<b>setAppletPermanentActivation(true);</b><br/>
						<br/>
						puis rafraichir la page.
					</p>
					<br/>
				</div>
			</div>
		</div>
	</body>
</html>