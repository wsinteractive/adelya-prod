<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link rel="stylesheet" href="<?php print PATH ?>css/wl.css">
		<script src="<?php print PATH ?>js/browserDetect.js"></script>
		<script type="text/javascript">
			window.onload = function () {
				document.getElementById("browserInfos").innerHTML = 'Vous utilisez actuellement <strong>' + BrowserDetect.browser + ' ' + BrowserDetect.version + '</strong> sous <strong>' + BrowserDetect.OS + ' ' + BrowserDetect.OStype + '</strong>';
			};
		</script>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div id="doc-integ" >
				<br/>

				<h1>Application de lecture de cartes Adelya</h1>
				<div class="bloc" id="subnav" >
					<table class="menu">
						<tr>
							<td>
								<a href="#presentation">Présentation</a><br/>
								<a href="#prerequis">Prérequis</a><br/>
								<a href="#installation">Téléchargement et installation</a><br/>
								<a href="#firefox" style="margin-left: 30px;">Firefox</a><br/>
								<a href="#edge" style="margin-left: 30px;">Edge</a><br/>
								<a>Ajout du certificat de sécurité</a><br/>
								<a href="#certificate_win" style="margin-left: 30px;">Windows</a><br/>
								<a href="#certificate_linux" style="margin-left: 30px;">Linux</a><br/>							
								<a href="#certificate_macos" style="margin-left: 30px;">Mac OS</a><br/>							
								<a href="#utilisation">Utilisation</a><br/>								
								<a href="#lo">Icône sous Loyalty Operator</a><br/>						
								<a href="faq.php">FAQ migration application de lecture de cartes</a><br/>				
							</td>
						</tr>
					</table>
				</div>

				<div class="bloc">
					<a name="presentation"></a><h2>Présentation</h2>
					<p>Afin de pouvoir utiliser un lecteur de cartes sur Loyalty Operator, vous devez installer l'application de lecture de cartes directement sur votre poste
						(compatible Windows XP ou supérieur, MacOS 10.7.4 ou supérieur, Linux).
					</p>
					<p>Cette application fera le lien entre votre lecteur de cartes et Loyalty Operator dans votre navigateur web.</p>	
					<p>L'utilisation de l'applet Java n'est plus supportée (sauf anciens systèmes). Si vous souhaitez en savoir plus, vous pouvez consulter cette page: <a href="applet.php">Applet Java</a>)
					</p>
					<br/>
				</div>

				<div class="bloc">
					<a name="prerequis"></a><h2>Prérequis</h2>
					<p>
						Votre système d'exploitation doit être récent et à jour pour pouvoir correctement utiliser Java, les drivers et notre application de lecture de cartes:<br/>
					<ul>
						<li><b>Windows</b>: Vista, Seven, 8, 8.1 et 10 sont supportés. Windows XP n'étant plus maintenu par microsoft, nous ne garantissons pas le fonctionnement de l'application.</li>
						<li><b>Linux</b>: un système récent (version noyau >= 2.6), avec des accès administrateur est nécessaire. Par exemple Ubuntu 13, Fedora 20, etc.</li>
						<li><b>Mac</b>: OS X version 10.7.4 requis au minimum</li>
					</ul>
					</p>
					<p>
						Vous devez également utiliser un navigateur récent:
					<ul>
						<li><b>Chrome 16</b> ou plus</li>
						<li><b>Safari 7</b> ou plus</li>
						<li><b>Firefox 11</b> ou plus</li>
						<li><b>Internet Explorer 10</b> minimum (sauf Windows 10: non compatible)</li>
						<li><b>Edge</b> sous réserve d'exécuter la commande indiquée dans <a href="#edge">Téléchargement et installation</a></li>
					</ul>
					</p>
					<center>
						<div style="text-align: center;border-width: 1px;border-style: dotted;display: inline-flex;padding-left: 10px;padding-right: 10px;">
							<img src="<?php print PATH ?>images/info.png"/>
							<p id="browserInfos" style="margin:auto;">					
							</p>
						</div>
					</center>
				</div>
				<div class="bloc">
					<a name="installation"></a><h2>Téléchargement et installation</h2>
					<p>
						<span style="font-size: 14px;font-weight: bold;">Windows (Vista, 7, 8 et 10) : </span> <u><a href="<?php print PATH ?>files/JBadger_install.exe">Cliquez ici pour télécharger l'installeur.</a></u><br/>										
						<span style="font-size: 14px;font-weight: bold;">Windows XP (ou si besoin d'avoir Java intégré) : </span> <u><a href="<?php print PATH ?>files/JBadger_install_standalone.exe">Cliquez ici pour télécharger l'installeur incluant Java 7.</a></u><br/>					
					<p>Lancez l'installeur téléchargé. Suivez ensuite les étapes d'installation.<br/>
						<br/>
						Si Java n'est pas installé ou n'est pas à jour (version <b>1.7.0</b> minimum), l'installeur vous proposera d'installer Java. <br/>
						<br/>
						L'installation va créer un raccourci "Adelya" -> "Lecture de cartes" dans votre menu démarrer,
						et l'application démarrera automatiquement à la fin de la procédure.</p>
					<p>A chaque lancement de l'application, celle-ci recherche les mises à jour.</p>
					<br/>
					<span style="font-size: 14px;font-weight: bold;">Linux (DEB: Ubuntu, etc.) : </span><u><a href="<?php print PATH ?>files/jbadger.deb">Cliquez ici pour télécharger l'installeur.</a></u><br/>
					<span style="font-size: 14px;font-weight: bold;">Linux (RPM: Fedora, etc.) : </span><u><a href="<?php print PATH ?>files/jbadger.rpm">Cliquez ici pour télécharger l'installeur.</a></u><br/>
					<br/>
					<br/>
					<span style="font-size: 14px;font-weight: bold;">Mac OS (version Beta): </span><u><a href="<?php print PATH ?>files/Adelya_JBadger.dmg">Cliquez ici pour télécharger l'installeur incluant Java 8.</a></u><br/>
					<p>Une fois téléchargé, ouvrez l'image DMG. Glissez alors l'application 'Lecture de cartes' dans le dossier 'Applications'<br/>
						<br/>
						Si l'application n'est pas autorisée par vos Mac, il vous suffit d'ouvrir le dossier 'Applications',
						puis clic droit sur 'Lecture de cartes', et sélectionnez 'ouvrir'. Il vous sera alors demandé d'autoriser
						l'application pour toutes les futures utilisations.<br/>
					</p>
					</p>
					<br/>
					<center>
						<img src="<?php print PATH ?>images/install.png" alt="Recherche mises à jour..."/><br/>
						<i>Recherche mises à jour</i><br/>
					</center>
					<br/><br/>					
					<a name="firefox"><h4>Firefox</h4></a><br/>
					<big><b>Attention</b></big>, pour Firefox une étape de configuration supplémentaire est nécessaire (voir <a href="#certificate_win">Ajout du certificat</a>)<br/>
					<br/>
					<a name="edge"><h4>Edge</h4></a><br/>
					<p>
					A cause d'une modification de la configuration Internet sous Windows 10, il est nécessaire d'exécuter cette commande avant de pouvoir utiliser l'application
					sous Edge ou Internet Explorer. Elle ajoute localhost.adelya.com comme site de confiance.<br/>
					<br/>
					Cette commande peut être utilisée plusieurs fois sans risque (en cas de doute,) et peut être exécuté directement dans la recherche du menu démarrer.<br/>
					<pre><code><b>reg add "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\ZoneMap\Domains\adelya.com\localhost" /v "*" /t REG_DWORD /d 2 /f</b></code></pre>
					</p>
					<br/>
				</div>
				<div class="bloc">
					<a name="certificate_win"></a><h2>Ajout du certificat de sécurité (Windows)</h2>
					<p>
						Pour que votre navigateur (Chrome, Internet Explorer, etc.) puisse communiquer avec l'application depuis une page sécurisée
						(comme le sont les pages de Loyalty Operator), il est nécessaire d'ajouter le certificat de sécurité Adelya.
					</p>
					<br/>
					<p>
					<h4>Firefox</h4><br/>
					Avant une première utilisation sous Firefox>, merci de lancer l'application, puis lancer la page suivante: <big><a href="javascript: top.location.href='wss.html';">Configuration Firefox</a></big><br/>
					Puis cliquez sur "Valider le certificat de l'application".<br/>
					Sur la page de sécurité du navigateur qui s'ouvre alors, indiquez que vous faites confiance au certificat.
					</p>
					<br/>
					<p>
					<h4>Autres navigateurs (Chrome, internet Explorer, etc.)</h4><br/>
					Le certificat a probablement été ajouté à l'installation. Si ce n'est pas le cas,
					au lancement de l'application, une alerte vous demandera d'ajouter ce certificat:	
					<br/>				
					<center>
						<img src="<?php print PATH ?>images/addcertif.png" alt="Menu"/><br/>
						<i>Demande d'ajout du certificat</i><br/>
					</center>

					<br/>
					Après avoir accepté, sous Windows (à partir de Windows XP SP2), une fenêtre technique vous demandera d'accepter
					l'ajout du certificat comme certificat de confiance. "localhost" signifie que le certificat est destiné à une source locale
					à votre poste, à savoir l'application de lecture de carte.<br/>
					Vous devez accepter l'ajout en cliquant sur OK.<br/>
					<br/>
					<center>
						<img src="<?php print PATH ?>images/certif_windows.png" alt="Menu"/><br/>
						<i>Demande d'ajout du certificat</i><br/>
					</center>
					<br/>
					</p>
					<p>
						Si vous avez manqué l'ajout du certificat, et souhaitez le relancer, cliquez du bouton droit sur l'icône de l'application.
						Si l'option "Ajouter le certificat de sécurité" apparaît dans le menu, vous pouvez la sélectionner pour ajouter le certificat.
						<br/>
					<center>
						<img src="<?php print PATH ?>images/menu2.png" alt="Menu"/><br/>
						<i>Menu avec l'option d'ajout du certificat</i><br/>
					</center>
					</p>
				</div>
				<div class="bloc">
					<a name="certificate_linux"></a><h2>Ajout du certificat de sécurité (Linux)</h2>
					<p>
						Pour que votre navigateur (Chrome, Internet Explorer, etc.) puisse communiquer avec l'application depuis une page sécurisée
						(comme le sont les pages de Loyalty Operator), il est nécessaire d'ajouter le certificat de sécurité Adelya.
					</p>
					<p>
						Sous Linux, vous pouvez utiliser la commande certutil pour ajouter le certificat de sécurité.<br/>
						<br/>				
						Ouvrez un terminal linux, puis placez-vous dans le répertoire d'installation JBadger (probablement <i>/opt/Jbadger/</i>).
						Puis saisissez la commande suivante:<br/>
					<pre><code><b>certutil -A -t "C,," -n "Adelya localhost" -d sql:$HOME/.pki/nssdb -i ./cert_64.cer</b></code></pre>
					</p>
					<p>
						Si certutil n'est pas présent sur votre poste, vous allez devoir l'ajouter.
						<b>Vous devez disposer du mot de passe ROOT pour l'installer:</b><br/>
					<li>Sous Debian/Ubuntu: <b><br/>
							<pre><code>sudo apt-get install libnss3-tools</code></pre></b></li>
					</p>
				</div>
				<div class="bloc">
					<a name="certificate_macos"></a><h2>Ajout du certificat de sécurité (Mac OS)</h2>
					<p>
						Pour que votre navigateur (Chrome, Safari, etc.) puisse communiquer avec l'application depuis une page sécurisée
						(comme le sont les pages de Loyalty Operator), il est nécessaire d'ajouter le certificat de sécurité Adelya.
					</p>
					<br/>
					<p>
					<h4>Firefox</h4><br/>
					Avant une première utilisation sous Firefox>, merci de lancer l'application, puis lancer la page suivante: <big><a href="javascript: top.location.href='wss.html';"></big>Configuration Firefox</a><br/>
					Puis cliquez sur "Valider le certificat de l'application".<br/>
					Sur la page de sécurité du navigateur qui s'ouvre alors, indiquez que vous faites confiance au certificat.
					</p>
					<br/>
					<p>
					<h4>Autres navigateurs (Chrome, Safari, etc.)</h4><br/>
					Au lancement de l'application, une alerte vous demandera d'ajouter ce certificat:	
					<br/>				
					<center>
						<img src="<?php print PATH ?>images/addcertif.png" alt="Menu"/><br/>
						<i>Demande d'ajout du certificat</i><br/>
					</center>

					<br/>
					Après avoir accepté, vos identifiants utilisateurs Mac OS vous seront demandé pour valider l'ajout du certificat.<br/>
					<br/>
					</p>
					<p>
						Si vous avez manqué l'ajout du certificat, et souhaitez le relancer, cliquez du bouton droit sur l'icône de l'application.
						Si l'option "Ajouter le certificat de sécurité" apparaît dans le menu, vous pouvez la sélectionner pour ajouter le certificat.
						<br/>
					<center>
						<img src="<?php print PATH ?>images/menu2.png" alt="Menu"/><br/>
						<i>Menu avec l'option d'ajout du certificat</i><br/>
					</center>
					</p>
				</div>
				<div class="bloc">
					<a name="utilisation"></a><h2>Utilisation de l'application</h2>
					<p>Sous Windows, l'application démarre automatiquement au lancement de votre ordinateur, vous n'avez rien à faire.</p>
					<p>
						L'application se présente sous la forme d'une icône dans la barre des tâches de votre système. Une coche vert est présente si un lecteur est reconnu, et une coche
						rouge si aucun lecteur n'est reconnu.
					<center>
						<img src="<?php print PATH ?>images/taskbar.png" alt="Icone de l'application"/><br/>
						<i>Icone de l'application sous Windows</i><br/>
					</center>

					</p>
					<p>
						Un clic droit sur l'icône de l'application vous permet d'accéder au menu de celle-ci.
					<center>
						<img src="<?php print PATH ?>images/menu.png" alt="Menu"/><br/>
						<i>Menu de l'application</i><br/>
					</center>
					</p>
					<p>
						Les options sont:
					<ul> 
						<li> <b>Informations:</b> vous permet d'afficher les informations sur l'application et les lecteurs de cartes </li> 
						<li> <b>Applet autorisée:</b> si vous sélectionnez cette option, l'applet Java sera activée si vous fermez l'application </li> 
						<li> <b>Afficher logs:</b> vous permer de consulter les logs de l'application (utiles pour le support technique) </li> 
						<li> <b>Aide:</b> vous redirige vers la page de support Adelya </li> 
						<li> <b>Quitter:</b> ferme l'application de lecture de cartes </li> 
					</ul> 
					<br/>
					</p>
				</div>

				<div class="bloc">
					<a name="lo"></a><h2>Icône sous Loyalty Operator</h2>
					<p>L'application de lecture de cartes indique son statut dans Loyalty Operator sous la forme d'une icône en bas de page.</p>
					<br/>
					<center>
						<img src="<?php print PATH ?>images/appli.png" alt="Aperçu de l'application de lecture de cartes"/><br/>
						<i>Etat des lecteurs</i><br/>
					</center>
					<br/>
					<h4>Etat du lecteur de cartes</h4>
					<p>L'icône de l'application indique l'état du lecteur de cartes.</p>
					<center>
						<table border="1">
							<tr>
								<td ><center><img src="<?php print PATH ?>images/readerOK.png"/><br/>Un lecteur est connecté et reconnu</center></td>
							<td ><center><img src="<?php print PATH ?>images/readerKO.png"/><br/></center>
							<ul>
								<u>Raisons possibles:</u>
								<li>Aucun lecteur n'est connecté</li>
								<li>l'application n'est pas lancée</li>
								<li>le certificat n'a pas été ajouté</li>					
							</ul>
							</td>
							<td ><center><img src="<?php print PATH ?>images/readerUknwn.png"/><br/>Un lecteur non reconnu est connecté. L'application peut ne pas fonctionner.</center></td>
							</tr>
						</table>
					</center>
					<br/><br/>
				</div>
			</div>
		</div>
	</body>
</html>