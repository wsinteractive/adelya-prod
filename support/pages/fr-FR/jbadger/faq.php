<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link rel="stylesheet" href="<?php print PATH ?>css/wl.css">
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div id="doc-integ" >
				<br/>

				<h1>FAQ migration application de lecture de cartes</h1>
				<div class="bloc" id="subnav" >
					<table class="menu">
						<tr>
							<td>
								<ul>
									<li><a href="#concerne">Suis-je concerné ?</a></li>
									<li><a href="#dejainstallee">Comment savoir si l’application de lecture de cartes est déjà installée sur mon poste ?</a></li>
									<li><a href="#versionOS">Je ne connais pas la version de mon OS ou de mon navigateur ?</a></li>				
									<li><a href="#okinstall">J’ai déjà installé l’application lecture de cartes ?</a></li>								
									<li><a href="#echecmaj">Je n’arrive pas à installer la mise à jour sur mon poste ?</a></li>		
								</ul>
							</td>
						</tr>
					</table>
				</div>

				<div class="bloc">
					<a name="concerne"></a><h2>Suis-je concerné ?</h2>
					<p>Vous n’êtes pas concernés et <u>vous n’avez rien à faire</u> :
					<ul>
						<li>Si vous n’utilisez pas de lecteur de cartes</li>
						<li>Si vous utilisez l’application Adelya Loyalty Operator sur tablette, sur smartphone ou sur TPE</li>
						<li>Si l’application lecture de cartes est déjà installée sur votre poste (pour savoir si l’application est déjà installée, <a href="#dejainstallee">voir ici</a>)</li>
					</ul>
					Dans tous les autres cas, vous devez faire la mise à jour : 
					<ul>
						<li>Vous utilisez un lecteur branché sur votre poste pour identifier vos clients avec leurs cartes</li>
						<li>Vous accédez à la plateforme Loyalty Operator d’Adelya depuis un navigateur Web (Chrome, Internet Explorer, Safari, Firefox, etc.)</li>
					</ul>
					Vous devez faire la mise à jour et télécharger l’application de lecture de cartes : pour ce faire, <a href="http://www.adelya.com/support/jbadger/jbadger.html">cliquez ici</a>.
					</p>				
				</div>

				<div class="bloc">
					<a name="dejainstallee"></a><h2>Comment savoir si l’application de lecture de cartes est déjà installée sur mon poste ?</h2>
					<p>
						Pour vérifier si l’application de lecture de carte a déjà été installée, nous vous invitons à vous connecter à votre plateforme de relation client.<br/>
						<br/>
						Vous devez être dans la partie Card de Loyalty Operator d’Adelya. 
						1. Cliquez sur l’icone "lecteur" en bas à gauche de la page.<br/>
						<br/>
					<center><img src="iconJbadger.png" alt="Icone lecteur" style="width:90%;"/></center><br/>
					<br/>
					2. Une fenêtre va s’afficher (si rien ne se passe, vérifiez que votre navigateur ne bloque pas l’affichage des nouvelles fenêtres)<br/>
					<br/>
					3. Regarder le titre de cette pop-up :<br/>
					<br/>
					S’il y a marqué Loyalty Operator: JBadger, alors l’application de lecture de cartes est déjà installée, vous n’avez rien à faire<br/>
					<br/>
					<center><img src="windowJbadger.png" alt="Fenêtre JBadger" style="width:90%;"/></center><br/>
					<br/>
					S’il y a marqué Loyalty Operator: Applet Java, l’application n’est pas installée. Vous devez faire la mise à jour (<a href="http://www.adelya.com/support/jbadger/jbadger.html#installation">cliquez ici)</a><br/>
					<br/>
					<center><img src="windowApplet.png" alt="Fenêtre Applet Java" style="width:90%;"/></center><br/>
					</p>

				</div>
				<div class="bloc">
					<a name="versionOS"></a><h2>Je ne connais pas la version de mon OS ou de mon navigateur ?</h2>
					<p>
						Pas de panique !!<br/>
						Nous vous proposons de vous rendre sur la page suivante pour retrouver ces informations : <a href="http://www.adelya.com/support/jbadger/jbadger.html#prerequis">cliquez ici</a>. <br/>
						<br/>
					<center><img src="showBrowser.png" alt="Fenêtre Applet Java" style="width:90%;"/></center><br/>
					<br/>
					Ces informations vous seront utiles pour choisir la version de l’application de lecture de cartes à télécharger.
					</p>
				</div>
				<div class="bloc">
					<a name="okinstall"></a><h2>J’ai déjà installé l’application lecture de cartes ?</h2>
					<p>
						Dans ce cas là, vous n’avez rien à faire, vous pouvez continuer à profiter de votre plateforme de relation client.<br/>
						<br/>
						L’application se met à jour automatiquement si besoin.
					</p>
				</div>
				<div class="bloc">
					<a name="echecmaj"></a><h2>Je n’arrive pas à installer la mise à jour sur mon poste ?</h2>
					<p>					
						Pour installer le logiciel, vous devez disposer des droits d'administration sur votre poste.<br/>
						Ainsi l’installation de la mise à jour peut être restreinte sur certains postes en raison de règles de sécurité. Cela peut être le cas notamment si vous faites partie d’une enseigne ou d’un réseau ou si vous installez le logiciel sur un poste de caisse.<br/>
						<br/>
						Dans ce cas là, nous vous invitons à contacter votre service informatique. 
					</p>
				</div>
			</div>
		</div>
	</body>
</html>