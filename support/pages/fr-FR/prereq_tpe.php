<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div id="doc-integ" >
				<br/>

				<h1>Loyalty Operator sur terminal de paiement</h1>
				<div class="bloc" id="subnav" >
					<table class="menu">
						<tr>
							<td>
								<a href="#presentation">Présentation</a><br/>
								<a href="#prerequis">Prérequis</a><br/>
								<a href="#faq">FAQ</a><br/>
							</td>
						</tr>
					</table>

				</div>

				<div class="bloc">
					<a name="presentation"></a><h2>Présentation</h2>
					<p>
						Loyalty Operator Card est disponible sur certains TPE Ingenico. Cette application permet de lire les cartes de fidélité
						directement sur le TPE, de gérer l'ajout de points / chiffre d'affaire et d'utiliser les coupons sur le même
						terminal que celui utilisé pour le paiement.
					</p>
				</div>


				<div class="bloc">
					<a name="prerequis"></a><h2>Pré-requis</h2>
					<p>
						Loyalty Operator Card est compatible avec les terminaux <b>icl250</b>, <b>iwl250</b> et <b>iwl280</b> d'Ingenico, en connexion IP.<br/>
						<br/>
						Le terminal doit:<br/>
					<ul>
						<li>Etre connecté à Internet via une connexion IP (cable, et non pas 2G/3G mobile)</li>
						<li>Etre compatible pour la lecture sans contact ("Contactless")</li>
					</ul>
					<br/>
					Pour souscrire au service, merci de vous mettre en relation avec votre contact commercial chez Adelya.
					<br/>
					<center>
						<table>
							<tr>
								<td style="padding:20px;"><img src="<?php print PATH ?>images/ict250.jpg"><br/><center>Ingenico ict250</center></td>
							<td style="padding:20px;"><img src="<?php print PATH ?>images/iwl250.jpg"><br/><center>Ingenico iwl250</center></td>
							<td style="padding:20px;"><img src="<?php print PATH ?>images/iwl280.jpg"><br/><center>Ingenico iwl280</center></td>
							</tr>
						</table>
					</center>
					</p>
				</div>

				<div class="bloc">
					<a name="faq"></a><h2>FAQ</h2>
					<p>
					<h4><b>Comment accéder au menu de l'application ?</b></h4>
					<br/>
					Au lancement de l'application, si vous êtes connectés, l'écran de lecture de cartes s'affichera. Appuyez sur <span class="redbutton">X</span>
					pour accéder au menu.
					</p>
					<p>
					<h4><b>Comment me déconnecter ?</b></h4>
					<br/>
					Accédez au menu comme décrit plus haut. L'option "Se déconnecter" est la dernière du menu.
					</p>
					<p>
					<h4><b>L'application affiche toujours l'erreur "Impossible de se connecter à la base", que dois-je faire ?</b></h4>
					<br/>
					Vérifiez que votre terminal est bien connecté au bluetooth et au réseau (B entouré d'un rectangle bleu, ainsi que jauge dans le vert sus sa gauche).<br/>
					Si le problème persiste, vous pouvez essayer de redémarrer le terminal en appuyant simultanément sur <span class="button">.</center></span>
					et <span class="yellowbutton">&#60;</span>.
					</p>
					<p>
					<h4><b>Configuration réseau / pare-feu</b></h4>
					<br/>
					Afin de réaliser l'installation puis de fonctionner, l'application a besoin que les ports suivants soient ouverts (TCP et UDP) dans votre configuration
					réseau (pare-feu, box internet, etc.).<br/>
					<br/>
					<table class="border"><colgroup><col width="150" /><col width="100" /><col width="350" /></colgroup>
						<tbody>
							<tr>
								<td><b>IP</b></td>
								<td><b>Port</b></td>
								<td><b>Description</b></td>
							</tr>
							<tr>
								<td>83.206.130.148</td>
								<td>21000</td>
								<td>Installation service Incendo d&rsquo;Ingenico</td>
							</tr>
							<tr>
								<td>217.109.089.152</td>
								<td>
									<span class="il">7006</span>
								</td>
								<td>Installation plugin Adelya</td>
							</tr>
							<tr>
								<td>91.208.214.30</td>
								<td>61001</td>
								<td>Service Incendo initialisation (PKIv1)</td>
							</tr>
							<tr>
								<td>91.208.214.30</td>
								<td>61000</td>
								<td>Service Incendo donn&eacute;es (PKIv1)</td>
							</tr>
							<tr>
								<td>91.208.214.1</td>
								<td>63001</td>
								<td>Service Incendo initialisation (PKIv3)</td>
							</tr>
							<tr>
								<td>91.208.214.1</td>
								<td>63000</td>
								<td>Service Incendo donn&eacute;es (PKIv3)</td>
							</tr>
						</tbody>
					</table>
					</p>
				</div>
			</div>
		</div>
	</body>
</html>