<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div class="jumbotron">
				<div class="container">
					<p>L'application Adelya vous permet d'automatiser l'envoi de courrier postal à vos clients</p>
					<p>La lettre envoyée est de type "double fenêtre", il est donc important lorsque vous allez créer votre lettre de tenir compte des zones occupées par l'adresse de l'expéditeur et du client</p>
					<p><a href="<?php print PATH ?>files/gabarit.pdf">Télécharger le gabarit modèle</a></p>
				</div>
			</div>
		</div>
	</body>
</html>