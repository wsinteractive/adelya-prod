<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div id="doc-integ" >
				<h1>Dépannage Mac OS</h1>
				<div class="bloc" id="subnav" >
					<table class="menu">
						<tr>
							<td>
								<a href="#kownProblems">Problèmes connus en fonction des versions</a><br/>
								<a href="#prerequis">Rappel des pré-requis</a><br/>
								<a href="#java">Problèmes Java</a><br/>
								<a href="#drivers">Problèmes de pilotes de votre lecteur</a><br/>		
								<a href="#pcscd">Problèmes de détection du lecteur</a><br/>		
							</td>
						</tr>
					</table>

				</div>

				<div class="bloc">
					<a name="kownProblems"></a><h2>Problèmes connus en fonction des versions</h2>

					<ul>
						<li><b>MacOS version &#139;= 10.6: </b>Nous ne pouvons pas garantir le fonctionnement des lecteurs sur ces versions.</li>
						<li><b>MacOS version 10.7.5 ou 10.8: </b>Java 7 ne reconnait pas les lecteurs, Java 8 doit donc être utilisé, ou un retour à Java 6 est possible,
							comme décrit dans ce paragraphe: <a href="#java">Problèmes Java</a></li>
						<li><b>MacOS version &#139;= 10.10: </b><br/>
							Java 7 ou Java 8 peuvent-être installés et utilisés, dans ce cas la compatibilité avec les navigateurs Internet est la suivante:
							<ul>
								<li>Firefox: Le lecteur fonctionne</li>
								<li>Chrome: Chrome doit être installé en version 64bits pour faire fonctionner Java</li>
								<li>Safari: Java fonctionne, mais le lecteur est parfois non reconnu</li>
							</ul>
							L'utilisation de Java 6 quand cela est possible, permet d'être compatible avec tous les navigateurs.<br/>
							<br/>
							<b>ATTENTION: </b>Si votre Mac a été mis à jour en version 10.9 ou 10.10, il sera probablement nécessaire d'intaller le patch suivant pour reconnaitre le lecteur: <a href="#pcscd">Problèmes de détection du lecteur</a>
						</li>
					</ul>
				</div>

				<div class="bloc">
					<a name="prerequis"></a><h2>Rappel des pré-requis</h2>

					<p>Comme indiqué sur la page concernant les pré-requis techniques (<a href="<?php print PATH ?>pages/<?php print $_SESSION["lang"] ?>/prereq.php#lightclient">Pré-requis PC / MacOS</a>),
						il est nécessaire d'utiliser au minimum la version <b>10.7.4</b> de MacOS, un navigateur internet récent, ainsi que <b>Java 6</b> ou <b>Java 8</b>.</p>
				</div>


				<div class="bloc">
					<a name="java"></a><h2>Problèmes Java</h2>

					<p>Comme indiqué sur la page concernant l'applet de lecture Java (<a href="<?php print PATH ?>pages/<?php print $_SESSION["lang"] ?>/jbadger/applet.php#macos">Applet de lecture Java</a>),
						un bug sous Java 7 pour les environnements Mac OS peut empêcher la détection des lecteurs de cartes. Il est donc recommandé d'installer Java 8 ou de revenir à Java 6 (mis à jour régulièrement)
						en suivant la procédure indiquée par Apple:<br/><br/>
						<a target="_blank" href="http://support.apple.com/kb/HT5559?viewlocale=en_US">http://support.apple.com/kb/HT5559?viewlocale=en_US</a>
					</p>
				</div>


				<div class="bloc">
					<a name="drivers"></a><h2>Problèmes de pilotes de votre lecteur</h2>

					<p><i>Avant d'utiliser votre lecteur de cartes, vous devez installer les pilotes pour Mac OS. Vous trouverez ces pilotes en sélectionnant
							la page de votre lecteur dans le menu de gauche.</i></p>

					<p>Si malgré l'installation des pilotes, et l'utilisation de Java 6, votre lecteur ne semble pas fonctionner (lecteur reconnu mais carte non détectée par exemple),
						vous pouvez tenter de supprimer des pilotes installés précédemment, et recommencer l'installation.
						<img src="<?php print PATH ?>images/readerOK.png"></p><br/>
					<p>Pour cela vous devez placer dans la corbeille tous les fichiers du dossier <pre>/usr/libexec/SmartCardServices/drivers/</pre>
					à l'exception de <pre>ifd-CCID.bundle</pre></p>
				</div>


				<div class="bloc">
					<a name="pcscd"></a><h2>Problèmes de détection du lecteur</h2>

					<p><i>Avant d'utiliser votre lecteur de cartes, vous devez installer les pilotes pour Mac OS. Vous trouverez ces pilotes en sélectionnant
							la page de votre lecteur dans le menu de gauche.</i></p>

					<p>Si malgré l'installation des pilotes, et l'utilisation de Java 6, votre lecteur ne semble pas reconnu (croix rouge sur l'icone de l'applet),
						il est peut-être nécessaire de corriger un problème connu sur l'application pcscd de votre Mac.
						<img src="<?php print PATH ?>images/readerKO.png"> Ce problème survient généralement après la mise à jour de Mac OS.
						<br/>
					</p>
					<p>Pour corriger ce problème, téléchargez l'installeur suivant: <a href=<?php print PATH ?>files/pcscd_autostart.pkg.zip">Patch pcscd.</a><br/>
						Une fois téléchargé, lancez-le en double-cliquant sur le fichier, et suivez les étapes d'installation. Un redémarrage de votre Mac sera nécessaire.</p>
				</div>
			</div>
		</div>
	</body>
</html>