<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link href="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script src="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.js"></script>
		<script src="<?php print PATH ?>js/browserDetect.js"></script>
		<script type="text/javascript">
			function DetectOS() {
				if (BrowserDetect.OS == "Windows") {
					$("#accordion").accordion("activate", 0)
				} else if (BrowserDetect.OS == "Mac OS") {
					$("#accordion").accordion("activate", 1)
				} else if (BrowserDetect.OS == "Linux") {
					$("#accordion").accordion("activate", 2)
				}
			}

			$(function () {
				$("#accordion").accordion();
			});
		</script>
		<style>
			.ui-widget-content a {
				color: blue;
			}
			.ui-state-active { 
				background: #e6e6e6;
			}

		</style>
    </head>
    <body onload="DetectOS();">
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div>
				<b>1 - Installation du lecteur de carte Identive 4700 F</b><br/><br/>
				<div class="menu_panel" style="margin: 10px;">
					<div class="row">
						<div class="col-xs-6 col-md-3">
							<a href="#accordion" class="thumbnail" style="border-color: #337ab7;">
								<img src="<?php print PATH ?>images/cloud4700.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="omnikey.php" class="thumbnail">
								<img src="<?php print PATH ?>images/omnikey.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="scm.php" class="thumbnail">
								<img src="<?php print PATH ?>images/scm.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="accesso.php" class="thumbnail">
								<img src="<?php print PATH ?>images/accesso.jpg"/>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div id="accordion">
				<h3><a href="#">Windows</a></h3>
				<div id="windowsos">
					<b>Windows XP, Vista, 7, 8, ...</b><br/>
					Les pilotes du lecteurs doivent être téléchargés et installés avant le branchement du lecteur.<br/>
					Merci de télécharger les pilotes en suivant ce lien: <a href="<?php print PATH ?>files/Identive_Cloud_Smart_Card_Reader.msi">Pilotes Identive CLOUD 4700 F</a><br/><br/>
					Un redémarrage du PC est recommandé après l'installation.
					<br/><br/>
					Une fois le driver installé, branchez le lecteur sur un port USB de votre ordinateur, et patientez jusqu'à la fin de l'installation du lecteur.
					<br/><br/>
					<b>Attention ! Si vous avez installé le lecteur sous Windows 7 sans utiliser le driver fourni, il peut être nécessaire d'effectuer une mise à jour.
						Plus d'information: <a href="cloud_w7.php">Mise à jour driver</a></b>
					<br/><br/>
					Pour plus d'information sur le lecteur, vous pouvez consulter la page du fabricant:
					<a target="_blank" href="http://www.identive-group.com/products-and-solutions/identification-products/desktop-readers-terminals/dual-interface-desktop-readers/cloud-4700-f-dual-interface-desktop-reader#downloads">Identive 4700 F</a>
					<br/><br/>
					<b><span style="color: red">Note à l'attention des utilisateurs de Windows 7 :</span></b><br/>
					Si vous utilisez windows 7, il est fort probable lors de chaque passage de cartes que Windows tente de les identifier<br/><br/>
					Pour désactiver ou réactiver la fonctionnalité qui contrôle cette identification, vous pouvez utiliser l'un des patchs ci-dessous<br/>
					- Désactiver la détection des cartes en utilisant ce programme :<br/>
					<a href="<?php print PATH ?>files/fix_card32.exe">Patch pour Windows 7 32 bits</a><br/>
					<a href="<?php print PATH ?>files/fix_card64.exe">Patch pour Windows 7 64 bits</a><br/><br/>
					- Réactiver la détection des cartes en utilisant ce programme :<br/>
					<a href="<?php print PATH ?>files/revert_card32.exe">Patch pour Windows 7 32 bits</a><br/>
					<a href="<?php print PATH ?>files/revert_card64.exe">Patch pour Windows 7 64 bits</a><br/>
				</div>
				<h3><a href="#">Mac OS</a></h3>
				<div id="macos">
					Les pilotes du lecteur doivent être téléchargés et installés avant le branchement du lecteur. Les versions de MacOS X supérieures ou égales
					à <b>10.7</b> sont supportées. Cependant, des problèmes techniques au niveau de MacOS empêchent parfois de garantir la compatibilité du lecteur avec
					MacOS, quelle que soit la version.<br/>
					<br/>
					Merci de télécharger les pilotes en suivant ce lien: <a href="<?php print PATH ?>files/scmccid_mac_5.0.35.zip">Pilotes Identive CLOUD 4700 F</a><br/><br/>
					Décompressez le fichier télécharger, et lancez l'installeur (fichier .pkg)<br/>
					Un redémarrage de votre Mac peut être nécessaire après l'installation.
					<br/><br/>
					Pour plus d'information sur le lecteur, vous pouvez consulter la page du fabricant:
					<a target="_blank" href="http://www.identive-group.com/products-and-solutions/identification-products/desktop-readers-terminals/dual-interface-desktop-readers/cloud-4700-f-dual-interface-desktop-reader#downloads">Identive 4700 F</a>
					<br/><br/><br/>
				</div>
				<h3><a href="#">Linux</a></h3>
				<div id="linuxos">						
					1. Vous devez installer les applications pcscd et pcsc-tools. Par exemple avec la commande apt-get:<br/>
					<b>sudo apt-get install pcscd</b><br/>
					<b>sudo apt-get install pcsc-tools</b><br/>  
					<br/>
					2. Vous pouvez vérifier la bonne reconnaissance de votre lecteur en utilisant la commande pcsc_scan.<br/>
					Le lecteur doit alors apparaitre dans la liste des lecteurs affichés.<br/>
					<br/>
					Si le lecteur n'apparait pas dans la liste, veuillez redémarrer votre machine, ou redémarrer le processus pcscd.<br/>
					<br/>
					3. Aucun driver spécifique n'est nécessaire. Nous conseillons notamment de ne pas installer les drivers fournis par le
					fabricant.<br/>
					<br/>
					Pour plus d'information sur le lecteur, vous pouvez consulter la page du fabricant:
					<a target="_blank" href="http://www.identive-group.com/products-and-solutions/identification-products/desktop-readers-terminals/dual-interface-desktop-readers/cloud-4700-f-dual-interface-desktop-reader#downloads">Identive 4700 F</a>
					<br/><br/><br/>
				</div>		
			</div>
			<div>
				<br/><br/>
				<b>2 - Installation de l'application de lecture de cartes</b><br/><br/>
				Une fois le lecteur installé, vous devez installer l'application de lecture de cartes afin de pouvoir l'utiliser sous Loyalty Operator.<br/>
				<br/>
				Vous trouverez toutes les informations ainsi que les téléchargements en suivant ce lien: <a href="<?php print PATH ?>pages/<?php print $_SESSION["lang"] ?>/jbadger/jbadger.php">Application de lecture de cartes (JBadger)</a>
			</div>
		</div>
	</body>
</html>