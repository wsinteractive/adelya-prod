<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link href="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script src="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.js"></script>
		<script src="<?php print PATH ?>js/browserDetect.js"></script>
    </head>
    <body>
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<h3>Etapes d'installations lecteur Omnikey 5321 sous Windows XP et inférieur</h3><br/>
			<b>Installation :</b> Brancher le lecteur sur votre ordinateur
			Le message suivant doit apparaitre :<br>
			<center>
				<img src="http://www.adelya.com/download/omnikey/files/SCREEN1.JPG"/>
			</center>		
			<br/>Cliquer sur Oui, cette fois seulement puis sur le bouton "Suivant"<br/>
			Windows va rechercher les pilotes du lecteur, une fois trouvés vous devriez avoir l'écran ci-dessous :<br/>
			<center>
				<img src="http://www.adelya.com/download/omnikey/files/SCREEN2.JPG"/>
			</center>
			<br/>Choisir  "Installer automatiquement" et cliquer sur suivant.

			<br/>Le pilote du lecteur doit être installé.<br/><br/>

			Pour vérifier,<br/>
			- Faire un clic droit sur "Poste de Travail" puis cliquer sur "Propriété"<br/>
			- Cliquer sur l'onglet "Matériel" puis sur "Gestionnaire de Périphérique"
			Si le lecteur est installé, vous devriez voir son nom marqué dans la liste :<br/>
			<center>
				<img src="http://www.adelya.com/download/omnikey/files/SCREEN3.JPG"/>
			</center>
			<br/><span style="color:red;font-weight: bold">Si ce n'est pas le cas :</span>
			<br/>Vous devriez trouver un point d'exclamation sur le matériel comme ci-dessous :<br/>
			<center>
				<img src="http://www.adelya.com/download/omnikey/files/SCREEN4.JPG"/>
			</center><br/>
			Pour installer :<br/>
			- Cliquer avec le bouton droit sur "Smart Card Reader USB" puis sur "Mettre à jour le pilote"
			- Suivez les étapes d'installation en haut de ce guide
		</div>
	</body>
</html>