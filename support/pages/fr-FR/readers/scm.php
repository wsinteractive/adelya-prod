<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link href="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script src="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.js"></script>
		<script src="<?php print PATH ?>js/browserDetect.js"></script>
		<script type="text/javascript">
			function DetectOS() {
				if (BrowserDetect.OS == "Windows") {
					$("#accordion").accordion("activate", 0)
				} else if (BrowserDetect.OS == "Mac OS") {
					$("#accordion").accordion("activate", 1)
				} else if (BrowserDetect.OS == "Linux") {
					$("#accordion").accordion("activate", 2)
				}
			}

			$(function () {
				$("#accordion").accordion();
			});
		</script>
		<style>
			.ui-widget-content a {
				color: blue;
			}
			.ui-state-active { 
				background: #e6e6e6;
			}

		</style>
    </head>
    <body onload="DetectOS();">
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div>
				<b>1 - Installation du lecteur de carte SCM</b><br/><br/>
				<div class="menu_panel" style="margin: 10px;">
					<div class="row">
						<div class="col-xs-6 col-md-3">
							<a href="cloud.php" class="thumbnail">
								<img src="<?php print PATH ?>images/cloud4700.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="omnikey.php" class="thumbnail">
								<img src="<?php print PATH ?>images/omnikey.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="#accordion" class="thumbnail" style="border-color: #337ab7;">
								<img src="<?php print PATH ?>images/scm.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="accesso.php" class="thumbnail">
								<img src="<?php print PATH ?>images/accesso.jpg"/>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div id="accordion">
				<h3><a href="#">Windows</a></h3>
				<div id="windowsos">
					L'installation du lecteur SCL010 ou SCL011 est réalisée de manière automatique pour toute version de Windows égale ou supérieure à Windows XP (XP, Vista, 7, etc.).
					<br/>
					Branchez le lecteur sur un port USB de votre ordinateur, et patientez jusqu'à la fin de l'installation automatique par Windows.
					<br/><br/>
					En cas de problème de pilotes sur votre poste, des pilotes spécifiques peuvent être téléchargés à cette adresse:
					<a target="_blank" href="http://support.identive-group.com/downloads.php?lang=en">SCM SCL011 drivers</a>
					<br/><br/>
					<b><span style="color: red">Note à l'attention des utilisateurs de Windows 7 :</span></b><br/>
					Si vous utilisez windows 7, il est fort probable lors de chaque passage de cartes que Windows tente de les identifier<br/><br/>
					Pour désactiver ou réactiver la fonctionnalité qui contrôle cette identification, vous pouvez utiliser l'un des patchs ci-dessous<br/>
					- Désactiver la détection des cartes en utilisant ce programme :<br/>
					<a href="<?php print PATH ?>files/fix_card32.exe">Patch pour Windows 7 32 bits</a><br/>
					<a href="<?php print PATH ?>files/fix_card64.exe">Patch pour Windows 7 64 bits</a><br/><br/>
					- Réactiver la détection des cartes en utilisant ce programme :<br/>
					<a href="<?php print PATH ?>files/revert_card32.exe">Patch pour Windows 7 32 bits</a><br/>
					<a href="<?php print PATH ?>files/revert_card64.exe">Patch pour Windows 7 64 bits</a><br/>
				</div>
				<h3><a href="#">Mac OS</a></h3>
				<div id="macos">
					L'installation du lecteur SCL010 ou SCL011 est réalisée de manière automatique sur Mac OS.<br/>
					Les versions de MacOS X supérieures ou égales
					à <b>10.7</b> sont supportées. Cependant, des problèmes techniques au niveau de MacOS empêchent parfois de garantir la compatibilité du lecteur avec
					MacOS, quelle que soit la version.<br/>
					<br/>
					Branchez le lecteur sur un port USB de votre Mac, et patientez jusqu'à la fin de l'installation automatique.
					<br/><br/>
					En cas de problème de pilotes sur votre poste, des pilotes spécifiques peuvent être téléchargés à cette adresse:
					<a target="_blank" href="http://support.identive-group.com/downloads.php?lang=en">SCM SCL011 drivers</a>
					<br/><br/>
					<strong><span style="color: red;">Attention : Vous avez besoin de Java Version 1.6 et de la version 10.6.x de MAC pour que le lecteur fonctionne</span></strong>
					<br/><br/><br/>
				</div>
				<h3><a href="#">Linux</a></h3>
				<div id="linuxos">
					Pour installer le lecteur SCL010 / SCL011 sous linux :<br/><br/>
					- Vous devez installer les applications pcscd et pcsc-tools. Par exemple avec la commande apt-get:<br/>
					<b>sudo apt-get install pcscd</b><br/>
					<b>sudo apt-get install pcsc-tools</b><br/>  
					<br/>
					- Vous pouvez ensuite vérifier la bonne reconnaissance de votre lecteur en utilisant la commande pcsc_scan.<br/>
					Le lecteur doit alors apparaitre dans la liste des lecteurs affichés.<br/>
					<br/>
					- Si le lecteur n'apparait pas dans la liste, veuillez redémarrer votre machine, ou redémarrer le processus pcscd.
					<br/><br/>
					En cas de problème de pilotes sur votre poste, des pilotes spécifiques peuvent être téléchargés à cette adresse:
					<a target="_blank" href="http://support.identive-group.com/downloads.php?lang=en">SCM SCL011 drivers</a>
					<br/><br/><br/>
				</div>		
			</div>
			<div>
				<br/><br/>
				<b>2 - Installation de l'application de lecture de cartes</b><br/><br/>
				Une fois le lecteur installé, vous devez installer l'application de lecture de cartes afin de pouvoir l'utiliser sous Loyalty Operator.<br/>
				<br/>
				Vous trouverez toutes les informations ainsi que les téléchargements en suivant ce lien: <a href="<?php print PATH ?>pages/<?php print $_SESSION["lang"] ?>/jbadger/jbadger.php">Application de lecture de cartes (JBadger)</a><br/>
				<br/>
			</div>
		</div>
	</body>
</html>