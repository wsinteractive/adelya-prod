<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
    </head>
    <body onload="DetectOS();">
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<center><h1>Lecteur Identive CLOUD 4700 F - Problème connu sous Windows 7</h1></center><br/><br/>
			<center><img src="<?php print PATH ?>images/cloud4700.jpg"/></center><br/>

			Si vous utilisez Windows 7 (ceci ne concerne pas les versions XP, Vista ou 8), le lecteur Identive CLOUD 4700 F peut présenter des lenteurs lors de
			la lecture de certains mobiles NFC. La lecture peut également échouer.
			<br/><br/>
			Le problème est lié au pilote par défault utilisé par Windows pour ce lecteur.
			<br/><br/>

			<h2>Comment reconnaitre le pilote utilisé</h2><br/>
			<u>Ouvrez le gestionnaire de périphérique:</u> Menu démarrer -> Panneau de configuration -> Matériel et audio -> Gestionnaire de périphérique.<br/>
			<br/>
			Sous la catégorie "Lecteurs de cartes à puce", 2 lignes correspondent à votre lecteur Identive CLOUD 4700 F. Une correspondant à la partie 
			cartes à puces, et l'autre à la partie sans contact du lecteur.<br/><br/>
			- Si les lecteurs se nomment <b>CLOUD 4700 F Contact/Contactless reader</b>, vous utilisez le bon pilote et aucune mise à jour n'est nécessaire.<br/><br/>
			- Si les lecteurs se nomment <b>Lecteur de carte à puce Microsoft Usbccid (WUDF)</b>, vous utilisez le pilote Windows et une mise à jour est conseillée.<br/>
			<br/>
			<center><img src="<?php print PATH ?>images/DriverWin4700.png"/><br/>
				Gestionnaire de périphérique montrant le lecteur utilisant le pilote Windows.
			</center><br/>

			<h2>Mettre à jour le pilote</h2><br/>
			Téléchargez les pilotes du lecteur: <a href="<?php print PATH ?>files/Identive_Cloud_Smart_Card_Reader.msi">Pilotes Identive CLOUD 4700 F</a><br/>
			<br/>
			Exécutez ensuite le fichier téléchargé et suivez les instructions. A la fin de l'installation, il vous faudra débrancher et rebrancher le lecteur,
			ou éventuellement redémarrer votre ordinateur.<br/>
			<br/>
			Page de support pour le lecteur Identive CLOUD 4700 F: <a href="cloud.php">Identive CLOUD 4700 F</a><br/>
		</div>
	</body>
</html>