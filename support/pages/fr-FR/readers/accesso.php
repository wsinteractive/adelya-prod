<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
    </head>
    <body onload="DetectOS();">
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div>
				<b>1 - Installation du lecteur de carte Accesso</b><br/><br/>
				<div class="menu_panel" style="margin: 10px;">
					<div class="row">
						<div class="col-xs-6 col-md-3">
							<a href="cloud.php" class="thumbnail">
								<img src="<?php print PATH ?>images/cloud4700.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="omnikey.php" class="thumbnail">
								<img src="<?php print PATH ?>images/omnikey.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="scm.php" class="thumbnail">
								<img src="<?php print PATH ?>images/scm.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="#accordion" class="thumbnail" style="border-color: #337ab7;">
								<img src="<?php print PATH ?>images/accesso.jpg"/>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div id="accordion">
				<b>Sur Windows :</b><br/>
				Pour installer le lecteur de carte Accesso, il faut installer les drivers adaptés:<br/>
				- Mode série: <a href="<?php print PATH ?>files/AccessoFull.zip">Installation Accesso 32/64bits (.zip)</a>&nbsp;&nbsp;<b>(Windows XP ou supérieur)</b><br/>
				<span style="color: red">
					Note à l'attention des utilisateurs de Windows 8 : Afin de pouvoir installer les pilotes, vous devrez dans un premier temps désactiver la vérification de signature des pilotes windows<br/>
					<a href="accesso_w8.php">Consulter le guide de configuration de windows 8</a>
				</span><br/>
				</br>
				- Mode PC/SC: <a href="<?php print PATH ?>files/installInsideAccesso_2.5.5.0.exe">Installation Accesso (.exe)</a>&nbsp;ou&nbsp;
				<a href="<?php print PATH ?>files/installInsideAccesso_2.5.5.0.zip">Installation Accesso (.zip)</a>&nbsp;&nbsp;<b>(Windows 2000/XP)</b>

				<br/><br/><br/>

				<b>Sur Mac / Linux :</b><br/>
				Il n'existe malheureusement pas de drivers pour ce lecteur sous Mac et Linux. Il ne peut donc pas être utilisé sur ces systèmes.

				<br/><br/><br/>

				<b>2 - Installation de l'application de lecture de cartes</b><br/><br/>
				Une fois le lecteur installé, vous devez installer l'application de lecture de cartes afin de pouvoir l'utiliser sous Loyalty Operator.<br/>
				<br/>
				Vous trouverez toutes les informations ainsi que les téléchargements en suivant ce lien: <a href="<?php print PATH ?>pages/<?php print $_SESSION["lang"] ?>/jbadger/jbadger.php">Application de lecture de cartes (JBadger)</a>
				<br/><br/><br/>

				<b>3 - Installation Réseau</b><br/><br/>
				Pour n'installer que les outils nécessaires à l'exécution de l'application sans le logiciel, téléchargez les fichiers suivants :<br/>
				<a href="<?php print PATH ?>files/AdelyaPrereq.exe">Installation Prérequis Seulement (.exe)</a><br/>
				<a href="<?php print PATH ?>files/dotnetfx2_0.exe">Framework 2.0 (.exe)</a>

				<br/><br/><br/>
				<b><span style="color: red">Note à l'attention des utilisateurs de Windows 7 :</span></b><br/>
				Si vous utilisez windows 7, il est fort probable lors de chaque passage de carte que Windows tente de les identifier<br/><br/>
				Pour désactiver ou réactiver la fonctionnalité qui contrôle cette identification, vous pouvez utiliser l'un des patchs ci-dessous<br/>
				- Désactiver la détection des cartes en utilisant ce programme :<br/>
				<a href="<?php print PATH ?>files/fix_card32.exe">Patch pour Windows 7 32 bits</a><br/>
				<a href="<?php print PATH ?>files/fix_card64.exe">Patch pour Windows 7 64 bits</a><br/><br/>
				- Réactiver la détection des cartes en utilisant ce programme :<br/>
				<a href="<?php print PATH ?>files/revert_card32.exe">Patch pour Windows 7 32 bits</a><br/>
				<a href="<?php print PATH ?>files/revert_card64.exe">Patch pour Windows 7 64 bits</a><br/>
			</div>
		</div>
	</body>
</html>