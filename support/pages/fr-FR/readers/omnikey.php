<?php
session_start();
//Variables indicant le chemin
define('PATH', './../../../');
include(PATH . "locale/translator.php");
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include(PATH . "include/header.php"); ?>
		<link href="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script src="<?php print PATH ?>js/jquery-ui-1.11.4/jquery-ui.js"></script>
		<script src="<?php print PATH ?>js/browserDetect.js"></script>
		<script type="text/javascript">
			function DetectOS() {
				if (BrowserDetect.OS == "Windows") {
					$("#accordion").accordion("activate", 0)
				} else if (BrowserDetect.OS == "Mac OS") {
					$("#accordion").accordion("activate", 1)
				} else if (BrowserDetect.OS == "Linux") {
					$("#accordion").accordion("activate", 2)
				}
			}

			$(function () {
				$("#accordion").accordion();
			});
		</script>
		<style>
			.ui-widget-content a {
				color: blue;
			}
			.ui-state-active { 
				background: #e6e6e6;
			}

		</style>
    </head>
    <body onload="DetectOS();">
		<?php include(PATH . "include/menu-top.php"); ?>
		<div style="width:98%;margin:0 auto;">
			<div>
				<b>1 - Installation du lecteur de carte Omnikey</b><br/><br/>
				<div class="menu_panel" style="margin: 10px;">
					<div class="row">
						<div class="col-xs-6 col-md-3">
							<a href="cloud.php" class="thumbnail">
								<img src="<?php print PATH ?>images/cloud4700.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="#accordion" class="thumbnail" style="border-color: #337ab7;">
								<img src="<?php print PATH ?>images/omnikey.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="scm.php" class="thumbnail">
								<img src="<?php print PATH ?>images/scm.jpg"/>
							</a>
						</div>
						<div class="col-xs-6 col-md-3">
							<a href="accesso.php" class="thumbnail">
								<img src="<?php print PATH ?>images/accesso.jpg"/>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div id="accordion">
				<h3><a href="#">Windows</a></h3>
				<div id="windowsos">
					<p>
						Pour utiliser votre lecteur de cartes sous Windows, vous devez installer les pilotes du lecteur avant de le brancher, comme indiqué ci-dessous.
						<br/>Puis vous devez installer notre application de lecture de cartes: <a href="<?php print PATH ?>pages/<?php print $_SESSION["lang"] ?>/jbadger/jbadger.php">Application de lecture de cartes (JBadger)</a>
					</p>
					<b>- Procédure d'installation des pilotes sous Windows XP ou antérieur :</b><br/>
					<a href="omnikey_install_guide.php">Consulter la procédure sous Windows XP ou antérieur</a><br/><br/>
					<b>- Installation des pilotes pour les autres versions de Windows :</b><br/>
					Pour installer le lecteur de carte Omnikey, il vous suffit de le brancher sur une prise USB de votre ordinateur.<br/>
					Windows reconnaitra alors le périphérique automatiquement.<br/><br/>

					Si vous rencontrez des problèmes ou que vous utilisez une ancienne version de windows, vous pouvez télécharger et exécuter le fichier suivant :<br/>
					<b><a href="<?php print PATH ?>files/OMNIKEY5x2x_V1_2_4_1.exe">Installation Omnikey</a>&nbsp;(Windows 2000/XP)</b><br/>
					<b><a href="<?php print PATH ?>files/OMNIKEY5x2x_V1_2_5_2.exe">Installation Omnikey</a>&nbsp;(Windows 2000/XP/VISTA/Windows 7)</b><br/>
					<b><a href="<?php print PATH ?>files/HID_OMNIKEY5x2x_x64_R1_2_9_2.exe">Installation Omnikey</a>&nbsp;(Windows 7 64 bits)</b>
					<br/><br/><br/>
					<b><span style="color: red">Note à l'attention des utilisateurs de Windows 7 :</span></b><br/>
					Si vous utilisez windows 7, il est fort probable lors de chaque passage de cartes que Windows tente de les identifier<br/><br/>
					Pour désactiver ou réactiver la fonctionnalité qui contrôle cette identification, vous pouvez utiliser l'un des patchs ci-dessous<br/>
					- Désactiver la détection des cartes en utilisant ce programme :<br/>
					<a href="<?php print PATH ?>files/fix_card32.exe">Patch pour Windows 7 32 bits</a><br/>
					<a href="<?php print PATH ?>files/fix_card64.exe">Patch pour Windows 7 64 bits</a><br/><br/>
					- Réactiver la détection des cartes en utilisant ce programme :<br/>
					<a href="<?php print PATH ?>files/revert_card32.exe">Patch pour Windows 7 32 bits</a><br/>
					<a href="<?php print PATH ?>files/revert_card64.exe">Patch pour Windows 7 64 bits</a><br/>
				</div>
				<h3><a href="#">Mac OS</a></h3>
				<div id="macos">
					Pour installer le lecteur de carte Omnikey sous Mac :<br/>
					<!--<b><a href="http://www.adelya.com/download/omnikey/files/ifdokrfid_mac_10.5_i386-2.4.0.1.dmg">Installation Omnikey</a>&nbsp;(MacOS X 10.5)</b><br/><br/>-->
					<b><a href="<?php print PATH ?>files/ifdokrfid_mac_universal-2.5.0.2.dmg">Installation Omnikey</a>&nbsp;(MacOS X 10.6, 10.7, 10.8)</b><br/><br/>
					<b><a href="<?php print PATH ?>files/ifdokccid_mac_universal-v4.1.6.dmg">Installation Omnikey MacOS 10.9, 10.10</a></b><br/><br/>						
					Plus d'information sur le lecteur: <a target="_blank" href="http://www.hidglobal.fr/products/readers/omnikey/5321-cl">HID Omnikey 5321 CL</a><br/>
					<br/>
				</div>
				<h3><a href="#">Linux</a></h3>
				<div id="linuxos">
					Pour installer le lecteur de carte Omnikey sous linux :<br/><br/>
					<u>Etape 1 : Ouvrir un terminal et entrez les commandes suivantes :</u><br/>							
					<b>
						sudo apt-get install pcscd<br/>              
						sudo apt-get install pcsc-tools<br/><br/>
					</b>
					<u>Etape 2 : Il est necessaire pour continuer de stopper un processus</u><br/>
					<p>Pour le faire, reperez le processus en question avec la commande suivante : <strong>ps -ax | grep pcscd</strong><p/>
					<p>Vous deviez avoir pour résultat une ligne ressemblant à : <strong><font color="red">23748</font> pts/3    00:00:02 pcscd</strong></p>
					<p>Stoppez le processus par la commande suivante (L'identifiant en rouge est à remplacer par celui obtenu via la commande précédente :<br/>
						<strong>kill <font color="red">23748</font></strong>
					</p>
					<p>Enfin relancer le processus par la commande suivante : <strong>sudo pcscd</strong></p>
					<br/>
					<p>Pour vérifier que l'installation à bien été réalisée, exécuter la commande : <strong>pcsc_scan</strong></p>
					<p>Cela doit vous afficher le lecteur OMNIKEY CardMan 5x21</p><br/>
					<u>Etape 3 : Télécharger l'une des archives ci-dessous en fonction de votre système et extraire le contenu dans un dossier</u><br/>
					<b><a href="<?php print PATH ?>files/ifdokrfid_lnx_i686-2.8.3.1.tar.gz">Installation Omnikey</a>&nbsp;(Linux 32bits)</b>
					<br/>
					<b><a href="<?php print PATH ?>files/ifdokrfid_lnx_x64-2.10.0.1.gz">Installation Omnikey</a>&nbsp;(Linux 64bits)</b>
					<br/>
					<p>Enfin, executez le fichier install dans le répertoire d'extraction pour lancer l'installation du lecteur</p>
					<p>Remarque : Une fois l'installation réalisée, un redémarrage de votre navigateur est fortement conseillé</p>
				</div>		
			</div>
			<div>
				<br/><br/>
				<b>2 - Installation de l'application de lecture de cartes</b><br/><br/>
				Une fois le lecteur installé, vous devez installer l'application de lecture de cartes afin de pouvoir l'utiliser sous Loyalty Operator.<br/>
				<br/>
				Vous trouverez toutes les informations ainsi que les téléchargements en suivant ce lien: <a href="<?php print PATH ?>pages/<?php print $_SESSION["lang"] ?>/jbadger/jbadger.php">Application de lecture de cartes (JBadger)</a><br/>
				<br/>
			</div>
		</div>
	</body>
</html>