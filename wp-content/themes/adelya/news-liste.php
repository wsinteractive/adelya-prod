<?php /** Template Name: Liste des news **/ ?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php //$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		wp_reset_query();
		$paged = get_query_var('paged') ? get_query_var('paged') : 1;

		$args = array( 'post_type' => 'adelya_actualites',   'paged' => $paged, 'posts_per_page' => 10 );
		// $temp_query = $args;
		// $wp_query   = NULL;
		// $wp_query   = $args;
		$loop = new WP_Query( $args ); if($loop->have_posts()) :  
		$GLOBALS['wp_query']->max_num_pages = $loop->max_num_pages;
		?>





		<?php
			// Start the Loop.
		while ( $loop->have_posts() ) : $loop->the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			// End the loop.
				endwhile;


				// Previous/next page navigation.
				the_posts_pagination( array(
					'prev_text'          => __( '<span class="icon icon-fleche-droite reverse"></span>', 'twentysixteen' ),
					'next_text'          => __( '<span class="icon icon-fleche-droite"></span>', 'twentysixteen' ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( '', 'twentysixteen' ) . ' </span>',
					) );

				




					// If no content, include the "No posts found" template.
				else :
					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>
			</main><!-- .site-main -->
		</div><!-- .content-area -->

		<?php get_footer(); ?>