<?php /** Template Name: Recherche blog **/

get_template_part( 'blog/blog', 'header' );  ?>

<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if( strlen($query_string) > 0 ) {
	foreach($query_args as $key => $string) {
		$query_split = explode("=", $string);
		$search_query[$query_split[0]] = urldecode($query_split[1]);
	} // foreach
} //if

$search = new WP_Query($search_query);
?>

<div class="main container">
	<div class="main-row">

		<?php get_template_part( 'blog/blog' , 'sidebarmobile'); ?>


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

		// Include the single post content template.
		if ( !is_singular( 'post' )){
			get_template_part( 'template-parts/content', 'single' );
		}

		if ( is_singular( 'attachment' ) ) {
				// Parent post navigation.
			the_post_navigation( array(
				'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
				) );
		} 

		if ( is_singular( 'post' ) ) {
			get_template_part( 'blog/blog', 'detail' );
		}

			// End of the loop.
		endwhile;
		?>

	<?php get_template_part( 'blog/blog', 'sidebar' ); ?>

</div><!-- .content-area -->
</div>

<?php get_template_part( 'blog/blog', 'footer' ); ?>