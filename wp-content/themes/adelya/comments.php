<?php /** Template : commentaire **/
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div class="comments">

	<?php if ( have_comments() ) : ?>
		<div class="comments-title">
			<?php
				$comments_number = get_comments_number();
				if ($comments_number > 1) {
					/* translators: %s: post title */
					printf(
						/* translators: 1: number of comments, 2: post title */
						_nx(
							'commentaire (%1$s)',
							'commentaires (%1$s)',
							$comments_number,
							'adelya'
						),
						number_format_i18n( $comments_number ),
						get_the_title()
					);
				}
			?>
		</div>

		<?php the_comments_navigation(); ?>

		<ul class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'ul',
					'short_ping'  => true,
					'avatar_size' => 42,
				) );
			?>
		</ul><!-- .comment-list -->

		<?php the_comments_navigation(); ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentysixteen' ); ?></p>
	<?php endif; ?>

	<?php
		comment_form( array(
			'title_reply_before' => '<div id="reply-title" class="comment-reply-title">',
			'title_reply_after'  => '</div>',
		) );
	?>

</div><!-- .comments-area -->
