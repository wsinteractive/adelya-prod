<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_template_part( 'blog/blog', 'header' );  ?>

<div class="main container">
	<div class="main-row">

		<?php get_template_part( 'blog/blog' , 'sidebarmobile'); ?>


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

		// Include the single post content template.
		if ( !is_singular( 'post' )){
			get_template_part( 'template-parts/content', 'single' );
		}

		if ( is_singular( 'attachment' ) ) {
				// Parent post navigation.
			the_post_navigation( array(
				'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
				) );
		} 

		if ( is_singular( 'post' ) ) {
			get_template_part( 'blog/blog', 'detail' );
		}

			// End of the loop.
		endwhile;
		?>

	<?php get_template_part( 'blog/blog', 'sidebar' ); ?>

</div><!-- .content-area -->
</div>

<?php get_template_part( 'blog/blog', 'footer' ); ?>
