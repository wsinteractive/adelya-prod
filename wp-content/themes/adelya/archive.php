<?php /** Template : article **/ ?>

<?php get_template_part( 'blog/blog', 'header' ); ?>



<div class="main container">
	<div class="main-row">

		<?php get_template_part( 'blog/blog' , 'sidebarmobile');  var_dump(have_posts()); ?>



		<div class="main-content">
			<?php if ( have_posts() ) : ?>

				<?php  $page_title ="";
				if( is_category() ) {
					$page_title = __('Tous les articles de la catégorie','adelya') ." ". get_category( get_query_var( 'cat' ) )->name;
				}
				if( is_author() ) {
					$author = get_userdata( get_query_var('author') );
					$page_title = __('Articles de','adelya')." ".$author->first_name." ".$author->last_name;
				} ?>


				<div class="postbox equal-height">
					<div class="col-12"><h2 style="font-size:25px;"><?php echo $page_title; ?></h2></div>
					<?php

			// Start the Loop.
					while ( have_posts() ) : the_post();
					get_template_part( 'blog/blog', 'item' );
					endwhile;

					echo adelya_pagination();
					?>
				</div>
			<?php endif; ?>
		</div>





		<?php get_template_part( 'blog/blog' , 'sidebar'); ?>
	</div><!-- .site-main -->
</div><!-- .content-area -->

<?php get_template_part( 'blog/blog', 'footer' ); ?>


