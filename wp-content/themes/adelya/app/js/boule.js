/* Initialisation du Boule */
;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
		tab_side_elements = null,
		tab_paths = null,
		side_green = null,
		side_orange = null,
		boule = null,
		side_element_content = null,
		boule_row = null,
		boule_img_left = null,
		boule_img_right = null,
		cross_close = null;

	function init() {
		side_element_content = document.querySelectorAll('.side-element-content');
		tab_side_elements = document.querySelectorAll('.side-element');
		tab_paths = document.querySelectorAll('.selector');
		side_green = document.querySelector('.side-green');
		side_orange = document.querySelector('.side-orange');
		boule = document.querySelector('.boule-svg');
		boule_row = document.querySelector('.boule');
		boule_img_left = document.querySelector('.boule-img.left');
		boule_img_right = document.querySelector('.boule-img.right');
		cross_close = document.querySelectorAll('.side-element-content .cross-close');


		for (var i = 0; i < tab_paths.length; i++) {
			tab_paths[i].addEventListener('click', function() {
				click_on_element(this);
			})
		}

		for (var i = 0; i < cross_close.length; i++) {
			cross_close[i].addEventListener('click', function() {
				click_on_cross(this);
			})
		}

		var mc = new Hammer(side_green);

		mc.on('swipeleft', function(ev) {
			click_on_element(ev.target);
		});



		responsive();
	}
	function responsive() {
		var max_H = 0;
		var max_content_H = 0;
		side_green.style.minHeight = '';
		side_orange.style.minHeight = '';
		for (var i = 0; i < side_element_content.length; i++) {
			if (max_content_H < side_element_content[i].clientHeight) {max_content_H = side_element_content[i].clientHeight}
			
		}
		if(boule.clientHeight < max_content_H){
			max_H = max_content_H;
		}else{
			max_H = boule.clientHeight;
		}
		//<side_green.style.minHeight = max_H+'px';
		//side_orange.style.minHeight = max_H+'px';
		if(window.innerHeight < 980){
			//boule_row.style.minHeight = max_H+'px';
		}
	}
	function click_on_cross(_e) {
		var that = _e;

		for (var i = 0; i < tab_paths.length; i++) {
			tab_paths[i].setAttribute('class', 'selector');
		}
		for (var i = 0; i < tab_side_elements.length; i++) {
			R4.removeClass(tab_side_elements[i], 'active');
		}

	}
	function click_on_element(_e) {
		var that = _e;
		var bloc = document.querySelector('#side-'+_e.getAttribute('id'));

		R4.removeClass(side_orange, 'active');
		R4.removeClass(side_green, 'active');
		R4.removeClass(boule_img_left, 'active');
		R4.removeClass(boule_img_right, 'active');
		R4.removeClass(boule_img_left, 'semi-active');
		R4.removeClass(boule_img_right, 'semi-active');

		for (var i = 0; i < tab_paths.length; i++) {
			tab_paths[i].setAttribute('class', 'selector');
		}
		for (var i = 0; i < tab_side_elements.length; i++) {
			R4.removeClass(tab_side_elements[i], 'active');
			tab_side_elements[i].style.top = "";
		}
		
		/* Si pas de resultat annuler pour pas d'erreur*/
		if(bloc == null){return;};

		if (R4.hasClass(bloc, 'active')) {
			R4.removeClass(bloc, 'active');
			bloc.style.top = "";
		}else{
			R4.addClass(bloc, 'active');
			bloc.style.top = "";
		}
		/*var x = _e.pageX - this.offsetLeft;
   		var y = _e.pageY - this.offsetTop;
		console.dir(that, x, y);*/


		if (wich_side(bloc) == 'green') {
			//R4.removeClass(side_green, 'active');
			//R4.removeClass(side_orange, 'active');
			//R4.removeClass(boule_img_left, 'active');
			//R4.removeClass(boule_img_left, 'semi-active');
			//R4.removeClass(boule_img_right, 'semi-active');

			if (R4.hasClass(side_green, 'active')) {
				//R4.removeClass(side_green, 'active');
				//R4.removeClass(that, 'active');
				that.setAttribute('class', 'selector');
				//R4.addClass(boule_img_left, 'active');
				//R4.addClass(boule_img_right, 'semi-active');
			}else{
				that.setAttribute('class', 'selector active')
				//R4.addClass(side_green, 'active');
				//R4.removeClass(boule_img_left, 'active');
				//R4.removeClass(boule_img_right, 'semi-active');
			}
			//R4.addClass(boule_img_left, 'active');
			//R4.addClass(boule_img_right, 'semi-active');

		}else if(wich_side(bloc) == 'orange'){
			//R4.removeClass(side_green, 'active');
			//R4.removeClass(side_orange, 'active');
			//R4.removeClass(boule_img_right, 'active');
			//R4.removeClass(boule_img_left, 'semi-active');
			//R4.removeClass(boule_img_right, 'semi-active');

			if (R4.hasClass(side_orange, 'active')) {
				//R4.removeClass(side_orange, 'active');
				//R4.removeClass(that, 'active');
				that.setAttribute('class', 'selector')
				//R4.addClass(boule_img_right, 'active');
				//R4.addClass(boule_img_left, 'semi-active');
			}else{
				that.setAttribute('class', 'selector active')
				//R4.addClass(side_orange, 'active');
				//R4.removeClass(boule_img_right, 'active');
				//R4.removeClass(boule_img_left, 'semi-active');
			}
			//R4.addClass(boule_img_right, 'active');
			//R4.addClass(boule_img_left, 'semi-active');
		}
	}

	function wich_side(_e) {
		var classString = _e.className;
		var classTab = classString.split(' ');

		return classTab[1];
	}

	var item_length = document.querySelectorAll('.boule');
	if(item_length.length){
		window.addEventListener("load", init);
		window.addEventListener("resize", function () {
			responsive();
		});
	}

}(window, window.document));
