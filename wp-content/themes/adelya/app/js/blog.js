$(window).load(function() {
	$('.flexslider').flexslider({
		animation: "slide",
		controlsContainer: $(".postslider-controls"),
		customDirectionNav: $(".postslider-navigation a")
	});
});

$('.border-spec-color').each(function(index, el) {
	$(this).css('background-color',$(this).attr('data-color'));
});

$('.font-spec-color').each(function(index, el) {
	$(this).css('color',$(this).attr('data-color'));
	if ($(this).find('.postbox-icon').length) {
		$(this).find('.postbox-icon').css('color',$(this).attr('data-color'));
	}
});

//----------- hover on items  ---------------- //
$('.postbox-content').hover(function() {
	$(this).find('.postbox-title a').css('color', $(this).find('.postbox-title').attr('data-color'));
}, function() {
	$(this).find('.postbox-title a').css('color', '#504949');
});

$('.postbox-content').click(function() {
	window.location.href = $(this).attr('data-link');
});

var color = $('.post-detail').attr('data-color');
$('.blog-content-txt').find('h2, h3, h4, h5').append('<span class="border-spec-color" style="background-color:'+color+'"></span>');
$('.blog-content-txt p').find('a').css('color',color);
$('.post-detail').find('.author-link').css('color',color);


//----------- equal height  ---------------- //
jQuery(document).ready(function($) {
	var container = $('.equal-height');
	var items = $('.equal-height .item');

	function equalHeight(group) {
		tallest = 0;
		group.each(function() {
			thisHeight = $(this).outerHeight(true);
			if(thisHeight > tallest) {
				tallest = thisHeight;
			}
		});
		if(tallest > 0){
			group.each(function(index, el) {
				$(this).css('min-height', tallest+'px');
			});
		}		
	}

	function clearHeight() {
		//items.css('min-height', '0px');
	}

	function setEqualHeight(){
		if(window.innerWidth > 768){
			container.each(function(index, el) {
				var items = $(this).find('.item');
				equalHeight(items);	
			});
		}else{
			clearHeight();
		}
	}

	setTimeout(setEqualHeight, 0);

	$(window).resize(function() {
		clearHeight();
		setEqualHeight();
	});

	$('#selectCat').change(function() {
  // set the window's location property to the value of the option the user has selected
  window.location = $(this).val();
});



	//comment form validation
	function addTips(message, input){
		if(!input.parent().find('.form-tips').length){
			input.parent().append('<div class="form-tips">'+message+'</div>');
			input.addClass('not-valid')
		}
	}

	function validateEmail(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	$("#commentform").submit(function(e){
		
		$(this).find('.form-tips').remove();
		$(this).find('.input').removeClass('not-valid');
		var inputComment = $(this).find('#comment');
		var inputAuthor = $(this).find('#author');
		var inputEmail = $(this).find('#email');
		var lang = $('html').attr('lang');
		var message = {
			"fr-FR" : {
				"required" : "Ce champ est requis",
				"email" : "Ceci n'est pas une adresse valide"
			},
			"en-US" : {
				"required" : "This field is required",
				"email" : "This adress is not valid"
			}
		};

		var valid = true;

		if( inputComment.val() == '' ){
			addTips(message[lang]['required'], inputComment);
			valid = false;
		}
		if( inputAuthor.val() == '' ){
			addTips(message[lang]['required'], inputAuthor);
			valid = false;
		}
		if( inputEmail.val() == '' ){
			addTips(message[lang]['required'], inputEmail);
			valid = false;
		}
		if( !validateEmail(inputEmail.val()) ){
			addTips(message[lang]['email'], inputEmail);
			valid = false;
		}
		if(valid){
			$(this).submit();
		}
		e.preventDefault();
	});



});//fin ready