;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
	content_element,
	tab_elements = [],
	nb_element = 0,
	index = 0,
	move_interval,
	move_time = 10000;

	window.Temoignages = {
		init : function (_contentClass, _elementsClass) {
			content_element = document.querySelector(_contentClass);
			tab_elements = document.querySelectorAll(_elementsClass);
			nb_element = tab_elements.length;

			Temoignages.contentStyle();
			Temoignages.elementsStyle();

			Temoignages.play();
		},

		contentStyle : function () {
			content_element.style.width = nb_element*100+"%";
		},

		elementsStyle : function () {
			for (var i = 0; i < nb_element; i++) {
				tab_elements[i].style.width = 100/nb_element+"%";
			};
		},

		prev : function () {
			index --;
			if (index <= -1) {index = nb_element-1};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		next : function () {
			index ++;
			if (index >= nb_element) {index = 0};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		move : function () {
			index ++;
			if (index >= nb_element) {index = 0};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		play : function () {
			move_interval = setInterval(Temoignages.move, move_time);
		},

		stop : function () {
			clearInterval(move_interval);
		}
	}

	window.Temoignages = window.Temoignages;
}(window, window.document));


/* Initialisation du temoignages */
;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
	temoignages_bloc = document.querySelectorAll('.temoignages-bloc'),
	temoignages_bloc_wrapper,
	temoignages_prev,
	temoignages_next,
	temoignages_overlay,
	temoignages_prev_wrapper,
	temoignages_next_wrapper;


	function init () {

		temoignages_bloc = document.querySelectorAll('.temoignages-bloc'),
		temoignages_bloc_wrapper = document.querySelector('.temoignages-bloc-wrapper'),
		temoignages_prev = document.querySelector('.temoignages-bloc-navigation.prev .icon'),
		temoignages_next = document.querySelector('.temoignages-bloc-navigation.next .icon'),
		temoignages_overlay = document.querySelector('.temoignages-bloc-overlay'),
		temoignages_prev_wrapper = document.querySelector('.temoignages-bloc-navigation.prev'),
		temoignages_next_wrapper = document.querySelector('.temoignages-bloc-navigation.next');

		var mc = new Hammer(temoignages_bloc_wrapper);

		temoignages_prev.addEventListener('click', function (_e) {
			window.Temoignages.prev();
		});
		temoignages_next.addEventListener('click', function (_e) {
			window.Temoignages.next();
		});
		mc.on('swiperight', function(ev) {
			window.Temoignages.next();
		});
		mc.on('swipeleft', function(ev) {
			window.Temoignages.prev();
		});
		posBtnTemoignages();
	}

	function lineHeightBtn () {
		temoignages_prev.style.lineHeight = temoignages_overlay.offsetHeight+"px";
		temoignages_next.style.lineHeight = temoignages_overlay.offsetHeight+"px";
		temoignages_prev_wrapper.style.height = temoignages_overlay.offsetHeight+"px";
		temoignages_next_wrapper.style.height = temoignages_overlay.offsetHeight+"px";
	}
	function posBtnPrev () {
		var W = (temoignages_overlay.offsetWidth+temoignages_prev.offsetWidth + 10);
		if(window.innerWidth > 750){
			temoignages_prev_wrapper.style.right = W+"px";
			temoignages_prev_wrapper.style.left = "auto";
		}else{
			temoignages_prev_wrapper.style.right = "";
			temoignages_prev_wrapper.style.left = "";
		}
	}
	function posBtnNext () {
		var W = (temoignages_overlay.offsetWidth+temoignages_next.offsetWidth + 10);
		if(window.innerWidth > 750){
			temoignages_next_wrapper.style.right = "auto";
			temoignages_next_wrapper.style.left = W+"px";
		}else{
			temoignages_next_wrapper.style.right = "";
			temoignages_next_wrapper.style.left = "";
		}
	}
	function posBtnTemoignages () {
		lineHeightBtn();
		posBtnPrev();
		posBtnNext();
	}

	var item_length = document.querySelectorAll('.temoignages-bloc-item');
	if(item_length.length > 1){
		window.Temoignages.init(".temoignages-bloc-ul", ".temoignages-bloc-item");
		window.addEventListener("load", init);
		window.addEventListener("resize", function () {
			init();
		});


	}

}(window, window.document));
