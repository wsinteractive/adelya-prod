;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
	content_element,
	tab_elements = [],
	nb_element = 0,
	index = 0,
	elements_visible = 0,
	move_interval,
	move_time = 10000;

	window.Partenaires = {
		init : function (_contentClass, _elementsClass) {
			content_element = document.querySelector(_contentClass);
			tab_elements = document.querySelectorAll(_elementsClass);
			nb_element = tab_elements.length;
			var test_text = document.querySelectorAll('.text_true');
			if (!test_text.length) {
				if (window.innerWidth < 650) {
					elements_visible  = 1;
				}
				if (window.innerWidth > 650) {
					elements_visible  = 2;
				}
				if (window.innerWidth > 980) {
					elements_visible  = 3;
				}
				if (window.innerWidth > 1200) {
					elements_visible  = 4;
				}
			}else{
				elements_visible  = 1;
			}

			Partenaires.contentStyle();
			Partenaires.elementsStyle();

			//Partenaires.play();
		},

		contentStyle : function () {
			content_element.style.width = (nb_element*100)/elements_visible+"%";
		},

		elementsStyle : function () {
			for (var i = 0; i < nb_element; i++) {
				tab_elements[i].style.width = 100/nb_element+"%";
			};
		},

		prev : function () {
			index --;
			if (index <= -1) {index = nb_element-elements_visible};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		next : function () {
			index ++;
			if (index >= nb_element-elements_visible+1) {index = 0};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		move : function () {
			index ++;
			if (index >= nb_element-elements_visible+1) {index = 0};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		play : function () {
			move_interval = setInterval(Partenaires.move, move_time);
		},

		stop : function () {
			clearInterval(move_interval);
		}
	}

	window.Partenaires = window.Partenaires;
}(window, window.document));


/* Initialisation du partenaires */
;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
	partenaires_bloc = document.querySelectorAll('.partenaires-bloc'),
	partenaires_bloc_wrapper,
	partenaires_prev,
	partenaires_next,
	partenaires_overlay,
	partenaires_prev_wrapper,
	partenaires_next_wrapper,
	elements_with_text;


	if (partenaires_bloc.length) {
		if (partenaires_bloc[0].getAttribute("data") === "true") { 
			elements_with_text = true;
		}else{
			elements_with_text = false;
		}
	}
	function init () {

		partenaires_bloc = document.querySelectorAll('.partenaires-bloc'),
		partenaires_bloc_wrapper = document.querySelector('.partenaires-bloc-wrapper'),
		partenaires_prev = document.querySelector('.partenaires-bloc-navigation.prev .icon'),
		partenaires_next = document.querySelector('.partenaires-bloc-navigation.next .icon'),
		partenaires_overlay = document.querySelector('.partenaires-bloc-overlay'),
		partenaires_prev_wrapper = document.querySelector('.partenaires-bloc-navigation.prev'),
		partenaires_next_wrapper = document.querySelector('.partenaires-bloc-navigation.next');

		

		var mc = new Hammer(partenaires_bloc_wrapper);

		partenaires_prev.addEventListener('click', function (_e) {
			window.Partenaires.prev();
		});
		partenaires_next.addEventListener('click', function (_e) {
			window.Partenaires.next();
		});
		mc.on('swiperight', function(ev) {
			window.Partenaires.prev();
		});
		mc.on('swipeleft', function(ev) {
			window.Partenaires.next();
		});
		posBtnpartenaires();
	}

	function lineHeightBtn () {
		partenaires_prev.style.lineHeight = partenaires_overlay.offsetHeight+"px";
		partenaires_next.style.lineHeight = partenaires_overlay.offsetHeight+"px";
		partenaires_prev_wrapper.style.height = partenaires_overlay.offsetHeight+"px";
		partenaires_next_wrapper.style.height = partenaires_overlay.offsetHeight+"px";
	}
	function posBtnPrev () {
		var W = (partenaires_overlay.offsetWidth+partenaires_prev.offsetWidth + 45);
		if(window.innerWidth > 750){
			partenaires_prev_wrapper.style.right = W+"px";
			partenaires_prev_wrapper.style.left = "auto";
		}else{
			partenaires_prev_wrapper.style.right = "";
			partenaires_prev_wrapper.style.left = "";
		}
	}
	function posBtnNext () {
		var W = (partenaires_overlay.offsetWidth+partenaires_next.offsetWidth + 45);
		if(window.innerWidth > 750){
			partenaires_next_wrapper.style.right = "auto";
			partenaires_next_wrapper.style.left = W+"px";
		}else{
			partenaires_next_wrapper.style.right = "";
			partenaires_next_wrapper.style.left = "";
		}
	}
	function posBtnpartenaires () {
		lineHeightBtn();
		posBtnPrev();
		posBtnNext();
	}

	var item_length = document.querySelectorAll('.partenaires-bloc-item');
	if(item_length.length > 4 && !elements_with_text || item_length.length > 1 && elements_with_text) {
		window.Partenaires.init(".partenaires-bloc-ul", ".partenaires-bloc-item");
		window.addEventListener("load", init);
		window.addEventListener("resize", function () {
			init();
			window.Partenaires.init(".partenaires-bloc-ul", ".partenaires-bloc-item");
		});
	}

}(window, window.document));
