;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
    square = null,
    overlay = null,
    closebtn = null,
    target = null,
    content = null;

	window.PopUp = {
		init : function (_tab_elemts) {

			for (var i = 0; i < _tab_elemts.length; i++) {
				var item = _tab_elemts[i];
				_tab_elemts[i].addEventListener('click', function (e) {
					window.PopUp.popOut(this);
					window.PopUp.toggleClassThis(this);
				})
			};
		},

		fadeIn : function (el) {
			el.style.opacity = 0;

			var last = +new Date();
			var tick = function() {
				el.style.opacity = +el.style.opacity + (new Date() - last) / 400;
				last = +new Date();

				if (+el.style.opacity < 1) {
					(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
				}
			};

			tick();
		},
		
		toggleClassBody : function () {
			var classes = document.body.className.split(' ');
			var existingIndex = classes.indexOf('popup-body');

			if (existingIndex >= 0){
				classes.splice(existingIndex, 1);
			}else{
				classes.push('popup-body');
			}
			document.body.className = classes.join(' ');
		},
		
		toggleClassThis : function (_e) {
			if(_e == null){
				_e = target;
			}else{
				target = _e;
			}
			var classes = _e.className.split(' ');
			var existingIndex = classes.indexOf('active');

			if (existingIndex >= 0){
				classes.splice(existingIndex, 1);
			}else{
				classes.push('active');
			}
			_e.className = classes.join(' ');
		},

		whatIsNavigator : function () {
			var ua = navigator.userAgent, tem,
			M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
			if(/trident/i.test(M[1])){
				tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
				return 'IE '+(tem[1] || '');
			}
			if(M[1] === 'Chrome'){
				tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
				if(tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
			}
			M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
			if((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
			//return table ['name', 'version'];
			return M;
		},

		popOut : function(_el) {
			var decalage_top = window.pageYOffset || window.scrollY;

			var body = document.querySelector("body");
			

			overlay = document.createElement("div");
			overlay.className = "popup-overlay";
			overlay.style.top = "0px";
			overlay.style.height = body.offsetHeight+"px";

			square = document.createElement("div");
			square.className = "popup-square";

			/*a remplacer par le template de client detail*/
			content = window.PopUp.templateClientDetail(_el);
			square.appendChild(content);

			closebtn = document.createElement("span");
			

			closebtn.className = "popup-close icon icon-cross cursor";
			square.appendChild(closebtn);
			
			window.PopUp.toggleClassBody();

			document.body.appendChild(overlay);
			document.body.appendChild(square);

			var content_H = content.offsetHeight;
			content_H += 25 + 25;
			square.style.height = content_H+"px";
			square.style.opacity = "1";

			square.style.top = decalage_top+(window.innerHeight/2)-(square.offsetHeight/2)+"px";
			square.style.left = "0px";
			square.style.right = "0px";

			window.PopUp.addEvent();

		},
		addEvent : function () {
			closebtn.addEventListener('click', function () {
				window.PopUp.popIn();
				window.PopUp.toggleClassThis(null);
				window.PopUp.toggleClassBody();
			})

			overlay.addEventListener('click', function () {
				window.PopUp.popIn();
				window.PopUp.toggleClassThis(null);
				window.PopUp.toggleClassBody();
			})

			window.addEventListener('resize', function () {
				var is_here = document.querySelectorAll('.popup-body');
				if(is_here.length){
					var content_H = content.offsetHeight;
					content_H += 25 + 25;
					square.style.height = content_H+"px";
				}
			})
		},
		/*removeEvent : function () {
			closebtn.removeEventListener('click', function () {
				window.PopUp.popIn();
				window.PopUp.toggleClassBody();
			})

			overlay.removeEventListener('click', function () {
				window.PopUp.popIn();
				window.PopUp.toggleClassBody();
			})
			window.removeEventListener('resize', function () {
				var content_H = content.offsetHeight;
				content_H += 25 + 25;
				square.style.height = content_H+"px";
			})
		},*/
		popIn : function() {
			if (square != null) {
				//window.PopUp.removeEvent();
				document.body.removeChild(square);
				square = null;
			}
			if (overlay != null) {
				document.body.removeChild(overlay);
				overlay = null;
			}
		},
		templateClientDetail : function (_el) {
			var template = null;

			template = document.createElement("div");
			template.className = "popup-content";

			var data_title = _el.getAttribute('data-title');
			var data_description = _el.getAttribute('data-description');
			var data_img = {'src' : _el.getAttribute('data-src'), 'alt' : _el.getAttribute('data-alt')};
			var data_link = _el.getAttribute('data-link');

			var img = document.createElement("img");
			img.className = "popup-img";
			img.src = data_img['src'];
			img.alt = data_img['alt'];
			template.appendChild(img);

			var title = document.createElement("p");
			title.className = "popup-title";
			title.innerHTML = data_title;
			template.appendChild(title);

			var description = document.createElement("p");
			description.className = "popup-txt";
			description.innerHTML = data_description;
			template.appendChild(description);

			var div = document.createElement("div");
			div.className = "popup-link-content";
			var link = document.createElement("a");
			link.className = "popup-link";
			link.href = data_link;
			link.title = data_title;
			link.target = "_blank";
			link.innerHTML = data_link;
			div.appendChild(link);
			template.appendChild(div);

			return template;
		}
	}

	window.PopUp = window.PopUp;
}(window, window.document));


/* Initialisation du PopUp */
;(function (window, document, undefined) {
	'use strict';
	var document = window.document;


	/*function init () {

	}*/
	var popup_items = document.querySelectorAll('.popup');
	/*if(popup_items.length){
		window.addEventListener("load", function () {
			window.PopUp.init();
		});
	}*/
	window.PopUp.init(popup_items);
	/*window.addEventListener("resize", function () {
		init();
	});*/

}(window, window.document));
