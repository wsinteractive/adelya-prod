/* Module slider */
;(function (window, document, undefined) {
	'use strict';
	var document = window.document;
	var slides = document.querySelectorAll('.slider-li');
	var dots = document.querySelectorAll('.slider-navigation--li');
	var content_slider = document.querySelector('.slider-ul');
	var max = slides.length;
	var index = 1;
	var autoplay = null;
	var time = 7000;
	window.Slider = { 
		init : function () {
			for(var i=0; i<dots.length; i++){ 
				dots[i].addEventListener("click", this.showSlide); 
			}
			window.Slider.autoPlay();

			var mc = new Hammer(content_slider);

			mc.on('swiperight', function(ev) {
				window.Slider.showSlide("prev")
			});
			mc.on('swipeleft', function(ev) {
				window.Slider.showSlide("next")
			});
		},
		clearClass : function () {
			for (var i = 0; i < slides.length; i++) {
				R4.removeClass(slides[i], '_active');
				R4.removeClass(dots[i], '_active');
			};
		},
		showSlide : function (e){
			var that = this;

			if (e == "next") {
				index ++;

				if (index >  max) {
					index = 1;
				}

				id = index;
			}else if (e == "prev") {
				index --;

				if (index ==  0) {
					index = max;
				}

				id = index;

			}else{
				var id = e.target.getAttribute('data-slide');
				index = id;
				clearInterval(autoplay);
				/*window.Slider.autoPlay("next");*/
			}
			var slide = document.querySelector('.slider-li[data-slide="'+id+'"]');
			window.Slider.clearClass();
			R4.addClass(slide, '_active');
			R4.addClass(document.querySelector('.slider-navigation--ol .slider-navigation--li[data-slide="'+id+'"]'), '_active');
			
		},

		autoPlay : function () {
			autoplay = setInterval(function() {
				window.Slider.showSlide("next");
			}, time);
		}
		

	}

	window.Slider.init();
}(window, window.document));


