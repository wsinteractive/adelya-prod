


function addClassLoaded(){

	setTimeout(function(){
		if(window.R4 != undefined){
			R4.addClass(document.getElementById('content'), 'loaded');
		}

	}, 700);

}


function ready(fn) {
	if (document.readyState != 'loading'){
		fn();
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
}

ready(addClassLoaded);