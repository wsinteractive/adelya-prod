new WOW().init();


(function (window, document, undefined) {
	window.R4 = {
		name : 'R4',

		version : '0.0.1',

		modiles : {},

		//*** FUNCTION ***\\
		// CLASS \\
		addClass : function (_e, _c) {
			_e.className = _e.className+' '+_c;
		},
		removeClass : function (_e, _c) {
			r = RegExp(" "+_c,'g');
			_e.className = _e.className.replace(r,'');
		},
		hasClass : function (_e, _c) {
			r = RegExp(_c,'g');
			return r.test(_e.className);
		},
		toggleClass : function (_e, _c) {
			t = this.hasClass(_e, _c) ? this.removeClass(_e, _c) : this.addClass(_e, _c);
		},

		//*** LISTENER ***\\
		// TRANSITION \\
		endOfTransition : function (_e, _c) {
			var that = this;
			_e.addEventListener('transitionend', function () {
				that.removeClass(_e, _c);
				return 1;
			});
			function removeClass (_e, _c) {
				this.removeClass(_e, _c);
			}
		},

		//*** TEST ***\\
		// NAVIGATEUR \\
		isWhatNavigateur : function () {
			var ua = navigator.userAgent, tem,
			M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
			if(/trident/i.test(M[1])){
				tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
				return 'IE '+(tem[1] || '');
			}
			if(M[1] === 'Chrome'){
				tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
				if(tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
			}
			M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
			if((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
			//return table ['name', 'version'];
			return M;
		},

		// MOBILE \\
		isMobile : function() {
			var bool = false;
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))bool = true})(navigator.userAgent||navigator.vendor||window.opera);
			//return bool;
			return bool;
		},

		// MOBILE & TABLETTE \\
		isMobileAndTablette : function() {
			var bool = false;
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))bool = true})(navigator.userAgent||navigator.vendor||window.opera);
			//return bool;
			return bool;
		},

		//LANGUAGES\\
		language : function () {
			var langage = (navigator.language || navigator.browserLanguage).split('-')[0];
			//return string;
			return langage;
		},

		wrapAll: function(elms) {
			var div = document.createElement('div');
			if (R4.hasClass(elms, 'alignright')) {
				R4.removeClass(elms, 'alignright')
				div.className = "wrapper_img alignright";
			}
			if (R4.hasClass(elms, 'alignleft')) {
				R4.removeClass(elms, 'alignleft')
				div.className = "wrapper_img alignleft";
			}
			if (R4.hasClass(elms, 'aligncenter')) {
				R4.removeClass(elms, 'aligncenter')
				div.className = "wrapper_img aligncenter";
			}
			var el = elms.length ? elms[0] : elms;

			var parent  = el.parentNode;
			var sibling = el.nextSibling;

			div.appendChild(el);

			while (elms.length) {
				div.appendChild(elms[0]);
			}

			if (sibling) {
				parent.insertBefore(div, sibling);
			} else {
				parent.appendChild(div);
			}

			
		}
	}
}(window, window.document));
// MADE WITH ❤ \\



// SAME HEIGHT \\
(function (window, document, undefined) {
	'use strict';
	var that = null,
	decalage = 0,
	max_width = 0,
	max_height = 0,
	tab_elem = null;
	window.sameHeight = {
		init: function (_tab, _max_width) {
			that = this;
			decalage = 0;
			max_width = _max_width;
			max_height = 0;
			tab_elem = document.querySelectorAll(_tab.toString());

			this.resize();
		},

		resize: function () {
			var that = this;
			if(window.innerWidth > max_width){
				this.searchMaxHeight(tab_elem);
				this.applySameHeight(tab_elem);
			}
			window.addEventListener('resize', function(event){
				if(window.innerWidth > max_width){
					that.resetMaxHeight(tab_elem);
					that.searchMaxHeight(tab_elem);
					that.applySameHeight(tab_elem);
				}else{
					that.resetMaxHeight(tab_elem);
				}
			});
		},

		searchMaxHeight: function (_elements) {
			max_height = 0;
			for (var i = 0, len = _elements.length; i < len; i++) {
				var elem = _elements[i];
				if(elem.offsetHeight > max_height) {
					max_height = elem.clientHeight;
				};
			};
		},

		applySameHeight: function (_elements) {
			for (var i = 0, len = _elements.length; i < len; i++) {
				var elem = _elements[i];

				elem.style.height = max_height+'px';
			};
		},

		resetMaxHeight: function (_elements) {
			for (var i = 0, len = _elements.length; i < len; i++) {
				var elem = _elements[i];

				elem.style.height = '';
			};
		}
	}
	window.sameHeight = window.sameHeight;
}(window, window.document));

(function () {
	'use strict';
	
	if (R4.isMobile()) {
		var body = document.querySelector('body')
		R4.addClass(body, 'is-mobile');
	}

	if(document.querySelectorAll('.news-bloc .news-bloc-item').length){
		sameHeight.init(['.news-bloc .news-bloc-item'], 630);
	}
	var data_href = document.querySelectorAll('[data-href]');

	if(data_href.length){
		for (var i = 0; i < data_href.length; i++) {
			data_href[i].addEventListener('click', function (e) {
				var url = this.getAttribute('data-href');
				window.location = url;
			})
		};
		
	}
	var rounded_div = document.querySelectorAll('.rounded_div');
	if(rounded_div.length){
		for (var i = 0; i < rounded_div.length; i++) {
			console.log('test');
			console.log(rounded_div[i].clientWidth);
			rounded_div[i].style.height = rounded_div[i].clientWidth+"px";
		}
		window.addEventListener('resize', function() {
			for (var i = 0; i < rounded_div.length; i++) {
				rounded_div[i].style.height = rounded_div[i].clientWidth+"px";
			}
		})
	}


	if(document.querySelectorAll('.subnav-border').length){
		sameHeight.init(['.subnav-border'], 550);
	}

	if(document.querySelectorAll('.mise-en-avant-same').length){
		sameHeight.init(['.mise-en-avant-same'], 768);
	}

	if (document.querySelectorAll('.parent-pageid-42').length) {
		var img_to_wrap = document.querySelectorAll('img.aligncenter, img.alignright, img.alignleft');

		for (var i = 0; i < img_to_wrap.length; i++) {
			R4.wrapAll(img_to_wrap[i]);
		}
	}




	/* CALL TO ACTION SCRIPT*/
	var calltoaction = document.querySelectorAll('.calltoaction'),
	header_content = document.querySelector('.slider') || document.querySelector('header .header');
	

	function  calculePosCallToAction() {
		var top_page = document.body.scrollTop || document.documentElement.scrollTop;
		var t_to_h_this = calltoaction[0].offsetTop - calltoaction[0].offsetHeight +  top_page,
		t_to_h_header =  header_content.offsetTop + header_content.offsetHeight;
		if (t_to_h_this > t_to_h_header) {
			if (!R4.hasClass(calltoaction[0], 'close')) {
				R4.addClass(calltoaction[0], 'close');
			}
		}
	}
	if(calltoaction.length){
		calculePosCallToAction();
		window.addEventListener('scroll', function() {
			calculePosCallToAction();
		})
	}
	

	/* CLIENT LISTING SCRIPT*/
	var clients_listing_bloc_item__sam = document.querySelectorAll('.clients-listing-bloc-item--same');
	if (clients_listing_bloc_item__sam.length) {
		sameHeight.init(['.clients-listing-bloc-item--same'], 630);
	}

	/* PARTENAIRE LISTING SCRIPT*/
	var partenaires_listing_bloc_item__sam = document.querySelectorAll('.partenaires-listing-bloc-item--same');
	if (partenaires_listing_bloc_item__sam.length) {
		sameHeight.init(['.partenaires-listing-bloc-item--same'], 630);
	}

	var trigger_overlay = document.getElementById('trigger-overlay');
	var overlay_door = document.querySelector('.overlay-door');
	var overlay = document.querySelector('.overlay-door .overlay-close');

	trigger_overlay.addEventListener('click', function(_e) {
		overlay_door.style.zIndex = "100";
		overlay_door.style.opacity = "1";
	})
	overlay.addEventListener('click', function(_e) {
		setTimeout(function(){ overlay_door.style.zIndex = "-1"; }, 1200);
		
		overlay_door.style.opacity = "-1";
	})


	/*Contact form*/

	var input_referrer = document.querySelector('.referrer[name="referrer"]');
	if (input_referrer) {
		input_referrer.setAttribute('value', document.referrer);
	}



}(window, window.document));


// SAME HEIGHT \\
(function (window, document, undefined) {
	'use strict';
	var w_W = window.innerWidth,
	w_H = window.innerHeight,
	block_header = document.querySelectorAll('.navigation-block, .slider'),
	header_H = 0,
	margBot = 50,
	tic = 20,
	scroll_use = false;

	function easeOutCuaic(t){
		t--;
		return t*t*t+1;
	}

	function scrollTo(element, duration) {
		var e = document.documentElement;
		if(e.scrollTop===0){
			var t = e.scrollTop;
			++e.scrollTop;
			e = t+1===e.scrollTop--?e:document.body;
		}
		scrollToC(e, e.scrollTop, element, duration);
	}

	function scrollToC(element, from, to, duration) {
		if (duration < 0) return;
		if(typeof from === "object")from=from.offsetTop;
		if(typeof to === "object")to=to.offsetTop;
		
		scrollToX(element, from, to, 0, 1/duration, 20, easeOutCuaic);
	}

	function scrollToX(element, x1, x2, t, v, step, operacion) {
		if (t < 0 || t > 1 || v <= 0) return;
		element.scrollTop = x1 - (x1-x2)*operacion(t);
		t += v * step;
		
		setTimeout(function() {
			scrollToX(element, x1, x2, t, v, step, operacion);
		}, step);
	}

	function createCallToScroll() {
		var body = document.querySelector('body');
		var calltoscroll = document.createElement('div');
		calltoscroll.className = "calltoscroll cursor wow";

		var calltoscroll_span = document.createElement('span');
		calltoscroll_span.className = "icon icon-arrow-down";

		calltoscroll.appendChild(calltoscroll_span);


		body.appendChild(calltoscroll);
		calltoscroll.addEventListener('click', function (_e) {
			scrollTo(header_H, 1000);
		})
		calltoscroll.style.top = w_H - calltoscroll.clientHeight - margBot +"px";
		calltoscroll.style.opacity = 1;
	}

	function init() {
		for (var i = 0; i < block_header.length; i++) {
			header_H += block_header[i].clientHeight;
		}
		if (w_H < header_H ) {
			window.addEventListener('scroll', function () {
				scroll_use = true;
			})
			setTimeout(function() {
				if (!scroll_use && !R4.isMobile()) {
					createCallToScroll();
				}
			}, 1000);
		}
	}


	window.addEventListener("load", init);
	window.addEventListener("resize", function () {
		//init();
	});
	///init();
	
}(window, window.document));
