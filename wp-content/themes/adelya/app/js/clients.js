;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
	content_element,
	tab_elements = [],
	nb_element = 0,
	index = 0,
	elements_visible = 0,
	move_interval,
	move_time = 10000;

	window.Clients = {
		init : function (_contentClass, _elementsClass) {
			content_element = document.querySelector(_contentClass);
			tab_elements = document.querySelectorAll(_elementsClass);
			nb_element = tab_elements.length;
			if (window.innerWidth < 650) {
				elements_visible  = 1;
			}
			if (window.innerWidth > 650) {
				elements_visible  = 2;
			}
			if (window.innerWidth > 980) {
				elements_visible  = 3;
			}
			if (window.innerWidth > 1200) {
				elements_visible  = 4;
			}

			Clients.contentStyle();
			Clients.elementsStyle();
			//Clients.play();
		},


		contentStyle : function () {
			content_element.style.width = (nb_element*100)/elements_visible+"%";
			var r = RegExp(" slider",'g');
			if(!r.test(content_element.className)){
				content_element.className += " slider";
			}
		},

		elementsStyle : function () {
			for (var i = 0; i < nb_element; i++) {
				tab_elements[i].style.width = 100/nb_element+"%";
			};
		},

		prev : function () {
			index --;
			if (index <= -1) {index = nb_element-elements_visible};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		next : function () {
			index ++;
			if (index >= nb_element-elements_visible+1) {index = 0};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		move : function () {
			index ++;
			if (index >= nb_element-elements_visible+1) {index = 0};

			content_element.style.WebkitTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.msTransform = "translateX(-"+100/nb_element*index+"%)";
			content_element.style.transform = "translateX(-"+100/nb_element*index+"%)";
		},

		play : function () {
			move_interval = setInterval(Clients.move, move_time);
		},

		stop : function () {
			clearInterval(move_interval);
		}
	}

	window.Clients = window.Clients;
}(window, window.document));


/* Initialisation du temoignages */
;(function (window, document, undefined) {
	'use strict';
	var document = window.document,
	clients_bloc = document.querySelectorAll('.clients-bloc'),
	clients_bloc_wrapper,
	clients_prev,
	clients_next,
	clients_overlay,
	clients_prev_wrapper,
	clients_next_wrapper;


	function init () {

		clients_bloc = document.querySelectorAll('.clients-bloc'),
		clients_bloc_wrapper = document.querySelector('.clients-bloc-wrapper'),
		clients_prev = document.querySelector('.clients-bloc-navigation.prev .icon'),
		clients_next = document.querySelector('.clients-bloc-navigation.next .icon'),
		clients_overlay = document.querySelector('.clients-bloc-overlay'),
		clients_prev_wrapper = document.querySelector('.clients-bloc-navigation.prev'),
		clients_next_wrapper = document.querySelector('.clients-bloc-navigation.next');

		var mc = new Hammer(clients_overlay);

		mc.on('swiperight', function(ev) {
			window.Clients.prev();
		});
		mc.on('swipeleft', function(ev) {
			window.Clients.next();
		});

		clients_prev.addEventListener('click', function (_e) {
			window.Clients.prev();
		});
		clients_next.addEventListener('click', function (_e) {
			window.Clients.next();
		});
		posBtnTemoignages();
	}

	function lineHeightBtn () {
		clients_prev.style.lineHeight = clients_overlay.offsetHeight+"px";
		clients_next.style.lineHeight = clients_overlay.offsetHeight+"px";
		clients_prev_wrapper.style.height = clients_overlay.offsetHeight+"px";
		clients_next_wrapper.style.height = clients_overlay.offsetHeight+"px";
	}
	function posBtnPrev () {
		var W = (clients_overlay.offsetWidth+clients_prev.offsetWidth + 20);
		if(window.innerWidth > 750){
			clients_prev_wrapper.style.right = W+"px";
			clients_prev_wrapper.style.left = "auto";
		}else{
			clients_prev_wrapper.style.right = "";
			clients_prev_wrapper.style.left = "";
		}
	}
	function posBtnNext () {
		var W = (clients_overlay.offsetWidth+clients_next.offsetWidth + 20);
		if(window.innerWidth > 750){
			clients_next_wrapper.style.right = "auto";
			clients_next_wrapper.style.left = W+"px";
		}else{
			clients_next_wrapper.style.right = "";
			clients_next_wrapper.style.left = "";
		}
	}
	function posBtnTemoignages () {
		lineHeightBtn();
		posBtnPrev();
		posBtnNext();
	}

	var item_lenght = document.querySelectorAll('.clients-bloc-item');
	if(item_lenght.length > 4){
		window.Clients.init(".clients-bloc-ul", ".clients-bloc-item");
		window.addEventListener("load", init);
		window.addEventListener("resize", function () {
			init();
			window.Clients.init(".clients-bloc-ul", ".clients-bloc-item");
		});
	}

}(window, window.document));
