<?php /** Template : header **/ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="user-scalable=no, width=device-width, maximum-scale=1.0, initial-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	<meta name="robots" content="noindex, nofollow">
	<!--[if lte IE 11]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/public/stylesheets/ie.css">
		<![endif]-->

	<!--[if lte IE 10]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/public/stylesheets/ie-lt9.css">
		<![endif]-->

		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-194x194.png" sizes="194x194">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/public/favicons/manifest.json">
		<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/public/favicons/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">


		<link rel='stylesheet' id='adelya-common-css'  href='<?php echo get_template_directory_uri(); ?>/public/stylesheets/adelya.css?ver=0.0.1' type='text/css' media='all' />



		<script type="text/javascript">var switchTo5x=true;</script>
		<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
		<script type="text/javascript">stLight.options({publisher: "a7eb92e0-a2b5-43d4-bcf8-1b89a025a197", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	</head>
	<?php
	global $wp_query;
	global $post;
	$specifique_class_header = "";
	///print_r($wp_query->post);
	$header_title = '';
	$header_subtitle = "";
	$currentLang = qtrans_getLanguage();
	if (!empty($post->ID)) {
		if ($wp_query->post->ID == 44 || $wp_query->post->post_parent == 44) {
			$specifique_class_header = "green";
		}
		if ($wp_query->post->ID == 332 || $wp_query->post->post_parent == 332) {
			$specifique_class_header = "orange";
		}

		$header_title = get_post_meta($post->ID,'_ma_valeur',true);
		$header_subtitle = $wp_query->post->post_excerpt;
		if($currentLang === 'en'){
			$header_title = get_post_meta($post->ID,'_ma_valeur_en',true);
		}
	}
	?>
	<body <?php body_class($specifique_class_header); ?>>
		<!-- Bock top navigation -->

		<div class="overlay overlay-door">
			<button type="button" class="overlay-close">
				<span class="overlay-close-cross1"></span>
				<span class="overlay-close-cross2"></span>
			</button>
			<div class="mobile-nav">
				<?php
				wp_nav_menu( array(
					'theme_location'	=> 'mobile',
					'container'			=> '',
					'container_class'	=> '',
					'menu_class'		=> 'mobile-nav-ul liset-reset',
					'walker' => new basic_walker_mobile()
					) );
					?>
				</div>
			</div>
			<div class="navigation-block">

				<?php if ( has_nav_menu( 'top' ) ) : ?>
					<div class="navigation-block-second">
						<nav id="site-navigation-second" class="second-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
							<div class="nav-langue icon icon-planete">
								<?php if ( function_exists( 'qtrans_generateLanguageSelectCode' ) ) qtrans_generateLanguageSelectCode( 'dropdown' ); ?>
								<span class="icon icon-triangle-plein"></span>
							</div>
							<?php
							wp_nav_menu( array(
								'theme_location'	=> 'top',
								'container'			=> '',
								'container_class'	=> '',
								'menu_class'		=> 'list-inline list-reset nav-list',
								'walker' => new basic_walker()
								) );
								?>
							</nav>
						</div>
					<?php endif; ?>

					<?php if ( has_nav_menu( 'main' ) ) : ?>
						<div class="navigation-block-main">
							<div class="navigation-block-main-logo">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
									<img src="<?php echo get_template_directory_uri(); ?>/public/images/logo.svg" alt="Adelya logo" onerror="this.removeAttribute('onerror'); this.src='<?php echo get_template_directory_uri(); ?>/public/images/logo.png'" >
								</a>
								<span class="punchline">
									<span><?php _e( 'Fidélisation, Relation Client et Marketing Mobile', 'adelya' ); ?></span>
								</span>
							</div>
							<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
								wp_nav_menu( array(
									'theme_location' => 'main',
									'menu_class'     => 'primary-menu',
									'container'			=> '',
									'container_class'	=> '',
									'menu_class'		=> 'list-inline list-reset nav-list',
									'walker' => new basic_walker()
									) );
									?>
								</nav>
								<div class="mobile-block">
									<button class="mobile-block-btn" id="trigger-overlay">
										<span class="mobile-block-txt">MENU</span>
										<span class="mobile-block-burger">
											<span class="mobile-block-border"></span>
											<span class="mobile-block-border"></span>
											<span class="mobile-block-border"></span>
										</span>
									</button>
								</div>
							</div>
						<?php endif; ?>
					</div>
					<!-- End of blocktonavigation -->

					<?php if(!is_front_page()): ?>
						<header>
							<div class="header">
								<div class="header-content">
									<div class="header-title">
										<?php
										if (!empty($post->ID)) {
											if (!empty($header_title)){
												echo '<h1 class="header-title">'.$header_title.'</h1>';
											}elseif (is_archive()) {
												$titre = '<h1 class="header-title">Actualités</h1>';
												if($currentLang === 'en'){
													$titre = '<h1 class="header-title">News</h1>';
												}
												echo $titre;
											}else{
												the_title( '<h1 class="header-title">', '</h1>' ); 
											}
											if (!empty($header_subtitle) && $post->post_type != "adelya_actualites") {
												echo '<span class="header-subtitle">'.$header_subtitle.'</span>';
											}
											if($post->post_type == "adelya_actualites" && !is_archive()){
												echo '<span class="header-subtitle">'.strftime( __("%d/%m/%Y"), strtotime($post->post_date) ).'</span>';
											}
										}else{
											echo("<h1 class='header-title'>404</h1>");
										}
										?>
									</div>
								</div>
							</div>
						</header>
					<?php endif; ?>

					<div id="content" class="site-content">