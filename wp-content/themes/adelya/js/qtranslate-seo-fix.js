	var inputTitle = null;
	var inputDescription = null;
	var previewTitle = null;
	var previewDescription = null;
	var title ={};
	var description = {};
	var inputs = {};
	var data = {};

	//init
	function init(callback){
		//vars 
		inputs.title = jQuery( "#snippet-editor-title" );
		inputs.description = jQuery( "#snippet-editor-meta-description" );
		// inputs.previewTitle = jQuery( "#snippet-editor-title" );
		// inputs.previewDescription = jQuery( "#snippet-editor-meta-description" );
		inputs.ogtitle = jQuery( "#yoast_wpseo_opengraph-title");
		inputs.ogdescription = jQuery( "#yoast_wpseo_opengraph-description");
		inputs.twtitle = jQuery( "#yoast_wpseo_twitter-title");
		inputs.twdescription = jQuery( "#yoast_wpseo_twitter-description");
		//get description et title
		for (var key in inputs) {
			extractMeta(inputs[key], key);
		}
		

	}


	//retourne le language actif
	function getActiveLanguage(){
		return jQuery('.qtranxs-lang-switch.active').attr('lang');
	}

	//initialise les données
	function extractMeta(input, fieldname){
		var field = input.val();
		data[fieldname] = [];
		//si des metas internationalisées existent
		if(field.indexOf("[:en]") > -1){
			data[fieldname]['en'] = field.substr(field.indexOf("[:en]") + 5)
			data[fieldname]['fr'] =  field.substr(0, field.indexOf("[:en]")).replace('[:fr]', '');
		}else{
			data[fieldname]['en'] = '';
			data[fieldname]['fr'] =  field;
		}
	}


	function updateValue(lang){
		for (var key in inputs) {
			data[key][lang] = inputs[key].val(); 
		}
	}

	function updateBeforeClick(){
		for (var key in inputs) {
			inputs[key].attr('value', '[:fr]'+data[key]['fr']+'[:en]'+data[key]['en'])
		}
	}

	function updateInput(lang){
		for (var key in inputs) {
			inputs[key].attr('value', data[key][lang]);
		}
	}


	setTimeout(function(){
		var lang = getActiveLanguage();
		init();
		updateInput(lang);

	//au switch
	jQuery( ".qtranxs-lang-switch" ).click(function() {
		lang = getActiveLanguage();
		updateInput(lang);
	});

	jQuery("#wpseo_meta input, #wpseo_meta textarea").on('input change', function() {
		updateValue(lang);
		console.log(data.title);
	});

	jQuery( "#publish" ).mousedown(function() {
		updateBeforeClick();
	});
	jQuery( "#wpb-save-post" ).mousedown(function() {
		updateBeforeClick();
	});

	jQuery( "#publish" ).mouseup(function() {
		updateBeforeClick();
	});
	jQuery( "#wpb-save-post" ).mouseup(function() {
		updateBeforeClick();
	});

}, 2000);



