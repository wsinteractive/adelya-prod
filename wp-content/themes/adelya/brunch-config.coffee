exports.config =
  # See http://brunch.io/#documentation for docs.
  files:
    javascripts:
      joinTo:
        'javascripts/vendor.js': /^vendor/
        'javascripts/app.js': /^app\/js/
      order:
        before: [
          'vendor/jquery.min.js',
        ]
    stylesheets:
      joinTo:
        'stylesheets/adelya.css': /^app\/less\/adelya.less/
        'admin/adelya-icon.css': /^app\/less\/admin.less/
        'stylesheets/ie-lt9.css': /^app\/less\/ie-lt9.less/
        'stylesheets/ie.css': /^app\/less\/ie.less/
        'stylesheets/adelya-blog.css': /^app\/less\/adelya-blog.less/
  watcher:
    usePolling: true
  modules:
    nameCleaner: (path) ->
      path.replace /^app\/js\//, ''
