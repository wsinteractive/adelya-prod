<?php
/** Adelya thème wordpress */


/************************* THEME SETUP *********************/
if ( ! function_exists( 'adelya_setup' ) ) :
	function adelya_setup() {
		/* Ajout des traductions */
		load_theme_textdomain( 'adelya', get_template_directory() . '/languages' );

		/* Laisse wordpress gérer les titres de page */
		add_theme_support( 'title-tag' );

		/* Ajout fonctionnalités post thumb (the_post_thumbnail();) */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1200, 9999 );

		/* ajout des menus */
		register_nav_menus( array(
			'main' => 'Navigation principale',
			'top'  => 'Haut de page',
			'footer'  =>'Footer',
			'mobile'  =>'Mobile',
			'blog' => 'Blog',
			) );


		class basic_walker extends Walker_Nav_Menu{
			function start_el($output, $item, $depth, $args){
				global $wp_query;

				$class_current = ($item->current == 1) ? "current_page" : "";
				$class_current_li = ($item->current == 1) ? "current_page_li" : "";

				$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
				$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
				$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

				$item_output = '';
				switch ($item->linktype) {
					case 'link_normal':
					$item_output = '<li class="nav-list-item '.$class_current_li.'">';
					break;

					case 'link_btn':
					$item_output = '<li class="nav-list-item '.$class_current_li.' full '.$item->linktypecolor.'">';
					break;

					case 'link_fb':
					$item_output = '<li class="nav-list-item '.$class_current_li.' facebook">';
					break;

					case 'link_tw':
					$item_output = '<li class="nav-list-item '.$class_current_li.' twitter">';
					break;

					case 'link_blog':
					$item_output = '<li class="nav-list-item '.$class_current_li.' blog">';
					break;

					default:
					$item_output = '<li class="nav-list-item '.$class_current_li.'">';
					break;
				}
				if ($item->linktype == 'link_fb') {
					$item_output .= '<a'. $attributes .' class="nav-list-link-icon icon-facebook id-'.$item->ID.'">';
				} elseif ($item->linktype == 'link_tw') {
					$item_output .= '<a'. $attributes .' class="nav-list-link-icon icon-twitter id-'.$item->ID.'">';
				} elseif ($item->linktype == 'link_blog') {
					$item_output .= '<a'. $attributes .' class="nav-list-link-icon icon-blog id-'.$item->ID.'">';
				} else {
					$item_output .= '<a'. $attributes .' class="nav-list-link '.$class_current.' id-'.$item->ID.'">';
					$item_output .= apply_filters( 'the_title', $item->title, $item->ID );
				}
				$item_output .= '</a>';
				$item_output .= '</li>';

				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			}

			function end_el($output, $item, $depth=0, $args=array()) {
				$output .= "";  
			}

		}

		class basic_walker_mobile extends Walker_Nav_Menu{
			function start_el($output, $item, $depth, $args){
				global $wp_query;

				$class_current = ($item->current == 1) ? "current_page" : "";

				$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
				$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
				$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

				$item_output = '';
				switch ($item->linktype) {
					case 'link_normal':
					$item_output = '<li class="mobile-nav-li">';
					break;

					case 'link_btn':
					$item_output = '<li class="mobile-nav-li full '.$item->linktypecolor.'">';
					break;

					case 'link_fb':
					$item_output = '<li class="mobile-nav-li facebook">';
					break;

					case 'link_tw':
					$item_output = '<li class="mobile-nav-li twitter">';
					break;

					case 'link_blog':
					$item_output = '<li class="nav-list-item '.$class_current_li.' blog">';
					break;

					default:
					$item_output = '<li class="mobile-nav-li">';
					break;
				}
				if ($item->linktype == 'link_fb') {
					$item_output .= '<a'. $attributes .' class="mobile-nav-li-link-icon icon-facebook">';
				} elseif ($item->linktype == 'link_tw') {
					$item_output .= '<a'. $attributes .' class="mobile-nav-li-link-icon icon-twitter">';
				} elseif ($item->linktype == 'link_blog') {
					$item_output .= '<a'. $attributes .' class="nav-list-link-icon icon-blog id-'.$item->ID.'">';
				} else {
					$item_output .= '<a'. $attributes .' class="mobile-nav-li-link '.$class_current.'">';
					$item_output .= apply_filters( 'the_title', $item->title, $item->ID );
				}
				$item_output .= '</a>';
				$item_output .= '</li>';

				$output .= apply_filters( 'walker_nav_menu_mobile_start_el', $item_output, $item, $depth, $args );
			}

			function end_el($output, $item, $depth=0, $args=array()) {
				$output .= "";  
			}

		}

		/* classes */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			) );

	}
	endif;
	add_action( 'init', 'adelya_setup' );


	/************************* STYLE & SCRIPTS *********************/


	function adelya_widgets_init() {
		register_sidebar( array(
			'name'          => 'Barre latérale - Blog',
			'id'            => 'sidebar-1',
			'description'   => 'Ajouter des widgets à ajouter dans la sidebar',
			'before_widget' => '<section id="%1$s" class="sidebar %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="sidebar-title">',
			'after_title'   => '</h2>',
			) );

	}
	add_action( 'widgets_init', 'adelya_widgets_init' );



	/************************* IMPORT *********************/

	require get_template_directory() . '/inc/template-tags.php';

	require get_template_directory() . '/inc/custom-post-type/actualites.php';
	require get_template_directory() . '/inc/custom-post-type/clients.php';
	require get_template_directory() . '/inc/custom-post-type/temoignages.php';
	require get_template_directory() . '/inc/custom-post-type/slider.php';
	require get_template_directory() . '/inc/custom-post-type/partenaires.php';
	require get_template_directory() . '/inc/custom-post-type/reseaux.php';

	include_once get_template_directory() . '/visual-composer/vc-override.php';
	include_once get_template_directory() . '/visual-composer/config/iconpicker.php';
	include_once get_template_directory() . '/visual-composer/shortcodes/shortcodes.php';

	/******************* custom meta box ******************/
// add meta box
	add_action('add_meta_boxes','initialisation_metaboxes');
	function initialisation_metaboxes(){
		global $post;
		add_meta_box('id_ma_meta', 'Titre du bandeau', 'ma_meta_function', 'page', 'side', 'core');
	}

// build meta box, and get meta
	function ma_meta_function($post){
		$fr = get_post_meta($post->ID,'_ma_valeur',true);
		$en = get_post_meta($post->ID,'_ma_valeur_en',true);
		echo '<div style="font-size:0;"><label for="" style="font-size:13px;text-align: left;width: 30%;vertical-align:middle;display:inline-block;color: #555;font-weight:bold;">Français</label><input id="mon_champ" type="text" style="width: 68%;vertical-align:middle;" name="mon_champ" value="'.$fr.'" /></div>';
		echo '<div style="font-size:0;"><label for="" style="font-size:13px;text-align: left;width: 30%;vertical-align:middle;display:inline-block;color: #555;font-weight:bold;">English</label><input id="mon_champ_en" type="text" style="width: 68%;vertical-align:middle;" name="mon_champ_en" value="'.$en.'" /></div>';
	}

// save meta box with update
	add_action('save_post','save_metaboxes');
	function save_metaboxes($post_ID){
		if(isset($_POST['mon_champ'])){
			update_post_meta($post_ID,'_ma_valeur', esc_html($_POST['mon_champ']));
		}
		if(isset($_POST['mon_champ_en'])){
			update_post_meta($post_ID,'_ma_valeur_en', esc_html($_POST['mon_champ_en']));
		}
	}

	/****************** excerpt meta box ******************/
	function wpc_excerpt_pages() {
		add_meta_box('postexcerpt', 'Sous-titre du bandeau', 'post_excerpt_meta_box', 'page', 'side', 'core');
	}
	add_action( 'admin_menu', 'wpc_excerpt_pages' );

	add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
	function fb_mce_editor_buttons( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}
/**
* Add styles/classes to the "Styles" drop-down
*/ 
add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );

function fb_mce_before_init( $settings ) {

	$style_formats = array(
		array(
			'title' => 'Télécharger un document', 
			'block' => 'div',  
			'classes' => 'download-btn icon icon-paper',
			),
		);

	$settings['style_formats'] = json_encode( $style_formats );

	return $settings;

}

// admin js
function load_custom_wp_admin_js() {
	wp_enqueue_script( 'qtranslateSeoFix', get_template_directory_uri() . '/js/qtranslate-seo-fix.js');
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_js' );


/**
* Contact form 7 custom empty value
*/
function my_wpcf7_form_elements($html) {
	$text = array('fr_FR' => 'Choisir ...', 'en_US' => 'Choose ...');
	$html = str_replace('<option value="">---</option>', '<option value="">' . $text[get_locale()] . '</option>', $html);
	return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');


// Enable qTranslate for WordPress SEO
function qtranslate_filter($text){
	return __($text);
}

add_filter('wpseo_title', 'qtranslate_filter', 10, 1);
add_filter('wpseo_opengraph_title', 'qtranslate_filter', 10, 1);
add_filter('wpseo_opengraph_description', 'qtranslate_filter', 10, 1);
add_filter('wpseo_opengraph_image', 'qtranslate_filter', 10, 1);
add_filter('wpseo_twitter_title', 'qtranslate_filter', 10, 1);
add_filter('wpseo_twitter_description', 'qtranslate_filter', 10, 1);
add_filter('wpseo_twitter_image', 'qtranslate_filter', 10, 1);
add_filter('wpseo_metadesc', 'qtranslate_filter', 10, 1);
add_filter('wpseo_metakey', 'qtranslate_filter', 10, 1);



/**
* Admin adelya custom rule
*/
function hide_menu_for_user_adelya_admin() {
		remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
		remove_submenu_page( 'themes.php', 'widgets.php' ); // hide the widgets submenu
		global $submenu;
		
		unset($submenu['themes.php'][6]); // Customize
		remove_menu_page( 'edit.php' ); //Posts
		remove_menu_page( 'edit-comments.php' ); //Comments
		remove_menu_page('edit.php?post_type=acf');
	}
	
	$user = wp_get_current_user();
	if ( in_array( 'admin-adelya', (array) $user->roles ) ) {
		add_action('admin_head', 'hide_menu_for_user_adelya_admin');
	}



	/********* qtranslate sitemap fix **********/
	function eg_create_sitemap() {
		$postsForSitemap = get_posts(array(
			'numberposts' => -1,
			'orderby' => 'modified',
			'post_type'  => array('adelya_actualites','page'),
			'order'    => 'DESC'
			));

		$sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
		$sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

		foreach($postsForSitemap as $post) {
			setup_postdata($post);
			$postdate = explode(" ", $post->post_modified);
			$url = get_site_url().'/en/';
			$url .= $post->post_type == "adelya_actualites" ? 'news/' : '';
			$url .= $post->post_parent ? qts_get_slug( $post->post_parent, "en" ).'/'.qts_get_slug( $post->ID, "en" ) : qts_get_slug( $post->ID, "en" ) ;
			$sitemap .= '<url><loc>'. $url .'</loc>'.
			'<lastmod>'. $postdate[0] .'</lastmod>'.
			'<changefreq>monthly</changefreq>'.
			'</url>';
		}

		$sitemap .= '</urlset>';

		$fp = fopen(ABSPATH . "sitemap_en.xml", 'w');
		fwrite($fp, $sitemap);
		fclose($fp);
	}
	add_action("publish_post", "eg_create_sitemap");
	add_action("publish_page", "eg_create_sitemap");
	add_action( "save_post","eg_create_sitemap");


	/********* remove website field on comment form **********/
	function wpb_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		unset($fields['url']);
		$fields['comment'] = $comment_field;
		return $fields;
	}

	add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );


	/********* pagination articles **********/
	function adelya_pagination($nbPages = '', $range = 2)
	{  
		$showitems = ($range * 2) +1;  

		global $paged;
		if(empty($paged)) $paged = 1;

		if($nbPages == '')
		{
			global $wp_query;
			$nbPages = $wp_query->max_num_pages;
			if(!$nbPages)
			{
				$nbPages = 1;
			}
		}  
		$output ="";

		if($nbPages > 1)
		{
			$output.= "<div class='pagination'>";
			if($paged > 1 ) $output.= "<a class='pagination-prev' href='".get_pagenum_link($paged - 1)."'></a>";

			for ($i=1; $i <= $nbPages; $i++)
			{
				if ($i == $paged+$range || $i == $paged-$range || $i == $paged)
				{
					$output.= ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
				}
			}

			if ($paged < $nbPages) $output.= "<a class='pagination-next' href='".get_pagenum_link($paged + 1)."'></a>";  
			$output.= "</div>\n";
			return $output;
		}
	}

	/********* change author slug **********/
	add_action('init', 'cng_author_base');
	function cng_author_base() {
		global $wp_rewrite;
		$author_slug = 'auteur'; 
		$wp_rewrite->author_base = $author_slug;
	}


	/**
 * Restrict native search widgets to the 'post' post type
 */
add_filter( 'widget_title', function( $title, $instance, $id_base )
{
    // Target the search base
    if( 'search' === $id_base )
        add_filter( 'get_search_form', 'wpse_post_type_restriction' );
    return $title;
}, 10, 3 );

function wpse_post_type_restriction( $html )
{
    // Only run once
    remove_filter( current_filter(), __FUNCTION__ );

    // Inject hidden post_type value
    return str_replace( 
        '</form>', 
        '<input type="hidden" name="post_type" value="post" /></form>',
        $html 
    );
}  
