<?php /********* POST TYPE ACTUALITES *************/

// ---- ajout du post type
if( ! function_exists( 'add_actu_type' ) ) :
	function add_actu_type() {
		$labels = array(
			'name'               => 'Actualités',
			'singular_name'      => 'Actualité',
			'add_new'            => 'Ajouter',
			'add_new_item'       => 'Ajouter une actualité',
			'edit_item'          => 'Modifier l\'actualité',
			'new_item'           => 'Nouveau',
			'all_items'          => 'Toutes les actualités',
			'view_item'          => 'Voir l\'actualité',
			'search_items'       => 'Chercher une actualité',
			'not_found'          => 'Pas d\'actualité trouvée',
			'not_found_in_trash' => 'Pas d\'actualité trouvé dans la corbeille', 
			'menu_name'          => 'Actualités'
			);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Actualités du site Adelya',
			'public'        => true,
			'menu_position' => 5,
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'rewrite'            => array( 'slug' => 'actualites' ),
			'has_archive'   => false
		// 'capabilities' => array(
		// 	'edit_post'          => 'edit_actualites', 
		// 	'read_post'          => 'read_actualites', 
		// 	'delete_post'        => 'delete_actualites', 
		// 	'edit_posts'         => 'edit_actualites', 
		// 	'edit_others_posts'  => 'edit_others_actualites', 
		// 	'publish_posts'      => 'publish_actualites',       
		// 	'read_private_posts' => 'read_private_actualites', 
		// 	'create_posts'       => 'edit_actualites', 
		// 	),
			);
		register_post_type( 'adelya_actualites', $args ); 
	}
	add_action( 'init', 'add_actu_type' ); 
	endif;

// ---- ajout de la catégorie
	if( ! function_exists( 'add_actu_categorie' ) ) :
		function add_actu_categorie() {
			$labels = array(
				'name'              => 'Catégorie',
				'singular_name'     => 'Catégories',
				'search_items'      => 'Rechercher une catégorie',
				'all_items'         => 'Toutes les catégories',
				'parent_item'       => 'Catégorie parente',
				'parent_item_colon' => 'Catégorie parent',
				'edit_item'         => 'Modifier la catégorie',
				'update_item'       => 'Mettre à jour',
				'add_new_item'      => 'Ajouter une catégorie',
				'new_item_name'     => 'Nouvelle catégorie',
				'menu_name'         => 'Catégorie',
				);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categorie' ),
				);

			register_taxonomy( 'adelya_categorie_actualites', array( 'adelya_actualites' ), $args );
		}
		add_action( 'init', 'add_actu_categorie');
		endif;
		?>