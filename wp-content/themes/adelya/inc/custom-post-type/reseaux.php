<?php // ---- ajout du post type
if( ! function_exists( 'add_reseaux_type' ) ) :
	function add_reseaux_type() {
		$labels = array(
			'name'               => 'Réseaux',
			'singular_name'      => 'reseaux',
			'add_new'            => 'Ajouter',
			'add_new_item'       => 'Ajouter un réseau social',
			'edit_item'          => 'Modifier le réseau social',
			'new_item'           => 'Nouveau',
			'all_items'          => 'Tous les réseaux sociaux',
			'view_item'          => 'Voir le réseau social',
			'search_items'       => 'Chercher un réseau social',
			'not_found'          => 'Pas de réseau social trouvé',
			'not_found_in_trash' => 'Pas de réseau social trouvé dans la corbeille', 
			'menu_name'          => 'Réseaux sociaux'
			);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Réseaux sociaux du blog',
			'public'        => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-share',
			'supports'      => array( 'title'),
			'has_archive'   => false
			);
		register_post_type( 'adelya_reseaux', $args ); 
	}
	add_action( 'init', 'add_reseaux_type' ); 
	endif;

	?>