<?php /********* POST TYPE ACTUALITES *************/

// ---- ajout du post type
if( ! function_exists( 'add_temoignages_type' ) ) :
	function add_temoignages_type() {
		$labels = array(
			'name'               => 'Témoignages',
			'singular_name'      => 'Témoignage',
			'add_new'            => 'Ajouter',
			'add_new_item'       => 'Ajouter un témoignage',
			'edit_item'          => 'Modifier le témoignage',
			'new_item'           => 'Nouveau',
			'all_items'          => 'Tous les témoignages',
			'view_item'          => 'Voir le témoignage',
			'search_items'       => 'Chercher un témoignage',
			'not_found'          => 'Pas de témoignage trouvé',
			'not_found_in_trash' => 'Pas de témoignage trouvé dans la corbeille', 
			'menu_name'          => 'Témoignages'
			);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Témoignages du site Adelya',
			'public'        => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-format-quote',
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive'   => false
		// 'capabilities' => array(
		// 	'edit_post'          => 'edit_actualites', 
		// 	'read_post'          => 'read_actualites', 
		// 	'delete_post'        => 'delete_actualites', 
		// 	'edit_posts'         => 'edit_actualites', 
		// 	'edit_others_posts'  => 'edit_others_actualites', 
		// 	'publish_posts'      => 'publish_actualites',       
		// 	'read_private_posts' => 'read_private_actualites', 
		// 	'create_posts'       => 'edit_actualites', 
		// 	),
			);
		register_post_type( 'adelya_temoignages', $args ); 
	}
	add_action( 'init', 'add_temoignages_type' ); 
	endif;

// ---- ajout de la catégorie
	if( ! function_exists( 'add_temoignages_categorie' ) ) :
		function add_temoignages_categorie() {
			$labels = array(
				'name'              => 'Catégorie',
				'singular_name'     => 'Catégories',
				'search_items'      => 'Rechercher une catégorie',
				'all_items'         => 'Toutes les catégories',
				'parent_item'       => 'Catégorie parente',
				'parent_item_colon' => 'Catégorie parent',
				'edit_item'         => 'Modifier la catégorie',
				'update_item'       => 'Mettre à jour',
				'add_new_item'      => 'Ajouter une catégorie',
				'new_item_name'     => 'Nouvelle catégorie',
				'menu_name'         => 'Catégorie',
				);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categorie' ),
				);

			register_taxonomy( 'adelya_categorie_temoignages', array( 'adelya_temoignages' ), $args );
		}
		add_action( 'init', 'add_temoignages_categorie');
		endif;
		?>