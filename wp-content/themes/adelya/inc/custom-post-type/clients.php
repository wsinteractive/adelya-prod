<?php /********* POST TYPE ACTUALITES *************/

// ---- ajout du post type
if( ! function_exists( 'add_clients_type' ) ) :
	function add_clients_type() {
		$labels = array(
			'name'               => 'Clients',
			'singular_name'      => 'Client',
			'add_new'            => 'Ajouter',
			'add_new_item'       => 'Ajouter un client',
			'edit_item'          => 'Modifier le client',
			'new_item'           => 'Nouveau',
			'all_items'          => 'Tous les clients',
			'view_item'          => 'Voir le client',
			'search_items'       => 'Chercher un client',
			'not_found'          => 'Pas de client trouvé',
			'not_found_in_trash' => 'Pas de client trouvé dans la corbeille', 
			'menu_name'          => 'Clients'
			);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Clients du site Adelya',
			'public'        => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-businessman',
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive'   => false
		// 'capabilities' => array(
		// 	'edit_post'          => 'edit_actualites', 
		// 	'read_post'          => 'read_actualites', 
		// 	'delete_post'        => 'delete_actualites', 
		// 	'edit_posts'         => 'edit_actualites', 
		// 	'edit_others_posts'  => 'edit_others_actualites', 
		// 	'publish_posts'      => 'publish_actualites',       
		// 	'read_private_posts' => 'read_private_actualites', 
		// 	'create_posts'       => 'edit_actualites', 
		// 	),
			);
		register_post_type( 'adelya_clients', $args ); 
	}
	add_action( 'init', 'add_clients_type' ); 
	endif;

// ---- ajout de la catégorie
	if( ! function_exists( 'add_client_categorie' ) ) :
		function add_client_categorie() {
			$labels = array(
				'name'              => 'Catégorie',
				'singular_name'     => 'Catégories',
				'search_items'      => 'Rechercher une catégorie',
				'all_items'         => 'Toutes les catégories',
				'parent_item'       => 'Catégorie parente',
				'parent_item_colon' => 'Catégorie parent',
				'edit_item'         => 'Modifier la catégorie',
				'update_item'       => 'Mettre à jour',
				'add_new_item'      => 'Ajouter une catégorie',
				'new_item_name'     => 'Nouvelle catégorie',
				'menu_name'         => 'Catégorie',
				);

			$args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categorie' ),
				);

			register_taxonomy( 'adelya_categorie_clients', array( 'adelya_clients' ), $args );
		}
		add_action( 'init', 'add_client_categorie');
		endif;
		?>