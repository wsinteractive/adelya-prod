<?php /********* POST TYPE ACTUALITES *************/

// ---- ajout du post type
if( ! function_exists( 'add_slider_type' ) ) :
	function add_slider_type() {
		$labels = array(
			'name'               => 'Slider',
			'singular_name'      => 'Slide',
			'add_new'            => 'Ajouter',
			'add_new_item'       => 'Ajouter une slide',
			'edit_item'          => 'Modifier la slide',
			'new_item'           => 'Nouveau',
			'all_items'          => 'Toutes les slides',
			'view_item'          => 'Voir la slide',
			'search_items'       => 'Chercher une slide',
			'not_found'          => 'Pas de slide trouvée',
			'not_found_in_trash' => 'Pas de slide trouvée dans la corbeille', 
			'menu_name'          => 'Slider'
			);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Slider du site Adelya',
			'public'        => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-images-alt2',
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive'   => false
		// 'capabilities' => array(
		// 	'edit_post'          => 'edit_actualites', 
		// 	'read_post'          => 'read_actualites', 
		// 	'delete_post'        => 'delete_actualites', 
		// 	'edit_posts'         => 'edit_actualites', 
		// 	'edit_others_posts'  => 'edit_others_actualites', 
		// 	'publish_posts'      => 'publish_actualites',       
		// 	'read_private_posts' => 'read_private_actualites', 
		// 	'create_posts'       => 'edit_actualites', 
		// 	),
			);
		register_post_type( 'adelya_slider', $args ); 
	}
	add_action( 'init', 'add_slider_type' ); 
endif;
?>