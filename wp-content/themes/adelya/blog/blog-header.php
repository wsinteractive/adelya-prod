<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="user-scalable=no, width=device-width, maximum-scale=1.0, initial-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />


	<?php wp_head(); ?>


  <meta name="robots" content="noindex, nofollow">
	<!--[if IE]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/public/stylesheets/ie.css">
        <![endif]-->

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/public/favicons/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-194x194.png" sizes="194x194">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/public/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/public/favicons/manifest.json">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/public/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <link rel='stylesheet' id='adelya-common-css'  href='<?php echo get_template_directory_uri(); ?>/public/stylesheets/adelya-blog.css?ver=0.0.1' type='text/css' media='all' />

        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript">stLight.options({publisher: "a7eb92e0-a2b5-43d4-bcf8-1b89a025a197", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
    </head>


    <body>

    	<div id="content" class="site-content">

    		<header class="header">
    			<div class="header-main">
    				<div class="container">
    					<div class="header-main-logo">
    						<a class="header-main-logo-link" href="<?php echo get_home_url(); ?>" rel="home">
    							<img class="header-main-logo-img" style="max-width:100px;" src="<?php echo get_template_directory_uri(); ?>/public/images/logo.svg" alt="Adelya logo" onerror="this.removeAttribute('onerror'); this.src='<?php echo get_template_directory_uri(); ?>/public/images/logo.png'">
    						</a>
    					</div>
                        <?php if (is_singular( 'post'))  : ?>
                          
                          <p class="header-main-punchline">
                              <span class="header-main-blog-txt">Blog</span>
                              <span class="header-main-title"><?php _e('C\'est bon à savoir !', 'adelya'); ?></span>
                              <span class="header-main-subtitle"><?php _e('Lorem ipsum dolor sit amet consecetur', 'adelya'); ?></span>
                          </p>
                      <?php else : ?>
                       <h1 class="header-main-punchline">
                          <span class="header-main-blog-txt">Blog</span>
                          <span class="header-main-title"><?php _e('C\'est bon à savoir !', 'adelya'); ?></span>
                          <span class="header-main-subtitle"><?php _e('Lorem ipsum dolor sit amet consecetur', 'adelya'); ?></span>
                      </h1>
                  <?php endif ?>

              </div>
          </div>
          <div class="header-nav">
            <div class="container">
               <?php
               wp_nav_menu( array(
                  'theme_location' => 'blog',
                  'menu_class'     => '',
                  'container'			=> '',
                  'container_class'	=> '',
                  'menu_class'		=> 'header-nav-ul'
                  ) );
                  ?>
              </div>
          </div>
      </header>


