<?php /** Template Name: Blog **/ ?>

<?php get_template_part( 'blog/blog', 'header' ); ?>
<div class="main container">
	<div class="main-row">
		<?php get_template_part( 'blog/blog' , 'sidebarmobile'); ?>
		<div class="main-content">
			<?php get_template_part( 'blog/blog' , 'content'); ?>
		</div>
		<?php get_template_part( 'blog/blog' , 'sidebar'); ?>
	</div>
</div>
<?php get_template_part( 'blog/blog', 'footer' ); ?>
