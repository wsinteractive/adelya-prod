<?php global $post; /** Template : footer **/ ?>

</div><!-- .site-content -->


<footer class="footer">
<div class="container">
		<div class="footer-copy">
			<p class="footer-copy-txt"><?php _e('© copyright Adelya Loyalty Operator', 'adelya'); ?> <a href="<?php echo get_permalink(57); ?>" target="_blank"><?php echo get_the_title(57); ?></a></p>
		</div>
		<div class="footer-link">
			<a class="footer-link-txt" href="http://www.adelya.com" title="Aller sur le site Adelya">www.adelya.com</a>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>

<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/public/javascripts/vendor.js?ver=0.0.1'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/public/javascripts/app.js?ver=0.0.1'></script>

<script type="text/javascript">
	require('blog');

	<?php if(is_front_page()): ?>
	require('home');
<?php endif; ?>

<?php if(!is_front_page()): ?>
	require('interior');
<?php endif; ?>



cookieWarn.init({
	styles: {
		"bg" : "#504949",
		"textColor": "#ffffff",
		"fontFamily": "Helvetica",
		"btnBg": "#ffffff",
		"btnTextColor": "#504949",
		"btnBorderRadius": "0px"
	}			
});
</script>

<script>
	// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	// 	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	// 	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	// ga('create', 'UA-11755565-1', 'auto');
	// ga('send', 'pageview');
</script>
		
	</body>
	</html>