<?php 
global $post;
$categorie = get_the_category();
$img = "";
if ( has_post_thumbnail() ){
	$img = get_the_post_thumbnail();
}
$title = get_the_title();
$intro = substr(get_the_excerpt(), 0,150);
$link = get_the_permalink();
$cat = $categorie[0]->name;
$catlink = get_category_link( $categorie[0]->term_id );
$author = __('Par','adelya')." ".get_the_author_meta('first_name')." ".get_the_author_meta('last_name');
$authorlink = get_author_posts_url(get_the_author_meta('ID'));
$date = __('Publié le ', 'adelya').strftime( __("%d/%m/%Y"), strtotime($post->post_date) );
$color = get_field('couleur', $categorie[0] );
?>

<div class="main-content">

	<article id="post-<?php the_ID(); ?>" class="post-detail" data-color="<?php echo $color ?>">

		<h1><?php echo $title; ?><span class="border-spec-color" data-color="<?php echo $color ?>"></span></h1>

		<span class="post-detail-date"><?php echo $date; ?></span>
		<a href="<?php echo $catlink ?>" class="postbox-cat font-spec-color" data-color="<?php echo $color ?>" title="<?php echo $cat ?>"><?php echo $cat; ?></a>
		<a href="<?php echo $authorlink ?>" class="postbox-author font-spec-color" data-color="<?php echo $color ?>" title="<?php echo $author ?>"><?php echo $author; ?></a>
		
		<div class="post-detail-intro"><?php echo $intro; ?></div>

		<?php if ( has_post_thumbnail() ) : ?>
			<div class="post-img">
				<?php the_post_thumbnail(); ?>
			</div>
		<?php endif; ?>

		



		<div class="blog-content">
			<div class="blog-content-txt">
				<?php the_content(); ?>
			</div>

			<div class="post-detail-share">
				<span class="post-detail-share-title"><?php _e('Partager','adelya') ?></span>
				<a class="mt-facebook mt-share-inline-bar-sm" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>">
					<img src="http://mojotech-static.s3.amazonaws.com/facebook@2x.png">
				</a>
				<a class="mt-twitter mt-share-inline-bar-sm" href="http://twitter.com/intent/tweet?text=&amp;url=<?php echo $link; ?>">
					<img src="http://mojotech-static.s3.amazonaws.com/twitter@2x.png">
				</a>
				<a class="mt-linkedin mt-share-inline-bar-sm" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $link; ?>&amp;summary=Adekya%20Blog">
					<img src="http://mojotech-static.s3.amazonaws.com/linkedin@2x.png">
				</a>
				<a class="mt-google mt-share-inline-bar-sm" href="https://plus.google.com/share?url=<?php echo $link; ?>">
					<img src="http://mojotech-static.s3.amazonaws.com/google@2x.png">
				</a>
				<!--a class="mt-pinterest mt-share-inline-bar-sm" href="http://www.pinterest.com/pin/create/button/?url=<?php echo $link; ?>&amp;media=&amp;guid=1234&amp;description=">
					<img src="http://mojotech-static.s3.amazonaws.com/pinterest@2x.png">
				</a-->
			</div>

			<?php 
			if (get_the_author_meta( 'first_name' )  != '' ) {
				get_template_part( 'blog/blog', 'author' ); 
			}
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
			?>
			
			<!-- Post en relation avec l'article courant -->
			<div class="related">
					<!-- <a href="<?php echo $catlink; ?>" class="font-spec-color" data-color="<?php echo $color; ?>"><?php echo $cat; ?></a> -->
				<div class="postbox equal-height">
				<?php 
				$relatedPosts = get_field('relation');  
				if($relatedPosts){
					$count = 0;
					echo '<p class="related-cat">';
					_e('Ceci peut vous intéresser : ', 'adelya');
					echo '</p>';

					if($count <= 2){
						foreach ($relatedPosts as $relatedPost) {
							$post = $relatedPost;  get_template_part( 'blog/blog', 'itemsmall' );
							$count++;
						}
					}
				}
				?>
				</div>
			</div>


		</div>

	</article><!-- #post-## -->

</div>