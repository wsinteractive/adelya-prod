<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div id="sidebar" class="main-aside aside">
		<div class="aside-container">
			<!-- Recherche -->
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
			<!-- Categories -->
			<div class="aside-cat">
				<p class="aside-title"><?php _e('Catégories', 'adelya'); ?></p>
				<ul class="aside-cat-ul">
					<?php $terms = get_terms( 'category' ); $currentCat = single_term_title('', false); ?>
					<?php  foreach ( $terms as $term ) : $color = get_field('couleur', $term ); ?>
						<li class="aside-cat-li <?php if($currentCat == $term->name){  echo 'current_page_item'; } ?>">
							<a class="aside-cat-a" href="<?php echo get_term_link($term); ?>" title="<?php echo $term->name;?>"><?php echo $term->name;?><span class='border-spec-color' data-color="<?php echo $color; ?>"></span></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<!-- newsletter -->
			<div class="aside-newsletter">
				<p class="aside-newsletter-title"><?php _e('Restez connecté', 'adelya'); ?></p>
				<p class="aside-newsletter-txt"><?php _e('Abonnez-vous à notre newsletter afin de recevoir des conseils marketing, technologiques et rester au courant des tendances du marché.', 'adelya'); ?></p>
				<img class="aside-newsletter-img" src="/wp-content/themes/adelya/public/images/keep-in-touch.svg" alt="Adelya Keep In Touch" onerror="this.removeAttribute('onerror'); this.src='/wp-content/themes/adelya/public/images/keep-in-touch.png'" />
				<?php echo do_shortcode( '[contact-form-7 id="1647" title="Inscription à la newsletter"]' ); ?>
			</div>
			<!-- follow -->
			<div class="aside-socials">
				<p class="aside-title"><?php _e('Suivez-nous', 'adelya'); ?></p>
				<?php $args = array(
					'post_status' => 'publish',
					'post_type' => 'adelya_reseaux',
					'orderby' => 'date',
					'order'   => 'DESC',
					);

				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) {
					global $post;
					while ( $the_query->have_posts() ) { 
						$the_query->the_post();
						$link = get_field('lien');
						$reseau = get_field('reseauxsocial');
						?>
						<a class="aside-socials-link <?php echo $reseau ?>" href="<?php echo $link ?>"  target="_blank" title="Adelya blog sur <?php echo get_the_title() ?>"></a>
						<?php } } ?>

					</div>
				</div>
			</div>
		<?php endif; ?>

