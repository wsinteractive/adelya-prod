<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
	<div id="sidebar-mobile" class="mobile-bar">
		<div class="mobile-bar-container">
			<!-- Recherche -->
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
			<!-- Categories -->
			<div class="mobile-bar-cat">
				<form>
					<select id="selectCat" class="mobile-bar-select">
						<option id="Cat" value=""><?php _e('Catégories', 'adelya'); ?></option>
						<?php $terms = get_terms( 'category' ); $currentCat = single_term_title('', false); ?>
						<?php  foreach ( $terms as $term ) : $color = get_field('couleur', $term ); ?>
							<option value="<?php echo get_term_link($term); ?>" <?php if($currentCat == $term->name) {echo 'selected';} ?>>
								<?php echo $term->name;?>
							</option>
						<?php endforeach; ?>
					</select>
				</form>
			</div>
		</div>
	</div>
<?php endif; ?>