<?php 
global $post; 
//récupération des données
$categorie = get_the_category();
$title = get_the_title();
$link = get_the_permalink();
$cat = $categorie[0]->name;
$catlink = get_category_link( $categorie[0]->term_id );
$author = get_the_author_meta('first_name')." ".get_the_author_meta('last_name');
$authorlink = get_author_posts_url(get_the_author_meta('ID'));
$date = __('Publié le ', 'adelya').strftime( __("%d/%m/%Y"), strtotime($post->post_date) );
$color = get_field('couleur', $categorie[0] );
?>
<article class="postbox-col">
	<div class="postbox-content item" data-link="<?php echo $link; ?>">
		<h2 class="postbox-title" data-color="<?php echo $color ?>">
		<a href="<?php echo get_the_permalink() ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a>
		<span class="border-spec-color" data-color="<?php echo $color ?>"></span>
		</h2>
		<span class="postbox-date"><?php echo $date; ?></span>
		<a class='postbox-cat font-spec-color' href="<?php echo $catlink ?>" data-color="<?php echo $color ?>" title="<?php echo $cat ?>"><?php echo $cat; ?></a>
		<a href="<?php echo $authorlink ?>" class="postbox-author font-spec-color" data-color="<?php echo $color; ?>" title="<?php echo $author ?>"><?php echo __('Par', 'adelya')." ".$author; ?></a>
		<span class="postbox-icon icon icon-fleche-droite"></span>
	</div>
</article>
