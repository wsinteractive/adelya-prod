<div class="author">
    <div class="author-avatar">
        <?php
        $author = get_the_author_meta('first_name')." ".get_the_author_meta('last_name');
        echo get_avatar( get_the_author_meta( 'user_email' ) );
        ?>
    </div>

    <div class="author-description">
        <span class="author-heading"><?php _e( 'A propos de l\'auteur', 'adelya' ); ?></span>
        <h2 class="author-title"><?php echo $author ?></h2>
        <p class="author-bio"><?php the_author_meta( 'description' ); ?></p>
        <a class="author-link" href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author"><?php printf( __( 'Tous les articles de %s', 'adelya' ), $author ); ?></a>
    </div>
</div>