<?php get_template_part( 'blog/blog', 'header' ); ?>



<div class="main container">
	<div class="main-row">

		<?php get_template_part( 'blog/blog' , 'sidebarmobile');  ?>



		<div class="main-content">
			<div class="postbox equal-height">
				<?php if ( have_posts() ) : ?>
					<div class="col-12">
					<h2><?php echo __('Résultats de recherche pour','adelya').' "'.get_search_query().'"'; ?></h2>
					</div>
					<?php

					// Start the Loop.
					while ( have_posts() ) : the_post();
					get_template_part( 'blog/blog', 'item' );
					endwhile;

					echo adelya_pagination();
					?>
				<?php else :  ?>
					<div class="col-12">
						<h2><?php echo __('Pas de résultats pour votre recherche','adelya'); ?></h2>
					</div>
				<?php endif; ?>
			</div>
		</div>





		<?php get_template_part( 'blog/blog' , 'sidebar'); ?>
	</div><!-- .site-main -->
</div><!-- .content-area -->

<?php get_template_part( 'blog/blog', 'footer' ); ?>