<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<form role="search" method="get" class="aside-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<input type="search" class="aside-search-field" placeholder="<?php _e( 'Recherche', 'placeholder', 'adelya' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'adelya' ); ?>" required/>
	</label>
	<button type="submit" class="aside-search-submit"><?php echo _e( 'Ok', 'submit button', 'adelya' ); ?></button>
</form>
