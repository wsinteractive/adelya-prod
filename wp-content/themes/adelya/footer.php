<?php /** Template : footer **/ ?>

			</div><!-- .site-content -->

			<footer class="footer">
				<?php
					$page = get_page_by_title('footer');
					$content = apply_filters('the_content', $page->post_content); 
					echo $content;
				?>
				<div class="footer-link-block">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_class'     => '',
							'container'			=> '',
							'container_class'	=> '',
							'menu_class'		=> 'list-inline list-reset nav-list',
							'walker' => new basic_walker()
							) );
					?>
				</div>
				<span class="copyright">©copyright Adelya Loyalty Operator</span>
			</footer>
			

		<?php wp_footer(); ?>

		<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/public/javascripts/vendor.js?ver=0.0.1'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/public/javascripts/app.js?ver=0.0.1'></script>

		<script type="text/javascript">
			require('common');

			<?php if(is_front_page()): ?>
				require('home');
			<?php endif; ?>

			<?php if(!is_front_page()): ?>
				require('interior');
			<?php endif; ?>



			cookieWarn.init({
				styles: {
					"bg" : "#504949",
					"textColor": "#ffffff",
					"fontFamily": "Helvetica",
					"btnBg": "#ffffff",
					"btnTextColor": "#504949",
					"btnBorderRadius": "0px"
				}			
			});
		</script>
		
		<script>
			/*(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-11755565-1', 'auto');
			ga('send', 'pageview');*/
		</script>
		
	</body>
</html>