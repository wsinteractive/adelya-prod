<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("actualite-detail"); ?>>
	<header class="entry-header">

		<div class="actualite-detail-thumbnail"><?php twentysixteen_post_thumbnail(); ?></div>
		<?php twentysixteen_excerpt(); ?>

	</header>
	<div class="clearfix"></div>


	<div class="actualite-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
			) );

		if ( '' !== get_the_author_meta( 'description' ) ) {
			get_template_part( 'template-parts/biography' );
		}
		?>
	</div><!-- .entry-content -->

	<footer class="actualite-detail-footer">
		<div class="actualite-detail-footer-share-wrapper">
			<?php
			$text_share = "Partager";
			$currentLang = qtrans_getLanguage();
			if($currentLang === 'en'){
				$text_share = "Share";
			}
			?>
			<span class="actualite-detail-footer-share-title"><?php print_r($text_share); ?></span>
			<span class='st_facebook_large' displayText='Facebook'></span>
			<span class='st_twitter_large' displayText='Tweet'></span>
			<span class='st_linkedin_large' displayText='LinkedIn'></span>
			<span class='st_pinterest_large' displayText='Pinterest'></span>
			<span class='st_email_large' displayText='Email'></span>
		</div>

		<?php
		$text_back_btn = "RETOUR À LA LISTE D'ACTUALITÉS";
		if($currentLang === 'en'){
			$text_back_btn = "BACK TO THE LIST OF NEWS";
		}
		?>
		<div class="actualite-detail-footer-link-wrapper"><a href="<?php echo get_page_link(1473); ?>" title="" class="actualite-detail-footer-link"><?php print_r($text_back_btn); ?></a></div> 
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
