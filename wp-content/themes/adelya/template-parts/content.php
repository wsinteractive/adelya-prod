<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
	global $post , $wpdb;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("actualite-listing-item cursor"); ?> data-href="<?php echo esc_url( get_permalink() ); ?>">
	<header class="entry-header" >
		<?php the_title('<h2 class="actualite-listing-title">', '</h2>' ); ?>
		<div><span class="actualite-listing-date"><?php echo strftime( __("%d/%m/%Y"), strtotime($post->post_date) ); ?></span></div>
	</header>

	<div class="actualite-listing-content">
		<?php twentysixteen_excerpt(); ?>
	</div>
	<span class="icon icon-fleche-droite"></span>
</article>
