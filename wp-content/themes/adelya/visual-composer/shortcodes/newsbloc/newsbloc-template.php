<?php
// [bartag foo="foo-value"]
add_shortcode( 'blocnews', 'blocnews_shortcode' );
function blocnews_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'intro' => '',
		), $atts ) );
	$output = '';
	extract( shortcode_atts( array(
		'category'        =>  '',
		'type'            =>  '1',
		'max_items'       =>  '3',
		'excerpt_length'  => '30',
		'show_comments'   => 'no',
		'img_size'   => 'default',
		'text' => ''
		), $atts ) ); 
	
	global $post , $wpdb;
	
	$args = array(
		'post_type' => 'adelya_actualites',
		'post_status' => 'publish',
		'posts_per_page' => (int)$max_items
		);


	$the_query = new WP_Query( $args );

	$output .= '<div class="news-bloc">';
	$output .= '<ul class="news-bloc-content list-inline list-reset">';
	while ( $the_query->have_posts() ) : $the_query->the_post();

	$output .='<li class="news-bloc-item cursor" data-href="'.get_permalink().'" title="'.get_the_title().'">';
	$output .='<div><span class="news-bloc-item--title">'.get_the_title().'</span></div>';
	$output .='<div><span class="news-bloc-item--date">'.strftime( __("%d/%m/%Y"), strtotime($post->post_date) ).'</span></div>';
	if($intro){
		$output .='<div><p class="news-bloc-item--text">'.wp_trim_words( get_the_excerpt(), $excerpt_length, '&hellip;' ).'</p></div>';
	}
	$output .='<span class="icon icon-fleche-droite"></span>';
	$output .='</li>';
	
	endwhile;

	$output .= '
</ul>
<div><a href="'.get_page_link(1473).'" title="'.$text.'" class="news-bloc-link">'.$text.'</a></div>
</div>
';
return $output;
}






