<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_newsbloc_elem' );
function add_newsbloc_elem() {

	vc_map( array(
		"name" => "Bloc trois actualités",
		"base" => "blocnews",
		"class" => "",
		"params" => array(
				array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Introduction", "my-text-domain" ),
					"param_name" => "intro"
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Libellé lien", "my-text-domain" ),
					"param_name" => "text",
				)
			),
		)
	);
}