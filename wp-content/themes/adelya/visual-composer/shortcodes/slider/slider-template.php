<?php
// [bartag foo="foo-value"]
add_shortcode( 'slider', 'slider_shortcode' );
function slider_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'slide_order' => false,
		'text' => ''
		), $atts ) );

	$args = array(
		'post_type'      => 'adelya_slider',
		'post_status' => 'publish'
		);

	/************** si tri par date ascendante **************/
	if($slide_order == 'date_asc'){
		$args['order'] = 'ASC';
	}

	/************** si tri par onglet trier *****************/
	if($slide_order == 'menu_order'){
		$args['order'] = 'ASC';
		$args['orderby'] = 'menu_order';
	}
	//************* récupération de l'image **********//
	$illustration = get_field('slider_illustration');

	$slides = '';
	$count = 1;
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() )  :
		$slides .= '<ul class="slider-ul">';
		while ( $the_query->have_posts() ) {
			$btn ="";
			$the_query->the_post();
			$img = get_field('slider_illustration');
			$class = ($count == 1) ? '_active' : '';
			$image = $img ? '<div class="slider-content--img"><img src="'.$img.'" alt="" class="slider-img"/></div>' : '';
			$btn = get_field('slider_choix_bouton') ? '<div class="slider-button '.$count.'"><a href="'.get_field('slider_bouton').'" title="'.get_field('slider_libelle_bouton').'" class="slider-link button line">'.get_field('slider_libelle_bouton').'</a></div>' : '';
			$slides .= 
			'<li class="slider-li '.get_field('slider_couleur_fond').' '.$class.'" data-slide="'.$count.'">
				<div class="slider-content">
					<div class="slider-content--txt">
						'.get_field('slider_text').'
						'.$btn.'
					</div>
					'.$image.'
				</div>
			</li>';
			$count++;
		}
		$slides .= '</ul>';


	//******* navigation dot *******//
		$navigation = 
		'<div class="slider-navigation">
			<ol class="slider-navigation--ol">';
			for ($i=1; $i < $count; $i++) { 
				$classDot = ($i == 1) ? '_active' : '';
				$navigation .= '<li class="slider-navigation--li '.$classDot.'" data-slide="'.$i.'">'.$i.'</li>';
			}
		$navigation .= 
			'</ol>
		</div>';



	$res = 
		"<div class='slider'>
			<div class='slider-container'>
				{$slides}
				{$navigation}
			</div>
		</div>";
	endif;

wp_reset_query();
return $res;
}






