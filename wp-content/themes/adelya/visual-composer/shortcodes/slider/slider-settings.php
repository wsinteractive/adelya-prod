<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_slider_elem' );
function add_slider_elem() {

	vc_map( array(
		"name" => "Slider accueil",
		"base" => "slider",
		"class" => "",
		"params" => array(
			array(
				'type' => 'dropdown',
				'heading' => "Trier par : ",
				'param_name' => 'slide_order',
				'value' => array(
					"Date la plus récente" => 'date_asc',
					"Ordre établi dans l'onglet trier" => 'menu_order'
					)
				)
			),
		)
	);
}