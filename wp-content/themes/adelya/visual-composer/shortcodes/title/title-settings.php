<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_title_elem' );
function add_title_elem() {

	vc_map( array(
		"name" => "Titre avec trace",
		"base" => "titre",
		"class" => "",
		"icon" => get_template_directory_uri()."/visual-composer/images/titre_avec_trace.png",
		"params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Titre", "my-text-domain" ),
					"param_name" => "titre",
					"value" => __( "This is test param for creating new project", "my-text-domain" ),
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Sous Titre", "my-text-domain" ),
					"param_name" => "soustitre",
					"value" => __( "This is test param for creating new project", "my-text-domain" ),
				),
				array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Sans la typographie fabfeltsscript", "my-text-domain" ),
					"param_name" => "typo"
				),
				array(
					'type' => 'dropdown',
					'heading' => "Style trace",
					'param_name' => 'style_trace',
					'value' => array( "Orange", "Red", "Green" ),
				)
			)
		)
	);
}