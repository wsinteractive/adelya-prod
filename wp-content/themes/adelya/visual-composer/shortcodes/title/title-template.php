<?php
// [bartag foo="foo-value"]
add_shortcode( 'titre', 'title_shortcode' );
function title_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'titre' => '',
		'soustitre' => '',
		'typo' => '',
		'style_trace' => '',
		), $atts ) );

	$typo_script = ($typo == "true") ? 'without_typo_script' : '';
	$soustitre_content = (!empty($soustitre)) ? "<span class='title-trace--subtitle ".$typo_script."'>${soustitre}</span>" : '';
	$class_trace = strtolower($style_trace);

	return "<div class='title-trace'>
				<h2 class='title-trace--title'>
						${titre}
						<span class='icon icon-trace ".$class_trace."'></span>
				</h2>
				${soustitre_content}
			</div>";
}






