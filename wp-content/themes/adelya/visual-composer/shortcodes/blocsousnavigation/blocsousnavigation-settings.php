<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_blocsousnavigation_elem' );
function add_blocsousnavigation_elem() {
	vc_map( array(
		"name" => "Bloc de sous navigation",
		"base" => "blocsousnavigation",
		"class" => "",
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Titre", "my-text-domain" ),
				"param_name" => "titre",
				),
			array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Affichage de la description des pages", "my-text-domain" ),
					"param_name" => "description"
				),
			array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Pour les enfants de la page Comment ça marche", "my-text-domain" ),
					"param_name" => "commentcamarche"
				)
			),
		)
	);
}