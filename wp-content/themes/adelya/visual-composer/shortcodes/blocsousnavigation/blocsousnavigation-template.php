<?php
// [bartag foo="foo-value"]
add_shortcode( 'blocsousnavigation', 'blocsousnavigation_shortcode' );
function blocsousnavigation_shortcode( $atts ) {
	global $wp_query;
	
	extract( shortcode_atts( array(
		'description' => '',
		'commentcamarche' => ''
		), $atts ) );

	$args = array(
		'post_type' => 'page',
		'numberposts' => -1,
		'post_parent' => $wp_query->post->ID,
		'order' => 'ASC',
		'orderby' => 'menu_order'
	);

	$the_query = new WP_Query($args);

	$output = "";

	$class_commentcamarche = "";
	if($commentcamarche == "true"){
		$class_commentcamarche = " commentcamarche";
	}

	if ( $the_query->have_posts() ) {
		$output .= "<div class='subnav".$class_commentcamarche."'>";
			$output .= "<ul class='subnav-ul list-inline list-reset'>";
			while ( $the_query->have_posts()) {
				$the_query->the_post();
				
				$bloc_description = "";
				if ($description == "true") {
					$bloc_description .= "<div class='subnav-description'>";
					$bloc_description .= get_the_excerpt();
					$bloc_description .= "</div>";
					$bloc_description .= "<div class='subnav-fleche'><span class='icon icon-fleche-droite'></span></div>";
				}

				$output .= "<li class='subnav-li cursor wow'>";
					$output .= "<a class='subnav-link' href='".get_permalink()."' title='".get_the_title()."'>";
						$output .= "<div class='subnav-img-wrapper-bg rounded_div'><div class='subnav-img-dashed rounded_div'><div class='subnav-img rounded_div'>".get_the_post_thumbnail(get_the_id())."</div></div></div>";
						if ($description == "true") {
							$output .= "<div class='subnav-border'>";
						}else{
							$output .= "<div>";
						}
							$output .= "<div class='subnav-title-wrapper'><span class='subnav-title'>".get_the_title()."</span></div>";
							$output .= $bloc_description;
						$output .= "</div>";
					$output .= "</a>";
				$output .= "</li>";
			}
			$output .= "</ul>";
		$output .= "</div>";
	}


	return $output;
}






