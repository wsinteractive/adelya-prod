<?php
// [bartag foo="foo-value"]
add_shortcode( 'clients', 'clients_shortcode' );
function clients_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'choose_mode' => 'title',
		'ids' => false,
		'order_categorie' => false,
		"order_title" => 'order_in',
		'taxonomies' => false,
		'link' => '',
		), $atts ) );
	$link_page = vc_build_link($link);

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'adelya_clients',
		//'orderby'=>'post__in'
		);

	/************** si choix par catégorie *******************/
	if($choose_mode === 'categorie' && $taxonomies){
		$choosen_cats = explode(',', str_replace(' ', '', $taxonomies));
		$args['tax_query'] = array( array (
			'taxonomy' => 'adelya_categorie_clients',
			'field' => 'id',
			'terms' => $choosen_cats
			)
		);
	}

	/************** si choix par titre **********************/
	if($choose_mode === 'title' && $ids){
		$choosen_ids = explode(',', str_replace(' ', '', $ids));
		$args['post__in'] = $choosen_ids;
	}

	/************** si tri par date ascendante **************/
	if($order_title == 'date_asc' || $order_categorie == 'date_asc'){
		$args['order'] = 'ASC';
	}

	/************** si tri par date descendante *************/
	if($order_title == 'date_desc' || $order_categorie == 'date_desc'){
		$args['order'] = 'DESC';
	}

	/************** si tri par onglet trier *****************/
	if($order_title == 'menu_order' || $order_categorie == 'menu_order'){
		$args['order'] = 'ASC';
		$args['orderby'] = 'menu_order';
	}

	/************** si tri choisi dans l'élément ************/
	if($order_title == 'order_in'){
		$args['orderby'] = 'post__in';
	}

	$the_query = new WP_Query( $args );

	$output = '';
	$output .= '<div class="clients-bloc">';
			$output .= '
			<div class="clients-bloc-wrapper">';
			
			if($the_query->post_count > 4){
				$output .= '<div class="clients-bloc-navigation prev"><span class="icon icon-fleche-droite cursor"></span></div>';
			}
			$output .= '
				<div class="clients-bloc-overlay">
					<div class="clients-bloc-margin">
					<ul class="clients-bloc-ul list-inline list-reset">';
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$logo = get_field('clients_logo');
						$output .= "<li class='clients-bloc-item cursor' data-href='".$link_page['url']."'>";
							$output .= '<img src="'.$logo['url'].'" alt="'.$logo['alt'].'" />';
						$output .= "</li>";
					}
				}
		 	$output .="</ul>";
		 	$output .="</div>";
		 $output .="</div>";
		if($the_query->post_count > 4){
			$output .= '<div class="clients-bloc-navigation next"><span class="icon icon-fleche-droite cursor"></span></div>';
		}
	 $output .="<div>
			 	<a href='".$link_page['url']."' title='".$link_page['title']."' target='".$link_page['target']."' class='clients-bloc-link'>".$link_page['title']."</a>
			 </div>
			</div>";
		 $output .="</div>";

	return $output;
}