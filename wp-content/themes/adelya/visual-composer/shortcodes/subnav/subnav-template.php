<?php
// [bartag foo="foo-value"]
add_shortcode( 'subnav', 'subnav_shortcode' );
function subnav_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'choose_mode' => 'title',
		), $atts ) );



	$args = array(
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_parent'    => $post->ID,
		'order'          => 'ASC',
		'orderby'        => 'menu_order'
		);

	$test = '';
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) {
		$test = "<ul>";
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$test .='<li><a href="'.get_the_permalink().'">'.get_the_title()."</a></li>";
		}
		$test .=" </ul>";
	}
	wp_reset_query();
	return $test;
}