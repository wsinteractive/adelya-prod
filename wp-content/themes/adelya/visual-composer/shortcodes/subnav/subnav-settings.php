<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_subnav_elem' );
function add_subnav_elem() {

	vc_map( array(
		"name" => "Sous-navigation",
		"base" => "subnav",
		"class" => "",
		"params" => array(
			array(
				'type' => 'dropdown',
				'heading' => "Choisir les témoignages par :",
				'param_name' => 'choose_mode',
				'value' => array(
					"Titre" => 'title',
					"Catégorie" => 'categorie',
					),
				),
			)
		)
	);
}