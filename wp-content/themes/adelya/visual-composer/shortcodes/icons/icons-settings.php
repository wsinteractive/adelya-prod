<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_icons_elem' );
function add_icons_elem() {

	add_filter( 'vc_iconpicker-type-custom', 'vc_iconpicker_type_custom' );

	function vc_iconpicker_type_custom( $icons ) {
		$custom_icons = array(
			array("icon-eye" => ""),
			array("icon-paper-clip" => ""),
			array("icon-mail" => ""),
			array("icon-toggle" => ""),
			array("icon-layout" => ""),
			array("icon-link" => ""),
			array("icon-bell" => ""),
			array("icon-lock" => ""),
			array("icon-unlock" => ""),
			array("icon-ribbon" => ""),
			array("icon-image" => ""),
			array("icon-signal" => ""),
			array("icon-target" => ""),
			array("icon-clipboard" => ""),
			array("icon-clock" => ""),
			array("icon-watch" => ""),
			array("icon-air-play" => ""),
			array("icon-camera" => ""),
			array("icon-video" => ""),
			array("icon-disc" => ""),
			array("icon-printer" => ""),
			array("icon-monitor" => ""),
			array("icon-server" => ""),
			array("icon-cog" => ""),
			array("icon-heart" => ""),
			array("icon-paragraph" => ""),
			array("icon-align-justify" => ""),
			array("icon-align-left" => ""),
			array("icon-align-center" => ""),
			array("icon-align-right" => ""),
			array("icon-book" => ""),
			array("icon-layers" => ""),
			array("icon-stack" => ""),
			array("icon-stack-2" => ""),
			array("icon-paper" => ""),
			array("icon-paper-stack" => ""),
			array("icon-search" => ""),
			array("icon-zoom-in" => ""),
			array("icon-zoom-out" => ""),
			array("icon-reply" => ""),
			array("icon-circle-plus" => ""),
			array("icon-circle-minus" => ""),
			array("icon-circle-check" => ""),
			array("icon-circle-cross" => ""),
			array("icon-square-plus" => ""),
			array("icon-square-minus" => ""),
			array("icon-square-check" => ""),
			array("icon-square-cross" => ""),
			array("icon-microphone" => ""),
			array("icon-record" => ""),
			array("icon-skip-back" => ""),
			array("icon-rewind" => ""),
			);

return array_merge( $icons, $custom_icons );
}


add_action( 'vc_base_register_admin_css', 'ggg' );

function add_css() {
	wp_register_style( 'vc_custom', vc_asset_url( 'style.css' ), false, WPB_VC_VERSION );
}

function ggg() {
	wp_enqueue_style( 'adelya-iconpicker', get_template_directory_uri() . '/css/adelya-iconpicker.css');
}




vc_map( array(
	"name" => "Icone",
	"base" => "icons",
	"class" => "",
	"params" => array(
		array(
			'type' => 'iconpicker',
			'heading' => __( 'Icon', 'js_composer' ),
			'param_name' => 'icon',
				'value' => 'icon-home', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					'type' => 'custom',
				// default true, display an "EMPTY" icon?
					'iconsPerPage' => 4000,
				// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
				'description' => __( 'Select icon from library.', 'js_composer' ),
				),
		)
	)
);
}

