<?php
// [bartag foo="foo-value"]
add_shortcode( 'icons', 'icons_shortcode' );
function icons_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'icon' => 'icon-eye'
		), $atts ) );

		return "<i class='{$icon}'></i>";
}