<?php 
add_shortcode( 'list_author', 'list_author_shortcode' );
function list_author_shortcode() {
	global $post;
	global $paged;
	if(empty($paged)) $paged = 1;
	$args = array(
		'role__not_in' => array('administrator'),
		'paged'          => $paged,
		'has_published_posts' => array('post')
		);

	$wp_query = new WP_User_Query( $args );
	$authors = $wp_query->get_results();
	// echo '<pre>';
	// var_dump($authors);
	// echo '</pre>';
	$pagination = adelya_pagination( $wp_query->max_num_pages, 1);

	if(!empty($authors)){
		$output = "";
		foreach ($authors as $author) {
			$author_info = get_userdata($author->ID);
			$authorName = $author_info->first_name." ".$author_info->last_name;
			$biography = $author_info->description;
			$archiveurl = esc_url( get_author_posts_url( $author_info->ID ) );
			$img = get_avatar( $author_info->ID);
			$postNumber = count_user_posts( $author_info->ID, 'post');
			if($postNumber > 0){
				$link = '<a class="author-link" href="'.$archiveurl.'" rel="author">'.  __( "Tous les articles de", "adelya" ) .' '. $authorName .'</a>';
			}else{
				$link = "";
			}
			$output .= "			
			<div class='author'>
				<div class='author-avatar'>
					${img}
				</div>

				<div class='author-description'>
					<span class='author-heading'>". __( 'A propos de l\'auteur', 'adelya' ). "</span>
					<h2 class='author-title'>${authorName}</h2>
					<p class='author-bio'>${biography}</p>
					${link}
				</div>
			</div>";
		}
		$output .= $pagination;
		return $output;
	}
}




