<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_footer_link_social_network' );
function add_footer_link_social_network() {

	vc_map( array(
		"name" => "Bloc de lien reseau sociaux",
		"base" => "footerlinksocialnetwork",
		"class" => "",
		"show_settings_on_create" => "false",
		"params" => array(
				array(
					"type" => "vc_link",
					"holder" => "div",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link_01",
					'group' => 'Lien 1'
				),
				array(
					'type' => 'iconpicker',
					'heading' => __( 'Icon', 'js_composer' ),
					'param_name' => 'icon_01',
					'value' => 'icon-home', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false,
						'type' => 'custom',
					// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
					'description' => __( 'Select icon from library.', 'js_composer' ),
					'group' => 'Lien 1'
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link_02",
					'group' => 'Lien 2'
				),
				array(
					'type' => 'iconpicker',
					'heading' => __( 'Icon', 'js_composer' ),
					'param_name' => 'icon_02',
					'value' => 'icon-home', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false,
						'type' => 'custom',
					// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
					'description' => __( 'Select icon from library.', 'js_composer' ),
					'group' => 'Lien 2'
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link_03",
					'group' => 'Lien 3'
				),
				array(
					'type' => 'iconpicker',
					'heading' => __( 'Icon', 'js_composer' ),
					'param_name' => 'icon_03',
					'value' => 'icon-home', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false,
						'type' => 'custom',
					// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
					'description' => __( 'Select icon from library.', 'js_composer' ),
					'group' => 'Lien 3'
				)
			)
		)
	);
}