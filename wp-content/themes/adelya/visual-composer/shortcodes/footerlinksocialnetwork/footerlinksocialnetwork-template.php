<?php
// [bartag foo="foo-value"]
add_shortcode( 'footerlinksocialnetwork', 'footerlinksocialnetwork_shortcode' );
function footerlinksocialnetwork_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'link_01' => '',
		'icon_01' => '',
		'link_02' => '',
		'icon_02' => '',
		'link_03' => '',
		'icon_03' => '',
		), $atts ) );

	$link_block_01 = vc_build_link($link_01);
	$link_block_02 = vc_build_link($link_02);
	$link_block_03 = vc_build_link($link_03);

	$content = '';

	$link_block_01_html = '';
	$link_block_02_html = '';
	$link_block_03_html = '';

	if (!empty($link_block_01['url'])) {
		$link_block_01_html='
		<div class="direct-link--link wow cursor" data-href="'.$link_block_01['url'].'" title="'.$link_block_01['title'].'" target="'.$link_block_01['target'].'">
			<span class="direct-link-round link-'.$icon_01.'">
					<span class="icon '.$icon_01.'"></span>
			</span>
		</div>';
	}
	if (!empty($link_block_02['url'])) {
		$link_block_02_html='
		<div class="direct-link--link wow cursor" data-href="'.$link_block_02['url'].'" title="'.$link_block_02['title'].'" target="'.$link_block_02['target'].'">
			<span class="direct-link-round link-'.$icon_02.'">
					<span class="icon '.$icon_02.'"></span>
			</span>
		</div>';
	}
	if (!empty($link_block_03['url'])) {
		$link_block_03_html='
		<div class="direct-link--link wow cursor" data-href="'.$link_block_03['url'].'" title="'.$link_block_03['title'].'" target="'.$link_block_03['target'].'">
			<span class="direct-link-round link-'.$icon_03.'">
					<span class="icon '.$icon_03.'"></span>
			</span>
		</div>';
	}



	$content .='
			<div class="footer-link-social-network">
				<div class="keep-in-touch">
					<span class="icon icon-keepInTouch"></span>
					<span class="icon icon-Trace_keepInT"></span>
				</div>
				<div class="direct-link-content">
					'.$link_block_01_html.'

					'.$link_block_02_html.'

					'.$link_block_03_html.'
				</div>
			</div>
	';

	return $content;
}