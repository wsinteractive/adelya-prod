<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_partenaires_categorie_listing_elem' );
function add_partenaires_categorie_listing_elem() {

	function autocomplete_partenaires_categorie_listing( $search_string ){
		$data = array();
		$vc_filter_by = vc_post_param( 'vc_filter_by', 'adelya_categorie_partenaires' );
		$vc_taxonomies_types = strlen( $vc_filter_by ) > 0 ? array( $vc_filter_by ) : array_keys( vc_taxonomies_types() );
		$vc_taxonomies = get_terms( $vc_taxonomies_types, array(
			'hide_empty' => false,
			'search' => $search_string,
		) );
		if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
			foreach ( $vc_taxonomies as $t ) {
				if ( is_object( $t ) ) {
					$data[] = vc_get_term_object( $t );
				}
			}
		}
		return $data;
	}
	add_filter( 'vc_autocomplete_partenaires_categorie_listing_taxonomies_callback', 'autocomplete_partenaires_categorie_listing', 10, 1 );
	add_filter( 'vc_autocomplete_partenaires_categorie_listing_taxonomies_render', 'vc_autocomplete_taxonomies_field_render', 10, 1 );


	vc_map( array(
		"name" => "Liste de partenaires par catégorie",
		"base" => "partenaires_categorie_listing",
		"class" => "",
		"params" => array(
			array(
				'type' => 'autocomplete',
				'heading' => "Catégorie de partenaire à afficher",
				'param_name' => 'taxonomies',
				'settings' => array(
					'multiple' => true,
					'min_length' => 1,
					'groups' => true,
					),
				'description' => "Taper le début de la catégorie du partenaire. Vous pouvez réordonner les éléments en glissant/déposant",
				),
			array(
				'type' => 'dropdown',
				'heading' => "Trier par : ",
				'param_name' => 'order_categorie',
				'value' => array(
					"Date la plus récente" => 'date_asc',
					"Date la plus ancienne" => 'date_desc',
					"Ordre établi dans l'onglet trier" => 'menu_order'
					),
				),
			),
		)
	);
}