<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_partenaires_elem' );
function add_partenaires_elem() {


	function autocomplete_post_partenaires( $search_string ) {
		$query = $search_string;
		$data = array();
		$args = array(
			's' => $query,
			'post_type' => 'adelya_partenaires',
			);
		$args['vc_search_by_title_only'] = true;
		$args['numberposts'] = - 1;
		if ( 0 === strlen( $args['s'] ) ) {
			unset( $args['s'] );
		}
		add_filter( 'posts_search', 'vc_search_by_title_only', 500, 2 );
		$posts = get_posts( $args );
		if ( is_array( $posts ) && ! empty( $posts ) ) {
			foreach ( $posts as $post ) {
				$data[] = array(
					'value' => $post->ID,
					'label' => $post->post_title,
					'group' => $post->post_type,
					);
			}
		}
		return $data;
	}
	function autocomplete_partenaires_categorie( $search_string ){
		$data = array();
		$vc_filter_by = vc_post_param( 'vc_filter_by', 'adelya_categorie_partenaires' );
		$vc_taxonomies_types = strlen( $vc_filter_by ) > 0 ? array( $vc_filter_by ) : array_keys( vc_taxonomies_types() );
		$vc_taxonomies = get_terms( $vc_taxonomies_types, array(
			'hide_empty' => false,
			'search' => $search_string,
		) );
		if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
			foreach ( $vc_taxonomies as $t ) {
				if ( is_object( $t ) ) {
					$data[] = vc_get_term_object( $t );
				}
			}
		}
		return $data;
	}
	add_filter( 'vc_autocomplete_partenaires_ids_callback', 'autocomplete_post_partenaires', 10, 1 ); 
	add_filter( 'vc_autocomplete_partenaires_ids_render', 'vc_include_field_render', 10, 1 );
	add_filter( 'vc_autocomplete_partenaires_taxonomies_callback', 'autocomplete_partenaires_categorie', 10, 1 );
	add_filter( 'vc_autocomplete_partenaires_taxonomies_render', 'vc_autocomplete_taxonomies_field_render', 10, 1 );


	vc_map( array(
		"name" => "Partenaires",
		"base" => "partenaires",
		"class" => "",
		"params" => array(
			array(
				'type' => 'dropdown',
				'heading' => "Choisir les partenaires par :",
				'param_name' => 'choose_mode',
				'value' => array(
					"Titre" => 'title',
					"Catégorie" => 'categorie',
					),
				),
			array(
				'type' => 'autocomplete',
				'heading' =>"Partenaires à afficher",
				'param_name' => 'ids',
				'show_settings_on_create' => true,
				'description' => "Taper le début du titre du partenaires. Vous pouvez réordonner les éléments en glissant/déposant",
				'settings' => array(
					'multiple' => true,
					'sortable' => true,
					),
				'dependency' => array(
					'element' => 'choose_mode',
					'value' => array( 'title' ),
					),
				),
			array(
				'type' => 'autocomplete',
				'heading' => "Catégorie de partenaire à afficher",
				'param_name' => 'taxonomies',
				'settings' => array(
					'multiple' => true,
					'min_length' => 1,
					'groups' => true,
					),
				'description' => "Taper le début de la catégorie du partenaire. Vous pouvez réordonner les éléments en glissant/déposant",
				'dependency' => array(
					'element' => 'choose_mode',
					'value' => array( 'categorie' ),
					)

				),
			array(
				'type' => 'dropdown',
				'heading' => "Trier par : ",
				'param_name' => 'order_title',
				'value' => array(
					"Ordre établi dans cet élément" => "order_in",	
					"Ordre établi dans l'onglet trier" => 'menu_order',
					"Date la plus récente" => 'date_asc',
					"Date la plus ancienne" => 'date_desc'

					),
				'dependency' => array(
					'element' => 'choose_mode',
					'value' => array( 'title' ),
					)

				),
			array(
				'type' => 'dropdown',
				'heading' => "Trier par : ",
				'param_name' => 'order_categorie',
				'value' => array(
					"Date la plus récente" => 'date_asc',
					"Date la plus ancienne" => 'date_desc',
					"Ordre établi dans l'onglet trier" => 'menu_order'
					),
				'dependency' => array(
					'element' => 'choose_mode',
					'value' => array( 'categorie' ),
					)

				),
				array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Affichage du texte", "my-text-domain" ),
					"param_name" => "text"
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link",
				)
			),
		)
	);
}