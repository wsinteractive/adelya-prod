<?php
// [bartag foo="foo-value"]
add_shortcode( 'partenaires_categorie_listing', 'partenaires_categorie_listing_shortcode' );
function partenaires_categorie_listing_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'choose_mode' => 'categorie',
		'ids' => false,
		'order_categorie' => false,
		"order_title" => 'order_in',
		'taxonomies' => false
		), $atts ) );

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'adelya_partenaires',
		//'orderby'=>'post__in'
		);

	$choosen_cats = "";

	/************** si choix par catégorie *******************/
	if($choose_mode === 'categorie' && $taxonomies){
		$choosen_cats = explode(',', str_replace(' ', '', $taxonomies));
		$args['tax_query'] = array( array (
			'taxonomy' => 'adelya_categorie_partenaires',
			'field' => 'id',
			'terms' => $choosen_cats
			)
		);
	}

	/************** si tri par date ascendante **************/
	if($order_title == 'date_asc' || $order_categorie == 'date_asc'){
		$args['order'] = 'ASC';
	}

	/************** si tri par date descendante *************/
	if($order_title == 'date_desc' || $order_categorie == 'date_desc'){
		$args['order'] = 'DESC';
	}

	/************** si tri par onglet trier *****************/
	if($order_title == 'menu_order' || $order_categorie == 'menu_order'){
		$args['order'] = 'ASC';
		$args['orderby'] = 'menu_order';
	}

	/************** si tri choisi dans l'élément ************/
	if($order_title == 'order_in'){
		$args['orderby'] = 'post__in';
	}

	$the_query = new WP_Query( $args );
	$output = '';
	$output .= '<div class="partenaires-listing-bloc">';

			$output .= '<div class="partenaires-listing-bloc-wrapper">';
			$output .= '<span class="partenaires-listing-bloc--title">'.get_cat_name($taxonomies).'</span>';
			$output .= '<ul class="partenaires-listing-bloc-ul list-inline list-reset">';
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$logo = get_field('partenaires_logo');
						$link = get_field('partenaires_lien');
						$output .= "<li class='partenaires-listing-bloc-item popup'
										data-title='".get_the_title()."'
										data-description='".get_field("partenaires_text")."'
										data-src='".$logo["url"]."'
										data-alt='".$logo["alt"]."'
										data-link='".get_field('partenaires_lien')."'>";
							$output .= "<div class='partenaires-listing-bloc-item-wrapper cursor'>";
								$output .= '<div class="partenaires-listing-bloc-item--same">';
								$output .= '<img src="'.$logo['url'].'" alt="'.$logo['alt'].'" />';
								$output .= '</div>';
								$output .= '<div class="partenaires-listing-bloc-item--title">'.get_the_title().'</div>';
							$output .= "</div>";
						$output .= "</li>";
					}
				}
		 	$output .="</ul>";
	$output .="</div>";

	return $output;
}