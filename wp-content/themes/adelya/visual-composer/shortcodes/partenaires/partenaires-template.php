<?php
// [bartag foo="foo-value"]
add_shortcode( 'partenaires', 'partenaires_shortcode' );
function partenaires_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'choose_mode' => 'title',
		'ids' => false,
		'order_categorie' => false,
		"order_title" => 'order_in',
		'taxonomies' => false,
		'text' => '',
		'link' => '',
		), $atts ) );
	$link_page = vc_build_link($link);

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'adelya_partenaires',
		//'orderby'=>'post__in'
		);

	/************** si choix par catégorie *******************/
	if($choose_mode === 'categorie' && $taxonomies){
		$choosen_cats = explode(',', str_replace(' ', '', $taxonomies));
		$args['tax_query'] = array( array (
			'taxonomy' => 'adelya_categorie_partenaires',
			'field' => 'id',
			'terms' => $choosen_cats
			)
		);
	}

	/************** si choix par titre **********************/
	if($choose_mode === 'title' && $ids){
		$choosen_ids = explode(',', str_replace(' ', '', $ids));
		$args['post__in'] = $choosen_ids;
	}

	/************** si tri par date ascendante **************/
	if($order_title == 'date_asc' || $order_categorie == 'date_asc'){
		$args['order'] = 'ASC';
	}

	/************** si tri par date descendante *************/
	if($order_title == 'date_desc' || $order_categorie == 'date_desc'){
		$args['order'] = 'DESC';
	}

	/************** si tri par onglet trier *****************/
	if($order_title == 'menu_order' || $order_categorie == 'menu_order'){
		$args['order'] = 'ASC';
		$args['orderby'] = 'menu_order';
	}

	/************** si tri choisi dans l'élément ************/
	if($order_title == 'order_in'){
		$args['orderby'] = 'post__in';
	}

	$the_query = new WP_Query( $args );
	$class_pour_la_taille_des_logo = '';
	if($the_query->post_count < 4){
		$class_pour_la_taille_des_logo = 'unquart';
	}

	$data_slider_text = "";
	if($text == "true"){
		$data_slider_text = true;
	}else{
		$data_slider_text = false;
	}
	$output = '';
	$output .= '<div class="partenaires-bloc" data-slider-text="'.$data_slider_text.'">';

			$output .= '<div class="partenaires-bloc-wrapper">';
			
			if($the_query->post_count > 4 && !$text || $the_query->post_count > 1 && $text){
				$output .= '<div class="partenaires-bloc-navigation prev"><span class="icon icon-fleche-droite cursor"></span></div>';
			}
			$output .= '
				<div class="partenaires-bloc-overlay">
					<div class="clients-bloc-margin">
						<ul class="partenaires-bloc-ul list-inline list-reset">';
				if($text == "true"){
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) {
							$the_query->the_post();
							$logo = get_field('partenaires_logo');
							$output .= "<li class='partenaires-bloc-item text_true'>";
								$output .= "<blockquote cite=''>";
								$output .= '<div class="partenaires-bloc-logo"><img src="'.$logo['url'].'" alt="'.$logo['alt'].'" /></div>';
								$output .= "<div class='partenaires-bloc-text'><p>".get_field('partenaires_text')."</p></div>";
								$output .= "</blockquote>";
							$output .= "</li>";
						}
					}
				}else{
					for ($i=0; $i < $the_query->post_count; $i++) {
							$the_query->the_post();
							$logo = get_field('partenaires_logo');
							$output .= "<li class='partenaires-bloc-item ".$class_pour_la_taille_des_logo." cursor' data-href='".$link_page['url']."'>";
								$output .= "<blockquote cite=''>";
									$output .= '<div class="partenaires-bloc-logo"><img src="'.$logo['url'].'"" alt="'.$logo['alt'].'" /></div>';
								$output .= "</blockquote>";
							$output .= "</li>";
					}
				}
			 	$output .="</ul>";
		 	$output .="</div>";
		 $output .="</div>";
			if($the_query->post_count > 4 && !$text || $the_query->post_count > 1 && $text){
				$output .= '<div class="partenaires-bloc-navigation next"><span class="icon icon-fleche-droite cursor"></span></div>';
			}
			$output .="<div> 
				 	<a href='".$link_page['url']."' title='".$link_page['title']."' target='".$link_page['target']."' class='clients-bloc-link'>".$link_page['title']."</a>
				 </div>
				</div>";
			$output .="</div>";
	$output .="</div>";

	return $output;
}