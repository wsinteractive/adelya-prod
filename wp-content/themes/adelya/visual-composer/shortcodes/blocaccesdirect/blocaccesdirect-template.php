<?php
// [bartag foo="foo-value"]
add_shortcode( 'blocaccesdirect', 'blocaccesdirect_shortcode' );
function blocaccesdirect_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'choose_mode' => 'title',
		'ids' => false,
		'order_categorie' => false,
		"order_title" => 'order_in',
		'taxonomies' => false,
		'description' => ''
		), $atts ) );

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'page',
		'orderby'=>'post__in'
		);

	/************** si choix par titre **********************/
	if($choose_mode === 'title' && $ids){
		$choosen_ids = explode(',', str_replace(' ', '', $ids));
		$args['post__in'] = $choosen_ids;
	}

	/************** si tri par date ascendante **************/
	if($order_title == 'date_asc' || $order_categorie == 'date_asc'){
		$args['order'] = 'ASC';
	}

	/************** si tri par date descendante *************/
	if($order_title == 'date_desc' || $order_categorie == 'date_desc'){
		$args['order'] = 'DESC';
	}

	/************** si tri par onglet trier *****************/
	if($order_title == 'menu_order' || $order_categorie == 'menu_order'){
		$args['order'] = 'ASC';
		$args['orderby'] = 'menu_order';
	}

	/************** si tri choisi dans l'élément ************/
	if($order_title == 'order_in'){
		$args['orderby'] = 'post__in';
	}

	$the_query = new WP_Query( $args );

	$output = "";



	if ( $the_query->have_posts() ) {
		$output .= "<div class='subnav'>";
			
			$output .= "<ul class='subnav-ul list-inline list-reset'>";
			$output .= '<div class="keep-in-touch wow">
					<span class="icon icon-keepInTouch"></span>
					<span class="icon icon-Trace_keepInT"></span>
				</div>';
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				
				$bloc_description = "";
				if ($description == "true") {
					$bloc_description .= "<div class='subnav-description'>";
					$bloc_description .= get_the_excerpt();
					$bloc_description .= "</div>";
					$bloc_description .= "<div class='subnav-fleche'><span class='icon icon-fleche-droite'></span></div>";
				}

				$output .= "<li class='subnav-li cursor wow'>";
					$output .= "<a class='subnav-link' href='".get_permalink()."' title='".get_the_title()."'>";
						$output .= "<div class='subnav-img-dashed rounded_div'><div class='subnav-img rounded_div'>".get_the_post_thumbnail(get_the_id())."</div></div>";
						if ($description == "true") {
							$output .= "<div class='subnav-border'>";
						}else{
							$output .= "<div>";
						}
							$output .= "<div class='subnav-title-wrapper'><span class='subnav-title'>".get_the_title()."</span></div>";
							$output .= $bloc_description;
						$output .= "</div>";
					$output .= "</a>";
				$output .= "</li>";
			}
			$output .= "</ul>";
		$output .= "</div>";
	}


	return $output;
}






