<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_bloccaccesdirect_elem' );
function add_bloccaccesdirect_elem() {


	function autocomplete_any( $search_string ) {
		$query = $search_string;
		$data = array();
		$args = array(
			's' => $query,
			'post_type' => 'page',
			);
		$args['vc_search_by_title_only'] = true;
		$args['numberposts'] = - 1;
		if ( 0 === strlen( $args['s'] ) ) {
			unset( $args['s'] );
		}
		add_filter( 'posts_search', 'vc_search_by_title_only', 500, 2 );
		$posts = get_posts( $args );
		if ( is_array( $posts ) && ! empty( $posts ) ) {
			foreach ( $posts as $post ) {
				$data[] = array(
					'value' => $post->ID,
					'label' => $post->post_title,
					'group' => $post->post_type,
					);
			}
		}
		return $data;
	}
	add_filter( 'vc_autocomplete_blocaccesdirect_ids_callback', 'autocomplete_any', 10, 1 ); 
	add_filter( 'vc_autocomplete_blocaccesdirect_ids_render', 'vc_include_field_render', 10, 1 );


	vc_map( array(
		"name" => "Bloc accès direct",
		"base" => "blocaccesdirect",
		"class" => "",
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Titre", "my-text-domain" ),
				"param_name" => "titre",
			),
			array(
				'type' => 'autocomplete',
				'heading' =>"Page à afficher",
				'param_name' => 'ids',
				'show_settings_on_create' => true,
				'description' => "Taper le début du titre d'une page. Vous pouvez réordonner les éléments en glissant/déposant",
				'settings' => array(
					'multiple' => true,
					'sortable' => true,
					),
				),
			array(
				'type' => 'dropdown',
				'heading' => "Trier par : ",
				'param_name' => 'order_title',
				'value' => array(
					"Ordre établi dans cet élément" => "order_in",	
					"Ordre établi dans l'onglet trier" => 'menu_order',
					"Date la plus récente" => 'date_asc',
					"Date la plus ancienne" => 'date_desc'

					),
				),
			array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Affichage de la description des pages", "my-text-domain" ),
					"param_name" => "description"
				)
			),
		)
	);
}