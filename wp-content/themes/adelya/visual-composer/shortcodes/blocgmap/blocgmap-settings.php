<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_blocgmap_elem' );
function add_blocgmap_elem() {
	vc_map( array(
		"name" => "Bloc Gmap",
		"base" => "blocgmap",
		"class" => "",
		"params" => array(
			array(
				"type" => "textfield",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Liste de points", "my-text-domain" ),
				"param_name" => "listpoint",
				"description" => __( "Lister les points séparé par un ;.", "my-text-domain" )
				),
			array(
				"type" => "textarea_html",
				"holder" => "div",
				"class" => "",
				"heading" => __( "Contenu", "my-text-domain" ),
				"param_name" => "content",
				)
			)
		)
	);
}