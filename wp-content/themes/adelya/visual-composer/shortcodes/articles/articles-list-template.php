<?php 
add_shortcode( 'list_articles', 'liste_articles_shortcode' );
function liste_articles_shortcode() {
	global $post;
	global $paged;
	if(empty($paged)) $paged = 1;
	$args = array(
		'post_status' => 'publish',
		'post_type' => 'post',
		'orderby' => 'date',
		'order'   => 'DESC',
		'posts_per_page' => 6,
		'paged'          => $paged
		);

	$the_query = new WP_Query( $args );
	$output = "<div class='postbox equal-height'>";
	if ( $the_query->have_posts() ) {
		global $post;
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$categorie = get_the_category();

			//récupération des données
			$img = "";
			if ( has_post_thumbnail() ){
				$img = get_the_post_thumbnail(get_the_ID(),'thumbnail');
			}
			$title = get_the_title();
			$intro = substr(get_the_excerpt(), 0,150);
			$link = get_the_permalink();
			$cat = $categorie[0]->name;
			$catlink = get_category_link( $categorie[0]->term_id );
			$author = get_the_author_meta('first_name')." ".get_the_author_meta('last_name');
			$authorlink = get_author_posts_url(get_the_author_meta('ID'));
			$date = __('Publié le ', 'adelya').strftime( __("%d/%m/%Y"), strtotime($post->post_date) );
			$color = get_field('couleur', $categorie[0] );
			$pagination = adelya_pagination( $the_query->max_num_pages, 1);

			$output .= "			
				<div class='postbox-col'>
				<div class='postbox-content item' data-link='". $link ."'>
					<h2 class='postbox-title' data-color='".$color."'><a href='".get_the_permalink()."' title='${title}'>${title}<span class='border-spec-color' data-color='".$color."'></span></a></h2>
					<span class='postbox-date'>${date}</span>
					<a href='${catlink}' class='postbox-cat font-spec-color' data-color='".$color."' title='${cat}'>${cat}</a>
					<a href='${authorlink}' class='postbox-author font-spec-color' data-color='".$color."' title='${author}'>".__('Par ', 'adelya')." ${author}</a>
					<div class='postbox-img'>
						${img}
					</div>
					<div class='postbox-intro'>${intro}</div>
					<a href='".get_the_permalink()."' title='${title}'><span class='postbox-icon icon icon-fleche-droite'></span></a>
					</div>
			</div>";
			} //fin while
			$output .= "</div>".$pagination;
			return $output;
		}
	}

