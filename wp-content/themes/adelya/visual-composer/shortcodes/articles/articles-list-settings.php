<?php 
/**** ajout du shortcode liste articles *********/
add_action( 'vc_before_init', 'add_list_articles_elem' );
function add_list_articles_elem() {

	vc_map( array(
		"name" => "Liste d'articles",
		"base" => "list_articles",
		"class" => "",
		"params" => array(),
		)
	);
}