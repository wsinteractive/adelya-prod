<?php


global $post;


add_shortcode( 'slider_articles', 'slider_articles_shortcode' );
function slider_articles_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'choose_mode' => 'title',
		'ids' => false,
		'order_categorie' => false,
		"order_title" => 'order_in',
		'taxonomies' => false
		), $atts ) );

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'post',
		//'orderby'=>'post__in'
		);

	/************** si choix par catégorie *******************/
	if($choose_mode === 'categorie' && $taxonomies){
		$choosen_cats = explode(',', str_replace(' ', '', $taxonomies));
		$args['tax_query'] = array( array (
			'taxonomy' => 'category',
			'field' => 'id',
			'terms' => $choosen_cats
			)
		);
	}

	/************** si choix par titre **********************/
	if($choose_mode === 'title' && $ids){
		$choosen_ids = explode(',', str_replace(' ', '', $ids));
		$args['post__in'] = $choosen_ids;
	}

	/************** si tri par date ascendante **************/
	if($order_title == 'date_asc' || $order_categorie == 'date_asc'){
		$args['order'] = 'ASC';
	}

	/************** si tri par date descendante *************/
	if($order_title == 'date_desc' || $order_categorie == 'date_desc'){
		$args['order'] = 'DESC';
	}

	/************** si tri par onglet trier *****************/
	if($order_title == 'menu_order' || $order_categorie == 'menu_order'){
		$args['order'] = 'ASC';
		$args['orderby'] = 'menu_order';
	}

	/************** si tri choisi dans l'élément ************/
	if($order_title == 'order_in'){
		$args['orderby'] = 'post__in';
	}

	$the_query = new WP_Query( $args );
	$output = '';
	$output .= '
	<div class="postslider">';
		$output .= '
		<div class="flexslider">
			<ul class="slides">';
				if ( $the_query->have_posts() ) {
					global $post;
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$categorie = get_the_category();
						$img = "";
						if ( has_post_thumbnail() ){
							$img = get_the_post_thumbnail(get_the_ID(), 'thumbnail');
						}
						$color = get_field('couleur', $categorie[0] );
						$output .= "<li class='postslider-item'><div class='postslider-item-container'><div class='postslider-item-left'>";
						$output .= "<h2 class='postslider-item-h2'><a href='".get_the_permalink()."' title='".get_the_title()."' class='postslider-item-h2'>".get_the_title()."</a><span class='border-spec-color' data-color='".$color."'></span></h2>"; //post_date post_author get_the_excerpt()
						$output .= "<span class='postslider-item-date'>".__('Publié le ', 'adelya').strftime( __("%d/%m/%Y"), strtotime($post->post_date) )."<a class='postslider-item-cat font-spec-color' href='".get_category_link( $categorie[0]->term_id )."' data-color='".$color."'>".$categorie[0]->name."</a></span>"; //post_date post_author
						$output .= "<p class='postslider-item-author font-spec-color' data-color='".$color."'>".__('Par ', 'adelya')."<a class='postslider-item-all font-spec-color' data-color='".$color."' href='".get_author_posts_url(get_the_author_meta('ID'))."' title='Tous les articles de ".get_the_author_meta('first_name')." ".get_the_author_meta('last_name')."'>".get_the_author_meta('first_name')." ".get_the_author_meta('last_name')."</a></p>";
						$output .= "<div class='postslider-item-intro'>".substr(get_the_excerpt(), 0,150)."... <a class='postslider-item-more' href='".get_the_permalink()."' title='".get_the_title()."'>".__('Lire la suite','adelya')."</a></div></div>";
						$output .= "<div class='postslider-item-right'>".$img."</div>";
						$output .= "</li>";
					}
				}
				$output .="</ul>";		
				$output .="</div>";
				if($the_query->post_count > 1){
					$output .= "<div class='postslider-navigation'>
					<a href='#' class='flex-prev'></a>
					<div class='postslider-controls'></div>
					<a href='#' class='flex-next'></a>
				</div>";
			}
			$output .="</div>";

			return $output;
		}