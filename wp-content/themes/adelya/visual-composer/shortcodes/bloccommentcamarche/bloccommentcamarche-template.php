<?php
// [bartag foo="foo-value"]
add_shortcode( 'bloccommentcamarche', 'bloccommentcamarche_shortcode' );
function bloccommentcamarche_shortcode( $atts, $content='') {
	global $post;
	extract( shortcode_atts( array(
		'titre' => '',
		'soustitre' => '',
		'link' => '',
		'typo' => '',
		), $atts ) );
	$href = vc_build_link($link);

	$soustitre_content = (empty($soustitre)) ? "<span>".$soustitre."</span>" : '';
	$typo_script = ($typo == "true") ? 'without_typo_script' : '';
	$output = "";

	$output = "<div class='bloc-comment-ca-marche'>";
	$output .= "<div class='bloc-comment-ca-marche-img'>";
	$output .= "<div class='bloc-comment-ca-marche-commentcamarche wow'><img src='".get_template_directory_uri()."/public/images/commentcamarche.png' /></div>";
	$output .= "<div class='bloc-comment-ca-marche-pyramide-decoupe wow'><img src='".get_template_directory_uri()."/public/images/pyramide-decoupe.png' /></div>";
		$output .= "<div class='title-trace'>";
			$output .= "<h2 class='title-trace--title'>";
				$output .= $titre;
				$output .= "<span class='icon icon-trace'></span>";
			$output .= "</h2>";
			$output .= "<span class='title-trace--subtitle ".$typo_script."'>${soustitre}</span>";


		$output .= "</div>";

		$output .= "<div class='bloc-comment-ca-marche-content'>".$content."</div>";
		$output .= "<div class='bloc-comment-ca-marche-link-content'><a href='".$href['url']."' title='".$href['title']."' class='bloc-comment-ca-marche-link'>".$href['title']."</a></div>";
	$output .= "</div>";
	$output .= "</div>";


	return $output;
}
