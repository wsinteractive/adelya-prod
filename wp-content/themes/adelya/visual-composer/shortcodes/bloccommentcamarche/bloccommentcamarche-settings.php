<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_bloccommentcamarche' );
function add_bloccommentcamarche() {

	vc_map( array(
		"name" => "Comment ça marche",
		"base" => "bloccommentcamarche",
		"class" => "",
		"params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Titre", "my-text-domain" ),
					"param_name" => "titre",
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Sous Titre", "my-text-domain" ),
					"param_name" => "soustitre",
				),
				array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Sans la typographie fabfeltsscript", "my-text-domain" ),
					"param_name" => "typo"
				),
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Text", "my-text-domain" ),
            		"param_name" => "content",
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link",
				),
			)
		)
	);
}