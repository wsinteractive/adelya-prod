<?php
// [bartag foo="foo-value"]
add_shortcode( 'footerdirectlink', 'footerdirectlink_shortcode' );
function footerdirectlink_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'titre_01' => '',
		'description_01' => '',
		'link_01' => '',
		'titre_02' => '',
		'description_02' => '',
		'link_02' => '',
		), $atts ) );

	$link_block_01 = vc_build_link($link_01);
	$link_block_02 = vc_build_link($link_02);

	$content = '';


	$content .='
			<div class="footer-direct-link">
				<div class="direct-link">
					<a href="'.$link_block_01['url'].'" title="'.$link_block_01['title'].'" target="'.$link_block_01['target'].'" class="wow">
						<span class="direct-link-half">
							<span class="direct-link-text">
								<span class="h1">'.$titre_01.'</span>
								<p>'.$description_01.'</p>
							</span>
							<span class="direct-link-link">
								<span class="icon icon-fleche-droite"></span>
							</span>
						</span>
					</a>
					<a href="'.$link_block_02['url'].'" title="'.$link_block_02['title'].'" target="'.$link_block_02['target'].'" class="wow">
						<span class="direct-link-half">
							<span class="direct-link-text">
								<span class="h1">'.$titre_02.'</span>
								<p>'.$description_02.'</p>
							</span>
							<span class="direct-link-link">
								<span class="icon icon-fleche-droite"></span>
							</span>
						</span>
					</a>
				</div>
			</div>
	';

	return $content;
}