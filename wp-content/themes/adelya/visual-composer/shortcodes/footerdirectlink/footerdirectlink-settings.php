<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_footer_direct_link' );
function add_footer_direct_link() {

	vc_map( array(
		"name" => "Blocs ovales",
		"base" => "footerdirectlink",
		"class" => "",
		"icon" => get_template_directory_uri()."/visual-composer/images/deux_ovales.png",
		"params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Titre", "my-text-domain" ),
					"param_name" => "titre_01",
					"group" => "Bloc de gauche",
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Desciption", "my-text-domain" ),
					"param_name" => "description_01",
					"group" => "Bloc de gauche",
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link_01",
					"group" => "Bloc de gauche",
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Titre", "my-text-domain" ),
					"param_name" => "titre_02",
					"group" => "Bloc de droite",
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Desciption", "my-text-domain" ),
					"param_name" => "description_02",
					"group" => "Bloc de droite",
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link_02",
					"group" => "Bloc de droite",
				)
			)
		)
	);
}