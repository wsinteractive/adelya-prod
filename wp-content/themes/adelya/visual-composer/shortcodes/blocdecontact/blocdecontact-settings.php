<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_blocdecontact' );
function add_blocdecontact() {

	vc_map( array(
		"name" => "Bloc de contact",
		"base" => "blocdecontact",
		"class" => "",
		"params" => array(
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Contenu", "my-text-domain" ),
					"param_name" => "content",
				),
				array(
					'type' => 'iconpicker',
					'heading' => __( 'Icon', 'js_composer' ),
					'param_name' => 'icon_01',
					'value' => 'icon-home', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false,
						'type' => 'custom',
					// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
					'description' => __( 'Select icon from library.', 'js_composer' ),
					'group' => 'Lien 1'
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link_01",
					'group' => 'Lien 1'
				),
				array(
					'type' => 'iconpicker',
					'heading' => __( 'Icon', 'js_composer' ),
					'param_name' => 'icon_02',
					'value' => 'icon-home', // default value to backend editor admin_label
					'settings' => array(
						'emptyIcon' => false,
						'type' => 'custom',
					// default true, display an "EMPTY" icon?
						'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
					'description' => __( 'Select icon from library.', 'js_composer' ),
					'group' => 'Lien 2'
				),
				array(
					"type" => "vc_link",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link_02",
					'group' => 'Lien 2'
				),
			)
		)
	);
}