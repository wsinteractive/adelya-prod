<?php
// [bartag foo="foo-value"]
add_shortcode( 'blocdecontact', 'blocdecontact_shortcode' );
function blocdecontact_shortcode( $atts, $content='') {
	extract( shortcode_atts( array(
		'icon_01' => '',
		'link_01' => '',
		'icon_02' => '',
		'link_02' => '',
		), $atts ) );
	$link_block_01 = vc_build_link($link_01);
	$link_block_02 = vc_build_link($link_02);

	$output_link_01 = '';
	if(!empty($link_block_01)){
		$output_link_01 .= "<div class='bloc-de-contact-link-content'>";
			$output_link_01 .= '<a href="'.$link_block_01['url'].'" title="'.$link_block_01['title'].'" target="'.$link_block_01['target'].'" class="bloc-de-contact-link"><span class="icon '.$icon_01.'"></span>'.$link_block_01['title'].'</a>';
		$output_link_01 .= "</div>";
	}

	$output_link_02 = '';
	if(!empty($link_block_02)){
		$output_link_02 .= "<div class='bloc-de-contact-link-content'>";
			$output_link_02 .= '<a href="'.$link_block_02['url'].'" title="'.$link_block_02['title'].'" target="'.$link_block_02['target'].'" class="bloc-de-contact-link"><span class="icon '.$icon_02.'"></span>'.$link_block_02['title'].'</a>';
		$output_link_02 .= "</div>";
	}


	$output = "";

	$output = "<div class='bloc-de-contact'>";
		$output .= "<div class='bloc-de-contact-content'>".$content."</div>";
		$output .= "<div class='bloc-de-contact-link-wrapper'>";
			$output .= $output_link_01 . $output_link_02;
		$output .= "</div>";
	$output .= "</div>";


	return $output;
}