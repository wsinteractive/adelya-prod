<?php
// [bartag foo="foo-value"]
add_shortcode( 'imgtext', 'imgtext_shortcode' );
function imgtext_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'img' => '',
		'text' => '',
		'typo' => '',
		), $atts ) );

	$text_content = (!empty($text)) ? "<span class='imgtext--text'>".$text."</span>" : '';
	$typo_script = ($typo == "true") ? 'without_typo_script' : '';

	$image = wp_get_attachment_image_src($img, 'full');

	return "<div class='imgtext ".$typo_script."'>
				<div class='imgtext--img wow fadeInDown'>
					<img src='".$image[0]."' alt='".$image[1]."' class='' />
				</div>
				${text_content}
			</div>";
}






