<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_imgtext_elem' );
function add_imgtext_elem() {

	vc_map( array(
		"name" => "Image et texte",
		"base" => "imgtext",
		"class" => "",
		"params" => array(
				array(
					"type" => "attach_image",
					"holder" => "img",
					"class" => "",
					"heading" => __( "Image", "my-text-domain" ),
					"param_name" => "img",
					"value" => __( "", "my-text-domain" ),
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Sous Titre", "my-text-domain" ),
					"param_name" => "text",
				),
				array(
					"type" => "checkbox",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Sans la typographie fabfeltsscript", "my-text-domain" ),
					"param_name" => "typo"
				)
			)
		)
	);
}