<?php
// [bartag foo="foo-value"]
add_shortcode( 'testimonials', 'testimonials_shortcode' );
function testimonials_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'choose_mode' => 'title',
		'ids' => false,
		'order_categorie' => false,
		"order_title" => 'order_in',
		'taxonomies' => false
		), $atts ) );

	$args = array(
		'post_status' => 'publish',
		'post_type' => 'adelya_temoignages',
		//'orderby'=>'post__in'
		);

	/************** si choix par catégorie *******************/
	if($choose_mode === 'categorie' && $taxonomies){
		$choosen_cats = explode(',', str_replace(' ', '', $taxonomies));
		$args['tax_query'] = array( array (
			'taxonomy' => 'adelya_categorie_temoignages',
			'field' => 'id',
			'terms' => $choosen_cats
			)
		);
	}

	/************** si choix par titre **********************/
	if($choose_mode === 'title' && $ids){
		$choosen_ids = explode(',', str_replace(' ', '', $ids));
		$args['post__in'] = $choosen_ids;
	}

	/************** si tri par date ascendante **************/
	if($order_title == 'date_asc' || $order_categorie == 'date_asc'){
		$args['order'] = 'ASC';
	}

	/************** si tri par date descendante *************/
	if($order_title == 'date_desc' || $order_categorie == 'date_desc'){
		$args['order'] = 'DESC';
	}

	/************** si tri par onglet trier *****************/
	if($order_title == 'menu_order' || $order_categorie == 'menu_order'){
		$args['order'] = 'ASC';
		$args['orderby'] = 'menu_order';
	}

	/************** si tri choisi dans l'élément ************/
	if($order_title == 'order_in'){
		$args['orderby'] = 'post__in';
	}

	$the_query = new WP_Query( $args );
	$output = '';
	$output .= '
		<div class="temoignages-bloc">';

			$output .= '
			<div class="temoignages-bloc-wrapper">';
			
			if($the_query->post_count > 1){
				$output .= '<div class="temoignages-bloc-navigation prev"><span class="icon icon-fleche-droite cursor"></span></div>';
			}
			$output .= '
				<div class="temoignages-bloc-overlay">
					<ul class="temoignages-bloc-ul list-inline list-reset">';
				if ( $the_query->have_posts() ) {
					while ( $the_query->have_posts() ) {
						$the_query->the_post();
						$output .= "<li class='temoignages-bloc-item'>";
							$output .= "<blockquote cite=''>";
							$output .= "&laquo;".get_field('temoignages_txt')."&raquo;";
								$output .= "<footer>";
									$output .= "<p><span class='temoignages-bloc-item--auteur'>".get_field('temoignages_auteur')."</span>";
									if(get_field('temoignages_intitule_de_poste')){
										$output .= ' - <span class="temoignages-bloc-item--poste">'.get_field('temoignages_intitule_de_poste')."</span></p>";
									}else{
										$output .= "</p>";
									}
									$output .= "<p><span class='temoignages-bloc-item--societe'>".get_field('temoignages_societe')."</span></p>";
								$output .= "</footer>";
							$output .= "</blockquote>";
						$output .= "</li>";
					}
				}
		 	$output .="</ul>";
		 $output .="</div>";
		if($the_query->post_count > 1){
			$output .= '<div class="temoignages-bloc-navigation next"><span class="icon icon-fleche-droite cursor"></span></div>';
		}
	//$output .="<div><a href='".get_post_type_archive_link('adelya_temoignages')."' title='' class='temoignages-bloc-link'>TOUS LES TÉMOIGNAGES</a></div>";
	$output .="</div>";
	$output .="</div>";

	return $output;
}