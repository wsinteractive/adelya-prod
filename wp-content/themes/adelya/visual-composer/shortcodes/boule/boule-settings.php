<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_boule_elem' );
function add_boule_elem() {

	vc_map( array(
		"name" => "Boule comment ça marche",
		"base" => "boule",
		"class" => "",
		"params" => array(
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Avis client", "my-text-domain" ),
					"param_name" => "avis-client",
					"value" => __( "This is test param for creating new project", "my-text-domain" ),
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Bon plan", "my-text-domain" ),
					"param_name" => "bon-plan",
					"value" => __( "This is test param for creating new project", "my-text-domain" ),
				)
			)
		)
	);
}