<?php
// [bartag foo="foo-value"]
add_shortcode( 'boule', 'boule_shortcode' );
function boule_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'avis-client' => '',
		'bon-plan' => '',
		), $atts ) );

	$green_text = '';
	$currentLang = qtrans_getLanguage();

	if($currentLang === 'en'){
		$green_text = '
		<div class="side-element green" id="side-pc-tablette-systemes">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>PC, tablets and cash register systems</h3>
				<p>Whatever the configuration of your point of sale or the heterogeneity of your channels network, if you have an Internet connection, there is a way to get connected to your ADELYA solution</p>
				<ul>
					<li>PC (accessible through your web browser)</li>
					<li>Tablet or Smartphone (Android application)</li>
					<li>Dedicated mobile terminal </li>
					<li>Your own systems (by integration with our systems via our documented API)</li>
					<li>PDQ (Credit card terminal) *under conditions</li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-communication-multi-canal">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>CRM</h3>
				<div><p>The tools of the modern marketing available to all!</p></div>
				<ul>
					<li>Create campaigns (email, SMS, post) with just a few clicks</li>
					<li>Precisely target the recipients (geographical criteria, consumption, date of birth…)</li>
					<li>Customizable, mass communication (configurable fields)</li>
					<li>Automate the communications within your relation with the clients </li>
					<li>Perform satisfaction surveys </li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-statistiques-rapports">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Statistics & Reports</h3>
				<div><p>Real-time data for an accurate controlling!</p></div>
				<ul>
					<li>Members, purchasing behavior, communication reading…</li>
					<li>Possibility to export the data for separate processing</li>
					<li>Technical architecture created to secure your data (servers redundancy, regular backups) </li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-jeux-concours">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Games & competitions</h3>
				<div><p>Liven up your community and offer attractive prizes!</p></div>
				<ul>
					<li>Quizzes</li>
					<li>Random draws/Lottery</li>
					<li>Based or non-based on clients\' consumption</li>
					<li>Private function within a dedicated program </li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-cartes-fidelite">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Loyalty Program</h3>
				<p>Deploy a loyalty program that meets your creativity!</p>
				<ul>
					<li>The power of ADELYA allows the configuration of all types of programs, from the simplest rules to the most sophisticated.</li>
					<li>Any detected event can serve as a trigger, for fine stimulation of your clients </li>
					<li>The loyalty cards are physical or digital (the 2 types can go together in the same solution)</li>
					<li>ADELYA can manage several loyalty programs on your client base </li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-pass-tourisme">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Pass Tourisme</h3>
				<p>Reinforce your territorial marketing with a dynamic Tourism Pass!</p>
				<ul>
					<li>Ease of creation, sale and use of the Pass</li>
					<li>Gain in operational time and facilitated management thanks to real-time statistics</li>
					<li>Multiplied sales managing a network of resellers</li>
					<li>A modern image with the contactless cards and the companion application to ease the usage</li>
					<li>A simplification of the cash flows management </li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-cartes-cadeaux">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Gift cards</h3>
				<div><p>Offer a way to please!</p></div>
				<ul>
					<li>Management of separable or non-separable gift cards</li>
					<li>Use on a unique point of sale or closed network</li>
					<li>Integrated or independent function </li>
					<li>Technology of NFC cards or Bar Code 1D/2D </li>
					<li>You are in control of the cash flows (private system) </li>
				</ul>
			</div>
		</div>
		';
	}else{
		$green_text = '
		<div class="side-element green" id="side-pc-tablette-systemes">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>PC, tablettes et systèmes de caisse</h3>
				<div><p>Quelle que soit la configuration de votre point de vente ou l’hétérogénéité de votre réseau d’enseignes, si vous disposez d’une connexion Internet, il existe un moyen pour se connecter à votre solution ADELYA.</p></div>
				<ul>
					<li>PC (par simple accès à votre navigateur Internet)</li>
					<li>Tablette ou Smartphone (application Androïd)</li>
					<li>Terminal mobile dédié</li>
					<li>Vos systèmes propres (par intégration avec nos systèmes via notre API documentée)</li>
					<li>TPE (Terminal de Paiement Electronique) *sous conditions</li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-communication-multi-canal">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Communication multi-canal</h3>
				<div><p>Les outils du marketing moderne à la portée de tous !</p></div>
				<ul>
					<li>Créez des campagnes (email, SMS, courrier) en quelques clics</li>
					<li>Ciblez précisément les destinataires (critères géographiques, consommation, date de naissance…)</li>
					<li>Communiquez de façon massive mais personnalisée (champs paramétrables)</li>
					<li>Automatisez les communications dans le cadre de votre relation avec els clients</li>
					<li>Faites des sondages de satisfaction</li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-statistiques-rapports">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Statistiques & Rapports</h3>
				<div><p>Des données temps-réel pour un pilotage précis !</p></div>
				<ul>
					<li>Membres, comportement d’achat, lecture des communications…</li>
					<li>Possibilité d’exporter les données pour traitement séparé</li>
					<li>Architecture technique conçue pour sécuriser vos données (redondance des serveurs, sauvegardes régulières)</li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-jeux-concours">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Jeux & concours</h3>
				<div><p>Animez votre communauté et proposez des lots attractifs !</p></div>
				<ul>
					<li>Quizzs</li>
					<li>Tirages au sort/Tombolas</li>
					<li>Basés ou non sur la consommation des clients</li>
					<li>Fonction privé dans le cadre d’un programme dédié</li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-cartes-fidelite">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Programme de Fidélité</h3>
				<div><p>Déployez un programme de fidélité à la hauteur de votre créativité !</p></div>
				<ul>
					<li>La puissance d’ADELYA permet de paramétrer tous types de programmes, des règles les plus simples aux plus sophistiquées.</li>
					<li>Tout événement détecté peut servir de déclencheur, pour une animation fine de vos clients</li>
					<li>Les cartes de fidélités sont matérielles ou immatérielles (les 2 types peuvent cohabiter dans la même solution)</li>
					<li>ADELYA peut gérer plusieurs programmes de fidélité sur votre base de clients</li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-pass-tourisme">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Pass Tourisme</h3>
				<div><p>Renforcez votre marketing territorial avec un Pass Tourisme dynamique !</p></div>
				<ul>
					<li>Facilité de création, vente et utilisation des Pass</li>
					<li>Gain de temps opérationnel et gestion facilitée grâce aux statistiques temps-réel</li>
					<li>Ventes démultipliées en gérant un réseau de revendeurs</li>
					<li>Une image moderne avec les cartes sans-contact et l’application. Compagnon pour faciliter l’usage</li>
					<li>Un simplification de la gestion des flux financiers</li>
				</ul>
			</div>
		</div>
		<div class="side-element green" id="side-cartes-cadeaux">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Cartes/Chèques Cadeaux</h3>
				<div><p>Offrez un moyen de faire plaisir !</p></div>
				<ul>
					<li>Gestion de cartes cadeau sécables ou non-sécables</li>
					<li>Utilisation sur point de vente unique ou réseau fermé</li>
					<li>Fonction intégré ou autonome</li>
					<li>Technologie des cartes NFC ou Code Barre 1D/2D</li>
					<li>Vous restez maître des flux financiers (système privé)</li>
				</ul>
			</div>
		</div>';
	}

	$orange_text = '';

	if ($currentLang === 'en') {
		$orange_text = '
		<div class="side-element orange" id="side-sites-applis">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Web sites and mobile app</h3>
				<div><p>Your clients are permanently in touch with you</p></div>
				<ul>
					<li>On the website </li>
					<li>Using an app on their smartphone (Android, iOS, Windows)</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-store-locator">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Store locator</h3>
				<div><p>So that your clients know where to find you!</p></div>
				<ul>
					<li>Localization of the points of sale with possible geo-tracking of the web user to map them in the clearest way</li>
					<li>A mini-site for each store in order to improve your referencing and to provide the reader with the essentials to prepare for their visit</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-mini-site">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Mini site</h3>
				<div><p>Make yourself known, expose yourself!</p></div>
				<ul>
					<li>Website to be visible to your current and potential new clients</li>
					<li>Possibility to integrate our modules into your existing site</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-bon-plan">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Good deals</h3>
				<div><p>Special offers and privileges for the members of your community</p></div>
				<ul>
					<li>A page dedicated to goods planning on the Internet site</li>
					<li>Management of the available stock</li>
					<li>Booking coordinated with the client file in the back office</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-wallet-espace-client">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Wallet & advantages</h3>
				<div><p>A unique application to manage the loyalty on the whole of your network!</p></div>
				<ul>
					<li>Choice of stores for which the client wants to join</li>
					<li>Their loyalty status in each of these stores</li>
					<li>Management of his personal data</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-click-collect">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Click & Collect</h3>
				<div><p>And your clients can order and book over the Internet </p></div>
				<ul>
					<li>Catalogue of your products in an online store</li>
					<li>Possibility to book over the Internet site and then pick up the products in store (giving you therefore possibilities for additional sales)</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-plannings-rdv">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Schedules and Appointments</h3>
				<div><p>Let your clients register themselves to come to see you!</p></div>
				<ul>
					<li>Management of planning</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-avis-client">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Customers opinions</h3>
				<div><p>Vox populi, vox dei!</p></div>
				<ul>
					<li>Send an review request after each purchase…</li>
					<li>…and let it be known that you are appreciated by your clients</li>
				</ul>
			</div>
		</div>
		';
	}else{
		$orange_text = '
		<div class="side-element orange" id="side-sites-applis">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Sites Web et appli mobile</h3>
				<div><p>Vos clients sont en contact permanent avec vous</p></div>
				<ul>
					<li>Sur le site Web</li>
					<li>Sur une appli dans leur smartphone (Androïd, iOS, Windows)</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-store-locator">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Store locator</h3>
				<div><p>Pour que vos clients sachent où vous trouver !</p></div>
				<ul>
					<li>Localisation des points de vente avec géolocalisation possible de l’internaute pour l’aiguiller au mieux</li>
					<li>Un minisite pour chaque magasin afin d’améliorer votre référencement et donner au lecteur les informations essentielles pour préparer sa visite</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-mini-site">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Mini-site</h3>
				<div><p>Faites-vous connaître, exposez-vous!</p></div>
				<ul>
					<li>Site web pour être visible de vos clients et prospects</li>
					<li>Possibilité d’intégrer nos modules sur votre site existant</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-bon-plan">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Bons Plans</h3>
				<div><p>Des offres spéciales et privilèges pour les membres de votre communauté</p></div>
				<ul>
					<li>Une page dédiée aux bons plans sur le site Internet</li>
					<li>Gestion du stock disponible</li>
					<li>Réservation coordonnée avec la fiche client dans le backoffice</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-wallet-espace-client">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Wallet & avantages</h3>
				<div><p>Une application unique pour gérer la fidélité sur l’ensemble de votre réseau !</p></div>
				<ul>
					<li>Choix des magasins pour lesquels le client veut adhérer</li>
					<li>Son statut fidélité dans chacun de ces magasins</li>
					<li>Gestion de ses données personnelles</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-click-collect">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Click & Collect</h3>
				<div><p>Et vos clients peuvent commander et réserver sur internet</p></div>
				<ul>
					<li>Catalogue de vos produits dans une boutique en ligne</li>
					<li>Possibilité de réserver sur le site Internet pour récupérer le produit en magasin (vous donnant ainsi des possibilités de vente additionnelle)</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-plannings-rdv">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Plannings et Rendez-vous</h3>
				<div><p>Laissez vos clients s’inscrire eux-même pour venir vous voir !</p></div>
				<ul>
					<li>Gestion de planning</li>
				</ul>
			</div>
		</div>
		<div class="side-element orange" id="side-avis-client">
			<div class="side-element-content">
				<span class="cross-close"></span>
				<h3>Avis clients</h3>
				<div><p>Vox populi, vox dei !</p></div>
				<ul>
					<li>Envoyez une demande d’avis après chaque achat…</li>
					<li>…et faites savoir que vous êtes apprécié de vos clients</li>
				</ul>
			</div>
		</div>
		';
	}


	$img_left = '';

	if ($currentLang === 'en') {
		$img_left = '/public/images/left_en.png';
	}else{
		$img_left = '/public/images/left.png';
	}

	$img_right = '';

	if ($currentLang === 'en') {
		$img_right = '/public/images/right_en.png';
	}else{
		$img_right = '/public/images/right.png';
	}


	$schemat = '';
	if ($currentLang === 'en') {
		$schemat = '
		<g class="selector" id="center">
			<circle class="cls-2" cx="301.77" cy="301.44" r="79.79" />
			<path class="cls-3" d="M312.13,385.1a10.78,10.78,0,0,1,2,.19,6.73,6.73,0,0,1,1.74.55l-0.46,2.31a8.5,8.5,0,0,0-1.31-.39,6.81,6.81,0,0,0-1.47-.16,4.44,4.44,0,0,0-3.09,1,4.82,4.82,0,0,0,0,6.21,4.44,4.44,0,0,0,3.09,1,6.75,6.75,0,0,0,1.47-.16,8.34,8.34,0,0,0,1.31-.39l0.46,2.31a6.73,6.73,0,0,1-1.74.55,10.78,10.78,0,0,1-2,.19,8.2,8.2,0,0,1-3-.5,6,6,0,0,1-2.15-1.38,5.79,5.79,0,0,1-1.31-2.09,8.16,8.16,0,0,1,0-5.3A5.79,5.79,0,0,1,307,387a6,6,0,0,1,2.15-1.38A8.19,8.19,0,0,1,312.13,385.1Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M330.25,391.72a7.34,7.34,0,0,1-.44,2.56,6.2,6.2,0,0,1-1.28,2.1,6,6,0,0,1-2,1.43,7.52,7.52,0,0,1-5.54,0,6,6,0,0,1-2.05-1.43,6.09,6.09,0,0,1-1.27-2.1,7.82,7.82,0,0,1,0-5.12,6.11,6.11,0,0,1,1.27-2.1,6,6,0,0,1,2.05-1.43,7.52,7.52,0,0,1,5.54,0,6,6,0,0,1,2,1.43,6.22,6.22,0,0,1,1.28,2.1A7.35,7.35,0,0,1,330.25,391.72Zm-6.52,4.13a3.3,3.3,0,0,0,1.46-.31,3,3,0,0,0,1.07-.85,3.68,3.68,0,0,0,.64-1.3,6.64,6.64,0,0,0,0-3.32,3.7,3.7,0,0,0-.64-1.3,3,3,0,0,0-1.07-.85,3.64,3.64,0,0,0-2.93,0,3,3,0,0,0-1.06.85,3.67,3.67,0,0,0-.64,1.3,6.64,6.64,0,0,0,0,3.32,3.66,3.66,0,0,0,.64,1.3,3,3,0,0,0,1.06.85A3.28,3.28,0,0,0,323.72,395.85Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M341.78,389.38a3.64,3.64,0,0,1-.72,2.25,4.43,4.43,0,0,1-2,1.46l4.2,5h-3.88l-3.51-4.43h-0.58v4.43h-3.15V385.33h4.07a9.25,9.25,0,0,1,2.21.25,5.53,5.53,0,0,1,1.77.75A3.48,3.48,0,0,1,341.78,389.38Zm-3,.06a1.39,1.39,0,0,0-.67-1.28,3.59,3.59,0,0,0-1.88-.41h-0.9v3.59h0.77a3.39,3.39,0,0,0,2-.5A1.62,1.62,0,0,0,338.75,389.44Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M344.41,385.33h8v2.49h-4.82v2.57h4.59v2.49h-4.59v2.72h4.87v2.5h-8V385.33Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M277.93,408.36v2.49h-3.44v10.29h-3.15V410.85h-3.44v-2.49h10Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M279.44,408.36h8v2.49h-4.82v2.57h4.59v2.49h-4.59v2.72h4.87v2.5h-8V408.36Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M295.91,408.13a10.78,10.78,0,0,1,2,.19,6.73,6.73,0,0,1,1.74.55l-0.46,2.31a8.45,8.45,0,0,0-1.31-.39,6.75,6.75,0,0,0-1.47-.16,4.44,4.44,0,0,0-3.09,1,4.82,4.82,0,0,0,0,6.21,4.44,4.44,0,0,0,3.09,1,6.75,6.75,0,0,0,1.47-.16,8.34,8.34,0,0,0,1.31-.39l0.46,2.31a6.73,6.73,0,0,1-1.74.55,10.78,10.78,0,0,1-2,.19,8.2,8.2,0,0,1-3-.5,6,6,0,0,1-2.15-1.38,5.79,5.79,0,0,1-1.31-2.09,8.16,8.16,0,0,1,0-5.3,5.79,5.79,0,0,1,1.31-2.09,6,6,0,0,1,2.15-1.38A8.18,8.18,0,0,1,295.91,408.13Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M309.49,421.14v-5.2h-4.68v5.2h-3.15V408.36h3.15v5h4.68v-5h3.15v12.78h-3.15Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M323.45,421.14l-5.28-7.62v7.62h-3V408.36h3l5.28,7.62v-7.62h3v12.78h-3Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M341.43,414.75a7.34,7.34,0,0,1-.44,2.56,6.2,6.2,0,0,1-1.28,2.1,6,6,0,0,1-2,1.43,7.52,7.52,0,0,1-5.54,0,6,6,0,0,1-2.05-1.43,6.09,6.09,0,0,1-1.27-2.1,7.82,7.82,0,0,1,0-5.12,6.11,6.11,0,0,1,1.27-2.1,6,6,0,0,1,2.05-1.43,7.53,7.53,0,0,1,5.54,0,6.06,6.06,0,0,1,2,1.43,6.22,6.22,0,0,1,1.28,2.1A7.34,7.34,0,0,1,341.43,414.75Zm-6.52,4.13a3.3,3.3,0,0,0,1.46-.31,3,3,0,0,0,1.06-.85,3.67,3.67,0,0,0,.64-1.3,6.63,6.63,0,0,0,0-3.32,3.68,3.68,0,0,0-.64-1.31,3,3,0,0,0-1.06-.85,3.65,3.65,0,0,0-2.93,0,3,3,0,0,0-1.06.85,3.66,3.66,0,0,0-.64,1.31,6.63,6.63,0,0,0,0,3.32,3.65,3.65,0,0,0,.64,1.3,3,3,0,0,0,1.06.85A3.28,3.28,0,0,0,334.9,418.88Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M346.47,408.36v10.29h4.7v2.5h-7.85V408.36h3.15Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M365,414.75a7.34,7.34,0,0,1-.44,2.56,6.2,6.2,0,0,1-1.28,2.1,6,6,0,0,1-2,1.43,7.52,7.52,0,0,1-5.54,0,6,6,0,0,1-2.05-1.43,6.09,6.09,0,0,1-1.27-2.1,7.82,7.82,0,0,1,0-5.12,6.11,6.11,0,0,1,1.27-2.1,6,6,0,0,1,2.05-1.43,7.53,7.53,0,0,1,5.54,0,6.06,6.06,0,0,1,2,1.43,6.22,6.22,0,0,1,1.28,2.1A7.34,7.34,0,0,1,365,414.75Zm-6.52,4.13a3.3,3.3,0,0,0,1.46-.31,3,3,0,0,0,1.06-.85,3.67,3.67,0,0,0,.64-1.3,6.63,6.63,0,0,0,0-3.32,3.68,3.68,0,0,0-.64-1.31,3,3,0,0,0-1.06-.85,3.65,3.65,0,0,0-2.93,0,3,3,0,0,0-1.06.85,3.66,3.66,0,0,0-.64,1.31,6.63,6.63,0,0,0,0,3.32,3.65,3.65,0,0,0,.64,1.3,3,3,0,0,0,1.06.85A3.28,3.28,0,0,0,358.5,418.88Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M373.45,408.13a10,10,0,0,1,4.18.78l-0.42,2.37a9.32,9.32,0,0,0-3.45-.65,5.58,5.58,0,0,0-1.73.25,3.35,3.35,0,0,0-1.31.78,3.5,3.5,0,0,0-.84,1.34,5.7,5.7,0,0,0-.3,1.95,4,4,0,0,0,1,3,4.25,4.25,0,0,0,3.08,1,5.11,5.11,0,0,0,.84-0.07,4.06,4.06,0,0,0,.67-0.16v-3.57H378v5.27a8.83,8.83,0,0,1-2.1.67,13.25,13.25,0,0,1-2.66.26,8.23,8.23,0,0,1-2.88-.47,6.14,6.14,0,0,1-2.15-1.31,5.64,5.64,0,0,1-1.35-2,7,7,0,0,1-.47-2.62,7.89,7.89,0,0,1,.47-2.79,5.86,5.86,0,0,1,1.37-2.15,6.16,6.16,0,0,1,2.21-1.38A8.43,8.43,0,0,1,373.45,408.13Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M386.21,416.5v4.64h-3.15V416.5l-4.2-8.14h3.36l2.42,5.37,2.42-5.37h3.36Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M280.14,320.62a2.87,2.87,0,0,1,.39,1.17,2.81,2.81,0,0,1-.14,1.21,3.58,3.58,0,0,1-.67,1.16,4.68,4.68,0,0,1-1.22,1,7.36,7.36,0,0,1-1.47.66,6.46,6.46,0,0,1-1.54.31L275,324.31a7,7,0,0,0,1.21-.21,5,5,0,0,0,1.29-.54,2,2,0,0,0,.75-0.7,0.78,0.78,0,0,0,0-.83,0.87,0.87,0,0,0-.63-0.4,4.29,4.29,0,0,0-1.4.09,5.56,5.56,0,0,1-2.42,0,2.47,2.47,0,0,1-1.46-1.2,2.61,2.61,0,0,1-.29-2.33,4,4,0,0,1,1.83-1.94,6.85,6.85,0,0,1,1.46-.65,5.16,5.16,0,0,1,1.35-.24l0.56,1.75a5.77,5.77,0,0,0-1.17.22,5,5,0,0,0-1.15.5,1.79,1.79,0,0,0-.66.6,0.65,0.65,0,0,0,0,.7,0.94,0.94,0,0,0,.23.27,0.73,0.73,0,0,0,.32.13,2,2,0,0,0,.5,0l0.76-.09q0.65-.1,1.26-0.13a4.16,4.16,0,0,1,1.13.08,2.78,2.78,0,0,1,1,.4A2.48,2.48,0,0,1,280.14,320.62Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M278.42,313.85l5.44-2.65,0.83,1.71-3.29,1.6,0.85,1.76,3.13-1.52,0.83,1.71L283.08,318l0.9,1.86,3.33-1.62,0.83,1.71-5.48,2.67Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M290.36,308.39a8.16,8.16,0,0,1,1.47-.41,5.2,5.2,0,0,1,1.38-.08l0.3,1.76a6.32,6.32,0,0,0-1,.08,5.12,5.12,0,0,0-1.09.29,3.38,3.38,0,0,0-1.91,1.57,3.66,3.66,0,0,0,1.7,4.4,3.38,3.38,0,0,0,2.47-.12,5.18,5.18,0,0,0,1-.52,6.42,6.42,0,0,0,.82-0.64l1,1.51a5.12,5.12,0,0,1-1.08.87,8.38,8.38,0,0,1-1.37.68,6.26,6.26,0,0,1-2.25.46,4.55,4.55,0,0,1-1.9-.39,4.39,4.39,0,0,1-1.5-1.13,6.21,6.21,0,0,1-1.45-3.76,4.4,4.4,0,0,1,.35-1.84,4.55,4.55,0,0,1,1.15-1.57A6.2,6.2,0,0,1,290.36,308.39Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M300.82,305.27l2.31-.64,1.66,6a3.44,3.44,0,0,1-.33,2.89,5.17,5.17,0,0,1-5.83,1.61,3.44,3.44,0,0,1-1.77-2.31l-1.66-6,2.31-.64,1.68,6.06a1.72,1.72,0,0,0,.76,1.11,2.1,2.1,0,0,0,2.47-.68,1.72,1.72,0,0,0,.08-1.34Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M313.4,305.87a2.76,2.76,0,0,1-.25,1.77,3.37,3.37,0,0,1-1.31,1.35l3.8,3.22-2.9.5-3.2-2.87-0.43.08,0.57,3.32-2.36.41-1.65-9.57,3-.53a7.06,7.06,0,0,1,1.68-.1,4.22,4.22,0,0,1,1.42.33A2.64,2.64,0,0,1,313.4,305.87Zm-2.26.43a1.06,1.06,0,0,0-.67-0.87,2.73,2.73,0,0,0-1.46-.07l-0.68.12,0.46,2.69,0.57-.1a2.58,2.58,0,0,0,1.42-.63A1.23,1.23,0,0,0,311.14,306.3Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M315.49,302.53l6-.5,0.16,1.89-3.65.3,0.16,1.95,3.47-.29,0.16,1.89-3.47.29,0.17,2.06,3.69-.31,0.16,1.89-6.07.51Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M332,301.87a8.32,8.32,0,0,1,1.52.2,5.11,5.11,0,0,1,1.3.47l-0.41,1.74a6.3,6.3,0,0,0-1-.33,5.11,5.11,0,0,0-1.11-.16,3.36,3.36,0,0,0-2.37.69,3.66,3.66,0,0,0-.17,4.72,3.38,3.38,0,0,0,2.32.86,5.13,5.13,0,0,0,1.12-.08,6.52,6.52,0,0,0,1-.26l0.29,1.77a5.09,5.09,0,0,1-1.33.37,8.19,8.19,0,0,1-1.53.09,6.22,6.22,0,0,1-2.25-.46,4.52,4.52,0,0,1-1.59-1.11,4.4,4.4,0,0,1-.93-1.62,6.22,6.22,0,0,1,.14-4,4.39,4.39,0,0,1,1-1.55,4.55,4.55,0,0,1,1.67-1A6.22,6.22,0,0,1,332,301.87Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M339.36,302.65l-0.93,7.76,3.55,0.43-0.23,1.88-5.92-.71,1.16-9.64Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M352.87,310a5.6,5.6,0,0,1-.73,1.83,4.73,4.73,0,0,1-1.28,1.36,4.61,4.61,0,0,1-1.75.74,5.72,5.72,0,0,1-4.12-.88,4.54,4.54,0,0,1-1.3-1.39,4.6,4.6,0,0,1-.61-1.76,5.93,5.93,0,0,1,.81-3.81,4.62,4.62,0,0,1,1.27-1.36,4.56,4.56,0,0,1,1.75-.74,5.71,5.71,0,0,1,4.11.88,4.58,4.58,0,0,1,1.29,1.39,4.74,4.74,0,0,1,.62,1.76A5.61,5.61,0,0,1,352.87,310Zm-5.5,2a2.49,2.49,0,0,0,1.13,0,2.28,2.28,0,0,0,.93-0.47,2.8,2.8,0,0,0,.69-0.87,5,5,0,0,0,.52-2.47,2.79,2.79,0,0,0-.27-1.07,2.29,2.29,0,0,0-.66-0.8,2.77,2.77,0,0,0-2.17-.46,2.3,2.3,0,0,0-.92.47,2.78,2.78,0,0,0-.68.87,5.05,5.05,0,0,0-.53,2.47,2.8,2.8,0,0,0,.27,1.07,2.3,2.3,0,0,0,.65.8A2.48,2.48,0,0,0,347.36,312.06Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M361.53,307.71l2.27,0.74-1.93,5.9a3.44,3.44,0,0,1-1.87,2.23,5.17,5.17,0,0,1-5.75-1.88,3.44,3.44,0,0,1-.19-2.9l1.93-5.9,2.27,0.74-2,6a1.72,1.72,0,0,0,0,1.34,2.1,2.1,0,0,0,2.44.8,1.72,1.72,0,0,0,.81-1.07Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-2" d="M366.24,309.39l2.71,1.2a7.25,7.25,0,0,1,2,1.29,5.58,5.58,0,0,1,1.26,1.66,4.56,4.56,0,0,1-2.83,6.37,5.56,5.56,0,0,1-2.08.18,7.3,7.3,0,0,1-2.3-.62l-2.71-1.2Zm-0.34,8.4a4.08,4.08,0,0,0,1.29.35,3,3,0,0,0,1.18-.12,2.56,2.56,0,0,0,1-.58,3.44,3.44,0,0,0,1-2.28,2.55,2.55,0,0,0-.23-1.12,3,3,0,0,0-.7-1,4.08,4.08,0,0,0-1.13-.73l-0.65-.29-2.41,5.41Z" transform="translate(-25.28 -104.81)" />
		</g>
		<g class="selector" id="sites-applis">
			<path class="cls-4" d="M628.83,406.25A301.76,301.76,0,0,0,343,104.9v57.67c127.26,8.24,228.28,114.37,228.28,243.68S470.26,641.69,343,649.93V707.6A301.77,301.77,0,0,0,628.83,406.25Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M508.41,208.69l10.67-7.87,3.55,3.09L506.49,214.6,503,211.53l6.34-13-12,8.08-3.53-3.07L502.09,186l3.55,3.09-6.31,11.65,11.11-7.48,3.84,3.34Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M524.83,206.44l8.12,8.31-2.6,2.54-4.91-5-2.68,2.62,4.68,4.79-2.6,2.54-4.68-4.79-2.84,2.78,5,5.09-2.6,2.54-8.18-8.37Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M534.62,236.87a4.42,4.42,0,0,1-2.05,1,4.77,4.77,0,0,1-2.3-.13,8.14,8.14,0,0,1-2.42-1.23,14.11,14.11,0,0,1-2.41-2.23L521.62,230l14-12.26,4.4,5A10,10,0,0,1,541.6,225a6.31,6.31,0,0,1,.62,2.12A4.23,4.23,0,0,1,542,229a3.48,3.48,0,0,1-1,1.45,4.24,4.24,0,0,1-2.79,1.15,5.84,5.84,0,0,1-3.12-1,7.44,7.44,0,0,1,.76,1.68,5.15,5.15,0,0,1,.24,1.67,4,4,0,0,1-.37,1.57A4.22,4.22,0,0,1,534.62,236.87Zm-2.71-3.65a1.81,1.81,0,0,0,.66-1.71,4.36,4.36,0,0,0-1.14-2.14l-1-1.14-3.29,2.87,1.07,1.22a3.54,3.54,0,0,0,2.06,1.38A2,2,0,0,0,531.91,233.21Zm2-6.23a3.24,3.24,0,0,0,1.71,1.11,1.84,1.84,0,0,0,1.69-.47,1.66,1.66,0,0,0,.63-1.42,3,3,0,0,0-.83-1.71l-1-1.18L533,226Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M547.6,253.5a5.52,5.52,0,0,1-2.15,1,5.43,5.43,0,0,1-2.35,0,6.87,6.87,0,0,1-2.34-1.06,9,9,0,0,1-2.15-2.14,14.47,14.47,0,0,1-1.56-2.68,12.56,12.56,0,0,1-.9-2.88l3.37-1.33a13.43,13.43,0,0,0,.65,2.27,9.71,9.71,0,0,0,1.29,2.35,3.84,3.84,0,0,0,1.5,1.3,1.51,1.51,0,0,0,1.59-.23,1.68,1.68,0,0,0,.64-1.28,8.26,8.26,0,0,0-.44-2.65,10.7,10.7,0,0,1-.47-4.63,4.75,4.75,0,0,1,2-3,5,5,0,0,1,4.39-1,7.65,7.65,0,0,1,4.06,3.11,13,13,0,0,1,1.53,2.66,9.8,9.8,0,0,1,.73,2.52l-3.24,1.41a11.18,11.18,0,0,0-.65-2.18,9.76,9.76,0,0,0-1.18-2.1,3.43,3.43,0,0,0-1.27-1.13,1.25,1.25,0,0,0-1.33.2,1.82,1.82,0,0,0-.47.49,1.4,1.4,0,0,0-.19.64,3.76,3.76,0,0,0,.08,1q0.11,0.58.32,1.43,0.31,1.23.5,2.38a8,8,0,0,1,.07,2.17,5.31,5.31,0,0,1-.58,1.9A4.78,4.78,0,0,1,547.6,253.5Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M547,262.74l-2.56-3.81L560,248.54l2.56,3.81Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M571.82,267.32l-3.11,1.89-2.6-4.29-12.83,7.78-2.38-3.93L563.73,261l-2.6-4.28,3.11-1.89Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M573.27,270l5.45,10.26-3.22,1.71-3.29-6.21-3.31,1.76L572,283.4l-3.22,1.71-3.14-5.91-3.51,1.86,3.33,6.28L562.29,289l-5.49-10.34Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M573.73,300.43a5.52,5.52,0,0,1-2.3.52,5.44,5.44,0,0,1-2.3-.5,6.86,6.86,0,0,1-2.08-1.51,9,9,0,0,1-1.67-2.53,14.46,14.46,0,0,1-1-2.94,12.62,12.62,0,0,1-.3-3l3.57-.62a13.53,13.53,0,0,0,.17,2.35,9.7,9.7,0,0,0,.79,2.56,3.84,3.84,0,0,0,1.2,1.57,1.5,1.5,0,0,0,1.6.1,1.68,1.68,0,0,0,.88-1.13,8.21,8.21,0,0,0,.11-2.68,10.69,10.69,0,0,1,.48-4.63,4.74,4.74,0,0,1,2.56-2.57,5,5,0,0,1,4.51-.12,7.65,7.65,0,0,1,3.35,3.87,13.06,13.06,0,0,1,1,2.92,9.84,9.84,0,0,1,.2,2.62l-3.46.73a11.13,11.13,0,0,0-.2-2.27,9.69,9.69,0,0,0-.73-2.3,3.44,3.44,0,0,0-1-1.37,1.25,1.25,0,0,0-1.34-.07,1.82,1.82,0,0,0-.56.38,1.41,1.41,0,0,0-.31.59,3.74,3.74,0,0,0-.11,1q0,0.59,0,1.47,0.06,1.27,0,2.43a8,8,0,0,1-.37,2.14,5.33,5.33,0,0,1-1,1.75A4.77,4.77,0,0,1,573.73,300.43Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M561.1,529.8l2.07-3.91,8.31,0.27-9-5.37,2.11-4,15.83,9.95-1.94,3.67-12.59-.57,7.57,10.08-1.94,3.66-17.14-7.47,2.11-4,9.5,4.38Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M551,558.66a10.76,10.76,0,0,1-2.79-2.57,9.08,9.08,0,0,1-1.57-3.22,8.79,8.79,0,0,1-.14-3.64,10,10,0,0,1,1.54-3.81,10.1,10.1,0,0,1,2.84-3,8.77,8.77,0,0,1,3.38-1.39,8.89,8.89,0,0,1,3.58.11,11.39,11.39,0,0,1,6.29,4,8.92,8.92,0,0,1,1.58,3.21,8.77,8.77,0,0,1,.13,3.65,10.1,10.1,0,0,1-1.54,3.82,10,10,0,0,1-2.83,3,8.8,8.8,0,0,1-3.37,1.38,9.09,9.09,0,0,1-3.59-.09A10.75,10.75,0,0,1,551,558.66Zm0.09-11.26a4.81,4.81,0,0,0-.78,2,4.41,4.41,0,0,0,.21,2,5.38,5.38,0,0,0,1.09,1.82,9.69,9.69,0,0,0,4.07,2.62,5.35,5.35,0,0,0,2.11.24,4.4,4.4,0,0,0,1.89-.63,5.3,5.3,0,0,0,2.31-3.59,4.43,4.43,0,0,0-.21-2,5.35,5.35,0,0,0-1.09-1.82,9.68,9.68,0,0,0-4.07-2.62,5.34,5.34,0,0,0-2.11-.24,4.42,4.42,0,0,0-1.88.62A4.8,4.8,0,0,0,551.09,547.39Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M537.64,569.69a4.43,4.43,0,0,1-1.45-1.79,4.78,4.78,0,0,1-.35-2.28,8.17,8.17,0,0,1,.7-2.63,14.09,14.09,0,0,1,1.68-2.82l3.48-4.64,14.92,11.19-4,5.35a10,10,0,0,1-1.89,2,6.3,6.3,0,0,1-1.94,1,4.21,4.21,0,0,1-1.85.17,3.47,3.47,0,0,1-1.63-.68,4.23,4.23,0,0,1-1.71-2.49,5.84,5.84,0,0,1,.34-3.26,7.42,7.42,0,0,1-1.48,1.09,5.13,5.13,0,0,1-1.59.58,4,4,0,0,1-1.61,0A4.22,4.22,0,0,1,537.64,569.69Zm3-3.41a1.8,1.8,0,0,0,1.81.29,4.35,4.35,0,0,0,1.86-1.56l0.91-1.21-3.5-2.62-1,1.3a3.54,3.54,0,0,0-.92,2.3A2,2,0,0,0,540.65,566.28Zm6.5,0.64a3.24,3.24,0,0,0-.73,1.9,1.84,1.84,0,0,0,.81,1.55,1.66,1.66,0,0,0,1.52.32,3,3,0,0,0,1.5-1.17l0.94-1.25L548,565.84Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M528.75,572.11l2.93-3.54,14.37,11.88L543.12,584Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M537.24,590.75l-11.15-10-4.59,5.1-2.71-2.44,7.67-8.51,13.86,12.48Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M530.5,598.08l-8.23,8.2-2.57-2.58,5-5L522,596.08l-4.74,4.72-2.57-2.58,4.74-4.72-2.81-2.82-5,5L509,593.13l8.29-8.26Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M496.06,609.24l5.53-4.52-1.4-3.52,3.66-3L510.28,617l-3.9,3.19-17.18-10,3.67-3Zm6.92-1-3.71,3,6.82,4.59Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M484.21,629.48a5.45,5.45,0,0,1-1-2.53,5.81,5.81,0,0,1,.27-2.64,8.48,8.48,0,0,1,1.44-2.56,12.65,12.65,0,0,1,2.44-2.3l0.38-.28L484,614.07l3.71-2.71,11,15.06-4.34,3.17a13.79,13.79,0,0,1-2.85,1.63,7.74,7.74,0,0,1-2.73.63,5.21,5.21,0,0,1-2.47-.54A5.77,5.77,0,0,1,484.21,629.48Zm3.56-2.63q1.52,2.08,4.48-.08l0.66-.48L489.78,622l-0.61.45a4.49,4.49,0,0,0-1.81,2.22A2.36,2.36,0,0,0,487.76,626.85Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M470.67,638.23a5.45,5.45,0,0,1-.9-2.59,5.79,5.79,0,0,1,.43-2.62,8.5,8.5,0,0,1,1.59-2.47,12.62,12.62,0,0,1,2.58-2.16l0.4-.26-3.41-5.3,3.86-2.48L485.31,636l-4.52,2.91a13.71,13.71,0,0,1-2.94,1.46,7.72,7.72,0,0,1-2.77.46,5.2,5.2,0,0,1-2.43-.68A5.78,5.78,0,0,1,470.67,638.23Zm3.71-2.42q1.39,2.17,4.48.18l0.68-.44-2.86-4.45-0.64.41a4.49,4.49,0,0,0-1.94,2.11A2.37,2.37,0,0,0,474.38,635.81Z" transform="translate(-25.28 -104.81)" />
		</g>
		<g class="selector" id="pc-tablette-systemes">
			<path class="cls-5" d="M82.83,406.25c0-129.95,102-236.5,230.17-243.81V104.81A301.77,301.77,0,0,0,113.67,619.64,299.59,299.59,0,0,0,313,707.69V650.06C184.85,642.75,82.83,536.2,82.83,406.25Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M161.93,621.16a5.44,5.44,0,0,1,2.07-1.79,5.8,5.8,0,0,1,2.59-.58,8.48,8.48,0,0,1,2.89.56,12.65,12.65,0,0,1,3,1.59l0.39,0.28,3.65-5.13,3.74,2.66-10.81,15.2L165,630.81a13.85,13.85,0,0,1-2.45-2.19,7.72,7.72,0,0,1-1.46-2.39,5.19,5.19,0,0,1-.27-2.51A5.78,5.78,0,0,1,161.93,621.16Zm3.62,2.54q-1.49,2.1,1.5,4.22l0.66,0.47,3.07-4.31-0.62-.44a4.5,4.5,0,0,0-2.68-1A2.37,2.37,0,0,0,165.55,623.7Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M148.45,618.57a15.87,15.87,0,0,1-2.1-2.06,9.8,9.8,0,0,1-1.46-2.22l2.65-2.19a12.32,12.32,0,0,0,1.13,1.66,9.92,9.92,0,0,0,1.51,1.54,6.48,6.48,0,0,0,4.44,1.68,7,7,0,0,0,5.71-7,6.49,6.49,0,0,0-2.56-4,9.94,9.94,0,0,0-1.81-1.17,12.39,12.39,0,0,0-1.85-.76l1.6-3a9.77,9.77,0,0,1,2.47,1,15.7,15.7,0,0,1,2.45,1.63,11.93,11.93,0,0,1,2.91,3.3,8.73,8.73,0,0,1,1.16,3.54,8.44,8.44,0,0,1-.45,3.57,11.93,11.93,0,0,1-4.87,6,8.46,8.46,0,0,1-3.4,1.17,8.74,8.74,0,0,1-3.71-.41A12,12,0,0,1,148.45,618.57Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M91.94,554.91l3-2.06L97.78,557l12.37-8.5,2.6,3.78-12.37,8.5,2.84,4.13-3,2.06Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M97.89,535.75l3.67,6.12,3.69-.87,2.44,4.06L88.12,548.7l-2.59-4.32,12.42-15.55,2.43,4.06Zm-0.05,7-2.46-4.11-5.52,6.09Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M86.21,517.31a4.43,4.43,0,0,1,2.25-.52,4.77,4.77,0,0,1,2.2.68,8.15,8.15,0,0,1,2.05,1.78A14.18,14.18,0,0,1,94.51,522l2.63,5.16-16.61,8.48-3-6a9.93,9.93,0,0,1-.93-2.56,6.29,6.29,0,0,1-.08-2.2,4.24,4.24,0,0,1,.66-1.74A3.49,3.49,0,0,1,78.46,522a4.22,4.22,0,0,1,3-.44,5.84,5.84,0,0,1,2.78,1.74,7.43,7.43,0,0,1-.33-1.81,5.16,5.16,0,0,1,.18-1.68,4,4,0,0,1,.74-1.43A4.24,4.24,0,0,1,86.21,517.31Zm-1.69,9.76a3.24,3.24,0,0,0-1.38-1.49,1.84,1.84,0,0,0-1.75,0,1.66,1.66,0,0,0-1,1.22,3,3,0,0,0,.39,1.86l0.71,1.4,3.59-1.83ZM88,521.52A1.81,1.81,0,0,0,86.9,523a4.35,4.35,0,0,0,.59,2.36l0.69,1.35,3.89-2-0.74-1.45a3.53,3.53,0,0,0-1.67-1.84A2,2,0,0,0,88,521.52Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M71,516l13.74-6L82,503.64l3.33-1.46,4.61,10.48-17.07,7.5Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M67.06,506.79L63,495.91l3.41-1.28,2.47,6.58,3.51-1.32L70,493.62l3.41-1.28,2.35,6.26,3.72-1.4L77,490.55l3.41-1.28,4.12,11Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M57.73,479.48l3.48-1.08,1.49,4.78L77,478.72l1.37,4.38-14.33,4.46,1.49,4.79-3.48,1.08Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M66.69,461.52a5.52,5.52,0,0,1,2.36-.08,5.42,5.42,0,0,1,2.16.92A6.87,6.87,0,0,1,73,464.22a9,9,0,0,1,1.17,2.8,14.5,14.5,0,0,1,.42,3.07,12.57,12.57,0,0,1-.26,3l-3.62,0A13.52,13.52,0,0,0,71,470.7a9.71,9.71,0,0,0-.3-2.67,3.84,3.84,0,0,0-.89-1.77,1.5,1.5,0,0,0-1.55-.39,1.67,1.67,0,0,0-1.08.95,8.28,8.28,0,0,0-.6,2.62,10.7,10.7,0,0,1-1.33,4.46,4.75,4.75,0,0,1-3,2,5,5,0,0,1-4.45-.72,7.64,7.64,0,0,1-2.58-4.42,13,13,0,0,1-.4-3,9.81,9.81,0,0,1,.29-2.61l3.53-.07a11.2,11.2,0,0,0-.23,2.26,9.69,9.69,0,0,0,.29,2.39,3.43,3.43,0,0,0,.74,1.53,1.25,1.25,0,0,0,1.31.32,1.8,1.8,0,0,0,.63-0.27,1.4,1.4,0,0,0,.42-0.52,3.72,3.72,0,0,0,.29-0.92q0.12-.58.25-1.45,0.18-1.26.44-2.39a8,8,0,0,1,.76-2,5.31,5.31,0,0,1,1.26-1.54A4.77,4.77,0,0,1,66.69,461.52Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M49,370.18a15.7,15.7,0,0,1,.6-2.88,9.8,9.8,0,0,1,1.08-2.43l3.27,1a12.25,12.25,0,0,0-.78,1.84,9.94,9.94,0,0,0-.48,2.1,6.49,6.49,0,0,0,1,4.65A5.8,5.8,0,0,0,58,376.65a5.8,5.8,0,0,0,4.68-1.14,6.48,6.48,0,0,0,2-4.31,10,10,0,0,0,0-2.16,12.42,12.42,0,0,0-.36-2l3.42-.29a9.77,9.77,0,0,1,.52,2.61,15.77,15.77,0,0,1,0,2.94A11.93,11.93,0,0,1,67,376.56a8.72,8.72,0,0,1-2.35,2.89A8.47,8.47,0,0,1,61.35,381a11.92,11.92,0,0,1-7.68-.86,8.44,8.44,0,0,1-2.82-2.23,8.74,8.74,0,0,1-1.66-3.34A11.93,11.93,0,0,1,49,370.18Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M67.36,352.15l-1.29,7L69.41,361l-0.86,4.65L51.47,355.4l0.91-5L72,347l-0.86,4.65Zm-4.66,5.21,0.87-4.71-8.17.92Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M70.09,332a5.52,5.52,0,0,1,2.12,1,5.43,5.43,0,0,1,1.47,1.83,6.86,6.86,0,0,1,.67,2.48,9,9,0,0,1-.29,3,14.42,14.42,0,0,1-1.08,2.9,12.62,12.62,0,0,1-1.65,2.53L68.18,344a13.48,13.48,0,0,0,1.34-1.94,9.7,9.7,0,0,0,1-2.49,3.85,3.85,0,0,0,.05-2,1.5,1.5,0,0,0-1.19-1.08,1.67,1.67,0,0,0-1.4.33,8.29,8.29,0,0,0-1.77,2,10.67,10.67,0,0,1-3.27,3.3,4.74,4.74,0,0,1-3.61.39,5,5,0,0,1-3.58-2.74,7.64,7.64,0,0,1-.18-5.11,13,13,0,0,1,1.09-2.87,9.82,9.82,0,0,1,1.49-2.17l3.15,1.6A11.14,11.14,0,0,0,60,333.16a9.68,9.68,0,0,0-.87,2.25,3.42,3.42,0,0,0-.07,1.7,1.25,1.25,0,0,0,1,.9,1.8,1.8,0,0,0,.68.06,1.4,1.4,0,0,0,.61-0.26,3.73,3.73,0,0,0,.69-0.68q0.38-.45.9-1.16,0.75-1,1.52-1.9a8,8,0,0,1,1.63-1.44,5.33,5.33,0,0,1,1.84-.76A4.77,4.77,0,0,1,70.09,332Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M79.59,320.24l-7.22-2.33-2.1,6.5,7.22,2.33-1.41,4.37-17.75-5.73L59.75,321l7,2.25,2.1-6.5-7-2.25,1.41-4.37L81,315.87Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M78.16,289.27a5.31,5.31,0,0,1,2.59,2.26,6.48,6.48,0,0,1,.8,3.53l9.16-2.73-2.24,5.19-8,2.15-0.33.77L86.12,303l-1.82,4.22-17.13-7.38,2.35-5.45a13.58,13.58,0,0,1,1.61-2.81,8.09,8.09,0,0,1,2-1.93A5.07,5.07,0,0,1,78.16,289.27Zm-1.67,4.1a2,2,0,0,0-2.1.16,5.25,5.25,0,0,0-1.64,2.28L72.23,297,77,299.1l0.44-1a5,5,0,0,0,.47-2.94A2.36,2.36,0,0,0,76.49,293.37Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M74.76,282.71L80,272.34,83.26,274l-3.18,6.27,3.35,1.7,3-6,3.25,1.65-3,6,3.55,1.8,3.21-6.34,3.25,1.65L91.4,291.14Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M86.47,260.39a14.53,14.53,0,0,1,4.09-4.67L93.22,258a13.6,13.6,0,0,0-3.39,3.85,8.16,8.16,0,0,0-1,2.35,4.89,4.89,0,0,0,0,2.23,5.09,5.09,0,0,0,1.06,2.06,8.33,8.33,0,0,0,2.22,1.82,5.86,5.86,0,0,0,4.56.93,6.2,6.2,0,0,0,3.57-3.11,7.38,7.38,0,0,0,.54-1.11,5.69,5.69,0,0,0,.29-1l-4.48-2.65,2.1-3.54,6.62,3.92a12.81,12.81,0,0,1-.72,3.13,19.33,19.33,0,0,1-1.65,3.53,12,12,0,0,1-2.73,3.26A9,9,0,0,1,97,275.44a8.25,8.25,0,0,1-3.55.19,10.25,10.25,0,0,1-3.64-1.36,11.55,11.55,0,0,1-3.16-2.67A8.56,8.56,0,0,1,85,268.29a9,9,0,0,1-.09-3.8A12.32,12.32,0,0,1,86.47,260.39Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M110.63,257.81l-2.55,3.82L92.57,251.28l2.55-3.82Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M115.35,241.69a5.53,5.53,0,0,1,1.57,1.76,5.42,5.42,0,0,1,.68,2.25,6.86,6.86,0,0,1-.31,2.55,9,9,0,0,1-1.4,2.69,14.5,14.5,0,0,1-2.09,2.29,12.56,12.56,0,0,1-2.48,1.72L109,252.14a13.4,13.4,0,0,0,2-1.3,9.71,9.71,0,0,0,1.85-1.94,3.85,3.85,0,0,0,.79-1.81,1.5,1.5,0,0,0-.69-1.45,1.68,1.68,0,0,0-1.42-.22,8.25,8.25,0,0,0-2.4,1.21,10.7,10.7,0,0,1-4.27,1.83,4.75,4.75,0,0,1-3.49-1,5,5,0,0,1-2.29-3.88,7.65,7.65,0,0,1,1.75-4.81,13,13,0,0,1,2.09-2.25,9.81,9.81,0,0,1,2.19-1.45l2.32,2.67a11.21,11.21,0,0,0-1.89,1.27,9.73,9.73,0,0,0-1.65,1.76,3.41,3.41,0,0,0-.7,1.55,1.25,1.25,0,0,0,.59,1.21,1.8,1.8,0,0,0,.61.31,1.39,1.39,0,0,0,.67,0,3.73,3.73,0,0,0,.9-0.37q0.52-.28,1.27-0.74,1.08-.67,2.12-1.19a8,8,0,0,1,2.05-.72,5.33,5.33,0,0,1,2,0A4.78,4.78,0,0,1,115.35,241.69Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M115.31,221.1l2.81,2.32-3.19,3.87,11.58,9.55-2.92,3.54L112,230.82l-3.19,3.87L106,232.37Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M117.33,218.86l7.89-8.53,2.67,2.47L123.12,218l2.75,2.55,4.54-4.91,2.67,2.47L128.54,223l2.92,2.7,4.83-5.22,2.67,2.47L131,231.52Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M142.36,201.92a5.31,5.31,0,0,1,1.5,3.1,6.47,6.47,0,0,1-.66,3.56l9.5,1.1-4.1,3.89-8.17-1.16-0.61.58,4.45,4.69-3.33,3.16L128.1,207.32l4.3-4.09a13.49,13.49,0,0,1,2.58-2,8.07,8.07,0,0,1,2.62-1A5.06,5.06,0,0,1,142.36,201.92ZM139.21,205a2,2,0,0,0-2-.68,5.26,5.26,0,0,0-2.4,1.45l-1,.91,3.61,3.8,0.81-.77a5,5,0,0,0,1.59-2.51A2.36,2.36,0,0,0,139.21,205Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M164.28,192.9a5.52,5.52,0,0,1,1.07,2.11,5.4,5.4,0,0,1,.08,2.35,6.85,6.85,0,0,1-1,2.39,9,9,0,0,1-2,2.24,14.41,14.41,0,0,1-2.61,1.67,12.57,12.57,0,0,1-2.84,1l-1.48-3.3a13.48,13.48,0,0,0,2.24-.75,9.64,9.64,0,0,0,2.29-1.4,3.82,3.82,0,0,0,1.23-1.55,1.5,1.5,0,0,0-.3-1.58,1.67,1.67,0,0,0-1.31-.58,8.26,8.26,0,0,0-2.63.56,10.69,10.69,0,0,1-4.6.68,4.75,4.75,0,0,1-3.12-1.85,5,5,0,0,1-1.22-4.34,7.64,7.64,0,0,1,2.92-4.2,13,13,0,0,1,2.59-1.65,9.89,9.89,0,0,1,2.49-.84l1.55,3.17a11.24,11.24,0,0,0-2.15.75,9.71,9.71,0,0,0-2,1.28,3.44,3.44,0,0,0-1.08,1.32,1.25,1.25,0,0,0,.26,1.32,1.81,1.81,0,0,0,.51.45,1.39,1.39,0,0,0,.65.16,3.73,3.73,0,0,0,1-.13q0.57-.14,1.42-0.39,1.21-.37,2.35-0.61a8,8,0,0,1,2.17-.17,5.34,5.34,0,0,1,1.93.49A4.77,4.77,0,0,1,164.28,192.9Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M172.85,185.21l3.94,5.51-3.74,2.67-3.94-5.51-11.89-6.1,4-2.85,7.43,4.33L167,174.83l4-2.85Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M190.35,175.25a5.5,5.5,0,0,1,.81,2.22,5.41,5.41,0,0,1-.2,2.34,6.83,6.83,0,0,1-1.23,2.26,9,9,0,0,1-2.29,2,14.4,14.4,0,0,1-2.79,1.35,12.53,12.53,0,0,1-2.94.69l-1.08-3.46a13.5,13.5,0,0,0,2.31-.48,9.67,9.67,0,0,0,2.44-1.12,3.84,3.84,0,0,0,1.41-1.4,1.5,1.5,0,0,0-.11-1.6,1.67,1.67,0,0,0-1.23-.73,8.23,8.23,0,0,0-2.67.24,10.69,10.69,0,0,1-4.65.13,4.74,4.74,0,0,1-2.88-2.21,5,5,0,0,1-.7-4.46,7.64,7.64,0,0,1,3.4-3.82,13,13,0,0,1,2.77-1.33,9.82,9.82,0,0,1,2.57-.54l1.17,3.33a11.16,11.16,0,0,0-2.22.49,9.75,9.75,0,0,0-2.18,1,3.43,3.43,0,0,0-1.22,1.18,1.25,1.25,0,0,0,.1,1.34,1.82,1.82,0,0,0,.45.51,1.4,1.4,0,0,0,.63.23,3.75,3.75,0,0,0,1,0c0.39,0,.87-0.12,1.45-0.22q1.25-.22,2.41-0.32A8,8,0,0,1,187,173a5.32,5.32,0,0,1,1.86.72A4.76,4.76,0,0,1,190.35,175.25Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M197.89,156.11l1.75,3.19-4.39,2.42,7.24,13.15-4,2.21-7.24-13.15-4.39,2.42-1.75-3.19Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M200.6,154.77l10.49-5,1.57,3.29-6.34,3,1.62,3.39,6-2.88,1.57,3.28-6,2.88,1.71,3.59,6.42-3.06,1.57,3.28-10.56,5Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M232.3,158l-4.12,1.6-6.1-5.65,3.26,9.94-4.2,1.63-5.45-17.89,3.86-1.5,9.14,8.67,0.88-12.57,3.86-1.5,8.06,16.87-4.2,1.63L233,149.72Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M254.15,148a5.53,5.53,0,0,1,.21,2.35,5.43,5.43,0,0,1-.8,2.21,6.89,6.89,0,0,1-1.77,1.86,9,9,0,0,1-2.73,1.32,14.45,14.45,0,0,1-3,.58,12.5,12.5,0,0,1-3-.1l-0.14-3.62a13.43,13.43,0,0,0,2.36.14,9.67,9.67,0,0,0,2.65-.44,3.83,3.83,0,0,0,1.72-1,1.5,1.5,0,0,0,.31-1.57,1.68,1.68,0,0,0-1-1,8.26,8.26,0,0,0-2.65-.47,10.68,10.68,0,0,1-4.52-1.09,4.75,4.75,0,0,1-2.2-2.89,5,5,0,0,1,.49-4.48,7.64,7.64,0,0,1,4.28-2.8,13.07,13.07,0,0,1,3-.56,9.8,9.8,0,0,1,2.62.15l0.26,3.52a11.19,11.19,0,0,0-2.27-.11,9.75,9.75,0,0,0-2.38.42,3.42,3.42,0,0,0-1.49.82,1.25,1.25,0,0,0-.26,1.32,1.82,1.82,0,0,0,.3.61,1.4,1.4,0,0,0,.54.39,3.74,3.74,0,0,0,.94.24q0.58,0.09,1.46.17,1.26,0.11,2.41.32a8,8,0,0,1,2.07.65,5.35,5.35,0,0,1,1.6,1.18A4.78,4.78,0,0,1,254.15,148Z" transform="translate(-25.28 -104.81)" />
		</g>
		<g class="selector" id="store-locator">
			<path class="cls-7" d="M421.78,207.53A219.57,219.57,0,0,0,329.14,186v89.57a130.15,130.15,0,0,1,53.44,12.33Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M357.05,244.4a2.29,2.29,0,0,1,.88.43,2.25,2.25,0,0,1,.61.76,2.87,2.87,0,0,1,.28,1,3.74,3.74,0,0,1-.12,1.25,6,6,0,0,1-.45,1.2,5.2,5.2,0,0,1-.68,1l-1.31-.73a5.5,5.5,0,0,0,.56-0.8,4,4,0,0,0,.41-1,1.58,1.58,0,0,0,0-.82,0.62,0.62,0,0,0-.49-0.45,0.69,0.69,0,0,0-.58.13,3.44,3.44,0,0,0-.73.84,4.41,4.41,0,0,1-1.36,1.37,2,2,0,0,1-1.49.16,2.07,2.07,0,0,1-1.48-1.13,3.17,3.17,0,0,1-.07-2.12,5.44,5.44,0,0,1,.45-1.19,4.09,4.09,0,0,1,.62-0.9l1.3,0.66a4.68,4.68,0,0,0-.53.78,4,4,0,0,0-.36.93,1.41,1.41,0,0,0,0,.7,0.52,0.52,0,0,0,.41.37,0.74,0.74,0,0,0,.28,0,0.59,0.59,0,0,0,.26-0.11,1.58,1.58,0,0,0,.29-0.28l0.37-.48q0.31-.42.63-0.78a3.32,3.32,0,0,1,.68-0.59,2.19,2.19,0,0,1,.76-0.31A2,2,0,0,1,357.05,244.4Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M353.41,236.74l1.46,0.37-0.51,2,6,1.53-0.47,1.84-6-1.53-0.51,2-1.46-.37Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M359.18,229.68a4.44,4.44,0,0,1,1.43.64,3.75,3.75,0,0,1,1,1.06,3.63,3.63,0,0,1,.53,1.41,4.55,4.55,0,0,1-.82,3.24,3.61,3.61,0,0,1-1.14,1,3.67,3.67,0,0,1-1.42.43,4.73,4.73,0,0,1-3-.76,3.69,3.69,0,0,1-1-1.05,3.62,3.62,0,0,1-.53-1.41,4.55,4.55,0,0,1,.82-3.24,3.64,3.64,0,0,1,1.14-1,3.77,3.77,0,0,1,1.42-.44A4.44,4.44,0,0,1,359.18,229.68Zm1.45,4.43a2,2,0,0,0,0-.9,1.81,1.81,0,0,0-.34-0.75,2.2,2.2,0,0,0-.67-0.57,4,4,0,0,0-1.94-.49,2.19,2.19,0,0,0-.86.18,1.82,1.82,0,0,0-.66.5,2.19,2.19,0,0,0-.43,1.71,1.84,1.84,0,0,0,.34.74,2.2,2.2,0,0,0,.67.57,4,4,0,0,0,1.94.49,2.22,2.22,0,0,0,.86-0.18,1.82,1.82,0,0,0,.66-0.49A2,2,0,0,0,360.63,234.11Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M359.53,222.59a2.19,2.19,0,0,1,1.21.75,2.68,2.68,0,0,1,.56,1.39l3.57-1.71-0.58,2.27-3.11,1.4-0.09.34,2.59,0.66-0.47,1.84-7.48-1.9,0.6-2.38a5.58,5.58,0,0,1,.47-1.25,3.37,3.37,0,0,1,.7-0.92A2.1,2.1,0,0,1,359.53,222.59Zm-0.42,1.78a0.84,0.84,0,0,0-.85.2,2.17,2.17,0,0,0-.52,1l-0.13.53,2.1,0.53,0.11-.45a2,2,0,0,0,0-1.23A1,1,0,0,0,359.11,224.37Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M357.55,220.45l1.18-4.66,1.46,0.37L359.47,219l1.5,0.38,0.68-2.68,1.46,0.37-0.68,2.68,1.6,0.4,0.72-2.85,1.46,0.37L365,222.34Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M362.43,257.76l6,1.53,0.7-2.75,1.46,0.37-1.16,4.59L362,259.6Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M368.92,247.85a4.44,4.44,0,0,1,1.43.64,3.74,3.74,0,0,1,1,1.06,3.64,3.64,0,0,1,.53,1.41,4.12,4.12,0,0,1-.1,1.7,4.17,4.17,0,0,1-.72,1.54,3.62,3.62,0,0,1-1.14,1,3.67,3.67,0,0,1-1.42.43,4.72,4.72,0,0,1-3-.76,3.69,3.69,0,0,1-1-1.05,3.64,3.64,0,0,1-.53-1.41,4.18,4.18,0,0,1,.1-1.7,4.14,4.14,0,0,1,.72-1.54,3.65,3.65,0,0,1,1.14-1,3.76,3.76,0,0,1,1.42-.43A4.44,4.44,0,0,1,368.92,247.85Zm1.45,4.43a2,2,0,0,0,0-.9,1.82,1.82,0,0,0-.34-0.75,2.24,2.24,0,0,0-.67-0.57,4,4,0,0,0-1.94-.49,2.22,2.22,0,0,0-.86.18,1.84,1.84,0,0,0-.66.5,2.2,2.2,0,0,0-.43,1.71,1.85,1.85,0,0,0,.34.74,2.2,2.2,0,0,0,.67.57,4,4,0,0,0,1.94.49,2.22,2.22,0,0,0,.86-0.18,1.84,1.84,0,0,0,.66-0.49A2,2,0,0,0,370.37,252.28Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M366.27,242a6.47,6.47,0,0,1,.41-1.15,4.06,4.06,0,0,1,.58-0.93l1.28,0.61a5.08,5.08,0,0,0-.42.71,4.13,4.13,0,0,0-.31.84,2.68,2.68,0,0,0,.14,2,2.91,2.91,0,0,0,3.63.92,2.68,2.68,0,0,0,1.06-1.66,4.07,4.07,0,0,0,.12-0.88,5.08,5.08,0,0,0,0-.83l1.42,0.07a4.09,4.09,0,0,1,.07,1.1A6.56,6.56,0,0,1,374,244a5,5,0,0,1-.73,1.67,3.62,3.62,0,0,1-1.13,1.05,3.5,3.5,0,0,1-1.42.45,4.93,4.93,0,0,1-3.1-.79,3.49,3.49,0,0,1-1-1.07,3.6,3.6,0,0,1-.49-1.46A4.93,4.93,0,0,1,366.27,242Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M374.48,235.54l-0.73,2.86,1.33,0.83-0.48,1.9-6.77-4.68,0.51-2,8.19-.89-0.48,1.9Zm-2.07,2,0.49-1.92-3.4.15Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M370.48,226l1.46,0.37-0.51,2,6,1.53L377,231.74l-6-1.53-0.51,2-1.46-.37Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M376.25,218.93a4.44,4.44,0,0,1,1.43.64,3.74,3.74,0,0,1,1,1.06,3.64,3.64,0,0,1,.53,1.41,4.12,4.12,0,0,1-.1,1.7,4.17,4.17,0,0,1-.72,1.54,3.62,3.62,0,0,1-1.14,1,3.67,3.67,0,0,1-1.42.43,4.72,4.72,0,0,1-3-.76,3.69,3.69,0,0,1-1-1.05,3.64,3.64,0,0,1-.53-1.41,4.18,4.18,0,0,1,.1-1.7,4.14,4.14,0,0,1,.72-1.54,3.65,3.65,0,0,1,1.14-1,3.76,3.76,0,0,1,1.42-.43A4.44,4.44,0,0,1,376.25,218.93Zm1.45,4.43a2,2,0,0,0,0-.9,1.82,1.82,0,0,0-.34-0.75,2.24,2.24,0,0,0-.67-0.57,4,4,0,0,0-1.94-.49,2.22,2.22,0,0,0-.86.18,1.84,1.84,0,0,0-.66.5,2.2,2.2,0,0,0-.43,1.71,1.85,1.85,0,0,0,.34.74,2.2,2.2,0,0,0,.67.57,4,4,0,0,0,1.94.49,2.22,2.22,0,0,0,.86-0.18,1.84,1.84,0,0,0,.66-0.49A2,2,0,0,0,377.7,223.36Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M376.59,211.84a2.2,2.2,0,0,1,1.21.75,2.68,2.68,0,0,1,.56,1.39l3.57-1.71-0.58,2.27-3.11,1.4-0.09.34,2.59,0.66-0.47,1.84-7.48-1.9,0.6-2.38a5.57,5.57,0,0,1,.47-1.25,3.35,3.35,0,0,1,.7-0.92A2.09,2.09,0,0,1,376.59,211.84Zm-0.42,1.78a0.84,0.84,0,0,0-.85.2,2.17,2.17,0,0,0-.52,1l-0.13.53,2.1,0.53,0.11-.45a2.05,2.05,0,0,0,0-1.23A1,1,0,0,0,376.18,213.62Z" transform="translate(-25.28 -104.81)" />

		</g>
		<g class="selector" id="click-collect">
			<path class="cls-7" d="M497.31,547a220.52,220.52,0,0,0,42.86-83.45l-87.24-21.88A130.36,130.36,0,0,1,428.22,489Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M476.33,469.8a6.47,6.47,0,0,1,1,.66,4,4,0,0,1,.78.77l-0.88,1.11a5.09,5.09,0,0,0-.6-0.57,4.12,4.12,0,0,0-.74-0.49A2.68,2.68,0,0,0,474,471a2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.06,4.06,0,0,0,.83.32,5.09,5.09,0,0,0,.81.15l-0.39,1.37a4.07,4.07,0,0,1-1.08-.18,6.55,6.55,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.61,3.61,0,0,1-.77-1.33,3.5,3.5,0,0,1-.13-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.53-.15A4.94,4.94,0,0,1,476.33,469.8Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M481.05,472.38l-2.83,5.53,2.53,1.3-0.69,1.34-4.22-2.16,3.52-6.87Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M482.66,481.87L481,481l3.52-6.87,1.69,0.87Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M491,477.32a6.47,6.47,0,0,1,1,.66,4.06,4.06,0,0,1,.78.77l-0.88,1.11a5.08,5.08,0,0,0-.6-0.57,4.13,4.13,0,0,0-.74-0.49,2.68,2.68,0,0,0-1.94-.3,2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.07,4.07,0,0,0,.83.32,5.08,5.08,0,0,0,.81.15l-0.39,1.37a4.09,4.09,0,0,1-1.08-.18,6.56,6.56,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.62,3.62,0,0,1-.77-1.33,3.5,3.5,0,0,1-.12-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.54-.15A4.93,4.93,0,0,1,491,477.32Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M490.51,485.9L494,479l1.69,0.87L494,483.19l3.85-2.18,1.91,1-4.06,2.2,0.7,4.75-2-1-0.48-4.49-1.72,3.36Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M457.08,484.53q-0.61-1-1.07-1.77a4,4,0,0,1-1.63.13,4.48,4.48,0,0,1-1.37-.44,3.94,3.94,0,0,1-.89-0.6,2.51,2.51,0,0,1-.55-0.71,1.68,1.68,0,0,1-.19-0.79,1.89,1.89,0,0,1,.23-0.85A1.6,1.6,0,0,1,452,479a2.06,2.06,0,0,1,.52-0.32,3.52,3.52,0,0,1,.63-0.2l0.7-.13a4.74,4.74,0,0,1-.31-1.17,1.84,1.84,0,0,1,.21-1.07,2,2,0,0,1,.51-0.64,1.79,1.79,0,0,1,.72-0.36,2.22,2.22,0,0,1,.88,0,3.38,3.38,0,0,1,1,.34,3.13,3.13,0,0,1,.76.53,2.18,2.18,0,0,1,.47.64,1.68,1.68,0,0,1,.16.71,1.6,1.6,0,0,1-.19.73,1.75,1.75,0,0,1-.38.51,2.32,2.32,0,0,1-.51.35,3.47,3.47,0,0,1-.59.24l-0.63.18,0.39,0.8,0.4,0.76q0.36-.19.76-0.46l0.85-.58,0.88,1.23q-0.48.35-.91,0.63a9.37,9.37,0,0,1-.84.49l0.92,1.55Zm-3.28-3.22a2.12,2.12,0,0,0,1.51.15c-0.14-.27-0.3-0.57-0.46-0.9s-0.3-.63-0.43-0.92a3.67,3.67,0,0,0-.68.19,0.78,0.78,0,0,0-.42.38,0.73,0.73,0,0,0-.06.59A1,1,0,0,0,453.79,481.31Zm1.47-4.46a1,1,0,0,0-.1.56,2.4,2.4,0,0,0,.17.66,5.48,5.48,0,0,0,.81-0.21,0.84,0.84,0,0,0,.48-0.42,0.53,0.53,0,0,0,0-.48,0.8,0.8,0,0,0-.38-0.38,0.81,0.81,0,0,0-.57-0.09A0.64,0.64,0,0,0,455.26,476.86Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M467.87,481.08a6.47,6.47,0,0,1,1,.66,4.06,4.06,0,0,1,.78.77l-0.88,1.11a5.08,5.08,0,0,0-.6-0.57,4.13,4.13,0,0,0-.74-0.49,2.68,2.68,0,0,0-1.94-.3,2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.07,4.07,0,0,0,.83.32,5.08,5.08,0,0,0,.81.15l-0.39,1.37a4.09,4.09,0,0,1-1.08-.18,6.56,6.56,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.62,3.62,0,0,1-.77-1.33,3.5,3.5,0,0,1-.12-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.54-.15A4.93,4.93,0,0,1,467.87,481.08Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M475.77,489.63a4.44,4.44,0,0,1-.94,1.25,3.75,3.75,0,0,1-1.27.78,3.63,3.63,0,0,1-1.49.21,4.55,4.55,0,0,1-3-1.53,3.61,3.61,0,0,1-.71-1.33,3.67,3.67,0,0,1-.1-1.48,4.73,4.73,0,0,1,1.41-2.75A3.69,3.69,0,0,1,471,484a3.62,3.62,0,0,1,1.5-.2,4.55,4.55,0,0,1,3,1.53,3.64,3.64,0,0,1,.7,1.33,3.77,3.77,0,0,1,.11,1.48A4.44,4.44,0,0,1,475.77,489.63Zm-4.64.42a2,2,0,0,0,.87.24,1.81,1.81,0,0,0,.81-0.16,2.2,2.2,0,0,0,.7-0.52,4,4,0,0,0,.91-1.78,2.19,2.19,0,0,0,0-.88,1.82,1.82,0,0,0-.34-0.75,2.19,2.19,0,0,0-1.57-.81,1.84,1.84,0,0,0-.8.17,2.2,2.2,0,0,0-.7.52,4,4,0,0,0-.91,1.78,2.22,2.22,0,0,0,0,.88,1.82,1.82,0,0,0,.33.75A2,2,0,0,0,471.13,490Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M480.25,487.59l-2.83,5.53,2.53,1.3-0.69,1.34L475,493.58l3.52-6.87Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M485.32,490.19l-2.83,5.53L485,497l-0.69,1.34-4.22-2.16,3.52-6.87Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M488.7,491.92l4.28,2.19-0.69,1.34-2.59-1.33L489,495.51l2.46,1.26-0.69,1.34-2.46-1.26-0.75,1.46,2.62,1.34L489.49,501l-4.31-2.21Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M497.61,496.33a6.47,6.47,0,0,1,1,.66,4.06,4.06,0,0,1,.78.77l-0.88,1.11a5.08,5.08,0,0,0-.6-0.57,4.13,4.13,0,0,0-.74-0.49,2.68,2.68,0,0,0-1.94-.3,2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.07,4.07,0,0,0,.83.32,5.08,5.08,0,0,0,.81.15l-0.39,1.37a4.09,4.09,0,0,1-1.08-.18,6.56,6.56,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.62,3.62,0,0,1-.77-1.33,3.5,3.5,0,0,1-.12-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.54-.15A4.93,4.93,0,0,1,497.61,496.33Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M505.49,500.53l-0.69,1.34-1.85-.95-2.83,5.52-1.69-.87,2.83-5.52-1.84-.95,0.69-1.34Z" transform="translate(-25.28 -104.81)" />
		</g>

		<g class="selector" id="pass-tourisme">
			<path class="cls-6" d="M215.29,474.1l-78.71,45.44A222.62,222.62,0,0,0,214,596.91l45.47-78.76A131.4,131.4,0,0,1,215.29,474.1Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M187.62,531.45a2.25,2.25,0,0,1,.59,1,2.39,2.39,0,0,1,.06,1.1,3.48,3.48,0,0,1-.43,1.14,5.27,5.27,0,0,1-.85,1.1l-0.14.14,1.84,1.84-1.34,1.34-5.46-5.46,1.57-1.57a5.75,5.75,0,0,1,1.06-.85,3.18,3.18,0,0,1,1.08-.43,2.15,2.15,0,0,1,1,.06A2.39,2.39,0,0,1,187.62,531.45Zm-1.29,1.3q-0.75-.75-1.83.32l-0.24.24,1.55,1.55,0.22-.22a1.87,1.87,0,0,0,.6-1A1,1,0,0,0,186.34,532.75Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M193.87,530.45l-2.09,2.09,0.72,1.39-1.38,1.38-3.42-7.49,1.47-1.47,7.49,3.42-1.38,1.38Zm-2.81.68,1.4-1.4-3-1.61Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M199.23,523.94a2.28,2.28,0,0,1,.53.82,2.26,2.26,0,0,1,.14,1,2.86,2.86,0,0,1-.29,1,3.71,3.71,0,0,1-.74,1,5.91,5.91,0,0,1-1,.8,5.24,5.24,0,0,1-1.12.55L196,527.81a5.74,5.74,0,0,0,.89-0.41,4,4,0,0,0,.88-0.68,1.59,1.59,0,0,0,.44-0.69,0.62,0.62,0,0,0-.19-0.63,0.69,0.69,0,0,0-.56-0.18,3.39,3.39,0,0,0-1.06.34,4.43,4.43,0,0,1-1.86.48,2,2,0,0,1-1.36-.63,2.07,2.07,0,0,1-.69-1.73,3.17,3.17,0,0,1,1-1.86,5.33,5.33,0,0,1,1-.79,4.12,4.12,0,0,1,1-.46l0.78,1.24a4.61,4.61,0,0,0-.85.4,4,4,0,0,0-.79.62,1.41,1.41,0,0,0-.39.59,0.52,0.52,0,0,0,.16.53,0.73,0.73,0,0,0,.23.16,0.58,0.58,0,0,0,.27,0,1.53,1.53,0,0,0,.39-0.09l0.57-.22q0.48-.21.94-0.35a3.33,3.33,0,0,1,.88-0.16,2.21,2.21,0,0,1,.81.12A2,2,0,0,1,199.23,523.94Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M203.57,519.6a2.28,2.28,0,0,1,.53.82,2.26,2.26,0,0,1,.14,1,2.86,2.86,0,0,1-.29,1,3.72,3.72,0,0,1-.74,1,6,6,0,0,1-1,.8,5.25,5.25,0,0,1-1.12.55l-0.75-1.29a5.55,5.55,0,0,0,.89-0.41,4,4,0,0,0,.88-0.68,1.58,1.58,0,0,0,.44-0.69,0.62,0.62,0,0,0-.19-0.63,0.69,0.69,0,0,0-.57-0.18,3.39,3.39,0,0,0-1.06.34,4.43,4.43,0,0,1-1.86.48,2,2,0,0,1-1.36-.63,2.07,2.07,0,0,1-.69-1.73,3.16,3.16,0,0,1,1-1.86,5.45,5.45,0,0,1,1-.79,4.07,4.07,0,0,1,1-.45l0.78,1.24a4.59,4.59,0,0,0-.85.4,4,4,0,0,0-.79.61,1.42,1.42,0,0,0-.38.59,0.52,0.52,0,0,0,.16.53,0.72,0.72,0,0,0,.23.16,0.59,0.59,0,0,0,.27,0,1.57,1.57,0,0,0,.39-0.09l0.57-.22q0.48-.21.94-0.35A3.28,3.28,0,0,1,202,519a2.17,2.17,0,0,1,.81.12A2,2,0,0,1,203.57,519.6Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M185.94,549.23L187,550.3l-1.47,1.47,4.39,4.39-1.34,1.34-4.39-4.39-1.47,1.47-1.06-1.07Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M194.51,546.12a4.46,4.46,0,0,1,.91,1.28,3.76,3.76,0,0,1,.35,1.44,3.64,3.64,0,0,1-.26,1.48,4.55,4.55,0,0,1-2.36,2.36,3.63,3.63,0,0,1-1.49.27,3.7,3.7,0,0,1-1.44-.36,4.73,4.73,0,0,1-2.19-2.19,3.7,3.7,0,0,1-.36-1.44,3.63,3.63,0,0,1,.27-1.49,4.54,4.54,0,0,1,2.36-2.36,3.64,3.64,0,0,1,1.48-.26,3.72,3.72,0,0,1,1.44.35A4.43,4.43,0,0,1,194.51,546.12Zm-1,4.55a2,2,0,0,0,.49-0.75,1.84,1.84,0,0,0,.09-0.82,2.21,2.21,0,0,0-.28-0.83,4,4,0,0,0-1.42-1.42,2.22,2.22,0,0,0-.83-0.28,1.8,1.8,0,0,0-.82.09,2,2,0,0,0-.75.49,2,2,0,0,0-.5.76,1.83,1.83,0,0,0-.08.82,2.24,2.24,0,0,0,.28.83,4,4,0,0,0,1.42,1.42,2.21,2.21,0,0,0,.83.28,1.85,1.85,0,0,0,.81-0.09A2,2,0,0,0,193.49,550.67Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M195.86,539.32L197.2,538l3.49,3.49a2.74,2.74,0,0,1,.91,2.13,4.11,4.11,0,0,1-3.4,3.4,2.74,2.74,0,0,1-2.12-.91l-3.49-3.49,1.34-1.34,3.53,3.53a1.36,1.36,0,0,0,1,.47,1.67,1.67,0,0,0,1.44-1.44,1.36,1.36,0,0,0-.47-1Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M204.09,534.54a2.2,2.2,0,0,1,.65,1.27,2.69,2.69,0,0,1-.23,1.48l3.94,0.35-1.65,1.65-3.39-.39-0.25.25L205,541l-1.34,1.34-5.46-5.46,1.74-1.74a5.58,5.58,0,0,1,1-.83,3.35,3.35,0,0,1,1.07-.43A2.1,2.1,0,0,1,204.09,534.54Zm-1.27,1.32a0.84,0.84,0,0,0-.83-0.26,2.18,2.18,0,0,0-1,.63l-0.38.38,1.53,1.53,0.33-.33a2.07,2.07,0,0,0,.63-1.06A1,1,0,0,0,202.82,535.86Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M210.32,535.77L209,537.11l-5.46-5.46,1.34-1.34Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M213.38,529.45a2.25,2.25,0,0,1,.67,1.78,2.87,2.87,0,0,1-.29,1,3.73,3.73,0,0,1-.74,1,5.88,5.88,0,0,1-1,.8,5.2,5.2,0,0,1-1.12.55l-0.75-1.29a5.63,5.63,0,0,0,.89-0.41,4,4,0,0,0,.88-0.68,1.61,1.61,0,0,0,.44-0.69,0.62,0.62,0,0,0-.19-0.63,0.69,0.69,0,0,0-.57-0.18,3.42,3.42,0,0,0-1.06.34,4.41,4.41,0,0,1-1.86.48,2,2,0,0,1-1.36-.63,2.08,2.08,0,0,1-.69-1.73,3.16,3.16,0,0,1,1-1.85,5.31,5.31,0,0,1,1-.79,4.1,4.1,0,0,1,1-.46l0.78,1.24a4.66,4.66,0,0,0-.85.4,4.07,4.07,0,0,0-.79.62,1.43,1.43,0,0,0-.38.59,0.52,0.52,0,0,0,.16.53,0.75,0.75,0,0,0,.23.16,0.58,0.58,0,0,0,.27,0,1.61,1.61,0,0,0,.39-0.09l0.57-.22q0.48-.21.94-0.35a3.37,3.37,0,0,1,.88-0.16,2.23,2.23,0,0,1,.81.12A2,2,0,0,1,213.38,529.45Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M218.56,525.8l-1.29,1.29L214,526l2.89,3.22-1.32,1.32-5-5.87,1.21-1.21,4.91,1.76-1.76-4.91,1.21-1.21,5.87,5-1.32,1.32-3.22-2.89Z" transform="translate(-25.28 -104.81)" />
			<path class="cls-3" d="M217.39,517.79l3.4-3.4,1.07,1.06-2.06,2.06,1.1,1.1,2-2,1.07,1.07-2,2,1.16,1.16,2.08-2.08,1.06,1.06-3.42,3.42Z" transform="translate(-25.28 -104.81)" />
		</g>

	</g>
	<g class="selector" id="communication-multi-canal">
		<path class="cls-6" d="M324.05,186a219.53,219.53,0,0,0-105.69,28.36l44.91,77.79a130,130,0,0,1,60.78-16.56V186Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M282.82,221.44a9.23,9.23,0,0,1,.35,1.69,5.78,5.78,0,0,1,0,1.56l-2,.2a7.14,7.14,0,0,0,0-1.18,5.86,5.86,0,0,0-.24-1.25,3.82,3.82,0,0,0-1.62-2.28,4.14,4.14,0,0,0-5.09,1.57,3.82,3.82,0,0,0-.06,2.79,5.81,5.81,0,0,0,.5,1.16,7.1,7.1,0,0,0,.66,1l-1.78,1a5.79,5.79,0,0,1-.89-1.28,9.3,9.3,0,0,1-.66-1.6,7,7,0,0,1-.34-2.57,5.12,5.12,0,0,1,.59-2.11,5,5,0,0,1,1.39-1.6,7,7,0,0,1,4.34-1.34,5,5,0,0,1,2,.54,5.14,5.14,0,0,1,1.68,1.41A7,7,0,0,1,282.82,221.44Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M283.2,235.14a3.12,3.12,0,0,1-2,0,3.8,3.8,0,0,1-1.7-1.28l-3.06,4.72-1-3.18,2.75-4L278,230.9,274.41,232l-0.8-2.58,10.48-3.23,1,3.34a7.9,7.9,0,0,1,.35,1.87,4.74,4.74,0,0,1-.17,1.64A3,3,0,0,1,283.2,235.14Zm-0.81-2.47a1.2,1.2,0,0,0,.88-0.87,3.1,3.1,0,0,0-.14-1.65l-0.23-.74-2.94.91,0.19,0.63a2.91,2.91,0,0,0,.91,1.49A1.38,1.38,0,0,0,282.39,232.67Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M280.56,246.08l-0.77-2.49,3.57-3.34-6,1.49-0.78-2.53,10.72-2.44,0.72,2.33-5.47,5,7.34,1,0.72,2.33-10.24,4L279.62,249l5.77-2.12Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="cartes-fidelite">
		<path class="cls-6" d="M105.88,416.59A219.45,219.45,0,0,0,134,515.13l78.71-45.44a130,130,0,0,1-16.28-57.83Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M132,449.31l1.9,5.91,2.7-.87,0.46,1.43-4.51,1.45-2.37-7.34Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M143.83,449.54a4.44,4.44,0,0,1,.22,1.55,3.73,3.73,0,0,1-.34,1.44,3.63,3.63,0,0,1-.91,1.2,4.55,4.55,0,0,1-3.18,1,3.63,3.63,0,0,1-1.44-.44,3.68,3.68,0,0,1-1.12-1,4.73,4.73,0,0,1-.95-2.94,3.7,3.7,0,0,1,.34-1.44,3.63,3.63,0,0,1,.92-1.2,4.54,4.54,0,0,1,3.18-1,3.66,3.66,0,0,1,1.44.44,3.75,3.75,0,0,1,1.12,1A4.42,4.42,0,0,1,143.83,449.54Zm-3,3.58a2,2,0,0,0,.78-0.45,1.82,1.82,0,0,0,.45-0.69,2.2,2.2,0,0,0,.13-0.87,4,4,0,0,0-.61-1.91,2.23,2.23,0,0,0-.61-0.63,1.83,1.83,0,0,0-.77-0.29,2.2,2.2,0,0,0-1.68.54,1.84,1.84,0,0,0-.45.69,2.22,2.22,0,0,0-.13.87,3.75,3.75,0,0,0,.19,1,3.7,3.7,0,0,0,.43.92,2.21,2.21,0,0,0,.61.63,1.84,1.84,0,0,0,.76.3A2,2,0,0,0,140.84,453.12Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M148.47,449.16l0.86,2.67-1.81.58-0.86-2.67-3.92-3.9,1.93-.62,2.38,2.64,0.39-3.54,1.93-.62Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M155.89,448.14l-2.81.91v1.57l-1.86.6,0.37-8.22,2-.64,5.1,6.46-1.86.6Zm-2.81-.68,1.88-.61-1.93-2.8Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M159,440.62l1.9,5.91,2.7-.87,0.46,1.43-4.51,1.45-2.36-7.34Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M167.28,437.94l0.46,1.43-2,.64,1.9,5.91-1.81.58-1.9-5.91-2,.64-0.46-1.43Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M173.27,441.17l0.86,2.67-1.81.58-0.86-2.67-3.92-3.9,1.93-.62,2.38,2.64,0.4-3.54,1.93-.62Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M137.27,464.85a2.25,2.25,0,0,1,.08,1.13,2.41,2.41,0,0,1-.45,1,3.53,3.53,0,0,1-.9.82,5.24,5.24,0,0,1-1.26.59l-0.19.06,0.8,2.48-1.81.58-2.36-7.34,2.12-.68a5.7,5.7,0,0,1,1.33-.27,3.17,3.17,0,0,1,1.16.11,2.15,2.15,0,0,1,.9.53A2.38,2.38,0,0,1,137.27,464.85Zm-1.74.57q-0.33-1-1.77-.55l-0.32.1,0.67,2.08,0.3-.1a1.87,1.87,0,0,0,1-.64A1,1,0,0,0,135.53,465.42Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M143.63,462.73a2.2,2.2,0,0,1,0,1.42,2.68,2.68,0,0,1-.88,1.21l3.34,2.11-2.23.72-2.84-1.9-0.33.11,0.82,2.55-1.81.58-2.36-7.34,2.34-.75a5.61,5.61,0,0,1,1.31-.27,3.36,3.36,0,0,1,1.15.1A2.09,2.09,0,0,1,143.63,462.73Zm-1.73.59a0.84,0.84,0,0,0-.62-0.61,2.18,2.18,0,0,0-1.16.11l-0.52.17,0.66,2.06,0.44-.14a2,2,0,0,0,1-.65A1,1,0,0,0,141.9,463.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M152.65,461.31a4.41,4.41,0,0,1,.22,1.55,3.73,3.73,0,0,1-.34,1.44,3.64,3.64,0,0,1-.91,1.2,4.53,4.53,0,0,1-3.18,1,3.63,3.63,0,0,1-1.44-.44,3.69,3.69,0,0,1-1.12-1,4.74,4.74,0,0,1-.95-2.94,3.69,3.69,0,0,1,.34-1.44,3.63,3.63,0,0,1,.92-1.2,4.17,4.17,0,0,1,1.5-.82,4.13,4.13,0,0,1,1.69-.21,3.64,3.64,0,0,1,1.44.44,3.77,3.77,0,0,1,1.12,1A4.43,4.43,0,0,1,152.65,461.31Zm-3,3.58a2,2,0,0,0,.78-0.45,1.81,1.81,0,0,0,.45-0.69,2.2,2.2,0,0,0,.13-0.87,4,4,0,0,0-.61-1.91,2.22,2.22,0,0,0-.61-0.63,1.83,1.83,0,0,0-.77-0.29,2.2,2.2,0,0,0-1.68.54,1.84,1.84,0,0,0-.45.69,2.22,2.22,0,0,0-.13.87,4,4,0,0,0,.61,1.91,2.21,2.21,0,0,0,.61.63,1.85,1.85,0,0,0,.76.3A2,2,0,0,0,149.66,464.89Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M156.26,455.95a6,6,0,0,1,2.55-.33l0.2,1.44a5.62,5.62,0,0,0-2.11.26,3.37,3.37,0,0,0-.95.46,2,2,0,0,0-.61.69,2.12,2.12,0,0,0-.24.93,3.43,3.43,0,0,0,.19,1.17,2.43,2.43,0,0,0,1.16,1.54,2.57,2.57,0,0,0,2,0,3,3,0,0,0,.47-0.2,2.37,2.37,0,0,0,.36-0.22l-0.66-2.05,1.62-.52,1,3a5.31,5.31,0,0,1-1.08.77,8,8,0,0,1-1.48.64,5,5,0,0,1-1.74.26,3.73,3.73,0,0,1-1.48-.36,3.42,3.42,0,0,1-1.15-.91,4.22,4.22,0,0,1-.75-1.42,4.77,4.77,0,0,1-.25-1.69,3.54,3.54,0,0,1,.39-1.49,3.72,3.72,0,0,1,1-1.2A5.11,5.11,0,0,1,156.26,455.95Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M166.55,455.35a2.2,2.2,0,0,1,0,1.42,2.69,2.69,0,0,1-.88,1.21l3.34,2.11-2.23.72-2.84-1.9-0.33.11,0.82,2.55-1.81.58-2.36-7.34,2.34-.75a5.61,5.61,0,0,1,1.31-.26,3.34,3.34,0,0,1,1.15.1A2.09,2.09,0,0,1,166.55,455.35Zm-1.73.59a0.84,0.84,0,0,0-.62-0.61,2.19,2.19,0,0,0-1.16.11l-0.52.17,0.66,2.06,0.44-.14a2,2,0,0,0,1-.65A1,1,0,0,0,164.82,455.94Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M173.92,456.94l-2.81.91v1.57l-1.86.6,0.37-8.22,2-.64,5.1,6.46-1.86.6Zm-2.81-.68,1.89-.61-1.94-2.8Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M182.1,454.59l-1.74.56L178,452.67l1.1,4.18-1.78.57-1.81-7.52,1.63-.53,3.56,3.81,0.67-5.17,1.63-.53,2.92,7.17-1.78.57-1.54-4Z" transform="translate(-25.28 -104.81)" />

	</g>

	<g class="selector" id="cartes-cadeaux">

		<path class="cls-6" d="M218.36,599.46a219.53,219.53,0,0,0,105.69,28.36V536.94a130,130,0,0,1-60.22-16.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M241.6,478.1c0.3-0.9,0.6-1.7,1.1-2.3l1.3,0.6c-0.4,0.6-0.7,1.2-0.9,1.9c-0.1,0.4-0.1,0.7-0.1,1
		c0,0.3,0.1,0.6,0.2,0.9s0.4,0.5,0.6,0.7c0.3,0.2,0.6,0.4,1.1,0.5c0.8,0.2,1.4,0.2,1.9-0.1c0.5-0.3,0.9-0.8,1.1-1.6
		c0-0.2,0.1-0.3,0.1-0.5c0-0.2,0-0.3,0-0.4l-2-0.6l0.5-1.6l3,0.9c0.1,0.4,0.1,0.8,0,1.3c0,0.5-0.1,1-0.3,1.6
		c-0.2,0.6-0.4,1.1-0.7,1.6c-0.3,0.4-0.7,0.8-1.1,1c-0.4,0.2-0.9,0.4-1.4,0.4c-0.5,0.1-1,0-1.6-0.2c-0.6-0.2-1.1-0.4-1.5-0.7
		c-0.4-0.3-0.8-0.7-1-1.1c-0.2-0.4-0.4-0.9-0.4-1.5C241.3,479.3,241.4,478.7,241.6,478.1z"/>
		<path class="cls-3" d="M250.7,474.4l-0.5,1.8l-7.3-2.1l0.5-1.8L250.7,474.4z"/>
		<path class="cls-3" d="M251.1,472.9l-7.3-2.1l1.3-4.6l1.4,0.4l-0.8,2.8l1.7,0.5l0.8-2.7l1.4,0.4l-0.8,2.7l2.8,0.8L251.1,472.9z"/>
		<path class="cls-3" d="M246.8,460.1l1.4,0.4l-0.6,2l5.9,1.7l-0.5,1.8l-5.9-1.7l-0.6,2l-1.4-0.4L246.8,460.1z"/>
		<path class="cls-3" d="M252.4,488.8c0.1-0.4,0.2-0.8,0.4-1.1s0.4-0.7,0.6-0.9l1.3,0.6c-0.1,0.2-0.3,0.4-0.4,0.7
		c-0.1,0.3-0.2,0.5-0.3,0.8c-0.2,0.7-0.2,1.4,0.1,1.9c0.3,0.5,0.8,0.9,1.6,1.1c0.8,0.2,1.4,0.1,2-0.2c0.5-0.3,0.9-0.9,1-1.6
		c0.1-0.3,0.1-0.6,0.1-0.9c0-0.3,0-0.6,0-0.8l1.4,0.1c0.1,0.3,0.1,0.7,0.1,1.1c0,0.4-0.1,0.8-0.2,1.2c-0.2,0.6-0.4,1.2-0.7,1.6
		c-0.3,0.4-0.7,0.8-1.1,1c-0.4,0.2-0.9,0.4-1.4,0.4c-0.5,0-1,0-1.6-0.1c-0.6-0.1-1-0.4-1.5-0.6c-0.4-0.3-0.8-0.6-1-1.1
		c-0.3-0.4-0.4-0.9-0.5-1.4C252.2,490.1,252.2,489.5,252.4,488.8z"/>
		<path class="cls-3" d="M260.3,482.5l-0.8,2.8l1.3,0.8l-0.5,1.9l-6.5-4.8l0.6-2l8.1-0.6l-0.5,1.9L260.3,482.5z M258.3,484.4l0.5-1.9
		l-3.3,0.1L258.3,484.4z"/>
		<path class="cls-3" d="M259.2,472.9c0.5,0.1,0.9,0.4,1.2,0.7c0.3,0.4,0.5,0.8,0.5,1.4l3.5-1.7l-0.6,2.2l-3.1,1.4l-0.1,0.3l2.5,0.7
		l-0.5,1.8l-7.3-1.9l0.6-2.3c0.1-0.5,0.3-0.9,0.5-1.2c0.2-0.4,0.4-0.7,0.7-0.9c0.3-0.2,0.6-0.4,0.9-0.5
		C258.4,472.8,258.8,472.8,259.2,472.9z M258.7,474.6c-0.3-0.1-0.6,0-0.8,0.2c-0.2,0.2-0.4,0.6-0.5,1l-0.1,0.5l2.1,0.5l0.1-0.4
		c0.1-0.5,0.1-0.9,0-1.2C259.3,474.9,259.1,474.7,258.7,474.6z"/>
		<path class="cls-3" d="M257.2,470.8l0.6-2.2c0.2-0.6,0.4-1.2,0.7-1.7c0.3-0.5,0.7-0.9,1.1-1.2c0.4-0.3,0.9-0.5,1.4-0.6
		c0.5-0.1,1-0.1,1.6,0.1c0.5,0.1,1,0.4,1.4,0.7c0.4,0.3,0.7,0.7,0.9,1.2c0.2,0.5,0.4,1,0.4,1.6c0,0.6,0,1.2-0.2,1.9l-0.6,2.2
		L257.2,470.8z M263.7,469.9c0.1-0.4,0.1-0.7,0.1-1c0-0.3-0.1-0.6-0.2-0.9c-0.1-0.3-0.3-0.5-0.6-0.7c-0.2-0.2-0.5-0.3-0.9-0.4
		c-0.4-0.1-0.7-0.1-1-0.1c-0.3,0-0.6,0.2-0.8,0.3c-0.2,0.2-0.5,0.4-0.6,0.7c-0.2,0.3-0.3,0.6-0.4,1l-0.1,0.5l4.5,1.2L263.7,469.9z"
		/>
		<path class="cls-3" d="M265.6,459.6c0.3,0.1,0.6,0.2,0.9,0.4c0.3,0.2,0.4,0.5,0.6,0.7c0.1,0.3,0.2,0.6,0.3,1c0,0.4,0,0.8-0.1,1.2
		c-0.1,0.4-0.3,0.8-0.4,1.2c-0.2,0.4-0.4,0.7-0.7,1l-1.3-0.7c0.2-0.2,0.4-0.5,0.6-0.8c0.2-0.3,0.3-0.6,0.4-1c0.1-0.3,0.1-0.6,0-0.8
		c-0.1-0.2-0.2-0.4-0.5-0.4c-0.2,0-0.4,0-0.6,0.1c-0.2,0.1-0.4,0.4-0.7,0.8c-0.5,0.6-0.9,1.1-1.3,1.3c-0.4,0.2-0.9,0.3-1.5,0.1
		c-0.7-0.2-1.2-0.6-1.4-1.1c-0.3-0.6-0.3-1.3-0.1-2.1c0.1-0.4,0.3-0.8,0.5-1.2c0.2-0.4,0.4-0.7,0.6-0.9l1.3,0.7
		c-0.2,0.2-0.4,0.5-0.5,0.8c-0.2,0.3-0.3,0.6-0.4,0.9c-0.1,0.3-0.1,0.5,0,0.7c0,0.2,0.2,0.3,0.4,0.4c0.1,0,0.2,0,0.3,0
		c0.1,0,0.2,0,0.3-0.1c0.1-0.1,0.2-0.2,0.3-0.3c0.1-0.1,0.2-0.3,0.4-0.5c0.2-0.3,0.4-0.5,0.6-0.8c0.2-0.2,0.4-0.4,0.7-0.6
		c0.2-0.1,0.5-0.3,0.7-0.3C265,459.5,265.3,459.6,265.6,459.6z"/>
	</g>
	<g class="selector" id="jeux-concours">

		<path class="cls-6" d="M134,298.66A219.58,219.58,0,0,0,105.64,406.9c0,1.11,0,2.22.05,3.33l90.64-4.72a130.12,130.12,0,0,1,15.85-61.71Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M141.84,351.7a5.19,5.19,0,0,1,2.15.53l-0.29,1.22a4.84,4.84,0,0,0-1.78-.44,2.89,2.89,0,0,0-.91.08,1.75,1.75,0,0,0-.71.37,1.81,1.81,0,0,0-.48.67,3,3,0,0,0-.21,1,2.09,2.09,0,0,0,.46,1.6,2.22,2.22,0,0,0,1.57.62,2.63,2.63,0,0,0,.44,0,2.06,2.06,0,0,0,.35-0.07l0.1-1.86L144,355.5l-0.16,2.74a4.57,4.57,0,0,1-1.11.29,6.88,6.88,0,0,1-1.39.06,4.29,4.29,0,0,1-1.48-.33,3.22,3.22,0,0,1-1.08-.75,3,3,0,0,1-.64-1.09A3.65,3.65,0,0,1,138,355a4.13,4.13,0,0,1,.33-1.44,3.07,3.07,0,0,1,.78-1.08,3.22,3.22,0,0,1,1.19-.65A4.42,4.42,0,0,1,141.84,351.7Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M149.23,357.62l-2.55-.14-0.48,1.26-1.69-.09,2.85-6.51,1.8,0.1,2.1,6.79-1.69-.09Zm-2.05-1.42,1.71,0.1-0.69-2.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M156.54,358.26l-1.58-.09-1.15-2.74-0.41,3.71-1.61-.09,0.88-6.62,1.48,0.08,1.69,4.17,2.15-4,1.48,0.08,0.12,6.68L158,359.4v-3.74Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M161,352.9l4.14,0.23-0.07,1.3-2.51-.14-0.08,1.34,2.39,0.13-0.07,1.3-2.39-.13-0.08,1.42,2.54,0.14-0.07,1.3-4.17-.24Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M170.44,358.11a2,2,0,0,1-.22.81,1.93,1.93,0,0,1-.54.64,2.43,2.43,0,0,1-.82.4,3.21,3.21,0,0,1-1.08.1,5.13,5.13,0,0,1-1.09-.18,4.52,4.52,0,0,1-1-.41l0.4-1.23a4.74,4.74,0,0,0,.77.34,3.45,3.45,0,0,0,.94.18,1.37,1.37,0,0,0,.7-0.12,0.54,0.54,0,0,0,.3-0.49,0.6,0.6,0,0,0-.21-0.47,2.93,2.93,0,0,0-.83-0.48,3.83,3.83,0,0,1-1.38-.92,1.69,1.69,0,0,1-.38-1.24,1.79,1.79,0,0,1,.72-1.44,2.73,2.73,0,0,1,1.78-.41,4.61,4.61,0,0,1,1.08.19,3.55,3.55,0,0,1,.86.37L170.08,355a4.06,4.06,0,0,0-.75-0.32,3.5,3.5,0,0,0-.85-0.15,1.23,1.23,0,0,0-.6.09,0.45,0.45,0,0,0-.25.41,0.64,0.64,0,0,0,0,.24,0.49,0.49,0,0,0,.13.2,1.34,1.34,0,0,0,.28.2l0.47,0.24q0.41,0.19.77,0.4a2.91,2.91,0,0,1,.61.48,1.9,1.9,0,0,1,.39.59A1.7,1.7,0,0,1,170.44,358.11Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M121.11,369.46q-0.82-.55-1.46-1a3.47,3.47,0,0,1-1.24.67,3.9,3.9,0,0,1-1.24.13,3.42,3.42,0,0,1-.91-0.17,2.17,2.17,0,0,1-.69-0.37,1.46,1.46,0,0,1-.42-0.56,1.62,1.62,0,0,1-.12-0.75,1.36,1.36,0,0,1,.13-0.52,1.75,1.75,0,0,1,.3-0.43,3.08,3.08,0,0,1,.43-0.38l0.51-.35a4.09,4.09,0,0,1-.65-0.81,1.59,1.59,0,0,1-.21-0.92,1.69,1.69,0,0,1,.18-0.68,1.55,1.55,0,0,1,.44-0.53,1.91,1.91,0,0,1,.68-0.33,2.9,2.9,0,0,1,.91-0.08,2.7,2.7,0,0,1,.78.15,1.87,1.87,0,0,1,.6.34,1.44,1.44,0,0,1,.37.51,1.38,1.38,0,0,1,.11.64,1.51,1.51,0,0,1-.12.53,2,2,0,0,1-.28.46,3,3,0,0,1-.39.39l-0.44.36,0.59,0.49,0.58,0.46c0.14-.18.29-0.39,0.44-0.63l0.46-.75,1.13,0.66q-0.26.45-.5,0.82a8.08,8.08,0,0,1-.49.68l1.27,0.9Zm-3.72-1.39a1.84,1.84,0,0,0,1.25-.41l-0.68-.55-0.66-.58a3.18,3.18,0,0,0-.47.38,0.68,0.68,0,0,0-.2.45,0.63,0.63,0,0,0,.16.49A0.86,0.86,0,0,0,117.39,368.07Zm-0.4-4a0.86,0.86,0,0,0,.11.48,2.1,2.1,0,0,0,.36.46,4.74,4.74,0,0,0,.56-0.45,0.72,0.72,0,0,0,.23-0.5,0.46,0.46,0,0,0-.15-0.39,0.69,0.69,0,0,0-.44-0.17,0.69,0.69,0,0,0-.48.13A0.55,0.55,0,0,0,117,364Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M128.42,363a5.66,5.66,0,0,1,1,.16,3.5,3.5,0,0,1,.89.34L130,364.64a4.45,4.45,0,0,0-.67-0.24,3.54,3.54,0,0,0-.76-0.13,2.32,2.32,0,0,0-1.64.44,2.51,2.51,0,0,0-.18,3.23,2.31,2.31,0,0,0,1.58.62,3.51,3.51,0,0,0,.77,0,4.39,4.39,0,0,0,.7-0.17l0.17,1.21a3.48,3.48,0,0,1-.92.24,5.65,5.65,0,0,1-1,0,4.25,4.25,0,0,1-1.53-.35,3.11,3.11,0,0,1-1.08-.78,3,3,0,0,1-.62-1.13,4.24,4.24,0,0,1,.16-2.76,3,3,0,0,1,.74-1,3.1,3.1,0,0,1,1.16-.66A4.25,4.25,0,0,1,128.42,363Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M137.65,366.94a3.84,3.84,0,0,1-.3,1.32,3.25,3.25,0,0,1-.73,1.06,3.13,3.13,0,0,1-1.11.68,3.92,3.92,0,0,1-2.88-.16,3.12,3.12,0,0,1-1-.8,3.18,3.18,0,0,1-.6-1.13,4.07,4.07,0,0,1,.15-2.67,3.18,3.18,0,0,1,.72-1.06,3.12,3.12,0,0,1,1.11-.68,3.61,3.61,0,0,1,1.46-.19,3.56,3.56,0,0,1,1.42.36,3.15,3.15,0,0,1,1,.8,3.25,3.25,0,0,1,.6,1.13A3.85,3.85,0,0,1,137.65,366.94Zm-3.52,2a1.72,1.72,0,0,0,.77-0.12,1.57,1.57,0,0,0,.58-0.41,1.91,1.91,0,0,0,.37-0.66A3.47,3.47,0,0,0,136,366a1.91,1.91,0,0,0-.3-0.7,1.57,1.57,0,0,0-.53-0.48,1.73,1.73,0,0,0-.75-0.2,1.71,1.71,0,0,0-.77.12,1.59,1.59,0,0,0-.57.41,1.91,1.91,0,0,0-.37.66,3.47,3.47,0,0,0-.1,1.73,1.91,1.91,0,0,0,.3.7,1.59,1.59,0,0,0,.52.48A1.71,1.71,0,0,0,134.13,368.89Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M143,369.51l-1.58-.09-1.15-2.74-0.41,3.71-1.61-.09,0.88-6.62,1.48,0.08,1.69,4.17,2.15-4,1.48,0.08,0.12,6.68-1.61-.09v-3.74Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M152.14,366.59a2,2,0,0,1-.28.94,2.08,2.08,0,0,1-.67.67,3,3,0,0,1-1,.38,4.54,4.54,0,0,1-1.2.08h-0.17l-0.13,2.25-1.64-.09,0.38-6.65,1.92,0.11a4.91,4.91,0,0,1,1.16.2,2.73,2.73,0,0,1,.9.45,1.84,1.84,0,0,1,.56.71A2.06,2.06,0,0,1,152.14,366.59Zm-1.58-.08q0.05-.92-1.26-1l-0.29,0-0.11,1.89,0.27,0a1.61,1.61,0,0,0,1-.2A0.85,0.85,0,0,0,150.56,366.51Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M153,364.47l4.14,0.23L157.1,366l-2.51-.14-0.08,1.34,2.39,0.13-0.07,1.3-2.39-.13-0.08,1.42,2.54,0.14-0.07,1.3-4.17-.24Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M163,365l-0.07,1.3-1.79-.1-0.3,5.35-1.64-.09,0.3-5.35-1.79-.1,0.07-1.3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M165.14,371.83l-1.64-.09,0.38-6.65,1.64,0.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M171.56,365.52l-0.07,1.3-1.79-.1-0.3,5.35-1.64-.09,0.3-5.35-1.79-.1,0.07-1.3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M173.66,372.31l-1.64-.09,0.38-6.65,1.64,0.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M181.68,369.43a3.84,3.84,0,0,1-.3,1.32,3.25,3.25,0,0,1-.73,1.06,3.13,3.13,0,0,1-1.11.68,3.56,3.56,0,0,1-1.45.19,3.6,3.6,0,0,1-1.43-.36,3.12,3.12,0,0,1-1-.8,3.18,3.18,0,0,1-.6-1.13,4.07,4.07,0,0,1,.15-2.67,3.17,3.17,0,0,1,.72-1.06A3.12,3.12,0,0,1,177,366a3.92,3.92,0,0,1,2.88.16,3.15,3.15,0,0,1,1,.8,3.24,3.24,0,0,1,.6,1.13A3.84,3.84,0,0,1,181.68,369.43Zm-3.52,2a1.74,1.74,0,0,0,.77-0.12,1.58,1.58,0,0,0,.58-0.41,1.92,1.92,0,0,0,.37-0.66,3.47,3.47,0,0,0,.1-1.73,1.91,1.91,0,0,0-.3-0.7,1.56,1.56,0,0,0-.53-0.48,1.9,1.9,0,0,0-1.52-.09,1.59,1.59,0,0,0-.57.41,1.91,1.91,0,0,0-.37.66,3.47,3.47,0,0,0-.1,1.73,1.92,1.92,0,0,0,.3.7,1.59,1.59,0,0,0,.52.48A1.71,1.71,0,0,0,178.16,371.38Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M186.8,373.05l-2.52-4.12-0.22,4-1.58-.09,0.38-6.65,1.58,0.09,2.52,4.12,0.22-4,1.58,0.09-0.38,6.65Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M194.15,371.47a2,2,0,0,1-.22.81,1.93,1.93,0,0,1-.54.64,2.43,2.43,0,0,1-.82.4,3.23,3.23,0,0,1-1.08.1,5.19,5.19,0,0,1-1.09-.18,4.51,4.51,0,0,1-1-.41l0.4-1.23a4.82,4.82,0,0,0,.77.34,3.44,3.44,0,0,0,.94.18,1.36,1.36,0,0,0,.7-0.12,0.54,0.54,0,0,0,.3-0.49,0.6,0.6,0,0,0-.21-0.47,2.92,2.92,0,0,0-.83-0.48,3.83,3.83,0,0,1-1.38-.92,1.69,1.69,0,0,1-.38-1.24,1.79,1.79,0,0,1,.72-1.44,2.73,2.73,0,0,1,1.78-.41,4.66,4.66,0,0,1,1.08.19,3.51,3.51,0,0,1,.86.37l-0.35,1.21a4.07,4.07,0,0,0-.75-0.32,3.5,3.5,0,0,0-.85-0.15,1.22,1.22,0,0,0-.6.09,0.45,0.45,0,0,0-.25.41,0.66,0.66,0,0,0,0,.24,0.5,0.5,0,0,0,.13.2,1.39,1.39,0,0,0,.28.2l0.47,0.24q0.41,0.19.77,0.4a2.88,2.88,0,0,1,.61.48,1.9,1.9,0,0,1,.39.59A1.71,1.71,0,0,1,194.15,371.47Z" transform="translate(-25.28 -104.81)" />

	</g>

	<g class="selector" id="statistiques-rapports">

		<path class="cls-6" d="M258.88,294.69L214,216.89a222.62,222.62,0,0,0-77.37,77.37l78.13,45.11A131.38,131.38,0,0,1,258.88,294.69Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M189.75,260.17a2.3,2.3,0,0,1-.81.55,2.24,2.24,0,0,1-1,.16,2.88,2.88,0,0,1-1-.26,3.74,3.74,0,0,1-1-.72,5.89,5.89,0,0,1-.82-1,5.19,5.19,0,0,1-.57-1.11l1.28-.78a5.66,5.66,0,0,0,.42.88,4,4,0,0,0,.69.87,1.59,1.59,0,0,0,.7.42,0.62,0.62,0,0,0,.63-0.21,0.69,0.69,0,0,0,.17-0.57,3.41,3.41,0,0,0-.37-1,4.43,4.43,0,0,1-.52-1.85,2,2,0,0,1,.6-1.38,2.07,2.07,0,0,1,1.72-.73,3.17,3.17,0,0,1,1.88,1,5.39,5.39,0,0,1,.81,1,4.08,4.08,0,0,1,.48,1l-1.22.8a4.64,4.64,0,0,0-.42-0.84,4,4,0,0,0-.63-0.77,1.42,1.42,0,0,0-.6-0.37,0.52,0.52,0,0,0-.53.17,0.73,0.73,0,0,0-.16.23,0.57,0.57,0,0,0,0,.28,1.55,1.55,0,0,0,.1.39q0.09,0.23.23,0.56t0.37,0.93a3.35,3.35,0,0,1,.18.88,2.19,2.19,0,0,1-.1.82A2,2,0,0,1,189.75,260.17Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M198.21,260.78l-1,1.09-1.5-1.43-4.3,4.48L190,263.6l4.3-4.48-1.5-1.43,1-1.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M197.64,268.85l-2.13-2-1.38.75-1.41-1.36,7.41-3.58,1.51,1.44-3.27,7.56L197,270.26Zm-0.74-2.79,1.43,1.37,1.54-3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M207.92,270.08l-1,1.09-1.5-1.43-4.3,4.48-1.37-1.31,4.3-4.48-1.5-1.43,1-1.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M204.65,277.64l-1.37-1.31,5.34-5.57,1.37,1.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M211,280.56a2.3,2.3,0,0,1-.81.55,2.25,2.25,0,0,1-1,.16,2.84,2.84,0,0,1-1-.27,3.71,3.71,0,0,1-1-.72,6,6,0,0,1-.82-1,5.15,5.15,0,0,1-.57-1.11l1.28-.78a5.57,5.57,0,0,0,.42.88,4,4,0,0,0,.69.87,1.59,1.59,0,0,0,.7.42,0.62,0.62,0,0,0,.63-0.21,0.7,0.7,0,0,0,.17-0.57,3.44,3.44,0,0,0-.37-1,4.42,4.42,0,0,1-.52-1.85,2,2,0,0,1,.6-1.38,2.07,2.07,0,0,1,1.72-.73,3.17,3.17,0,0,1,1.88,1,5.35,5.35,0,0,1,.81,1,4,4,0,0,1,.48,1l-1.22.8a4.58,4.58,0,0,0-.42-0.84,4,4,0,0,0-.63-0.77,1.43,1.43,0,0,0-.6-0.37,0.52,0.52,0,0,0-.53.18,0.73,0.73,0,0,0-.16.23,0.57,0.57,0,0,0,0,.28,1.56,1.56,0,0,0,.1.39c0.06,0.15.14,0.34,0.23,0.56s0.27,0.63.37,0.93a3.33,3.33,0,0,1,.18.88,2.19,2.19,0,0,1-.1.82A2,2,0,0,1,211,280.56Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M219.49,281.17l-1,1.09-1.5-1.43-4.3,4.48L211.28,284l4.3-4.48-1.5-1.44,1-1.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M216.23,288.73l-1.37-1.31,5.34-5.57,1.37,1.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M225.52,286.76a6.62,6.62,0,0,1,.79.92,4.05,4.05,0,0,1,.53,1l-1.17.81a5.11,5.11,0,0,0-.41-0.72,4.13,4.13,0,0,0-.57-0.68,2.68,2.68,0,0,0-1.77-.85,2.91,2.91,0,0,0-2.59,2.71,2.68,2.68,0,0,0,.92,1.74,4.16,4.16,0,0,0,.71.54,5.13,5.13,0,0,0,.74.38l-0.76,1.2a4,4,0,0,1-1-.49,6.41,6.41,0,0,1-1-.75,4.94,4.94,0,0,1-1.09-1.46,3.6,3.6,0,0,1-.36-1.5,3.49,3.49,0,0,1,.3-1.46,4.92,4.92,0,0,1,2.21-2.31,3.49,3.49,0,0,1,1.44-.37,3.61,3.61,0,0,1,1.51.3A4.94,4.94,0,0,1,225.52,286.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M227.85,296.68a2.29,2.29,0,0,1-.81.55,2.26,2.26,0,0,1-1,.16,2.89,2.89,0,0,1-1-.27,3.75,3.75,0,0,1-1-.72,6,6,0,0,1-.82-1,5.17,5.17,0,0,1-.57-1.11l1.28-.78a5.62,5.62,0,0,0,.42.88,4,4,0,0,0,.69.87,1.59,1.59,0,0,0,.7.42,0.62,0.62,0,0,0,.63-0.21,0.7,0.7,0,0,0,.17-0.57,3.43,3.43,0,0,0-.37-1,4.43,4.43,0,0,1-.52-1.85,2,2,0,0,1,.6-1.38,2.07,2.07,0,0,1,1.72-.73,3.17,3.17,0,0,1,1.88,1,5.36,5.36,0,0,1,.81,1,4.06,4.06,0,0,1,.48,1l-1.22.8a4.64,4.64,0,0,0-.42-0.84,4,4,0,0,0-.63-0.77,1.43,1.43,0,0,0-.6-0.37,0.52,0.52,0,0,0-.53.17,0.73,0.73,0,0,0-.16.23,0.57,0.57,0,0,0,0,.28,1.56,1.56,0,0,0,.1.39q0.09,0.23.23,0.56c0.14,0.32.27,0.63,0.37,0.93a3.35,3.35,0,0,1,.18.88,2.2,2.2,0,0,1-.1.82A2,2,0,0,1,227.85,296.68Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M178.16,271.7q-0.31-1.1-.52-2a4,4,0,0,1-1.6-.34,4.47,4.47,0,0,1-1.19-.82,3.91,3.91,0,0,1-.68-0.83,2.54,2.54,0,0,1-.33-0.84,1.69,1.69,0,0,1,0-.81,1.88,1.88,0,0,1,.46-0.75,1.58,1.58,0,0,1,.51-0.36,2,2,0,0,1,.59-0.16,3.43,3.43,0,0,1,.66,0l0.71,0.07a4.71,4.71,0,0,1,0-1.21,1.84,1.84,0,0,1,.5-1,2,2,0,0,1,.67-0.46,1.77,1.77,0,0,1,.79-0.14,2.2,2.2,0,0,1,.85.23,3.35,3.35,0,0,1,.86.62,3.12,3.12,0,0,1,.57.73,2.19,2.19,0,0,1,.27.75,1.67,1.67,0,0,1-.05.73,1.61,1.61,0,0,1-.39.65,1.79,1.79,0,0,1-.51.38,2.35,2.35,0,0,1-.59.19,3.51,3.51,0,0,1-.64.05h-0.66c0,0.29.09,0.58,0.15,0.88s0.11,0.58.16,0.84q0.4-.08.86-0.22l1-.32,0.49,1.43q-0.56.2-1.05,0.35a9.53,9.53,0,0,1-.94.23l0.44,1.75Zm-2.22-4a2.13,2.13,0,0,0,1.41.58q-0.09-.46-0.18-1t-0.15-1a3.66,3.66,0,0,0-.7,0,0.78,0.78,0,0,0-.52.25,0.73,0.73,0,0,0-.22.55A1,1,0,0,0,175.94,267.67Zm2.68-3.85a1,1,0,0,0-.26.51,2.41,2.41,0,0,0,0,.68,5.62,5.62,0,0,0,.83,0,0.84,0.84,0,0,0,.58-0.26,0.53,0.53,0,0,0,.16-0.46,0.8,0.8,0,0,0-.26-0.47,0.81,0.81,0,0,0-.52-0.25A0.65,0.65,0,0,0,178.62,263.82Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M189.17,274.75a2.19,2.19,0,0,1-1.25.68,2.69,2.69,0,0,1-1.48-.2l-0.27,3.95-1.69-1.62,0.32-3.4-0.25-.24-1.85,1.93-1.37-1.31,5.34-5.57,1.77,1.7a5.56,5.56,0,0,1,.86,1,3.34,3.34,0,0,1,.46,1.06A2.09,2.09,0,0,1,189.17,274.75Zm-1.34-1.24a0.84,0.84,0,0,0,.24-0.84,2.18,2.18,0,0,0-.65-1l-0.39-.38-1.5,1.56,0.33,0.32a2,2,0,0,0,1.07.61A1,1,0,0,0,187.82,273.51Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M192,274.08l3.47,3.33-1,1.09-2.1-2-1.07,1.12,2,1.92-1,1.09-2-1.92-1.14,1.19,2.12,2-1,1.09-3.5-3.35Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M198.74,284a2.25,2.25,0,0,1-1,.61,2.4,2.4,0,0,1-1.1.08,3.51,3.51,0,0,1-1.15-.4,5.23,5.23,0,0,1-1.12-.83l-0.14-.14-1.8,1.88-1.37-1.32,5.34-5.57,1.61,1.54a5.76,5.76,0,0,1,.87,1,3.21,3.21,0,0,1,.45,1.07,2.15,2.15,0,0,1,0,1A2.39,2.39,0,0,1,198.74,284Zm-1.33-1.26q0.74-.77-0.36-1.82l-0.24-.23-1.52,1.58,0.23,0.22a1.86,1.86,0,0,0,1,.58A1,1,0,0,0,197.41,282.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M204,290.89a4.43,4.43,0,0,1-1.26.93,3.77,3.77,0,0,1-1.43.38,3.67,3.67,0,0,1-1.49-.23,4.54,4.54,0,0,1-2.41-2.31,3.63,3.63,0,0,1-.3-1.48,3.7,3.7,0,0,1,.33-1.45,4.72,4.72,0,0,1,2.14-2.23,3.69,3.69,0,0,1,1.43-.39,3.62,3.62,0,0,1,1.49.24,4.19,4.19,0,0,1,1.43.93,4.14,4.14,0,0,1,1,1.39,3.64,3.64,0,0,1,.29,1.48,3.75,3.75,0,0,1-.32,1.45A4.41,4.41,0,0,1,204,290.89Zm-4.57-.93a2,2,0,0,0,.76.48,1.82,1.82,0,0,0,.82.07,2.23,2.23,0,0,0,.83-0.3,4,4,0,0,0,1.39-1.45,2.23,2.23,0,0,0,.27-0.84,1.81,1.81,0,0,0-.11-0.82,2.19,2.19,0,0,0-1.28-1.22,1.83,1.83,0,0,0-.82-0.07,2.21,2.21,0,0,0-.83.3,4,4,0,0,0-1.39,1.45,2.21,2.21,0,0,0-.27.84,1.83,1.83,0,0,0,.1.81A2,2,0,0,0,199.39,290Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M210,294.69a2.2,2.2,0,0,1-1.25.68,2.69,2.69,0,0,1-1.48-.2L207,299.11l-1.69-1.62,0.32-3.4-0.25-.24-1.85,1.93-1.37-1.31,5.34-5.57,1.77,1.7a5.62,5.62,0,0,1,.86,1,3.36,3.36,0,0,1,.46,1.06,2.17,2.17,0,0,1,0,1A2.2,2.2,0,0,1,210,294.69Zm-1.35-1.24a0.84,0.84,0,0,0,.24-0.84,2.17,2.17,0,0,0-.65-1l-0.39-.38-1.5,1.56,0.33,0.32a2.05,2.05,0,0,0,1.07.61A1,1,0,0,0,208.62,293.44Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M216.62,297.67l-1,1.09-1.5-1.44-4.3,4.48-1.37-1.31,4.3-4.48-1.5-1.44,1-1.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M217.19,305.72a2.3,2.3,0,0,1-.81.55,2.24,2.24,0,0,1-1,.16,2.84,2.84,0,0,1-1-.27,3.73,3.73,0,0,1-1-.72,6,6,0,0,1-.82-1,5.22,5.22,0,0,1-.57-1.11l1.28-.78a5.57,5.57,0,0,0,.42.88,4,4,0,0,0,.7.87,1.59,1.59,0,0,0,.7.42,0.62,0.62,0,0,0,.63-0.21,0.69,0.69,0,0,0,.17-0.57,3.43,3.43,0,0,0-.37-1,4.41,4.41,0,0,1-.52-1.85,2,2,0,0,1,.6-1.38,2.07,2.07,0,0,1,1.72-.73,3.17,3.17,0,0,1,1.88,1,5.42,5.42,0,0,1,.81,1,4.05,4.05,0,0,1,.48,1l-1.22.8a4.61,4.61,0,0,0-.42-0.84,4,4,0,0,0-.63-0.77,1.42,1.42,0,0,0-.6-0.37,0.52,0.52,0,0,0-.53.18,0.74,0.74,0,0,0-.16.23,0.58,0.58,0,0,0,0,.28,1.58,1.58,0,0,0,.1.39c0.06,0.15.14,0.34,0.23,0.56s0.27,0.63.37,0.93a3.33,3.33,0,0,1,.18.88,2.21,2.21,0,0,1-.1.82A2,2,0,0,1,217.19,305.72Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="wallet-espace-client">

		<path class="cls-7" d="M454.2,436.75l87.22,21.87a221.35,221.35,0,0,0,2-94.3l-87.71,18.59A131.71,131.71,0,0,1,454.2,436.75Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M490.52,401.49l1.13-4.6,1.68,0-2,6.62-1.67,0-1.24-5-1.45,5-1.67,0-1.7-6.7,1.68,0,0.93,4.64,1.34-4.59,1.82,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M497.49,402.39l-2.55-.05-0.44,1.28-1.69,0,2.62-6.61,1.8,0,2.34,6.71-1.69,0Zm-2.1-1.34,1.71,0-0.79-2.83Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M502.12,397.11L502,402.47l2.45,0.05,0,1.3-4.09-.08,0.14-6.66Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M507,397.22l-0.11,5.36,2.45,0.05,0,1.3-4.09-.08,0.14-6.66Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M510.32,397.28l4.15,0.09,0,1.3-2.51-.05,0,1.34,2.39,0,0,1.3-2.39,0,0,1.42,2.54,0.05,0,1.3-4.18-.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M520.34,397.49l0,1.3-1.79,0-0.11,5.36-1.64,0,0.11-5.36-1.79,0,0-1.3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M472.91,415.29q-0.84-.52-1.5-1a3.46,3.46,0,0,1-1.21.71,3.86,3.86,0,0,1-1.23.17,3.4,3.4,0,0,1-.92-0.13,2.2,2.2,0,0,1-.7-0.34,1.46,1.46,0,0,1-.44-0.54,1.62,1.62,0,0,1-.15-0.74,1.36,1.36,0,0,1,.11-0.53,1.76,1.76,0,0,1,.28-0.44,3,3,0,0,1,.41-0.39l0.5-.37a4.09,4.09,0,0,1-.68-0.79,1.59,1.59,0,0,1-.25-0.91,1.69,1.69,0,0,1,.15-0.69,1.57,1.57,0,0,1,.42-0.55,1.92,1.92,0,0,1,.67-0.35,2.91,2.91,0,0,1,.91-0.11,2.73,2.73,0,0,1,.79.13,1.86,1.86,0,0,1,.61.32,1.45,1.45,0,0,1,.39.49,1.38,1.38,0,0,1,.13.64,1.52,1.52,0,0,1-.11.54,2,2,0,0,1-.26.46,3.08,3.08,0,0,1-.37.41l-0.42.38,0.61,0.47,0.6,0.44q0.21-.29.41-0.64l0.44-.77,1.15,0.62q-0.24.46-.47,0.84a8,8,0,0,1-.46.7l1.3,0.86ZM469.14,414a1.83,1.83,0,0,0,1.23-.45l-0.69-.52L469,412.5a3.24,3.24,0,0,0-.45.4,0.68,0.68,0,0,0-.18.46,0.63,0.63,0,0,0,.18.48A0.86,0.86,0,0,0,469.14,414Zm-0.55-4a0.86,0.86,0,0,0,.13.47,2.08,2.08,0,0,0,.38.45,4.87,4.87,0,0,0,.54-0.47,0.72,0.72,0,0,0,.22-0.51,0.46,0.46,0,0,0-.17-0.38,0.78,0.78,0,0,0-.92,0A0.56,0.56,0,0,0,468.59,410Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M480.71,414l-2.55-.05-0.44,1.28-1.69,0,2.62-6.61,1.8,0,2.34,6.71-1.69,0Zm-2.1-1.34,1.71,0-0.79-2.83Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M483.7,408.73l2,0a5,5,0,0,1,1.61.28,3.83,3.83,0,0,1,1.24.72,3.13,3.13,0,0,1-.1,4.78,3.8,3.8,0,0,1-1.27.66,5,5,0,0,1-1.62.22l-2,0Zm2,5.4a2.8,2.8,0,0,0,.91-0.12,2,2,0,0,0,.71-0.38,1.76,1.76,0,0,0,.47-0.63,2.37,2.37,0,0,0,0-1.71,1.75,1.75,0,0,0-.44-0.64,2.07,2.07,0,0,0-.7-0.42,2.78,2.78,0,0,0-.91-0.16h-0.49l-0.08,4.06h0.49Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M493.78,415.6l-1.84,0-2.11-6.71,1.72,0,1.34,5.1,1.56-5,1.72,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M500.23,414.45l-2.55-.05-0.44,1.28-1.69,0,2.62-6.61,1.8,0,2.34,6.71-1.69,0Zm-2.1-1.34,1.71,0L499,410.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M507.41,415.89l-2.67-4-0.08,4-1.58,0,0.14-6.66,1.58,0,2.67,4,0.08-4,1.58,0L509,415.92Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M515.14,409.39l0,1.3-1.79,0L513.21,416l-1.64,0,0.11-5.36-1.79,0,0-1.3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M519.48,414.85l-2.55-.05-0.44,1.28-1.69,0,2.62-6.61,1.8,0,2.34,6.71-1.69,0Zm-2.1-1.34,1.71,0-0.79-2.83Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M525.6,409.49a5.17,5.17,0,0,1,2.17.45l-0.25,1.23a4.85,4.85,0,0,0-1.79-.38,2.91,2.91,0,0,0-.9.11,1.74,1.74,0,0,0-.69.39,1.81,1.81,0,0,0-.46.69,3,3,0,0,0-.18,1,2.09,2.09,0,0,0,.51,1.58,2.21,2.21,0,0,0,1.59.56,2.69,2.69,0,0,0,.44,0,2.07,2.07,0,0,0,.35-0.08l0-1.86,1.47,0L527.85,416a4.55,4.55,0,0,1-1.1.33,6.86,6.86,0,0,1-1.39.11,4.3,4.3,0,0,1-1.49-.28,3.21,3.21,0,0,1-1.11-.71,3,3,0,0,1-.68-1.07,3.64,3.64,0,0,1-.22-1.37,4.12,4.12,0,0,1,.27-1.45,3.06,3.06,0,0,1,.74-1.11,3.2,3.2,0,0,1,1.17-.7A4.4,4.4,0,0,1,525.6,409.49Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M529.18,409.68l4.15,0.09,0,1.3L530.8,411l0,1.34,2.39,0,0,1.3-2.39,0,0,1.42,2.54,0.05,0,1.3-4.18-.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M538.76,414.55a2,2,0,0,1-.19.82,1.94,1.94,0,0,1-.52.66,2.45,2.45,0,0,1-.81.43,3.23,3.23,0,0,1-1.07.14,5.13,5.13,0,0,1-1.1-.14,4.43,4.43,0,0,1-1-.37l0.36-1.24a4.84,4.84,0,0,0,.78.31,3.46,3.46,0,0,0,.95.14,1.37,1.37,0,0,0,.69-0.14,0.54,0.54,0,0,0,.28-0.5,0.6,0.6,0,0,0-.23-0.46,2.93,2.93,0,0,0-.85-0.45,3.81,3.81,0,0,1-1.41-.87,1.69,1.69,0,0,1-.42-1.22,1.79,1.79,0,0,1,.67-1.47,2.73,2.73,0,0,1,1.76-.47,4.62,4.62,0,0,1,1.09.15,3.55,3.55,0,0,1,.87.34l-0.31,1.22a4,4,0,0,0-.76-0.29,3.44,3.44,0,0,0-.85-0.12,1.23,1.23,0,0,0-.6.11,0.45,0.45,0,0,0-.23.42,0.65,0.65,0,0,0,0,.24,0.51,0.51,0,0,0,.14.19,1.38,1.38,0,0,0,.29.19l0.48,0.22c0.28,0.12.54,0.24,0.78,0.38a2.91,2.91,0,0,1,.63.45,1.92,1.92,0,0,1,.41.58A1.72,1.72,0,0,1,538.76,414.55Z" transform="translate(-25.28 -104.81)" />

	</g>

	<g class="selector" id="bon-plan">

		<path class="cls-7" d="M542.38,359.34a220.11,220.11,0,0,0-39.61-85.64l-70.47,55a130.12,130.12,0,0,1,22.4,49.23Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M446.7,224.4c0.8-0.4,1.7-0.6,2.7-0.6l0.4,1.5c-0.8,0-1.5,0.2-2.2,0.5c-0.3,0.1-0.7,0.4-1,0.6
		c-0.3,0.2-0.5,0.5-0.6,0.8c-0.1,0.3-0.2,0.7-0.1,1c0,0.4,0.2,0.8,0.3,1.2c0.2,0.7,0.7,1.2,1.4,1.5c0.7,0.2,1.4,0.1,2.1-0.2
		c0.2-0.1,0.3-0.2,0.5-0.3c0.1-0.1,0.2-0.2,0.4-0.3l-0.9-2.1l1.7-0.7l1.4,3.1c-0.3,0.4-0.7,0.7-1.1,0.9c-0.5,0.3-1,0.6-1.5,0.9
		c-0.6,0.3-1.2,0.4-1.8,0.5c-0.5,0-1.1,0-1.6-0.2c-0.5-0.2-1-0.5-1.3-0.8c-0.4-0.4-0.7-0.9-1-1.4c-0.3-0.6-0.4-1.2-0.4-1.8
		c0-0.6,0-1.1,0.2-1.6c0.2-0.5,0.5-1,0.9-1.4C445.6,225,446.1,224.7,446.7,224.4z"/>
		<path class="cls-3" d="M459.9,223.3c0.2,0.5,0.4,1.1,0.4,1.6c0,0.5,0,1.1-0.2,1.6c-0.2,0.5-0.5,1-0.8,1.4c-0.4,0.5-0.9,0.8-1.5,1.1
		c-0.6,0.3-1.2,0.4-1.8,0.4c-0.5,0-1.1-0.1-1.6-0.3c-0.5-0.2-0.9-0.5-1.3-0.9c-0.8-0.8-1.2-1.9-1.3-3c0-0.5,0-1.1,0.2-1.6
		c0.2-0.5,0.5-1,0.8-1.4c0.4-0.5,0.9-0.8,1.5-1.1c0.6-0.3,1.2-0.4,1.8-0.4c0.5,0,1.1,0.1,1.6,0.3c0.5,0.2,0.9,0.5,1.3,0.9
		C459.3,222.3,459.6,222.7,459.9,223.3z M457.1,227.4c0.3-0.1,0.6-0.3,0.8-0.6c0.2-0.2,0.3-0.5,0.4-0.8c0.1-0.3,0.1-0.6,0-0.9
		c-0.1-0.7-0.4-1.4-0.9-2c-0.2-0.2-0.4-0.4-0.7-0.6c-0.3-0.1-0.6-0.2-0.9-0.2c-0.7,0-1.3,0.3-1.7,0.8c-0.2,0.2-0.3,0.5-0.4,0.8
		c-0.1,0.3-0.1,0.6,0,0.9c0.1,0.7,0.4,1.4,0.9,2c0.2,0.2,0.4,0.4,0.7,0.6c0.3,0.1,0.5,0.2,0.8,0.2
		C456.5,227.6,456.8,227.5,457.1,227.4L457.1,227.4z"/>
		<path class="cls-3" d="M468.4,219.5c0.2,0.5,0.4,1.1,0.4,1.6c0,0.5,0,1.1-0.2,1.6c-0.2,0.5-0.5,1-0.8,1.4c-0.4,0.5-0.9,0.8-1.5,1.1
		c-0.6,0.3-1.2,0.4-1.8,0.4c-0.5,0-1.1-0.1-1.6-0.3c-0.5-0.2-0.9-0.5-1.3-0.9c-0.8-0.8-1.2-1.9-1.3-3c0-0.5,0-1.1,0.2-1.6
		c0.2-0.5,0.5-1,0.8-1.4c0.9-0.9,2-1.4,3.3-1.5c0.5,0,1.1,0.1,1.6,0.3c0.5,0.2,0.9,0.5,1.3,0.9C467.8,218.5,468.1,219,468.4,219.5z
		M465.6,223.6c0.3-0.1,0.6-0.3,0.8-0.6c0.2-0.2,0.3-0.5,0.4-0.8c0.1-0.3,0.1-0.6,0-0.9c-0.1-0.4-0.2-0.7-0.3-1.1
		c-0.1-0.3-0.3-0.6-0.6-0.9c-0.2-0.2-0.4-0.4-0.7-0.6c-0.3-0.1-0.6-0.2-0.9-0.2c-0.3,0-0.6,0.1-0.9,0.2c-0.3,0.1-0.6,0.3-0.8,0.6
		c-0.2,0.2-0.3,0.5-0.4,0.8c-0.1,0.3-0.1,0.6,0,0.9c0.1,0.7,0.4,1.4,0.9,2c0.2,0.2,0.4,0.4,0.7,0.6c0.3,0.1,0.5,0.2,0.8,0.2
		C465,223.8,465.3,223.7,465.6,223.6L465.6,223.6z"/>
		<path class="cls-3" d="M467.8,215.2l2.3-1.1c0.6-0.3,1.3-0.5,2-0.5c0.6-0.1,1.2,0,1.8,0.1c2.1,0.6,3.2,2.8,2.6,4.8
		c-0.1,0.2-0.1,0.4-0.2,0.6c-0.3,0.5-0.6,1-1.1,1.4c-0.5,0.4-1.1,0.8-1.7,1.1l-2.3,1.1L467.8,215.2z M472.9,220.2
		c0.4-0.2,0.7-0.4,1-0.6c0.3-0.2,0.5-0.5,0.6-0.8c0.1-0.3,0.2-0.6,0.2-1c0-0.7-0.3-1.4-0.9-1.9c-0.2-0.2-0.5-0.4-0.8-0.5
		c-0.3-0.1-0.7-0.1-1-0.1c-0.4,0-0.7,0.1-1.1,0.3l-0.6,0.2l2,4.6L472.9,220.2z"/>
		<path class="cls-3" d="M449.4,238.9l2.3-0.9c0.7-0.3,1.3-0.4,1.9-0.5c0.6,0,1.2,0,1.7,0.2c0.5,0.2,1,0.4,1.4,0.8
		c0.4,0.4,0.7,0.8,0.9,1.3c0.2,0.5,0.3,1.1,0.3,1.6c0,0.5-0.2,1.1-0.4,1.5c-0.3,0.5-0.6,0.9-1.1,1.3c-0.5,0.4-1,0.7-1.7,1l-2.3,0.9
		L449.4,238.9z M454.2,243.9c0.4-0.2,0.7-0.3,0.9-0.6c0.3-0.2,0.5-0.5,0.6-0.8c0.1-0.3,0.2-0.6,0.2-0.9c0-0.3-0.1-0.7-0.2-1
		s-0.3-0.6-0.6-0.9c-0.2-0.2-0.5-0.4-0.8-0.5c-0.3-0.1-0.6-0.1-1-0.1c-0.3,0-0.7,0.1-1.1,0.3l-0.5,0.2l1.8,4.5L454.2,243.9z"/>
		<path class="cls-3" d="M457.1,235.8l4.6-1.9l0.6,1.4l-2.8,1.1l0.6,1.5l2.7-1.1l0.6,1.4l-2.7,1.1l0.6,1.6l2.8-1.2l0.6,1.4l-4.6,1.9
		L457.1,235.8z"/>
		<path class="cls-3" d="M470,237.5l-2.8,1.2l0.1,1.6l-1.9,0.8l-0.3-8.5l2-0.8l5.8,6.3l-1.9,0.8L470,237.5z M467,237l1.9-0.8l-2.2-2.7
		L467,237z"/>
		<path class="cls-3" d="M472.5,229.4l2.4,5.9l2.7-1.1l0.6,1.4l-4.5,1.9l-3-7.4L472.5,229.4z"/>
		<path class="cls-3" d="M483.1,231.1c0.1,0.3,0.2,0.7,0.2,1c0,0.3-0.1,0.7-0.3,1c-0.2,0.3-0.4,0.6-0.7,0.9c-0.3,0.3-0.7,0.5-1.1,0.7
		c-0.4,0.2-0.8,0.3-1.3,0.4c-0.4,0.1-0.9,0.1-1.3,0.1l-0.2-1.5c0.3,0,0.6,0,1,0c0.4,0,0.7-0.1,1.1-0.3c0.3-0.1,0.5-0.3,0.7-0.5
		c0.2-0.2,0.2-0.4,0.1-0.7c-0.1-0.2-0.2-0.3-0.5-0.4c-0.2-0.1-0.6-0.1-1.1-0.1c-0.8,0-1.5-0.1-2-0.3c-0.5-0.2-0.8-0.6-1.1-1.1
		c-0.3-0.7-0.3-1.4,0-1.9c0.3-0.6,0.9-1,1.7-1.4c0.4-0.2,0.8-0.3,1.3-0.4c0.4-0.1,0.8-0.1,1.1,0l0.2,1.5c-0.3,0-0.6,0-1,0
		c-0.3,0-0.7,0.1-1,0.3c-0.3,0.1-0.5,0.2-0.6,0.4c-0.1,0.2-0.1,0.4-0.1,0.6c0,0.1,0.1,0.2,0.2,0.2c0.1,0.1,0.1,0.1,0.2,0.1
		c0.1,0,0.2,0.1,0.4,0.1c0.2,0,0.4,0,0.6,0c0.4,0,0.7,0,1,0c0.3,0,0.6,0.1,0.9,0.2c0.3,0.1,0.5,0.2,0.7,0.4
		C482.8,230.6,483,230.8,483.1,231.1z"/>
	</g>
	<g class="selector" id="mini-site">
		<path class="cls-7" d="M429.51,325.05l70-56.44a222.42,222.42,0,0,0-74.26-58.7l-37.64,80.43A131.29,131.29,0,0,1,429.51,325.05Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M416.77,286.59l-1.2,1.48-6-4.85,1.2-1.48Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M420.91,281.48l-5.58-.42,3.58,2.89-1.15,1.42-6-4.85,1.15-1.42,5.58,0.42-3.57-2.89,1.15-1.42,6,4.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M420.45,269.79l1.17,0.95-1.3,1.61,4.83,3.91-1.2,1.48-4.83-3.91-1.3,1.61-1.17-.95Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M421,269.08l3-3.74,1.17,0.95-1.83,2.26,1.21,1,1.74-2.15,1.17,0.95-1.74,2.15,1.28,1,1.85-2.29,1.17,0.95-3,3.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M430.45,261.32a2.2,2.2,0,0,1,.78,1.19,2.68,2.68,0,0,1-.08,1.5l4-.06-1.47,1.82-3.41,0L430,266l2.08,1.68-1.2,1.48-6-4.85,1.54-1.91a5.61,5.61,0,0,1,1-.94,3.36,3.36,0,0,1,1-.54A2.09,2.09,0,0,1,430.45,261.32Zm-1.12,1.44a0.84,0.84,0,0,0-.85-0.17,2.18,2.18,0,0,0-.91.73l-0.34.42,1.68,1.36,0.29-.36a2.05,2.05,0,0,0,.52-1.12A1,1,0,0,0,429.33,262.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M438.7,259.5l-5.58-.42L436.7,262l-1.15,1.42-6-4.85,1.15-1.42,5.58,0.42-3.57-2.89,1.15-1.42,6,4.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M434.81,252.05l3-3.74,1.17,0.95-1.83,2.26,1.21,1,1.74-2.15,1.17,0.95-1.74,2.15,1.28,1,1.85-2.29,1.17,0.95-3,3.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M442.11,243l1.17,0.95L442,245.59l4.83,3.91-1.2,1.48-4.83-3.91-1.3,1.61-1.17-.95Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M419.75,296.63a2.2,2.2,0,0,1,.78,1.19,2.68,2.68,0,0,1-.08,1.5l4-.06-1.47,1.82-3.41,0-0.22.27,2.08,1.68-1.2,1.48-6-4.85,1.54-1.91a5.57,5.57,0,0,1,1-.94,3.34,3.34,0,0,1,1-.54A2.1,2.1,0,0,1,419.75,296.63Zm-1.12,1.44a0.84,0.84,0,0,0-.85-0.17,2.18,2.18,0,0,0-.91.73l-0.34.42,1.68,1.36,0.29-.36a2.06,2.06,0,0,0,.52-1.12A1,1,0,0,0,418.62,298.08Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M418.84,293.86l3-3.74,1.17,0.95-1.83,2.26,1.21,1,1.74-2.15,1.17,0.95-1.74,2.15,1.28,1,1.85-2.29,1.17,0.95-3,3.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M428.71,293.93l-6-4.85,3.05-3.77,1.17,0.95-1.86,2.3,1.36,1.1,1.77-2.19,1.17,0.95-1.77,2.19,2.3,1.86Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M426.46,284.45l3-3.74,1.17,0.95-1.83,2.26,1.21,1,1.74-2.15,1.17,0.95-1.74,2.15,1.28,1,1.85-2.29,1.17,0.95-3,3.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M435.89,276.69a2.2,2.2,0,0,1,.78,1.19,2.69,2.69,0,0,1-.08,1.5l4-.06-1.47,1.82-3.41,0-0.22.27,2.08,1.68-1.2,1.48-6-4.85,1.55-1.91a5.64,5.64,0,0,1,1-.94,3.34,3.34,0,0,1,1-.54A2.09,2.09,0,0,1,435.89,276.69Zm-1.12,1.44a0.84,0.84,0,0,0-.86-0.17,2.18,2.18,0,0,0-.91.73l-0.34.42,1.68,1.36,0.29-.36a2,2,0,0,0,.52-1.12A1,1,0,0,0,434.77,278.13Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M435,273.92l3-3.74,1.17,0.95-1.83,2.26,1.21,1,1.74-2.15,1.17,0.95-1.74,2.15,1.28,1,1.85-2.29L444,275l-3,3.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M448,270.09l-5.58-.42,3.58,2.89L444.86,274l-6-4.85,1.15-1.42,5.58,0.42L442,265.24l1.15-1.42,6,4.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M446.39,259.61a6.59,6.59,0,0,1,.85-0.87,4,4,0,0,1,.92-0.6l0.91,1.09a5.2,5.2,0,0,0-.69.47,4.16,4.16,0,0,0-.63.63,2.68,2.68,0,0,0-.7,1.84,2.91,2.91,0,0,0,2.91,2.36,2.68,2.68,0,0,0,1.65-1.06,4.08,4.08,0,0,0,.48-0.75,5,5,0,0,0,.32-0.77l1.26,0.66a4.08,4.08,0,0,1-.4,1,6.48,6.48,0,0,1-.67,1,4.94,4.94,0,0,1-1.36,1.21,3.61,3.61,0,0,1-1.46.48,3.5,3.5,0,0,1-1.48-.18,4.94,4.94,0,0,1-2.49-2,3.5,3.5,0,0,1-.48-1.41,3.59,3.59,0,0,1,.17-1.53A4.92,4.92,0,0,1,446.39,259.61Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M455.92,260.33l-1.2,1.48-6-4.85,1.2-1.48Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M460.05,255.22l-5.58-.42,3.57,2.89-1.15,1.42-6-4.85,1.15-1.42,5.58,0.42-3.57-2.89,1.15-1.42,6,4.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M458.49,244.67a6,6,0,0,1,2-1.67l0.95,1.1a5.64,5.64,0,0,0-1.62,1.37,3.36,3.36,0,0,0-.54.9,2,2,0,0,0-.13.91,2.09,2.09,0,0,0,.31.91,3.44,3.44,0,0,0,.8.88,2.43,2.43,0,0,0,1.81.65,2.57,2.57,0,0,0,1.65-1.06,3.11,3.11,0,0,0,.29-0.42,2.25,2.25,0,0,0,.18-0.38l-1.67-1.36,1.07-1.32,2.47,2a5.28,5.28,0,0,1-.49,1.24,8,8,0,0,1-.89,1.35,5,5,0,0,1-1.31,1.17,3.71,3.71,0,0,1-1.43.51,3.41,3.41,0,0,1-1.46-.13,4.22,4.22,0,0,1-1.41-.77,4.76,4.76,0,0,1-1.13-1.28,3.54,3.54,0,0,1-.49-1.46,3.72,3.72,0,0,1,.19-1.56A5.09,5.09,0,0,1,458.49,244.67Z" transform="translate(-25.28 -104.81)" />

	</g>
	<g class="selector" id="avis-client">

		<path class="cls-7" d="M329.14,627.82a219.62,219.62,0,0,0,85.23-18.17l-36.88-82.76A130.24,130.24,0,0,1,329.14,537v90.86Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M368.94,550.71a6.48,6.48,0,0,1,.11,1.21,4.07,4.07,0,0,1-.13,1.09H367.5a5.18,5.18,0,0,0,.09-0.82,4.13,4.13,0,0,0-.07-0.89,2.69,2.69,0,0,0-1-1.72,2.91,2.91,0,0,0-3.68.7,2.68,2.68,0,0,0-.26,1.95,4.07,4.07,0,0,0,.26.85,5,5,0,0,0,.38.73l-1.32.53a4.08,4.08,0,0,1-.52-1,6.5,6.5,0,0,1-.34-1.17,4.94,4.94,0,0,1,0-1.82,3.62,3.62,0,0,1,.58-1.43,3.51,3.51,0,0,1,1.09-1,4.95,4.95,0,0,1,3.14-.6,3.5,3.5,0,0,1,1.39.54,3.62,3.62,0,0,1,1.06,1.12A5,5,0,0,1,368.94,550.71Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M370.31,558.68l0.35,1.87-4.85.92a2.74,2.74,0,0,1-2.27-.45,4.11,4.11,0,0,1-.9-4.72A2.74,2.74,0,0,1,364.6,555l4.85-.92L369.8,556l-4.91.93a1.36,1.36,0,0,0-.93.53,1.67,1.67,0,0,0,.38,2,1.36,1.36,0,0,0,1.05.15Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M366.57,568a2.28,2.28,0,0,1-1,0,2.25,2.25,0,0,1-.87-0.43,2.85,2.85,0,0,1-.68-0.81,3.71,3.71,0,0,1-.42-1.18,6,6,0,0,1-.1-1.28,5.21,5.21,0,0,1,.18-1.24l1.49,0.11a5.5,5.5,0,0,0-.17,1,4,4,0,0,0,.06,1.11,1.59,1.59,0,0,0,.33.75,0.62,0.62,0,0,0,.63.2,0.7,0.7,0,0,0,.47-0.37,3.41,3.41,0,0,0,.31-1.07,4.43,4.43,0,0,1,.65-1.81,2,2,0,0,1,1.29-.77,2.07,2.07,0,0,1,1.82.4,3.17,3.17,0,0,1,1,1.89,5.39,5.39,0,0,1,.09,1.27,4.07,4.07,0,0,1-.18,1.07l-1.46-.05a4.65,4.65,0,0,0,.15-0.93,4,4,0,0,0-.07-1,1.42,1.42,0,0,0-.27-0.65,0.52,0.52,0,0,0-.53-0.16,0.74,0.74,0,0,0-.27.1,0.59,0.59,0,0,0-.19.21,1.59,1.59,0,0,0-.14.37c0,0.16-.09.35-0.14,0.59s-0.15.67-.24,1a3.33,3.33,0,0,1-.36.82,2.21,2.21,0,0,1-.56.61A2,2,0,0,1,366.57,568Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M373.11,573.4l-1.48.28-0.39-2-6.1,1.16-0.35-1.87,6.1-1.16-0.39-2,1.48-.28Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M370.85,582.23a4.42,4.42,0,0,1-1.57,0,3.76,3.76,0,0,1-1.39-.52,3.64,3.64,0,0,1-1.08-1.05,4.54,4.54,0,0,1-.62-3.28,3.62,3.62,0,0,1,.62-1.38,3.67,3.67,0,0,1,1.1-1,4.72,4.72,0,0,1,3-.58,3.68,3.68,0,0,1,1.39.51,3.62,3.62,0,0,1,1.08,1.06,4.54,4.54,0,0,1,.62,3.28,3.66,3.66,0,0,1-.62,1.37,3.77,3.77,0,0,1-1.1,1A4.42,4.42,0,0,1,370.85,582.23Zm-3.18-3.41a2,2,0,0,0,.35.83,1.81,1.81,0,0,0,.63.54,2.2,2.2,0,0,0,.85.23,4,4,0,0,0,2-.37,2.23,2.23,0,0,0,.7-0.53,1.82,1.82,0,0,0,.39-0.73,2.2,2.2,0,0,0-.33-1.74,1.83,1.83,0,0,0-.63-0.53,2.22,2.22,0,0,0-.85-0.23,3.76,3.76,0,0,0-1,.06,3.72,3.72,0,0,0-1,.31,2.22,2.22,0,0,0-.7.53,1.84,1.84,0,0,0-.39.72A2,2,0,0,0,367.67,578.83Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M369.45,588.92l-0.34-1.8,2.76-2.06-4.29.58-0.35-1.83,7.69-.87,0.32,1.68L371,587.7l5,1.31,0.32,1.68-7.47,2-0.35-1.83,4.2-1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M376.72,592.47l0.9,4.72-1.48.28-0.54-2.86-1.52.29,0.52,2.72-1.48.28-0.52-2.72-1.62.31,0.55,2.89-1.48.28-0.9-4.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M376.55,604.69a2.2,2.2,0,0,1-1.41-.17,2.69,2.69,0,0,1-1.09-1l-2.51,3.06-0.44-2.3,2.23-2.58-0.06-.34-2.63.5L370.29,600l7.58-1.44,0.46,2.41a5.63,5.63,0,0,1,.1,1.34,3.36,3.36,0,0,1-.25,1.13A2.1,2.1,0,0,1,376.55,604.69Zm-0.37-1.79a0.84,0.84,0,0,0,.68-0.54,2.18,2.18,0,0,0,0-1.16l-0.1-.53-2.13.4,0.09,0.46a2.06,2.06,0,0,0,.52,1.12A1,1,0,0,0,376.18,602.9Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M374.88,611.77a2.3,2.3,0,0,1-1,0,2.24,2.24,0,0,1-.87-0.43,2.83,2.83,0,0,1-.68-0.81,3.71,3.71,0,0,1-.42-1.18,5.93,5.93,0,0,1-.1-1.28,5.19,5.19,0,0,1,.18-1.24l1.49,0.11a5.59,5.59,0,0,0-.17,1,4,4,0,0,0,.06,1.11,1.59,1.59,0,0,0,.33.75,0.62,0.62,0,0,0,.63.2,0.69,0.69,0,0,0,.47-0.37,3.42,3.42,0,0,0,.31-1.07,4.43,4.43,0,0,1,.65-1.81,2,2,0,0,1,1.29-.77,2.07,2.07,0,0,1,1.82.4,3.17,3.17,0,0,1,1,1.89,5.39,5.39,0,0,1,.09,1.27,4.07,4.07,0,0,1-.18,1.07l-1.46-.05a4.64,4.64,0,0,0,.15-0.93,4,4,0,0,0-.07-1,1.42,1.42,0,0,0-.27-0.65,0.52,0.52,0,0,0-.53-0.16,0.74,0.74,0,0,0-.27.1,0.59,0.59,0,0,0-.18.21,1.61,1.61,0,0,0-.14.37c0,0.16-.09.35-0.14,0.59s-0.15.67-.24,1a3.36,3.36,0,0,1-.36.82,2.21,2.21,0,0,1-.56.61A2,2,0,0,1,374.88,611.77Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M353.43,565a4.42,4.42,0,0,1-1.57,0,3.77,3.77,0,0,1-1.39-.52,3.64,3.64,0,0,1-1.08-1.05,4.54,4.54,0,0,1-.62-3.28,3.62,3.62,0,0,1,.62-1.38,3.69,3.69,0,0,1,1.1-1,4.74,4.74,0,0,1,3-.58,3.69,3.69,0,0,1,1.39.51,3.63,3.63,0,0,1,1.08,1.06,4.18,4.18,0,0,1,.63,1.59,4.13,4.13,0,0,1,0,1.7,3.64,3.64,0,0,1-.62,1.37,3.75,3.75,0,0,1-1.1,1A4.44,4.44,0,0,1,353.43,565Zm-3.18-3.41a2,2,0,0,0,.35.83,1.82,1.82,0,0,0,.63.54,2.2,2.2,0,0,0,.85.23,3.72,3.72,0,0,0,1-.06,3.8,3.8,0,0,0,1-.31,2.22,2.22,0,0,0,.7-0.53,1.82,1.82,0,0,0,.39-0.73,2.2,2.2,0,0,0-.33-1.73,1.84,1.84,0,0,0-.63-0.53,2.22,2.22,0,0,0-.85-0.23,4,4,0,0,0-2,.37,2.22,2.22,0,0,0-.7.53,1.84,1.84,0,0,0-.39.72A2,2,0,0,0,350.25,561.56Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M356,571.32a2.25,2.25,0,0,1-1.13-.06,2.4,2.4,0,0,1-.94-0.57,3.5,3.5,0,0,1-.7-1,5.21,5.21,0,0,1-.43-1.32l0-.19-2.56.49-0.35-1.87,7.58-1.44,0.41,2.18a5.74,5.74,0,0,1,.1,1.35,3.19,3.19,0,0,1-.25,1.13,2.14,2.14,0,0,1-.64.83A2.38,2.38,0,0,1,356,571.32Zm-0.35-1.8q1-.2.76-1.69l-0.06-.33-2.15.41,0.06,0.31a1.86,1.86,0,0,0,.51,1.07A1,1,0,0,0,355.65,569.53Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M351.42,575.09l-0.35-1.87,7.58-1.44,0.35,1.87Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M352.65,581.56l3.92-4-4.52.86-0.34-1.8,7.58-1.44,0.34,1.8-3.92,4,4.52-.86,0.34,1.8L353,583.36Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M353.64,586.76l-0.35-1.87,7.58-1.44,0.35,1.87Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M359.12,595a4.46,4.46,0,0,1-1.57,0,3.78,3.78,0,0,1-1.39-.52,3.64,3.64,0,0,1-1.08-1.05,4.14,4.14,0,0,1-.62-1.58,4.18,4.18,0,0,1,0-1.7,3.62,3.62,0,0,1,.62-1.38,3.68,3.68,0,0,1,1.1-1,4.72,4.72,0,0,1,3-.58,3.68,3.68,0,0,1,1.39.51,3.63,3.63,0,0,1,1.08,1.06,4.18,4.18,0,0,1,.63,1.59,4.13,4.13,0,0,1,0,1.7,3.66,3.66,0,0,1-.62,1.37,3.77,3.77,0,0,1-1.1,1A4.46,4.46,0,0,1,359.12,595Zm-3.18-3.41a2,2,0,0,0,.35.83,1.82,1.82,0,0,0,.63.54,2.22,2.22,0,0,0,.85.23,3.68,3.68,0,0,0,1-.06,3.72,3.72,0,0,0,1-.31,2.21,2.21,0,0,0,.7-0.53,1.82,1.82,0,0,0,.39-0.73,2.2,2.2,0,0,0-.33-1.73,1.83,1.83,0,0,0-.63-0.53A2.22,2.22,0,0,0,359,589a4,4,0,0,0-2,.37,2.23,2.23,0,0,0-.7.53,1.83,1.83,0,0,0-.39.72A2,2,0,0,0,355.94,591.56Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M356.48,601.74l3.92-4-4.52.86-0.34-1.8,7.58-1.44,0.34,1.8-3.92,4,4.52-.86,0.34,1.8-7.58,1.44Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M360.31,609.56a2.29,2.29,0,0,1-1,0,2.25,2.25,0,0,1-.87-0.43,2.84,2.84,0,0,1-.68-0.81,3.73,3.73,0,0,1-.42-1.18,5.9,5.9,0,0,1-.1-1.28,5.17,5.17,0,0,1,.18-1.24l1.49,0.1a5.62,5.62,0,0,0-.17,1,4,4,0,0,0,.06,1.11,1.59,1.59,0,0,0,.33.75,0.62,0.62,0,0,0,.63.2,0.7,0.7,0,0,0,.47-0.37,3.41,3.41,0,0,0,.31-1.07,4.43,4.43,0,0,1,.65-1.81,2,2,0,0,1,1.29-.77,2.07,2.07,0,0,1,1.82.4,3.16,3.16,0,0,1,1,1.89,5.39,5.39,0,0,1,.09,1.27,4.06,4.06,0,0,1-.18,1.07l-1.46-.05a4.58,4.58,0,0,0,.15-0.93,4,4,0,0,0-.06-1,1.41,1.41,0,0,0-.27-0.65,0.52,0.52,0,0,0-.53-0.16,0.73,0.73,0,0,0-.27.1,0.58,0.58,0,0,0-.19.21,1.58,1.58,0,0,0-.14.37q-0.06.24-.14,0.59c-0.07.34-.15,0.67-0.24,1a3.34,3.34,0,0,1-.36.82,2.2,2.2,0,0,1-.56.61A2,2,0,0,1,360.31,609.56Z" transform="translate(-25.28 -104.81)" />

	</g>

	<g class="selector" id="plannings-rdv">

		<path class="cls-7" d="M382.63,523.78l35.3,83.42A222.17,222.17,0,0,0,494,552l-68-59.28A131.12,131.12,0,0,1,382.63,523.78Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M423.05,524.37a2,2,0,0,1-.77.35,1.93,1.93,0,0,1-.84,0,2.44,2.44,0,0,1-.84-0.38,3.23,3.23,0,0,1-.77-0.76,5.13,5.13,0,0,1-.56-1,4.51,4.51,0,0,1-.32-1l1.2-.47a4.79,4.79,0,0,0,.23.81,3.47,3.47,0,0,0,.46.84,1.38,1.38,0,0,0,.53.46,0.54,0.54,0,0,0,.57-0.08,0.6,0.6,0,0,0,.23-0.46,3,3,0,0,0-.16-0.95,3.81,3.81,0,0,1-.17-1.65,1.69,1.69,0,0,1,.71-1.08,1.79,1.79,0,0,1,1.57-.37,2.73,2.73,0,0,1,1.45,1.11,4.63,4.63,0,0,1,.55,1,3.47,3.47,0,0,1,.26.9l-1.16.5a4,4,0,0,0-.23-0.78,3.48,3.48,0,0,0-.42-0.75,1.24,1.24,0,0,0-.45-0.41,0.45,0.45,0,0,0-.48.07,0.68,0.68,0,0,0-.17.18,0.5,0.5,0,0,0-.07.23,1.36,1.36,0,0,0,0,.34q0,0.21.11,0.51c0.08,0.29.13,0.58,0.18,0.85a2.87,2.87,0,0,1,0,.78,1.9,1.9,0,0,1-.21.68A1.72,1.72,0,0,1,423.05,524.37Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M429.42,524.94a5.57,5.57,0,0,1,.54.9,3.51,3.51,0,0,1,.31.9l-1.11.52a4.27,4.27,0,0,0-.24-0.67,3.56,3.56,0,0,0-.38-0.67,2.32,2.32,0,0,0-1.38-1,2.51,2.51,0,0,0-2.6,1.92,2.32,2.32,0,0,0,.53,1.61,3.57,3.57,0,0,0,.52.56,4.33,4.33,0,0,0,.57.43l-0.83.91a3.51,3.51,0,0,1-.77-0.56,5.65,5.65,0,0,1-.7-0.78,4.25,4.25,0,0,1-.71-1.4,3.11,3.11,0,0,1-.08-1.33,3,3,0,0,1,.47-1.19,4.25,4.25,0,0,1,2.22-1.64,3,3,0,0,1,1.28-.1,3.15,3.15,0,0,1,1.24.47A4.3,4.3,0,0,1,429.42,524.94Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M428.15,534.66l2.18-1.61-1.45-2-2.18,1.61-1-1.32,5.36-3.95,1,1.32-2.1,1.55,1.45,2,2.1-1.55,1,1.32L429.13,536Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M435.26,533.08l2.46,3.34-1,.77-1.49-2-1.08.79,1.42,1.92-1,.77-1.42-1.92-1.14.84,1.51,2-1,.77L429.9,537Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M438.41,537.35l1.2,1.63a5,5,0,0,1,.76,1.45,3.84,3.84,0,0,1,.19,1.42,3.13,3.13,0,0,1-3.85,2.83,3.82,3.82,0,0,1-1.3-.6,5,5,0,0,1-1.16-1.15L433,541.3Zm-3.05,4.89a2.77,2.77,0,0,0,.65.65,2.06,2.06,0,0,0,.74.33,1.75,1.75,0,0,0,.78,0,2.36,2.36,0,0,0,1.38-1,1.75,1.75,0,0,0,.24-0.74,2.07,2.07,0,0,0-.1-0.81,2.81,2.81,0,0,0-.43-0.82l-0.29-.39-3.27,2.41Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M444.88,546.14l1,1.32L442.42,550a2.36,2.36,0,0,1-1.93.5,3.55,3.55,0,0,1-2.46-3.34,2.37,2.37,0,0,1,1-1.7l3.43-2.53,1,1.32L440,546.79a1.18,1.18,0,0,0-.52.76,1.44,1.44,0,0,0,1,1.42,1.18,1.18,0,0,0,.88-0.27Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M447.58,549.81L443.26,553l1.45,2-1,.77-2.43-3.29,5.36-3.95Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M449.53,552.45l2.46,3.34-1,.77-1.49-2-1.08.79,1.42,1.93-1,.77-1.42-1.92-1.14.84,1.51,2-1,.77-2.48-3.37Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M451.5,563a2,2,0,0,1-.77.35,1.92,1.92,0,0,1-.84,0,2.44,2.44,0,0,1-.84-0.38,3.21,3.21,0,0,1-.77-0.76,5.08,5.08,0,0,1-.55-1,4.48,4.48,0,0,1-.32-1l1.2-.48a4.8,4.8,0,0,0,.23.81,3.47,3.47,0,0,0,.46.84,1.36,1.36,0,0,0,.53.46,0.54,0.54,0,0,0,.57-0.08,0.6,0.6,0,0,0,.23-0.46,3,3,0,0,0-.16-0.95,3.81,3.81,0,0,1-.17-1.65,1.7,1.7,0,0,1,.71-1.08,1.79,1.79,0,0,1,1.57-.37,2.73,2.73,0,0,1,1.45,1.11,4.64,4.64,0,0,1,.55,1,3.49,3.49,0,0,1,.26.9l-1.16.5a4,4,0,0,0-.23-0.78,3.47,3.47,0,0,0-.42-0.75,1.22,1.22,0,0,0-.45-0.4,0.44,0.44,0,0,0-.48.07,0.66,0.66,0,0,0-.17.17,0.51,0.51,0,0,0-.07.23,1.33,1.33,0,0,0,0,.34c0,0.14.07,0.31,0.12,0.51s0.13,0.58.18,0.85a2.86,2.86,0,0,1,0,.78,1.91,1.91,0,0,1-.21.68A1.71,1.71,0,0,1,451.5,563Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M407.33,524.44l-1.51-2.05-1.28.43-1-1.36,6.83-2,1.07,1.45-3.89,5.95-1-1.36ZM407.11,522l1,1.38,1.76-2.35Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M414.47,528.74a2,2,0,0,1-.9.38,2.05,2.05,0,0,1-.94-0.09,3.06,3.06,0,0,1-.92-0.51,4.51,4.51,0,0,1-.83-0.87l-0.1-.14L409,528.84l-1-1.32,5.36-3.95,1.14,1.55a4.89,4.89,0,0,1,.59,1,2.75,2.75,0,0,1,.23,1,1.85,1.85,0,0,1-.19.88A2.06,2.06,0,0,1,414.47,528.74Zm-0.95-1.27q0.74-.54,0-1.6l-0.17-.23-1.52,1.12L412,527a1.6,1.6,0,0,0,.8.64A0.85,0.85,0,0,0,413.53,527.48Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M417.79,533.24a2,2,0,0,1-.9.38,2.09,2.09,0,0,1-.94-0.09A3,3,0,0,1,415,533a4.49,4.49,0,0,1-.83-0.87l-0.1-.14-1.81,1.33-1-1.32,5.36-3.95,1.14,1.55a4.93,4.93,0,0,1,.59,1,2.74,2.74,0,0,1,.23,1,1.85,1.85,0,0,1-.19.88A2.06,2.06,0,0,1,417.79,533.24ZM416.84,532q0.74-.55,0-1.6l-0.17-.23-1.52,1.12,0.16,0.22a1.61,1.61,0,0,0,.8.64A0.85,0.85,0,0,0,416.84,532Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M421.22,539.86a3.83,3.83,0,0,1-1.21.61,3.23,3.23,0,0,1-1.28.11,3.12,3.12,0,0,1-1.23-.42,3.55,3.55,0,0,1-1.08-1,3.6,3.6,0,0,1-.64-1.33,3.13,3.13,0,0,1,0-1.3,3.17,3.17,0,0,1,.49-1.18,4.08,4.08,0,0,1,2.15-1.58,3.22,3.22,0,0,1,1.27-.12,3.13,3.13,0,0,1,1.23.42,3.6,3.6,0,0,1,1.08,1,3.56,3.56,0,0,1,.63,1.32,3.14,3.14,0,0,1,0,1.3,3.24,3.24,0,0,1-.49,1.18A3.84,3.84,0,0,1,421.22,539.86Zm-3.75-1.46a1.71,1.71,0,0,0,.58.52,1.56,1.56,0,0,0,.69.18,1.94,1.94,0,0,0,.75-0.13,3.45,3.45,0,0,0,1.39-1,1.91,1.91,0,0,0,.35-0.67,1.58,1.58,0,0,0,0-.71,1.73,1.73,0,0,0-.32-0.71,1.71,1.71,0,0,0-.58-0.52,1.59,1.59,0,0,0-.68-0.18,1.87,1.87,0,0,0-.75.13,3.46,3.46,0,0,0-1.39,1,1.91,1.91,0,0,0-.35.67,1.59,1.59,0,0,0,0,.71A1.72,1.72,0,0,0,417.47,538.39Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M420.12,544l-1-1.32,5.36-3.95,1,1.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M423.49,548.56l1.57-4.57-3.2,2.35-0.94-1.27,5.36-3.95,0.94,1.27L425.66,547l3.2-2.35,0.94,1.27-5.36,3.95Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M433.35,550.73l-1,.77-1.06-1.44-4.32,3.18-1-1.32,4.32-3.18-1.06-1.44,1-.77Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M432,558.27L431,557l1.38-2.63-3.12,2.05-1-1.3,5.66-3.55,0.88,1.19-2.14,4,4.42-.87,0.88,1.19-5.07,4.35-1-1.3,2.89-2.37Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M438.95,558.32l2.46,3.34-1,.77-1.49-2-1.08.79,1.42,1.93-1,.77L436.75,562l-1.14.84,1.51,2-1,.77-2.48-3.37Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M439.3,570l1.57-4.57-3.2,2.35-0.94-1.27,5.36-3.95,0.94,1.27-1.57,4.57,3.2-2.35,0.94,1.27-5.36,3.95Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M449.16,572.19l-1,.77-1.06-1.44-4.32,3.18-1-1.32,4.32-3.18L445,568.76l1-.77Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M448.46,579.12a2,2,0,0,1-.77.35,2,2,0,0,1-.84,0,2.47,2.47,0,0,1-.84-0.38,3.24,3.24,0,0,1-.77-0.76,5.17,5.17,0,0,1-.56-1,4.5,4.5,0,0,1-.32-1l1.2-.48a4.75,4.75,0,0,0,.23.81,3.41,3.41,0,0,0,.46.84,1.37,1.37,0,0,0,.53.46,0.54,0.54,0,0,0,.57-0.08,0.6,0.6,0,0,0,.23-0.46,2.92,2.92,0,0,0-.16-0.95,3.81,3.81,0,0,1-.17-1.65,1.69,1.69,0,0,1,.71-1.08,1.79,1.79,0,0,1,1.57-.37,2.73,2.73,0,0,1,1.45,1.11,4.65,4.65,0,0,1,.55,1,3.49,3.49,0,0,1,.26.9l-1.16.5a4,4,0,0,0-.23-0.78,3.5,3.5,0,0,0-.42-0.75,1.23,1.23,0,0,0-.45-0.41,0.45,0.45,0,0,0-.48.07,0.67,0.67,0,0,0-.17.17,0.51,0.51,0,0,0-.07.23,1.38,1.38,0,0,0,0,.34c0,0.14.06,0.31,0.12,0.51s0.13,0.58.18,0.85a2.89,2.89,0,0,1,0,.78,1.9,1.9,0,0,1-.21.68A1.71,1.71,0,0,1,448.46,579.12Z" transform="translate(-25.28 -104.81)" />

	</g>
	';
}else{
	// svg francais
	$schemat = '
	<g class="selector" id="center">
		<path class="cls-2" d="M269.45,318.77a8.31,8.31,0,0,1,1.35-.72,5.1,5.1,0,0,1,1.33-.38l0.68,1.65a6.31,6.31,0,0,0-1,.3,5.22,5.22,0,0,0-1,.51,3.37,3.37,0,0,0-1.52,1.95A3.66,3.66,0,0,0,271.9,326a3.36,3.36,0,0,0,2.38-.65,5.06,5.06,0,0,0,.86-0.72,6.28,6.28,0,0,0,.67-0.8l1.26,1.27a5.07,5.07,0,0,1-.87,1.08,8.14,8.14,0,0,1-1.19,1,6.21,6.21,0,0,1-2.09.94,4.53,4.53,0,0,1-1.94,0,4.41,4.41,0,0,1-1.71-.77,6.21,6.21,0,0,1-2.23-3.35,4.39,4.39,0,0,1-.06-1.87,4.54,4.54,0,0,1,.78-1.78A6.21,6.21,0,0,1,269.45,318.77Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M275.87,315.11l3.78,6.84,3.13-1.72,0.92,1.66-5.22,2.88-4.69-8.5Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M291.16,313.22a5.61,5.61,0,0,1,.47,1.92,4.75,4.75,0,0,1-.25,1.85,4.58,4.58,0,0,1-1,1.62,5.71,5.71,0,0,1-3.86,1.68,4.57,4.57,0,0,1-1.86-.37,4.65,4.65,0,0,1-1.52-1.08,5.94,5.94,0,0,1-1.56-3.57,4.64,4.64,0,0,1,.24-1.85,4.56,4.56,0,0,1,1-1.62,5.72,5.72,0,0,1,3.86-1.68,4.56,4.56,0,0,1,1.86.38,4.72,4.72,0,0,1,1.53,1.08A5.61,5.61,0,0,1,291.16,313.22Zm-3.29,4.86a2.51,2.51,0,0,0,.92-0.66,2.29,2.29,0,0,0,.48-0.92,2.79,2.79,0,0,0,.05-1.1,5,5,0,0,0-1-2.31,2.8,2.8,0,0,0-.84-0.71,2.31,2.31,0,0,0-1-.27,2.77,2.77,0,0,0-2,.89,2.33,2.33,0,0,0-.48.92,2.78,2.78,0,0,0-.05,1.1,5,5,0,0,0,1,2.31,2.78,2.78,0,0,0,.84.71,2.3,2.3,0,0,0,1,.27A2.5,2.5,0,0,0,287.87,318.08Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M296.8,306.26l2.28-.73,1.89,5.92a3.44,3.44,0,0,1-.21,2.9,5.17,5.17,0,0,1-5.77,1.84,3.44,3.44,0,0,1-1.85-2.24L291.25,308l2.28-.73,1.91,6a1.54,1.54,0,0,0,2.16,1.1,1.74,1.74,0,0,0,1.09-.81,1.71,1.71,0,0,0,0-1.34Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M301.63,304.86l2.9-.6a7.28,7.28,0,0,1,2.38-.12,5.62,5.62,0,0,1,2,.61,4.5,4.5,0,0,1,2.28,3.09,4.49,4.49,0,0,1-.86,3.74,5.58,5.58,0,0,1-1.59,1.36,7.29,7.29,0,0,1-2.23.84l-2.9.61Zm4.64,7a4.06,4.06,0,0,0,1.26-.47,3,3,0,0,0,.89-0.78,2.54,2.54,0,0,0,.46-1,3.45,3.45,0,0,0-.51-2.44,2.55,2.55,0,0,0-.84-0.77,3,3,0,0,0-1.13-.36,4.07,4.07,0,0,0-1.34.07l-0.7.15,1.21,5.8Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M322.47,308.67a2.87,2.87,0,0,1-.16,1.22,2.83,2.83,0,0,1-.65,1,3.57,3.57,0,0,1-1.11.75A4.71,4.71,0,0,1,319,312a7.54,7.54,0,0,1-1.61,0,6.5,6.5,0,0,1-1.52-.39l0.33-1.86a7,7,0,0,0,1.18.33,5,5,0,0,0,1.4.07,2,2,0,0,0,1-.31,0.78,0.78,0,0,0,.33-0.77,0.88,0.88,0,0,0-.4-0.63,4.27,4.27,0,0,0-1.29-.53,5.57,5.57,0,0,1-2.18-1.06,2.47,2.47,0,0,1-.8-1.71,2.6,2.6,0,0,1,.75-2.23,4,4,0,0,1,2.49-1,6.81,6.81,0,0,1,1.6.05,5.14,5.14,0,0,1,1.32.37l-0.26,1.82a5.81,5.81,0,0,0-1.15-.31,5.05,5.05,0,0,0-1.26-.05,1.79,1.79,0,0,0-.85.25,0.65,0.65,0,0,0-.28.64,0.93,0.93,0,0,0,.09.34,0.72,0.72,0,0,0,.23.26,2,2,0,0,0,.45.23l0.72,0.25c0.42,0.13.82,0.27,1.19,0.43a4.2,4.2,0,0,1,1,.56,2.8,2.8,0,0,1,.69.78A2.49,2.49,0,0,1,322.47,308.67Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M323.85,301.82h6.05v1.9h-3.66v2h3.49v1.9h-3.49v2.07H330v1.9h-6.1Zm1.69-.85,1.09-1.82h2.67L327.41,301h-1.87Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M337,302.11a8.2,8.2,0,0,1,1.51.27,5.1,5.1,0,0,1,1.28.53l-0.5,1.72a6.35,6.35,0,0,0-1-.38,5,5,0,0,0-1.1-.22,3.37,3.37,0,0,0-2.4.58,3.66,3.66,0,0,0-.4,4.7,3.36,3.36,0,0,0,2.27,1,5.15,5.15,0,0,0,1.12,0,6.68,6.68,0,0,0,1-.21l0.2,1.78a5.1,5.1,0,0,1-1.35.31,8.27,8.27,0,0,1-1.53,0,6.23,6.23,0,0,1-2.22-.57,4.55,4.55,0,0,1-1.54-1.18,4.4,4.4,0,0,1-.86-1.67,6.21,6.21,0,0,1,.34-4,4.4,4.4,0,0,1,1.12-1.5,4.54,4.54,0,0,1,1.72-.91A6.23,6.23,0,0,1,337,302.11Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M347.75,303.86l2.35,0.43L349,310.41a3.44,3.44,0,0,1-1.56,2.46,5.17,5.17,0,0,1-6-1.1,3.44,3.44,0,0,1-.58-2.85l1.12-6.11,2.35,0.43-1.13,6.18a1.71,1.71,0,0,0,.2,1.33,2.1,2.1,0,0,0,2.52.46,1.71,1.71,0,0,0,.66-1.17Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M358.83,309.9a2.77,2.77,0,0,1-1,1.49,3.38,3.38,0,0,1-1.77.64l2,4.56-2.83-.81-1.63-4-0.42-.12-0.93,3.24-2.3-.66,2.69-9.33,3,0.85a7,7,0,0,1,1.56.65,4.18,4.18,0,0,1,1.13.92A2.63,2.63,0,0,1,358.83,309.9Zm-2.23-.6a1.06,1.06,0,0,0-.22-1.07,2.74,2.74,0,0,0-1.29-.7l-0.66-.19L353.68,310l0.56,0.16a2.58,2.58,0,0,0,1.55.05A1.22,1.22,0,0,0,356.6,309.3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M361,317.77l-2.25-.82,3.33-9.12,2.25,0.82Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M369.72,318.28a2.9,2.9,0,0,1-.71,1,2.84,2.84,0,0,1-1.07.6,3.57,3.57,0,0,1-1.33.13,4.69,4.69,0,0,1-1.53-.4,7.47,7.47,0,0,1-1.4-.8,6.59,6.59,0,0,1-1.16-1.06l1.17-1.48a7,7,0,0,0,.88.86,5,5,0,0,0,1.2.72,2,2,0,0,0,1,.2,0.79,0.79,0,0,0,.66-0.52,0.88,0.88,0,0,0,0-.75,4.32,4.32,0,0,0-.89-1.08,5.57,5.57,0,0,1-1.42-2,2.46,2.46,0,0,1,.11-1.89,2.6,2.6,0,0,1,1.71-1.61,4,4,0,0,1,2.64.34,6.73,6.73,0,0,1,1.38.8,5.09,5.09,0,0,1,1,.95l-1.09,1.48A5.76,5.76,0,0,0,370,313a5,5,0,0,0-1.08-.64,1.78,1.78,0,0,0-.87-0.18,0.65,0.65,0,0,0-.55.44,1,1,0,0,0-.09.34,0.73,0.73,0,0,0,.08.34,1.9,1.9,0,0,0,.29.41l0.52,0.56q0.46,0.47.85,0.94a4.2,4.2,0,0,1,.6,1,2.78,2.78,0,0,1,.24,1A2.48,2.48,0,0,1,369.72,318.28Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-2" d="M374.17,312.89l5.34,2.85-0.89,1.67-3.23-1.72-0.92,1.72,3.08,1.64-0.89,1.67-3.08-1.64-1,1.83,3.27,1.74L375,324.33l-5.38-2.87Zm1.89,0,1.82-1.09,2.35,1.26-2.53.72Z" transform="translate(-25.28 -104.81)" />
		<circle class="cls-2" cx="301.77" cy="301.44" r="79.79" />
		<path class="cls-3" d="M312.13,385.1a10.78,10.78,0,0,1,2,.19,6.73,6.73,0,0,1,1.74.55l-0.46,2.31a8.5,8.5,0,0,0-1.31-.39,6.81,6.81,0,0,0-1.47-.16,4.44,4.44,0,0,0-3.09,1,4.82,4.82,0,0,0,0,6.21,4.44,4.44,0,0,0,3.09,1,6.75,6.75,0,0,0,1.47-.16,8.34,8.34,0,0,0,1.31-.39l0.46,2.31a6.73,6.73,0,0,1-1.74.55,10.78,10.78,0,0,1-2,.19,8.2,8.2,0,0,1-3-.5,6,6,0,0,1-2.15-1.38,5.79,5.79,0,0,1-1.31-2.09,8.16,8.16,0,0,1,0-5.3A5.79,5.79,0,0,1,307,387a6,6,0,0,1,2.15-1.38A8.19,8.19,0,0,1,312.13,385.1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M330.25,391.72a7.34,7.34,0,0,1-.44,2.56,6.2,6.2,0,0,1-1.28,2.1,6,6,0,0,1-2,1.43,7.52,7.52,0,0,1-5.54,0,6,6,0,0,1-2.05-1.43,6.09,6.09,0,0,1-1.27-2.1,7.82,7.82,0,0,1,0-5.12,6.11,6.11,0,0,1,1.27-2.1,6,6,0,0,1,2.05-1.43,7.52,7.52,0,0,1,5.54,0,6,6,0,0,1,2,1.43,6.22,6.22,0,0,1,1.28,2.1A7.35,7.35,0,0,1,330.25,391.72Zm-6.52,4.13a3.3,3.3,0,0,0,1.46-.31,3,3,0,0,0,1.07-.85,3.68,3.68,0,0,0,.64-1.3,6.64,6.64,0,0,0,0-3.32,3.7,3.7,0,0,0-.64-1.3,3,3,0,0,0-1.07-.85,3.64,3.64,0,0,0-2.93,0,3,3,0,0,0-1.06.85,3.67,3.67,0,0,0-.64,1.3,6.64,6.64,0,0,0,0,3.32,3.66,3.66,0,0,0,.64,1.3,3,3,0,0,0,1.06.85A3.28,3.28,0,0,0,323.72,395.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M341.78,389.38a3.64,3.64,0,0,1-.72,2.25,4.43,4.43,0,0,1-2,1.46l4.2,5h-3.88l-3.51-4.43h-0.58v4.43h-3.15V385.33h4.07a9.25,9.25,0,0,1,2.21.25,5.53,5.53,0,0,1,1.77.75A3.48,3.48,0,0,1,341.78,389.38Zm-3,.06a1.39,1.39,0,0,0-.67-1.28,3.59,3.59,0,0,0-1.88-.41h-0.9v3.59h0.77a3.39,3.39,0,0,0,2-.5A1.62,1.62,0,0,0,338.75,389.44Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M344.41,385.33h8v2.49h-4.82v2.57h4.59v2.49h-4.59v2.72h4.87v2.5h-8V385.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M277.93,408.36v2.49h-3.44v10.29h-3.15V410.85h-3.44v-2.49h10Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M279.44,408.36h8v2.49h-4.82v2.57h4.59v2.49h-4.59v2.72h4.87v2.5h-8V408.36Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M295.91,408.13a10.78,10.78,0,0,1,2,.19,6.73,6.73,0,0,1,1.74.55l-0.46,2.31a8.45,8.45,0,0,0-1.31-.39,6.75,6.75,0,0,0-1.47-.16,4.44,4.44,0,0,0-3.09,1,4.82,4.82,0,0,0,0,6.21,4.44,4.44,0,0,0,3.09,1,6.75,6.75,0,0,0,1.47-.16,8.34,8.34,0,0,0,1.31-.39l0.46,2.31a6.73,6.73,0,0,1-1.74.55,10.78,10.78,0,0,1-2,.19,8.2,8.2,0,0,1-3-.5,6,6,0,0,1-2.15-1.38,5.79,5.79,0,0,1-1.31-2.09,8.16,8.16,0,0,1,0-5.3,5.79,5.79,0,0,1,1.31-2.09,6,6,0,0,1,2.15-1.38A8.18,8.18,0,0,1,295.91,408.13Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M309.49,421.14v-5.2h-4.68v5.2h-3.15V408.36h3.15v5h4.68v-5h3.15v12.78h-3.15Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M323.45,421.14l-5.28-7.62v7.62h-3V408.36h3l5.28,7.62v-7.62h3v12.78h-3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M341.43,414.75a7.34,7.34,0,0,1-.44,2.56,6.2,6.2,0,0,1-1.28,2.1,6,6,0,0,1-2,1.43,7.52,7.52,0,0,1-5.54,0,6,6,0,0,1-2.05-1.43,6.09,6.09,0,0,1-1.27-2.1,7.82,7.82,0,0,1,0-5.12,6.11,6.11,0,0,1,1.27-2.1,6,6,0,0,1,2.05-1.43,7.53,7.53,0,0,1,5.54,0,6.06,6.06,0,0,1,2,1.43,6.22,6.22,0,0,1,1.28,2.1A7.34,7.34,0,0,1,341.43,414.75Zm-6.52,4.13a3.3,3.3,0,0,0,1.46-.31,3,3,0,0,0,1.06-.85,3.67,3.67,0,0,0,.64-1.3,6.63,6.63,0,0,0,0-3.32,3.68,3.68,0,0,0-.64-1.31,3,3,0,0,0-1.06-.85,3.65,3.65,0,0,0-2.93,0,3,3,0,0,0-1.06.85,3.66,3.66,0,0,0-.64,1.31,6.63,6.63,0,0,0,0,3.32,3.65,3.65,0,0,0,.64,1.3,3,3,0,0,0,1.06.85A3.28,3.28,0,0,0,334.9,418.88Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M346.47,408.36v10.29h4.7v2.5h-7.85V408.36h3.15Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M365,414.75a7.34,7.34,0,0,1-.44,2.56,6.2,6.2,0,0,1-1.28,2.1,6,6,0,0,1-2,1.43,7.52,7.52,0,0,1-5.54,0,6,6,0,0,1-2.05-1.43,6.09,6.09,0,0,1-1.27-2.1,7.82,7.82,0,0,1,0-5.12,6.11,6.11,0,0,1,1.27-2.1,6,6,0,0,1,2.05-1.43,7.53,7.53,0,0,1,5.54,0,6.06,6.06,0,0,1,2,1.43,6.22,6.22,0,0,1,1.28,2.1A7.34,7.34,0,0,1,365,414.75Zm-6.52,4.13a3.3,3.3,0,0,0,1.46-.31,3,3,0,0,0,1.06-.85,3.67,3.67,0,0,0,.64-1.3,6.63,6.63,0,0,0,0-3.32,3.68,3.68,0,0,0-.64-1.31,3,3,0,0,0-1.06-.85,3.65,3.65,0,0,0-2.93,0,3,3,0,0,0-1.06.85,3.66,3.66,0,0,0-.64,1.31,6.63,6.63,0,0,0,0,3.32,3.65,3.65,0,0,0,.64,1.3,3,3,0,0,0,1.06.85A3.28,3.28,0,0,0,358.5,418.88Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M373.45,408.13a10,10,0,0,1,4.18.78l-0.42,2.37a9.32,9.32,0,0,0-3.45-.65,5.58,5.58,0,0,0-1.73.25,3.35,3.35,0,0,0-1.31.78,3.5,3.5,0,0,0-.84,1.34,5.7,5.7,0,0,0-.3,1.95,4,4,0,0,0,1,3,4.25,4.25,0,0,0,3.08,1,5.11,5.11,0,0,0,.84-0.07,4.06,4.06,0,0,0,.67-0.16v-3.57H378v5.27a8.83,8.83,0,0,1-2.1.67,13.25,13.25,0,0,1-2.66.26,8.23,8.23,0,0,1-2.88-.47,6.14,6.14,0,0,1-2.15-1.31,5.64,5.64,0,0,1-1.35-2,7,7,0,0,1-.47-2.62,7.89,7.89,0,0,1,.47-2.79,5.86,5.86,0,0,1,1.37-2.15,6.16,6.16,0,0,1,2.21-1.38A8.43,8.43,0,0,1,373.45,408.13Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M386.21,416.5v4.64h-3.15V416.5l-4.2-8.14h3.36l2.42,5.37,2.42-5.37h3.36Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="sites-applis">
		<path class="cls-4" d="M628.83,406.25A301.76,301.76,0,0,0,343,104.9v57.67c127.26,8.24,228.28,114.37,228.28,243.68S470.26,641.69,343,649.93V707.6A301.77,301.77,0,0,0,628.83,406.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M501.1,205.82a5.45,5.45,0,0,1-1.82,1.45,5.31,5.31,0,0,1-2.25.54,6.76,6.76,0,0,1-2.49-.45,8.88,8.88,0,0,1-2.56-1.53,14.15,14.15,0,0,1-2.13-2.19,12.32,12.32,0,0,1-1.55-2.54l2.9-2.08a13.34,13.34,0,0,0,1.16,2,9.53,9.53,0,0,0,1.8,1.93,3.8,3.8,0,0,0,1.74.88,1.48,1.48,0,0,0,1.46-.6,1.65,1.65,0,0,0,.3-1.38,8.17,8.17,0,0,0-1.05-2.43,10.52,10.52,0,0,1-1.56-4.31,4.67,4.67,0,0,1,1.18-3.37,4.92,4.92,0,0,1,3.95-2,7.53,7.53,0,0,1,4.63,2,12.86,12.86,0,0,1,2.1,2.18,9.65,9.65,0,0,1,1.3,2.24l-2.76,2.12a11,11,0,0,0-1.14-1.93,9.61,9.61,0,0,0-1.63-1.73,3.38,3.38,0,0,0-1.48-.78,1.23,1.23,0,0,0-1.22.51,1.78,1.78,0,0,0-.34.58,1.38,1.38,0,0,0,0,.66,3.64,3.64,0,0,0,.31.9q0.24,0.52.65,1.29,0.59,1.1,1,2.15a7.91,7.91,0,0,1,.59,2.06,5.24,5.24,0,0,1-.1,2A4.72,4.72,0,0,1,501.1,205.82Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M502.8,214.79l-3.36-3,12.31-13.63,3.36,3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M527.55,213.19L525,215.74l-3.51-3.47-10.38,10.52-3.22-3.18L518.3,209.1l-3.51-3.47,2.52-2.55Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M529.54,215.34l7.68,8.48-2.66,2.41-4.65-5.13-2.74,2.48,4.42,4.88L529,230.87,524.53,226l-2.9,2.63,4.7,5.19-2.66,2.41-7.74-8.54Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M537.39,244.3a5.44,5.44,0,0,1-2.07,1.06,5.34,5.34,0,0,1-2.31.08,6.76,6.76,0,0,1-2.35-.93,8.87,8.87,0,0,1-2.21-2,14.19,14.19,0,0,1-1.66-2.56,12.38,12.38,0,0,1-1-2.79l3.25-1.46a13.27,13.27,0,0,0,.74,2.2,9.51,9.51,0,0,0,1.38,2.25,3.8,3.8,0,0,0,1.53,1.21,1.48,1.48,0,0,0,1.55-.3,1.65,1.65,0,0,0,.57-1.29,8.13,8.13,0,0,0-.55-2.59,10.53,10.53,0,0,1-.68-4.53,4.68,4.68,0,0,1,1.82-3.08,4.93,4.93,0,0,1,4.27-1.21,7.53,7.53,0,0,1,4.14,2.87,12.8,12.8,0,0,1,1.63,2.55,9.66,9.66,0,0,1,.83,2.45l-3.12,1.54a11,11,0,0,0-.74-2.12,9.63,9.63,0,0,0-1.26-2,3.37,3.37,0,0,0-1.3-1.06,1.23,1.23,0,0,0-1.3.26,1.76,1.76,0,0,0-.44.5,1.37,1.37,0,0,0-.16.64,3.77,3.77,0,0,0,.13.94q0.14,0.56.38,1.39,0.37,1.19.6,2.32a7.85,7.85,0,0,1,.17,2.14,5.24,5.24,0,0,1-.48,1.9A4.7,4.7,0,0,1,537.39,244.3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M552.39,265.61L564.6,261l2.55,3.87L549,270.71l-2.53-3.85L556,256.17l-13.56,4.47-2.53-3.85,12.55-14.33,2.55,3.87-9.08,9.38,12.52-4.15,2.76,4.19Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M568.55,267.8l5.51,10-3.14,1.73-3.33-6.07-3.24,1.78,3.17,5.78-3.14,1.73L561.21,277l-3.43,1.88,3.37,6.14L558,286.74l-5.55-10.1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M569.8,299.31a4.36,4.36,0,0,1-2.23.45,4.7,4.7,0,0,1-2.15-.74,8,8,0,0,1-2-1.81,14,14,0,0,1-1.69-2.75l-2.45-5.16,16.59-7.87,2.83,6a9.82,9.82,0,0,1,.84,2.55,6.24,6.24,0,0,1,0,2.17,4.15,4.15,0,0,1-.7,1.69,3.43,3.43,0,0,1-1.34,1.11,4.17,4.17,0,0,1-3,.35,5.75,5.75,0,0,1-2.69-1.79,7.41,7.41,0,0,1,.27,1.79,5.11,5.11,0,0,1-.22,1.65,3.94,3.94,0,0,1-.77,1.39A4.17,4.17,0,0,1,569.8,299.31Zm-1.6-4.19a1.78,1.78,0,0,0,1.08-1.44,4.29,4.29,0,0,0-.51-2.34L568.14,290l-3.89,1.84,0.69,1.45a3.48,3.48,0,0,0,1.59,1.86A2,2,0,0,0,568.21,295.13Zm3.54-5.37a3.19,3.19,0,0,0,1.32,1.51,1.81,1.81,0,0,0,1.72,0,1.63,1.63,0,0,0,1-1.18,2.94,2.94,0,0,0-.33-1.84l-0.66-1.4-3.59,1.7Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M585.36,458.22l1.3-6.91-3.29-1.77,0.86-4.58L601,455.06l-0.91,4.88-19.31,3.34,0.86-4.58Zm4.61-5.11-0.87,4.63,8-.88Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M589,481a5.37,5.37,0,0,1-2.38-1.26,5.71,5.71,0,0,1-1.45-2.18,8.32,8.32,0,0,1-.51-2.85,12.37,12.37,0,0,1,.41-3.28l0.12-.45-6-1.54,1.12-4.38,17.79,4.55-1.31,5.13a13.56,13.56,0,0,1-1.14,3,7.59,7.59,0,0,1-1.68,2.19,5.15,5.15,0,0,1-2.21,1.14A5.7,5.7,0,0,1,589,481Zm1.05-4.23q2.46,0.63,3.35-2.87l0.2-.77-5-1.29-0.18.72a4.43,4.43,0,0,0,0,2.82A2.33,2.33,0,0,0,590,476.76Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M584.2,496.11a5.37,5.37,0,0,1-2.3-1.4,5.7,5.7,0,0,1-1.32-2.26,8.38,8.38,0,0,1-.34-2.87,12.46,12.46,0,0,1,.6-3.25l0.14-.45L575.07,484l1.38-4.31,17.5,5.58-1.61,5a13.63,13.63,0,0,1-1.32,3,7.58,7.58,0,0,1-1.81,2.09,5.12,5.12,0,0,1-2.28,1A5.7,5.7,0,0,1,584.2,496.11Zm1.3-4.16q2.42,0.77,3.51-2.67l0.24-.76-5-1.58-0.23.71a4.42,4.42,0,0,0-.15,2.82A2.33,2.33,0,0,0,585.5,492Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M587.28,504.86l-13.81-5.26-2.4,6.31-3.35-1.28,4-10.54,17.16,6.53Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M565,510.83l1.78-4.16,16.88,7.24-1.78,4.16Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M563.33,527.31a5.43,5.43,0,0,1-1.84-1.42,5.35,5.35,0,0,1-1.06-2.06,6.76,6.76,0,0,1-.16-2.53,8.85,8.85,0,0,1,.87-2.86,14.13,14.13,0,0,1,1.62-2.59,12.22,12.22,0,0,1,2.09-2.12l2.71,2.32a13.32,13.32,0,0,0-1.68,1.61,9.56,9.56,0,0,0-1.45,2.21,3.78,3.78,0,0,0-.44,1.9,1.48,1.48,0,0,0,.93,1.27,1.65,1.65,0,0,0,1.41,0,8.11,8.11,0,0,0,2.1-1.6,10.52,10.52,0,0,1,3.81-2.54,4.67,4.67,0,0,1,3.56.34,4.93,4.93,0,0,1,2.92,3.35,7.52,7.52,0,0,1-.83,5,12.82,12.82,0,0,1-1.61,2.56,9.67,9.67,0,0,1-1.86,1.8l-2.72-2.17a11.1,11.1,0,0,0,1.6-1.57,9.56,9.56,0,0,0,1.28-2,3.37,3.37,0,0,0,.4-1.63,1.23,1.23,0,0,0-.79-1.07,1.75,1.75,0,0,0-.64-0.19,1.37,1.37,0,0,0-.65.13,3.64,3.64,0,0,0-.8.52q-0.45.36-1.1,0.94-0.92.84-1.84,1.53a8,8,0,0,1-1.86,1.06,5.27,5.27,0,0,1-1.92.37A4.68,4.68,0,0,1,563.33,527.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M550.82,544.13l2.26-3.73,8.15,0.75-8.51-5.8,2.3-3.8,15,10.7-2.12,3.49-12.34-1.29,6.85,10.34-2.12,3.49-16.42-8.34,2.3-3.8,9.08,4.86Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M539.26,571.89a10.59,10.59,0,0,1-2.6-2.68,8.92,8.92,0,0,1-1.36-3.26,8.65,8.65,0,0,1,.07-3.58,10.82,10.82,0,0,1,4.68-6.43,8.65,8.65,0,0,1,3.4-1.17,8.77,8.77,0,0,1,3.51.31,11.23,11.23,0,0,1,6,4.34,8.77,8.77,0,0,1,1.37,3.25,8.66,8.66,0,0,1-.08,3.59,10.81,10.81,0,0,1-4.69,6.43,8.66,8.66,0,0,1-3.39,1.16,8.93,8.93,0,0,1-3.52-.29A10.58,10.58,0,0,1,539.26,571.89ZM540,560.82a4.75,4.75,0,0,0-.88,2,4.34,4.34,0,0,0,.09,2,5.28,5.28,0,0,0,1,1.85A9.55,9.55,0,0,0,544,569.4a5.28,5.28,0,0,0,2.06.36,4.34,4.34,0,0,0,1.89-.51,5.23,5.23,0,0,0,2.48-3.4,4.37,4.37,0,0,0-.1-1.95,5.27,5.27,0,0,0-1-1.85,9.53,9.53,0,0,0-3.86-2.81,5.27,5.27,0,0,0-2.06-.36,4.38,4.38,0,0,0-1.88.5A4.73,4.73,0,0,0,540,560.82Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M525.49,582a4.36,4.36,0,0,1-1.33-1.84,4.71,4.71,0,0,1-.22-2.26,8.05,8.05,0,0,1,.83-2.54,13.9,13.9,0,0,1,1.81-2.68l3.68-4.37,14,11.83-4.25,5a9.79,9.79,0,0,1-2,1.82,6.18,6.18,0,0,1-2,.92,4.18,4.18,0,0,1-1.83.07,3.42,3.42,0,0,1-1.56-.76,4.16,4.16,0,0,1-1.54-2.54,5.76,5.76,0,0,1,.52-3.19,7.32,7.32,0,0,1-1.52,1,5.11,5.11,0,0,1-1.59.48,3.93,3.93,0,0,1-1.58-.13A4.17,4.17,0,0,1,525.49,582Zm3.15-3.19a1.78,1.78,0,0,0,1.76.38,4.28,4.28,0,0,0,1.91-1.43l1-1.14L530,573.88l-1,1.22a3.47,3.47,0,0,0-1,2.21A2,2,0,0,0,528.63,578.83Zm6.36,1a3.19,3.19,0,0,0-.83,1.83,1.81,1.81,0,0,0,.71,1.57,1.63,1.63,0,0,0,1.48.4,2.93,2.93,0,0,0,1.54-1.07l1-1.18-3-2.56Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M516.64,583.89l3.07-3.32L533.19,593l-3.07,3.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M524,602.69l-10.42-10.48L508.76,597l-2.53-2.54,8-8,13,13Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M517,609.5l-8.53,7.63-2.39-2.67,5.16-4.62-2.46-2.75-4.91,4.39-2.39-2.67,4.91-4.39-2.61-2.92-5.22,4.67-2.39-2.67,8.59-7.69Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M488,617.17a5.43,5.43,0,0,1-1-2.08,5.35,5.35,0,0,1-.07-2.31,6.77,6.77,0,0,1,.95-2.35,8.89,8.89,0,0,1,2-2.2,14.17,14.17,0,0,1,2.57-1.64,12.31,12.31,0,0,1,2.8-1l1.45,3.26a13.4,13.4,0,0,0-2.21.73,9.57,9.57,0,0,0-2.26,1.37,3.79,3.79,0,0,0-1.22,1.52,1.48,1.48,0,0,0,.29,1.55,1.65,1.65,0,0,0,1.29.57,8.14,8.14,0,0,0,2.59-.54,10.53,10.53,0,0,1,4.53-.65,4.67,4.67,0,0,1,3.06,1.84,4.93,4.93,0,0,1,1.19,4.28,7.52,7.52,0,0,1-2.9,4.12,12.87,12.87,0,0,1-2.56,1.61,9.72,9.72,0,0,1-2.45.82l-1.52-3.13a11,11,0,0,0,2.12-.73,9.55,9.55,0,0,0,2-1.25,3.37,3.37,0,0,0,1.07-1.29,1.23,1.23,0,0,0-.25-1.3,1.76,1.76,0,0,0-.5-0.45,1.36,1.36,0,0,0-.64-0.16,3.69,3.69,0,0,0-.94.12q-0.57.13-1.4,0.38c-0.8.24-1.57,0.43-2.32,0.59a7.85,7.85,0,0,1-2.13.16,5.23,5.23,0,0,1-1.9-.49A4.69,4.69,0,0,1,488,617.17Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="pc-tablette-systemes">
		<path class="cls-5" d="M82.83,406.25c0-129.95,102-236.5,230.17-243.81V104.81A301.77,301.77,0,0,0,113.67,619.64,299.59,299.59,0,0,0,313,707.69V650.06C184.85,642.75,82.83,536.2,82.83,406.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M166.14,620.82a5.37,5.37,0,0,1,2-1.77,5.72,5.72,0,0,1,2.55-.57,8.36,8.36,0,0,1,2.84.55,12.42,12.42,0,0,1,2.92,1.56l0.38,0.27,3.59-5.06,3.69,2.62-10.64,15-4.32-3.06a13.65,13.65,0,0,1-2.41-2.15,7.63,7.63,0,0,1-1.44-2.36,5.12,5.12,0,0,1-.27-2.48A5.68,5.68,0,0,1,166.14,620.82Zm3.57,2.5q-1.47,2.07,1.48,4.16l0.65,0.46,3-4.25-0.61-.43a4.43,4.43,0,0,0-2.64-1A2.33,2.33,0,0,0,169.71,623.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M152.86,618.29a15.51,15.51,0,0,1-2.07-2,9.66,9.66,0,0,1-1.44-2.19l2.6-2.16a12.14,12.14,0,0,0,1.11,1.63,9.73,9.73,0,0,0,1.49,1.51,6.38,6.38,0,0,0,4.37,1.65,6.93,6.93,0,0,0,5.61-6.93,6.39,6.39,0,0,0-2.53-3.93,9.75,9.75,0,0,0-1.79-1.14,11.86,11.86,0,0,0-1.82-.75l1.57-3a9.63,9.63,0,0,1,2.44,1,15.59,15.59,0,0,1,2.41,1.6,11.77,11.77,0,0,1,2.87,3.25,8.59,8.59,0,0,1,1.15,3.49,8.33,8.33,0,0,1-.43,3.52,11.74,11.74,0,0,1-4.79,5.92,8.32,8.32,0,0,1-3.35,1.15,8.59,8.59,0,0,1-3.65-.4A11.77,11.77,0,0,1,152.86,618.29Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M97,555.86l2.94-2,2.82,4,12.13-8.44,2.58,3.71-12.13,8.45,2.82,4-2.94,2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M102.77,537l3.66,6,3.63-.89,2.43,4L93.24,549.8l-2.58-4.24,12.12-15.4,2.43,4Zm0,6.88-2.45-4-5.4,6Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M91.12,518.88a4.36,4.36,0,0,1,2.21-.53,4.68,4.68,0,0,1,2.17.66,8,8,0,0,1,2,1.74,13.93,13.93,0,0,1,1.79,2.69l2.63,5.06L85.67,537l-3-5.85a9.78,9.78,0,0,1-.93-2.51,6.18,6.18,0,0,1-.1-2.17,4.17,4.17,0,0,1,.63-1.71,3.43,3.43,0,0,1,1.3-1.16,4.16,4.16,0,0,1,2.94-.46,5.75,5.75,0,0,1,2.75,1.69,7.37,7.37,0,0,1-.34-1.78,5.07,5.07,0,0,1,.16-1.66,3.91,3.91,0,0,1,.72-1.42A4.15,4.15,0,0,1,91.12,518.88Zm-1.59,9.63A3.19,3.19,0,0,0,88.15,527a1.81,1.81,0,0,0-1.72.05,1.64,1.64,0,0,0-.93,1.21,3,3,0,0,0,.4,1.83l0.71,1.37,3.52-1.83Zm3.34-5.5a1.78,1.78,0,0,0-1,1.48,4.29,4.29,0,0,0,.6,2.32l0.69,1.32,3.82-2-0.74-1.42a3.49,3.49,0,0,0-1.65-1.8A2,2,0,0,0,92.87,523Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M76.11,517.69l13.48-6.06-2.77-6.16L90.08,504l4.63,10.29L78,521.82Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M72.19,508.73l-4.13-10.67,3.34-1.29,2.5,6.46,3.45-1.33L75,495.74l3.34-1.29,2.38,6.15,3.65-1.41-2.53-6.53,3.34-1.29,4.16,10.75Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M62.74,481.93l3.41-1.1,1.52,4.7L81.73,481l1.39,4.3-14.07,4.54,1.52,4.7-3.41,1.1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M58.67,466.51l3.47-.9,1.24,4.78,14.3-3.73L78.83,471l-14.3,3.73,1.24,4.78-3.47.9Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M58.06,463.61L55.8,452.39l3.52-.71,1.36,6.79,3.62-.73L63,451.28l3.51-.71,1.3,6.46,3.84-.77-1.38-6.87,3.51-.71L76.06,460Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M66.17,434.72a5.43,5.43,0,0,1,2.32.15,5.34,5.34,0,0,1,2,1.11,6.76,6.76,0,0,1,1.54,2,8.86,8.86,0,0,1,.87,2.86,14.23,14.23,0,0,1,.11,3,12.47,12.47,0,0,1-.55,2.92l-3.54-.41a13.25,13.25,0,0,0,.49-2.27,9.54,9.54,0,0,0,0-2.64,3.78,3.78,0,0,0-.69-1.82,1.48,1.48,0,0,0-1.48-.54,1.65,1.65,0,0,0-1.15.82,8.14,8.14,0,0,0-.85,2.5,10.52,10.52,0,0,1-1.74,4.23,4.68,4.68,0,0,1-3.14,1.71A4.93,4.93,0,0,1,56,447.25,7.53,7.53,0,0,1,54,442.67a12.85,12.85,0,0,1-.09-3,9.7,9.7,0,0,1,.54-2.53l3.47,0.28a10.9,10.9,0,0,0-.45,2.2,9.54,9.54,0,0,0,0,2.38,3.37,3.37,0,0,0,.58,1.58,1.23,1.23,0,0,0,1.25.45,1.79,1.79,0,0,0,.64-0.2,1.37,1.37,0,0,0,.46-0.47,3.66,3.66,0,0,0,.38-0.88q0.17-.55.38-1.39,0.3-1.21.67-2.3a7.89,7.89,0,0,1,.95-1.92,5.24,5.24,0,0,1,1.39-1.38A4.7,4.7,0,0,1,66.17,434.72Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M66.8,366.7a5.43,5.43,0,0,1,2.21.74A5.34,5.34,0,0,1,70.69,369a6.77,6.77,0,0,1,1,2.33,8.9,8.9,0,0,1,.12,3,14.18,14.18,0,0,1-.67,3A12.37,12.37,0,0,1,69.85,380l-3.32-1.29a13.28,13.28,0,0,0,1.05-2.07,9.51,9.51,0,0,0,.64-2.56,3.79,3.79,0,0,0-.21-1.94,1.48,1.48,0,0,0-1.3-.9,1.65,1.65,0,0,0-1.32.5,8.12,8.12,0,0,0-1.46,2.21,10.54,10.54,0,0,1-2.76,3.66,4.67,4.67,0,0,1-3.47.86,4.92,4.92,0,0,1-3.86-2.2,7.52,7.52,0,0,1-.85-5,12.81,12.81,0,0,1,.68-2.95A9.67,9.67,0,0,1,54.84,366l3.28,1.15a11,11,0,0,0-1,2,9.53,9.53,0,0,0-.55,2.31,3.37,3.37,0,0,0,.16,1.67,1.23,1.23,0,0,0,1.09.75,1.79,1.79,0,0,0,.67,0,1.37,1.37,0,0,0,.57-0.34,3.69,3.69,0,0,0,.58-0.75q0.31-.49.72-1.25,0.6-1.1,1.23-2.05a7.88,7.88,0,0,1,1.4-1.62,5.25,5.25,0,0,1,1.69-1A4.7,4.7,0,0,1,66.8,366.7Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M67.48,355.39l6.58,1.14L73.29,361l-6.58-1.14-12.55,4L55,359.05l8.2-2.11-7-4.74L57,347.45Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M72.82,336.31a5.44,5.44,0,0,1,2.11,1,5.33,5.33,0,0,1,1.48,1.78,6.75,6.75,0,0,1,.71,2.43,8.89,8.89,0,0,1-.23,3,14.2,14.2,0,0,1-1,2.88,12.33,12.33,0,0,1-1.58,2.52l-3.15-1.67a13.24,13.24,0,0,0,1.29-1.94,9.51,9.51,0,0,0,.93-2.47,3.79,3.79,0,0,0,0-2,1.48,1.48,0,0,0-1.19-1,1.65,1.65,0,0,0-1.37.35,8.11,8.11,0,0,0-1.71,2A10.54,10.54,0,0,1,66,346.49a4.67,4.67,0,0,1-3.54.45,4.93,4.93,0,0,1-3.58-2.63,7.53,7.53,0,0,1-.27-5,12.84,12.84,0,0,1,1-2.85A9.7,9.7,0,0,1,61,334.27l3.13,1.52a11,11,0,0,0-1.22,1.88,9.59,9.59,0,0,0-.82,2.23,3.39,3.39,0,0,0,0,1.68,1.23,1.23,0,0,0,1,.87,1.78,1.78,0,0,0,.67,0,1.38,1.38,0,0,0,.6-0.27,3.71,3.71,0,0,0,.67-0.68q0.36-.45.87-1.16,0.72-1,1.46-1.9A7.9,7.9,0,0,1,68.92,337a5.26,5.26,0,0,1,1.8-.78A4.71,4.71,0,0,1,72.82,336.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M64.82,317.69l3.44,1-1.39,4.74L81,327.61l-1.27,4.34-14.18-4.17-1.39,4.74-3.44-1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M65.78,310.11l-4.45,2.22L63,307.58l3.94-.79Zm0,4.8,3.84-10.78,3.38,1.2-2.32,6.52,3.48,1.24,2.21-6.21,3.38,1.2-2.21,6.21,3.69,1.31,2.35-6.6,3.38,1.2-3.87,10.86Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M90.13,295.12l-1.76,4-8.18.31,9.19,4.66-1.79,4.06-16.25-8.68L73,295.72l12.4-.31L77.26,286l1.65-3.73,17.36,6.15-1.79,4.06-9.63-3.65Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M81.37,277.55l5.37-10.1,3.17,1.68-3.25,6.11L89.92,277l3.1-5.82,3.16,1.68-3.1,5.82,3.46,1.84,3.29-6.18L103,276l-5.41,10.18Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M106,260.39a5.44,5.44,0,0,1,1.7,1.59,5.34,5.34,0,0,1,.86,2.15,6.76,6.76,0,0,1-.08,2.53,8.89,8.89,0,0,1-1.14,2.76,14.25,14.25,0,0,1-1.85,2.42,12.33,12.33,0,0,1-2.28,1.91l-2.48-2.56a13.35,13.35,0,0,0,1.82-1.44,9.56,9.56,0,0,0,1.65-2.06,3.79,3.79,0,0,0,.62-1.85,1.48,1.48,0,0,0-.81-1.36,1.65,1.65,0,0,0-1.41-.1,8.13,8.13,0,0,0-2.25,1.4,10.54,10.54,0,0,1-4,2.17,4.68,4.68,0,0,1-3.51-.67,4.93,4.93,0,0,1-2.58-3.61,7.53,7.53,0,0,1,1.3-4.87,12.85,12.85,0,0,1,1.85-2.39,9.67,9.67,0,0,1,2-1.61l2.5,2.42a11,11,0,0,0-1.74,1.41,9.61,9.61,0,0,0-1.47,1.87,3.39,3.39,0,0,0-.56,1.58,1.23,1.23,0,0,0,.68,1.14,1.8,1.8,0,0,0,.62.25,1.38,1.38,0,0,0,.65-0.07,3.7,3.7,0,0,0,.85-0.44q0.48-.32,1.18-0.83,1-.75,2-1.35a7.87,7.87,0,0,1,2-.88,5.25,5.25,0,0,1,2-.19A4.7,4.7,0,0,1,106,260.39Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M100.3,245.6l3.31-4.52a13.74,13.74,0,0,1,3.19-3.19,10.57,10.57,0,0,1,3.58-1.67A8.63,8.63,0,0,1,121,244a10.54,10.54,0,0,1-.51,3.92,13.76,13.76,0,0,1-2.07,4l-3.3,4.52Zm15.4,4a7.72,7.72,0,0,0,1.17-2.25,5.68,5.68,0,0,0,.26-2.22,4.82,4.82,0,0,0-.67-2,6.53,6.53,0,0,0-3.81-2.78,4.85,4.85,0,0,0-2.15,0,5.69,5.69,0,0,0-2,.92,7.74,7.74,0,0,0-1.79,1.8l-0.8,1.09,9,6.61Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M112.26,229.61l7.33-8.79,2.75,2.3-4.43,5.31,2.84,2.37,4.22-5.06,2.75,2.3-4.22,5.06,3,2.51,4.49-5.38,2.75,2.3-7.38,8.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M132.54,206.54a15.46,15.46,0,0,1,2.22-1.86,9.63,9.63,0,0,1,2.32-1.22l1.9,2.8a12.1,12.1,0,0,0-1.73,1,9.73,9.73,0,0,0-1.65,1.34,6.38,6.38,0,0,0-2.07,4.19,6.93,6.93,0,0,0,6.36,6.26,6.38,6.38,0,0,0,4.16-2.13,9.76,9.76,0,0,0,1.31-1.67,12.16,12.16,0,0,0,.92-1.74l2.83,1.85a9.62,9.62,0,0,1-1.18,2.34,15.48,15.48,0,0,1-1.83,2.25,11.77,11.77,0,0,1-3.51,2.54,8.59,8.59,0,0,1-3.58.81,8.33,8.33,0,0,1-3.46-.77,11.74,11.74,0,0,1-5.42-5.34,8.33,8.33,0,0,1-.83-3.44,8.61,8.61,0,0,1,.75-3.59A11.77,11.77,0,0,1,132.54,206.54Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M157.76,203.73l-5.26,4.66,1.51,3.41-3.49,3.09-7.06-18.28,3.72-3.29,17.3,9.21L161,205.63ZM151,204.94l3.53-3.13-6.89-4.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M169.68,198.56l-3.54,2.82L154.7,187l3.54-2.82Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M178.94,184.85a5.44,5.44,0,0,1,.95,2.12,5.34,5.34,0,0,1,0,2.31,6.75,6.75,0,0,1-1.05,2.3,8.87,8.87,0,0,1-2.12,2.1,14.24,14.24,0,0,1-2.65,1.52,12.41,12.41,0,0,1-2.84.88l-1.29-3.32a13.24,13.24,0,0,0,2.24-.62,9.56,9.56,0,0,0,2.32-1.26,3.77,3.77,0,0,0,1.29-1.46,1.48,1.48,0,0,0-.22-1.56,1.65,1.65,0,0,0-1.26-.63,8.13,8.13,0,0,0-2.61.42,10.53,10.53,0,0,1-4.56.44,4.67,4.67,0,0,1-3-2,4.92,4.92,0,0,1-1-4.33,7.53,7.53,0,0,1,3.08-4,12.85,12.85,0,0,1,2.63-1.49,9.69,9.69,0,0,1,2.49-.7l1.37,3.2a11,11,0,0,0-2.15.63,9.59,9.59,0,0,0-2.08,1.15,3.37,3.37,0,0,0-1.12,1.24,1.23,1.23,0,0,0,.19,1.31,1.78,1.78,0,0,0,.48.47,1.37,1.37,0,0,0,.63.19,3.7,3.7,0,0,0,.95-0.08q0.57-.11,1.41-0.31,1.21-.3,2.35-0.48a7.93,7.93,0,0,1,2.14-.06,5.26,5.26,0,0,1,1.87.58A4.71,4.71,0,0,1,178.94,184.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M191.23,176.84a5.45,5.45,0,0,1,.83,2.17,5.34,5.34,0,0,1-.17,2.31,6.77,6.77,0,0,1-1.18,2.24,8.88,8.88,0,0,1-2.23,2,14.18,14.18,0,0,1-2.73,1.37,12.32,12.32,0,0,1-2.89.72l-1.11-3.39a13.33,13.33,0,0,0,2.27-.5,9.54,9.54,0,0,0,2.39-1.13,3.79,3.79,0,0,0,1.37-1.39,1.48,1.48,0,0,0-.13-1.57,1.65,1.65,0,0,0-1.22-.7,8.1,8.1,0,0,0-2.63.27,10.54,10.54,0,0,1-4.58.18,4.68,4.68,0,0,1-2.86-2.14,4.92,4.92,0,0,1-.74-4.38,7.53,7.53,0,0,1,3.3-3.81,12.86,12.86,0,0,1,2.71-1.34,9.71,9.71,0,0,1,2.52-.56l1.19,3.27a11,11,0,0,0-2.18.51,9.59,9.59,0,0,0-2.14,1,3.37,3.37,0,0,0-1.19,1.18,1.23,1.23,0,0,0,.12,1.32,1.77,1.77,0,0,0,.45.5,1.37,1.37,0,0,0,.62.22,3.64,3.64,0,0,0,1,0q0.57-.08,1.43-0.23,1.23-.23,2.37-0.35a7.88,7.88,0,0,1,2.14.06,5.26,5.26,0,0,1,1.84.68A4.7,4.7,0,0,1,191.23,176.84Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M187.13,164.31l9.94-5.67,1.78,3.11-6,3.43,1.83,3.21,5.73-3.26,1.77,3.11-5.73,3.26,1.94,3.4,6.08-3.47,1.77,3.11-10,5.71Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="statistiques-rapports">
		<path class="cls-6" d="M258.88,294.69L214,216.89a222.62,222.62,0,0,0-77.37,77.37l78.13,45.11A131.38,131.38,0,0,1,258.88,294.69Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M184,254.75a2.29,2.29,0,0,1-.81.54,2.25,2.25,0,0,1-1,.15,2.85,2.85,0,0,1-1-.27,3.73,3.73,0,0,1-1-.73,6,6,0,0,1-.82-1,5.2,5.2,0,0,1-.56-1.12l1.28-.77a5.6,5.6,0,0,0,.42.88,4,4,0,0,0,.69.87,1.59,1.59,0,0,0,.7.43,0.62,0.62,0,0,0,.63-0.2,0.69,0.69,0,0,0,.17-0.57,3.41,3.41,0,0,0-.36-1.05,4.42,4.42,0,0,1-.5-1.86,2,2,0,0,1,.61-1.37,2.07,2.07,0,0,1,1.72-.71,3.17,3.17,0,0,1,1.87,1,5.38,5.38,0,0,1,.8,1,4.07,4.07,0,0,1,.47,1l-1.23.79a4.62,4.62,0,0,0-.41-0.85,4,4,0,0,0-.62-0.78,1.42,1.42,0,0,0-.59-0.38,0.52,0.52,0,0,0-.53.17,0.74,0.74,0,0,0-.16.23,0.58,0.58,0,0,0,0,.27,1.57,1.57,0,0,0,.1.39q0.09,0.23.23,0.56t0.36,0.94a3.32,3.32,0,0,1,.17.88,2.2,2.2,0,0,1-.11.82A2,2,0,0,1,184,254.75Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M192.5,255.44l-1.05,1.08L190,255.07l-4.34,4.45-1.36-1.33,4.34-4.45-1.48-1.45,1.05-1.08Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M191.86,263.5l-2.11-2.06-1.38.74-1.4-1.37,7.44-3.52,1.49,1.46-3.33,7.53-1.4-1.37Zm-0.72-2.8,1.42,1.38,1.57-3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M202.12,264.82l-1.05,1.08-1.48-1.45-4.34,4.45-1.36-1.33,4.34-4.45-1.48-1.45,1.05-1.08Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M198.79,272.35L197.43,271l5.39-5.52,1.36,1.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M205.14,275.33a2.29,2.29,0,0,1-.81.54,2.25,2.25,0,0,1-1,.15,2.85,2.85,0,0,1-1-.27,3.73,3.73,0,0,1-1-.73,6,6,0,0,1-.82-1,5.2,5.2,0,0,1-.56-1.12l1.28-.77a5.6,5.6,0,0,0,.42.88,4,4,0,0,0,.69.87,1.59,1.59,0,0,0,.7.43,0.62,0.62,0,0,0,.63-0.2,0.69,0.69,0,0,0,.17-0.57,3.41,3.41,0,0,0-.36-1.05,4.42,4.42,0,0,1-.5-1.86,2,2,0,0,1,.61-1.37,2.07,2.07,0,0,1,1.72-.71,3.17,3.17,0,0,1,1.87,1,5.38,5.38,0,0,1,.8,1,4.07,4.07,0,0,1,.47,1l-1.23.79a4.62,4.62,0,0,0-.41-0.85,4,4,0,0,0-.62-0.78,1.42,1.42,0,0,0-.59-0.38,0.52,0.52,0,0,0-.53.17,0.74,0.74,0,0,0-.16.23,0.58,0.58,0,0,0,0,.27,1.57,1.57,0,0,0,.1.39q0.09,0.23.23,0.56t0.36,0.94a3.32,3.32,0,0,1,.17.88,2.2,2.2,0,0,1-.11.82A2,2,0,0,1,205.14,275.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M213.6,276l-1.05,1.08-1.48-1.45-4.34,4.45-1.36-1.33,4.34-4.45-1.48-1.45,1.05-1.08Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M210.27,283.55l-1.36-1.33,5.39-5.52,1.36,1.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M214,287.37l-0.08-.05-0.1-.08a4.17,4.17,0,0,1-1-1.4,3.63,3.63,0,0,1-.29-1.48,3.69,3.69,0,0,1,.34-1.44,4.73,4.73,0,0,1,2.16-2.21,3.69,3.69,0,0,1,1.43-.37,3.63,3.63,0,0,1,1.49.25,4.54,4.54,0,0,1,2.39,2.33,3.64,3.64,0,0,1,.28,1.48,3.76,3.76,0,0,1-.33,1.45,4.44,4.44,0,0,1-.89,1.29,4.37,4.37,0,0,1-1.67,1.09,3.49,3.49,0,0,1-1.89.14l0.47,2.17-1.59.35Zm0.88-1.22a2,2,0,0,0,.76.48,1.82,1.82,0,0,0,.82.08,2.21,2.21,0,0,0,.83-0.29,4,4,0,0,0,1.4-1.43,2.22,2.22,0,0,0,.27-0.83,1.82,1.82,0,0,0-.1-0.82,2.2,2.2,0,0,0-1.26-1.23,1.83,1.83,0,0,0-.82-0.08,2.21,2.21,0,0,0-.83.29,4,4,0,0,0-1.4,1.43,2.21,2.21,0,0,0-.27.83,1.83,1.83,0,0,0,.1.81A2,2,0,0,0,214.9,286.16Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M226.28,288.38l1.36,1.33-3.45,3.53a2.74,2.74,0,0,1-2.11.93,4.11,4.11,0,0,1-3.44-3.36,2.74,2.74,0,0,1,.88-2.14l3.45-3.53,1.36,1.33L220.83,290a1.37,1.37,0,0,0-.46,1,1.67,1.67,0,0,0,1.46,1.42,1.36,1.36,0,0,0,1-.48Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M228.7,290.75l3.44,3.36-1.05,1.08-2.08-2-1.08,1.11,2,1.93-1.05,1.08-2-1.93-1.15,1.18,2.11,2.05-1.05,1.08-3.47-3.38Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M232.95,302.45a2.29,2.29,0,0,1-.81.54,2.25,2.25,0,0,1-1,.15,2.85,2.85,0,0,1-1-.27,3.73,3.73,0,0,1-1-.73,6,6,0,0,1-.82-1,5.2,5.2,0,0,1-.56-1.12l1.28-.77a5.6,5.6,0,0,0,.42.88,4,4,0,0,0,.69.87,1.59,1.59,0,0,0,.7.43,0.62,0.62,0,0,0,.63-0.2,0.69,0.69,0,0,0,.17-0.57,3.41,3.41,0,0,0-.36-1.05,4.42,4.42,0,0,1-.5-1.86,2,2,0,0,1,.61-1.37,2.07,2.07,0,0,1,1.72-.71,3.17,3.17,0,0,1,1.87,1,5.38,5.38,0,0,1,.8,1,4.07,4.07,0,0,1,.47,1l-1.23.79a4.62,4.62,0,0,0-.41-0.85,4,4,0,0,0-.62-0.78,1.42,1.42,0,0,0-.59-0.38,0.52,0.52,0,0,0-.53.17,0.74,0.74,0,0,0-.16.23,0.58,0.58,0,0,0,0,.27,1.57,1.57,0,0,0,.1.39q0.09,0.23.23,0.56t0.36,0.94a3.32,3.32,0,0,1,.17.88,2.2,2.2,0,0,1-.11.82A2,2,0,0,1,232.95,302.45Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M183.14,269.14a2.2,2.2,0,0,1-1.26.67,2.68,2.68,0,0,1-1.48-.22l-0.3,3.94-1.67-1.63,0.35-3.4-0.25-.24-1.87,1.92-1.36-1.33,5.39-5.52,1.76,1.71a5.6,5.6,0,0,1,.85,1,3.35,3.35,0,0,1,.45,1.07A2.1,2.1,0,0,1,183.14,269.14Zm-1.33-1.25a0.84,0.84,0,0,0,.25-0.83,2.17,2.17,0,0,0-.64-1L181,265.7l-1.51,1.55,0.33,0.32a2,2,0,0,0,1.06.62A1,1,0,0,0,181.81,267.89Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M185.18,276.41l-2.11-2.06-1.38.74-1.4-1.37,7.44-3.52,1.49,1.46-3.33,7.53-1.4-1.37Zm-0.72-2.8,1.42,1.38,1.57-3Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M194.16,280a2.25,2.25,0,0,1-1,.6,2.4,2.4,0,0,1-1.1.07,3.5,3.5,0,0,1-1.14-.41,5.23,5.23,0,0,1-1.11-.84l-0.14-.14-1.82,1.87-1.36-1.33,5.39-5.52,1.59,1.55a5.72,5.72,0,0,1,.86,1.05,3.2,3.2,0,0,1,.44,1.07,2.15,2.15,0,0,1,0,1A2.39,2.39,0,0,1,194.16,280Zm-1.32-1.27q0.74-.76-0.34-1.82l-0.24-.23-1.53,1.57,0.22,0.22a1.86,1.86,0,0,0,1,.59A1,1,0,0,0,192.84,278.71Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M198.79,284.5a2.25,2.25,0,0,1-1,.6,2.4,2.4,0,0,1-1.1.07,3.5,3.5,0,0,1-1.14-.41,5.23,5.23,0,0,1-1.11-.84l-0.14-.14-1.82,1.87-1.36-1.33,5.39-5.52,1.59,1.55a5.72,5.72,0,0,1,.86,1.05,3.2,3.2,0,0,1,.44,1.07,2.15,2.15,0,0,1,0,1A2.39,2.39,0,0,1,198.79,284.5Zm-1.32-1.27q0.74-.76-0.34-1.82l-0.24-.23-1.53,1.57,0.22,0.22a1.86,1.86,0,0,0,1,.59A1,1,0,0,0,197.47,283.23Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M204,291.42a4.44,4.44,0,0,1-1.27.92,3.76,3.76,0,0,1-1.44.37,3.64,3.64,0,0,1-1.49-.24,4.54,4.54,0,0,1-2.39-2.33,3.63,3.63,0,0,1-.29-1.48,3.69,3.69,0,0,1,.34-1.44,4.73,4.73,0,0,1,2.16-2.21,3.69,3.69,0,0,1,1.43-.37,3.63,3.63,0,0,1,1.49.25,4.54,4.54,0,0,1,2.39,2.33,3.64,3.64,0,0,1,.28,1.48,3.76,3.76,0,0,1-.33,1.45A4.44,4.44,0,0,1,204,291.42Zm-4.56-1a2,2,0,0,0,.76.48,1.82,1.82,0,0,0,.82.08,2.21,2.21,0,0,0,.83-0.29,4,4,0,0,0,1.4-1.43,2.22,2.22,0,0,0,.27-0.83,1.82,1.82,0,0,0-.1-0.82,2.2,2.2,0,0,0-1.26-1.23,1.83,1.83,0,0,0-.82-0.08,2.21,2.21,0,0,0-.83.29,4,4,0,0,0-1.4,1.43,2.21,2.21,0,0,0-.27.83,1.83,1.83,0,0,0,.1.81A2,2,0,0,0,199.4,290.45Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M209.93,295.27a2.2,2.2,0,0,1-1.26.67,2.68,2.68,0,0,1-1.48-.22l-0.3,3.94L205.21,298l0.35-3.4-0.25-.24-1.87,1.92L202.08,295l5.39-5.52,1.76,1.71a5.6,5.6,0,0,1,.85,1,3.35,3.35,0,0,1,.45,1.07A2.1,2.1,0,0,1,209.93,295.27ZM208.59,294a0.84,0.84,0,0,0,.25-0.83,2.17,2.17,0,0,0-.64-1l-0.39-.38-1.51,1.55,0.33,0.32a2,2,0,0,0,1.06.62A1,1,0,0,0,208.59,294Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M216.55,298.32l-1.05,1.08L214,297.95l-4.34,4.45-1.36-1.33,4.34-4.45-1.48-1.45,1.05-1.08Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M217.05,306.36a2.29,2.29,0,0,1-.81.54,2.25,2.25,0,0,1-1,.15,2.85,2.85,0,0,1-1-.27,3.73,3.73,0,0,1-1-.73,6,6,0,0,1-.82-1,5.2,5.2,0,0,1-.56-1.12l1.28-.77a5.6,5.6,0,0,0,.42.88,4,4,0,0,0,.69.87,1.59,1.59,0,0,0,.7.43,0.62,0.62,0,0,0,.63-0.2,0.69,0.69,0,0,0,.17-0.57,3.41,3.41,0,0,0-.36-1.05,4.42,4.42,0,0,1-.5-1.86,2,2,0,0,1,.61-1.37,2.07,2.07,0,0,1,1.72-.71,3.17,3.17,0,0,1,1.87,1,5.38,5.38,0,0,1,.8,1,4.07,4.07,0,0,1,.47,1l-1.23.79a4.62,4.62,0,0,0-.41-0.85,4,4,0,0,0-.62-0.78,1.42,1.42,0,0,0-.59-0.38,0.52,0.52,0,0,0-.53.17,0.74,0.74,0,0,0-.16.23,0.58,0.58,0,0,0,0,.27,1.57,1.57,0,0,0,.1.39q0.09,0.23.23,0.56t0.36,0.94a3.32,3.32,0,0,1,.17.88,2.2,2.2,0,0,1-.11.82A2,2,0,0,1,217.05,306.36Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="jeux-concours">
		<path class="cls-6" d="M134,298.66A219.58,219.58,0,0,0,105.64,406.9c0,1.11,0,2.22.05,3.33l90.64-4.72a130.12,130.12,0,0,1,15.85-61.71Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M137.24,358.48a4.57,4.57,0,0,1-.8,1.9,6,6,0,0,1-.83.92l-1.28-1.07a5,5,0,0,0,.79-1.16,3.31,3.31,0,0,0,.31-1.35l0.2-6.18,1.9,0.06-0.19,5.87A7.24,7.24,0,0,1,137.24,358.48Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M139,351.64l4.81,0.15,0,1.51-2.91-.09,0,1.55,2.77,0.09,0,1.51-2.77-.09-0.05,1.64,2.94,0.09,0,1.51-4.84-.15Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M149.79,352l1.9,0.06L151.53,357a2.74,2.74,0,0,1-.93,2.11,4.11,4.11,0,0,1-4.81-.15,2.74,2.74,0,0,1-.79-2.17l0.16-4.93,1.9,0.06-0.16,5a1.37,1.37,0,0,0,.32,1,1.67,1.67,0,0,0,2,.06,1.36,1.36,0,0,0,.38-1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M157.05,356.05l2.48,4-2-.06L156,357.43l-1.63,2.42-2-.06,2.73-3.8-2.44-3.91,2,0.06,1.44,2.46,1.59-2.36,2,0.06Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M163.23,352.41l4.81,0.15,0,1.51-2.91-.09,0,1.55,2.77,0.09,0,1.51L165,357l-0.05,1.64,2.94,0.09,0,1.51-4.84-.15Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M174.83,352.78l0,1.51-2.07-.07-0.2,6.21-1.9-.06,0.2-6.21-2.07-.07,0-1.51Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M128.21,365.07a6.51,6.51,0,0,1,1.21.15,4.07,4.07,0,0,1,1,.37L130.13,367a5.12,5.12,0,0,0-.79-0.26,4.11,4.11,0,0,0-.88-0.13,2.68,2.68,0,0,0-1.88.56,2.91,2.91,0,0,0-.12,3.75,2.68,2.68,0,0,0,1.84.68,4.11,4.11,0,0,0,.89-0.07,5.07,5.07,0,0,0,.8-0.21l0.23,1.4a4,4,0,0,1-1.06.3,6.51,6.51,0,0,1-1.21.08,4.94,4.94,0,0,1-1.78-.36,3.61,3.61,0,0,1-1.27-.87,3.5,3.5,0,0,1-.75-1.29,4.94,4.94,0,0,1,.1-3.2,3.5,3.5,0,0,1,.83-1.24,3.62,3.62,0,0,1,1.32-.79A4.94,4.94,0,0,1,128.21,365.07Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M139,369.41a4.44,4.44,0,0,1-.32,1.54,3.76,3.76,0,0,1-.81,1.24,3.64,3.64,0,0,1-1.26.82,4.54,4.54,0,0,1-3.34-.11,3.63,3.63,0,0,1-1.21-.9,3.69,3.69,0,0,1-.72-1.29,4.73,4.73,0,0,1,.1-3.09,3.69,3.69,0,0,1,.8-1.24,3.63,3.63,0,0,1,1.27-.82,4.54,4.54,0,0,1,3.34.11,3.64,3.64,0,0,1,1.21.9,3.76,3.76,0,0,1,.73,1.29A4.44,4.44,0,0,1,139,369.41Zm-4,2.36a2,2,0,0,0,.89-0.16,1.82,1.82,0,0,0,.66-0.5,2.21,2.21,0,0,0,.41-0.77,4,4,0,0,0,.06-2,2.22,2.22,0,0,0-.36-0.8A1.82,1.82,0,0,0,136,367a2.2,2.2,0,0,0-1.77-.06,1.83,1.83,0,0,0-.65.5,2.21,2.21,0,0,0-.41.77,4,4,0,0,0-.06,2,2.21,2.21,0,0,0,.36.8,1.83,1.83,0,0,0,.62.54A2,2,0,0,0,135,371.78Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M145.05,373.46l-3-4.7-0.15,4.6L140,373.3l0.24-7.71,1.83,0.06,3,4.7,0.15-4.6,1.83,0.06-0.24,7.71Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M152.42,365.84a6.51,6.51,0,0,1,1.21.15,4.07,4.07,0,0,1,1,.37l-0.32,1.38a5.12,5.12,0,0,0-.79-0.26,4.11,4.11,0,0,0-.88-0.13,2.68,2.68,0,0,0-1.88.56,2.91,2.91,0,0,0-.12,3.75,2.68,2.68,0,0,0,1.84.68,4.11,4.11,0,0,0,.89-0.07,5.07,5.07,0,0,0,.8-0.21l0.23,1.4a4,4,0,0,1-1.06.3,6.51,6.51,0,0,1-1.21.08,4.94,4.94,0,0,1-1.78-.36,3.61,3.61,0,0,1-1.27-.87,3.5,3.5,0,0,1-.75-1.29,4.94,4.94,0,0,1,.1-3.2,3.5,3.5,0,0,1,.83-1.24,3.62,3.62,0,0,1,1.32-.79A4.94,4.94,0,0,1,152.42,365.84Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M163.22,370.18a4.44,4.44,0,0,1-.32,1.54,3.76,3.76,0,0,1-.81,1.24,3.64,3.64,0,0,1-1.26.82,4.54,4.54,0,0,1-3.34-.11,3.63,3.63,0,0,1-1.21-.9,3.69,3.69,0,0,1-.72-1.29,4.73,4.73,0,0,1,.1-3.09,3.69,3.69,0,0,1,.8-1.24,3.63,3.63,0,0,1,1.27-.82,4.54,4.54,0,0,1,3.34.11,3.64,3.64,0,0,1,1.21.9,3.76,3.76,0,0,1,.73,1.29A4.44,4.44,0,0,1,163.22,370.18Zm-4,2.36a2,2,0,0,0,.89-0.16,1.82,1.82,0,0,0,.66-0.5,2.21,2.21,0,0,0,.41-0.77,4,4,0,0,0,.06-2,2.22,2.22,0,0,0-.36-0.8,1.82,1.82,0,0,0-.63-0.54,2.2,2.2,0,0,0-1.77-.06,1.83,1.83,0,0,0-.65.5,2.21,2.21,0,0,0-.41.77,4,4,0,0,0-.06,2,2.21,2.21,0,0,0,.36.8,1.83,1.83,0,0,0,.62.54A2,2,0,0,0,159.21,372.54Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M169.1,366.5l1.9,0.06-0.16,4.93a2.74,2.74,0,0,1-.93,2.11,4.11,4.11,0,0,1-4.81-.15,2.74,2.74,0,0,1-.79-2.17l0.16-4.93,1.9,0.06-0.16,5a1.37,1.37,0,0,0,.32,1,1.67,1.67,0,0,0,2,.06,1.36,1.36,0,0,0,.38-1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M178.22,369.24a2.2,2.2,0,0,1-.48,1.34,2.68,2.68,0,0,1-1.24.84l2.44,3.11-2.34-.07-2-2.74h-0.35l-0.08,2.67-1.9-.06,0.24-7.71,2.45,0.08a5.6,5.6,0,0,1,1.33.19,3.35,3.35,0,0,1,1.05.49A2.1,2.1,0,0,1,178.22,369.24Zm-1.83,0a0.84,0.84,0,0,0-.38-0.78,2.17,2.17,0,0,0-1.13-.28l-0.54,0-0.07,2.17h0.46a2,2,0,0,0,1.2-.26A1,1,0,0,0,176.39,369.21Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M184.77,372.41a2.29,2.29,0,0,1-.23.95,2.25,2.25,0,0,1-.61.76,2.85,2.85,0,0,1-.94.49,3.73,3.73,0,0,1-1.25.15,6,6,0,0,1-1.27-.18,5.2,5.2,0,0,1-1.17-.44l0.43-1.44a5.6,5.6,0,0,0,.9.37,4,4,0,0,0,1.1.18,1.59,1.59,0,0,0,.8-0.15,0.62,0.62,0,0,0,.33-0.57,0.69,0.69,0,0,0-.26-0.54,3.41,3.41,0,0,0-1-.53,4.42,4.42,0,0,1-1.62-1,2,2,0,0,1-.48-1.42,2.07,2.07,0,0,1,.79-1.69,3.17,3.17,0,0,1,2.05-.53,5.38,5.38,0,0,1,1.26.18,4.07,4.07,0,0,1,1,.41l-0.37,1.41a4.62,4.62,0,0,0-.88-0.35,4,4,0,0,0-1-.15,1.42,1.42,0,0,0-.69.12,0.52,0.52,0,0,0-.28.48,0.74,0.74,0,0,0,0,.28,0.58,0.58,0,0,0,.16.23,1.57,1.57,0,0,0,.33.22l0.55,0.26q0.48,0.21.9,0.45a3.32,3.32,0,0,1,.72.53,2.2,2.2,0,0,1,.47.68A2,2,0,0,1,184.77,372.41Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="pass-tourisme">
		<path class="cls-6" d="M215.29,474.1l-78.71,45.44A222.62,222.62,0,0,0,214,596.91l45.47-78.76A131.4,131.4,0,0,1,215.29,474.1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M188.51,531.32a2.26,2.26,0,0,1,.62.95,2.39,2.39,0,0,1,.09,1.1,3.52,3.52,0,0,1-.39,1.15,5.21,5.21,0,0,1-.82,1.12l-0.13.14,1.9,1.79-1.3,1.38-5.62-5.29,1.52-1.62a5.7,5.7,0,0,1,1-.88,3.21,3.21,0,0,1,1.06-.46,2.16,2.16,0,0,1,1,0A2.39,2.39,0,0,1,188.51,531.32Zm-1.25,1.34q-0.78-.73-1.82.37l-0.23.24,1.59,1.5,0.21-.23a1.86,1.86,0,0,0,.57-1A1,1,0,0,0,187.26,532.66Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M194.72,530.13l-2,2.15,0.76,1.37-1.34,1.43-3.65-7.38,1.43-1.52,7.59,3.2-1.34,1.43Zm-2.79.77,1.36-1.44-3-1.51Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M199.88,523.46a2.29,2.29,0,0,1,.56.8,2.25,2.25,0,0,1,.16,1,2.86,2.86,0,0,1-.26,1,3.75,3.75,0,0,1-.71,1,6,6,0,0,1-1,.83,5.2,5.2,0,0,1-1.1.58l-0.79-1.27a5.55,5.55,0,0,0,.88-0.43,4,4,0,0,0,.86-0.7,1.59,1.59,0,0,0,.42-0.71,0.62,0.62,0,0,0-.21-0.63,0.69,0.69,0,0,0-.57-0.16,3.43,3.43,0,0,0-1,.38,4.42,4.42,0,0,1-1.85.54,2,2,0,0,1-1.38-.58,2.07,2.07,0,0,1-.74-1.71,3.17,3.17,0,0,1,1-1.89,5.43,5.43,0,0,1,1-.82,4.07,4.07,0,0,1,1-.48l0.82,1.21a4.65,4.65,0,0,0-.84.43,4,4,0,0,0-.77.64,1.41,1.41,0,0,0-.37.6,0.52,0.52,0,0,0,.18.53,0.74,0.74,0,0,0,.23.16,0.57,0.57,0,0,0,.28,0,1.55,1.55,0,0,0,.39-0.11l0.56-.24c0.32-.15.63-0.27,0.93-0.38a3.32,3.32,0,0,1,.88-0.19,2.21,2.21,0,0,1,.82.09A2,2,0,0,1,199.88,523.46Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M204.09,519a2.29,2.29,0,0,1,.56.8,2.25,2.25,0,0,1,.16,1,2.86,2.86,0,0,1-.26,1,3.75,3.75,0,0,1-.71,1,6,6,0,0,1-1,.83,5.2,5.2,0,0,1-1.1.58L201,523a5.55,5.55,0,0,0,.88-0.43,4,4,0,0,0,.86-0.7,1.59,1.59,0,0,0,.42-0.71,0.62,0.62,0,0,0-.21-0.63,0.69,0.69,0,0,0-.57-0.16,3.43,3.43,0,0,0-1,.38,4.42,4.42,0,0,1-1.85.54,2,2,0,0,1-1.38-.58,2.07,2.07,0,0,1-.74-1.71,3.17,3.17,0,0,1,1-1.89,5.43,5.43,0,0,1,1-.82,4.07,4.07,0,0,1,1-.48L201,517a4.65,4.65,0,0,0-.84.43,4,4,0,0,0-.77.64,1.41,1.41,0,0,0-.37.6,0.52,0.52,0,0,0,.18.53,0.74,0.74,0,0,0,.23.16,0.57,0.57,0,0,0,.28,0,1.55,1.55,0,0,0,.39-0.11l0.56-.24c0.32-.15.63-0.27,0.93-0.38a3.32,3.32,0,0,1,.88-0.19,2.21,2.21,0,0,1,.82.09A2,2,0,0,1,204.09,519Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M186.52,550.06l1.1,1-1.42,1.51,4.52,4.26-1.3,1.38L184.89,554l-1.42,1.51-1.1-1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M195,546.68a4.44,4.44,0,0,1,.94,1.25,3.74,3.74,0,0,1,.4,1.43,3.63,3.63,0,0,1-.22,1.49,4.55,4.55,0,0,1-2.29,2.43,3.62,3.62,0,0,1-1.48.31,3.67,3.67,0,0,1-1.45-.31,4.72,4.72,0,0,1-2.25-2.12,3.69,3.69,0,0,1-.4-1.43,3.63,3.63,0,0,1,.22-1.49,4.55,4.55,0,0,1,2.29-2.43,3.65,3.65,0,0,1,1.47-.31,3.76,3.76,0,0,1,1.45.31A4.44,4.44,0,0,1,195,546.68Zm-0.89,4.58a2,2,0,0,0,.47-0.77,1.82,1.82,0,0,0,.07-0.82,2.22,2.22,0,0,0-.31-0.82,4,4,0,0,0-1.46-1.37,2.21,2.21,0,0,0-.84-0.26,1.83,1.83,0,0,0-.82.11,2.2,2.2,0,0,0-1.21,1.29,1.85,1.85,0,0,0-.06.82,2.2,2.2,0,0,0,.31.82,4,4,0,0,0,1.46,1.37,2.22,2.22,0,0,0,.84.26,1.83,1.83,0,0,0,.81-0.11A2,2,0,0,0,194.1,551.26Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M196.12,539.85l1.3-1.38,3.59,3.38a2.74,2.74,0,0,1,1,2.1,4.11,4.11,0,0,1-3.29,3.5,2.74,2.74,0,0,1-2.15-.84l-3.59-3.38,1.3-1.38,3.64,3.42a1.36,1.36,0,0,0,1,.44,1.67,1.67,0,0,0,1.4-1.48,1.36,1.36,0,0,0-.5-0.94Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M204.21,534.82a2.2,2.2,0,0,1,.69,1.25,2.68,2.68,0,0,1-.19,1.49l3.95,0.23-1.6,1.7-3.4-.29-0.24.25,1.95,1.83-1.3,1.38-5.62-5.29,1.68-1.79a5.57,5.57,0,0,1,1-.87,3.36,3.36,0,0,1,1.06-.47A2.1,2.1,0,0,1,204.21,534.82ZM203,536.18a0.84,0.84,0,0,0-.84-0.23,2.17,2.17,0,0,0-1,.66l-0.37.4,1.58,1.48,0.32-.34a2,2,0,0,0,.6-1.08A1,1,0,0,0,203,536.18Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M210.48,535.86l-1.3,1.38L203.55,532l1.3-1.38Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M213.34,529.45a2.29,2.29,0,0,1,.56.8,2.25,2.25,0,0,1,.16,1,2.86,2.86,0,0,1-.26,1,3.75,3.75,0,0,1-.71,1,6,6,0,0,1-1,.83,5.2,5.2,0,0,1-1.1.58l-0.79-1.27a5.55,5.55,0,0,0,.88-0.43,4,4,0,0,0,.86-0.7,1.59,1.59,0,0,0,.42-0.71,0.62,0.62,0,0,0-.21-0.63,0.69,0.69,0,0,0-.57-0.16,3.43,3.43,0,0,0-1,.38,4.42,4.42,0,0,1-1.85.54,2,2,0,0,1-1.38-.58,2.07,2.07,0,0,1-.74-1.71,3.17,3.17,0,0,1,1-1.89,5.43,5.43,0,0,1,1-.82,4.07,4.07,0,0,1,1-.48l0.82,1.21a4.65,4.65,0,0,0-.84.43,4,4,0,0,0-.77.64,1.41,1.41,0,0,0-.37.6,0.52,0.52,0,0,0,.18.53,0.74,0.74,0,0,0,.23.16,0.57,0.57,0,0,0,.28,0,1.55,1.55,0,0,0,.39-0.11l0.56-.24c0.32-.15.63-0.27,0.93-0.38a3.32,3.32,0,0,1,.88-0.19,2.21,2.21,0,0,1,.82.09A2,2,0,0,1,213.34,529.45Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M218.4,525.64L217.15,527l-3.29-1,3,3.13-1.28,1.36-5.22-5.71,1.17-1.25,5,1.61-1.91-4.85,1.17-1.25,6,4.87-1.28,1.36-3.31-2.79Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M217,517.67l3.29-3.5,1.1,1-2,2.12,1.13,1.06,1.9-2,1.1,1-1.9,2,1.2,1.13,2-2.14,1.1,1L222.61,523Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="cartes-cadeaux">
		<path class="cls-6" d="M218.36,599.46a219.53,219.53,0,0,0,105.69,28.36V536.94a130,130,0,0,1-60.22-16.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M263.81,589.05a6.47,6.47,0,0,1,.52-1.1,4,4,0,0,1,.67-0.87l1.21,0.74a5.07,5.07,0,0,0-.5.66,4.11,4.11,0,0,0-.4.8,2.68,2.68,0,0,0-.06,2,2.91,2.91,0,0,0,3.52,1.29A2.68,2.68,0,0,0,270,591a4.05,4.05,0,0,0,.21-0.87,5.07,5.07,0,0,0,.05-0.83l1.4,0.22a4.08,4.08,0,0,1,0,1.1,6.55,6.55,0,0,1-.31,1.18,5,5,0,0,1-.9,1.58,3.61,3.61,0,0,1-1.23.93,3.5,3.5,0,0,1-1.46.31,4.93,4.93,0,0,1-3-1.1,3.49,3.49,0,0,1-.91-1.17,3.6,3.6,0,0,1-.34-1.51A4.93,4.93,0,0,1,263.81,589.05Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M272.64,583.44l-1,2.77,1.24,1L272.19,589l-6.26-5.35,0.72-2,8.23,0-0.67,1.84Zm-2.26,1.8,0.68-1.86-3.4-.2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M272.24,573.55a2.2,2.2,0,0,1,1.12.87,2.68,2.68,0,0,1,.41,1.44l3.72-1.34-0.8,2.2-3.24,1.07-0.12.33,2.51,0.92-0.65,1.78-7.25-2.65,0.84-2.31a5.57,5.57,0,0,1,.6-1.2,3.36,3.36,0,0,1,.79-0.85A2.1,2.1,0,0,1,272.24,573.55Zm-0.6,1.73a0.84,0.84,0,0,0-.86.12,2.17,2.17,0,0,0-.62,1l-0.19.51,2,0.75,0.16-.44a2,2,0,0,0,.13-1.22A1,1,0,0,0,271.64,575.28Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M272.31,566.25l1.41,0.52L273,568.72l5.83,2.14-0.65,1.78-5.83-2.14-0.71,1.95-1.41-.52Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M272.62,565.4l1.65-4.51,1.41,0.52-1,2.73,1.46,0.53,1-2.6,1.41,0.52-1,2.6,1.55,0.57,1-2.76,1.41,0.52L279.87,568Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M281.56,556.73a2.29,2.29,0,0,1,.83.52,2.25,2.25,0,0,1,.53.82,2.86,2.86,0,0,1,.17,1,3.75,3.75,0,0,1-.25,1.23,6,6,0,0,1-.57,1.15,5.2,5.2,0,0,1-.79,1l-1.23-.86a5.55,5.55,0,0,0,.64-0.74,4,4,0,0,0,.51-1,1.59,1.59,0,0,0,.11-0.81,0.62,0.62,0,0,0-.44-0.49,0.69,0.69,0,0,0-.59.07,3.43,3.43,0,0,0-.81.76,4.42,4.42,0,0,1-1.49,1.22,2,2,0,0,1-1.5,0,2.07,2.07,0,0,1-1.36-1.28,3.17,3.17,0,0,1,.14-2.11,5.43,5.43,0,0,1,.57-1.14,4.07,4.07,0,0,1,.7-0.83l1.23,0.79a4.65,4.65,0,0,0-.6.72,4,4,0,0,0-.46.89,1.41,1.41,0,0,0-.1.7,0.52,0.52,0,0,0,.37.41,0.74,0.74,0,0,0,.28.05,0.57,0.57,0,0,0,.27-0.08,1.55,1.55,0,0,0,.31-0.25l0.42-.44q0.35-.39.71-0.72a3.32,3.32,0,0,1,.73-0.52,2.21,2.21,0,0,1,.79-0.23A2,2,0,0,1,281.56,556.73Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M274.85,599.34a6.47,6.47,0,0,1,.52-1.1,4,4,0,0,1,.67-0.87l1.21,0.74a5.07,5.07,0,0,0-.5.66,4.11,4.11,0,0,0-.4.8,2.68,2.68,0,0,0-.06,2,2.91,2.91,0,0,0,3.52,1.29,2.68,2.68,0,0,0,1.22-1.54,4.05,4.05,0,0,0,.21-0.87,5.07,5.07,0,0,0,.05-0.83l1.4,0.22a4.08,4.08,0,0,1,0,1.1,6.55,6.55,0,0,1-.31,1.18,5,5,0,0,1-.9,1.58,3.61,3.61,0,0,1-1.23.93,3.5,3.5,0,0,1-1.46.31,4.93,4.93,0,0,1-3-1.1,3.49,3.49,0,0,1-.91-1.17,3.6,3.6,0,0,1-.34-1.51A4.93,4.93,0,0,1,274.85,599.34Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M283.68,593.73l-1,2.77,1.24,1-0.67,1.84L277,594l0.72-2,8.23,0-0.67,1.84Zm-2.26,1.8,0.68-1.86-3.4-.2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M279,588.46l0.81-2.21a5.77,5.77,0,0,1,.92-1.66A4.44,4.44,0,0,1,282,583.5a3.63,3.63,0,0,1,5.2,1.9,4.42,4.42,0,0,1,.25,1.64,5.77,5.77,0,0,1-.37,1.86l-0.81,2.21Zm6.68-.18a3.24,3.24,0,0,0,.21-1,2.38,2.38,0,0,0-.15-0.93,2,2,0,0,0-.51-0.75,2.74,2.74,0,0,0-1.86-.68,2,2,0,0,0-.87.24,2.4,2.4,0,0,0-.72.61,3.24,3.24,0,0,0-.51.93l-0.2.53,4.42,1.62Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M281.74,580.92l1.65-4.51,1.41,0.52-1,2.73,1.46,0.53,1-2.6,1.41,0.52-1,2.6,1.55,0.57,1-2.76,1.41,0.52L289,583.57Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M291.34,572.8l-1,2.77,1.24,1-0.67,1.84L284.63,573l0.72-2,8.23,0-0.67,1.84Zm-2.26,1.8,0.68-1.86-3.4-.2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M288.15,563.42l0.65-1.78,4.63,1.7a2.74,2.74,0,0,1,1.72,1.55,4.11,4.11,0,0,1-1.65,4.51,2.74,2.74,0,0,1-2.31.07l-4.63-1.7,0.65-1.78,4.69,1.72a1.36,1.36,0,0,0,1.07,0,1.67,1.67,0,0,0,.7-1.91,1.36,1.36,0,0,0-.83-0.67Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M294.3,557.8l4.54-1.11-0.7,1.9-2.85.61,1.78,2.31-0.7,1.9-2.75-3.78-4.48,1.08,0.69-1.89,2.79-.6L290.88,556l0.69-1.89Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="store-locator">
		<path class="cls-7" d="M421.78,207.53A219.57,219.57,0,0,0,329.14,186v89.57a130.15,130.15,0,0,1,53.44,12.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M357.05,244.4a2.29,2.29,0,0,1,.88.43,2.25,2.25,0,0,1,.61.76,2.87,2.87,0,0,1,.28,1,3.74,3.74,0,0,1-.12,1.25,6,6,0,0,1-.45,1.2,5.2,5.2,0,0,1-.68,1l-1.31-.73a5.5,5.5,0,0,0,.56-0.8,4,4,0,0,0,.41-1,1.58,1.58,0,0,0,0-.82,0.62,0.62,0,0,0-.49-0.45,0.69,0.69,0,0,0-.58.13,3.44,3.44,0,0,0-.73.84,4.41,4.41,0,0,1-1.36,1.37,2,2,0,0,1-1.49.16,2.07,2.07,0,0,1-1.48-1.13,3.17,3.17,0,0,1-.07-2.12,5.44,5.44,0,0,1,.45-1.19,4.09,4.09,0,0,1,.62-0.9l1.3,0.66a4.68,4.68,0,0,0-.53.78,4,4,0,0,0-.36.93,1.41,1.41,0,0,0,0,.7,0.52,0.52,0,0,0,.41.37,0.74,0.74,0,0,0,.28,0,0.59,0.59,0,0,0,.26-0.11,1.58,1.58,0,0,0,.29-0.28l0.37-.48q0.31-.42.63-0.78a3.32,3.32,0,0,1,.68-0.59,2.19,2.19,0,0,1,.76-0.31A2,2,0,0,1,357.05,244.4Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M353.41,236.74l1.46,0.37-0.51,2,6,1.53-0.47,1.84-6-1.53-0.51,2-1.46-.37Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M359.18,229.68a4.44,4.44,0,0,1,1.43.64,3.75,3.75,0,0,1,1,1.06,3.63,3.63,0,0,1,.53,1.41,4.55,4.55,0,0,1-.82,3.24,3.61,3.61,0,0,1-1.14,1,3.67,3.67,0,0,1-1.42.43,4.73,4.73,0,0,1-3-.76,3.69,3.69,0,0,1-1-1.05,3.62,3.62,0,0,1-.53-1.41,4.55,4.55,0,0,1,.82-3.24,3.64,3.64,0,0,1,1.14-1,3.77,3.77,0,0,1,1.42-.44A4.44,4.44,0,0,1,359.18,229.68Zm1.45,4.43a2,2,0,0,0,0-.9,1.81,1.81,0,0,0-.34-0.75,2.2,2.2,0,0,0-.67-0.57,4,4,0,0,0-1.94-.49,2.19,2.19,0,0,0-.86.18,1.82,1.82,0,0,0-.66.5,2.19,2.19,0,0,0-.43,1.71,1.84,1.84,0,0,0,.34.74,2.2,2.2,0,0,0,.67.57,4,4,0,0,0,1.94.49,2.22,2.22,0,0,0,.86-0.18,1.82,1.82,0,0,0,.66-0.49A2,2,0,0,0,360.63,234.11Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M359.53,222.59a2.19,2.19,0,0,1,1.21.75,2.68,2.68,0,0,1,.56,1.39l3.57-1.71-0.58,2.27-3.11,1.4-0.09.34,2.59,0.66-0.47,1.84-7.48-1.9,0.6-2.38a5.58,5.58,0,0,1,.47-1.25,3.37,3.37,0,0,1,.7-0.92A2.1,2.1,0,0,1,359.53,222.59Zm-0.42,1.78a0.84,0.84,0,0,0-.85.2,2.17,2.17,0,0,0-.52,1l-0.13.53,2.1,0.53,0.11-.45a2,2,0,0,0,0-1.23A1,1,0,0,0,359.11,224.37Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M357.55,220.45l1.18-4.66,1.46,0.37L359.47,219l1.5,0.38,0.68-2.68,1.46,0.37-0.68,2.68,1.6,0.4,0.72-2.85,1.46,0.37L365,222.34Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M362.43,257.76l6,1.53,0.7-2.75,1.46,0.37-1.16,4.59L362,259.6Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M368.92,247.85a4.44,4.44,0,0,1,1.43.64,3.74,3.74,0,0,1,1,1.06,3.64,3.64,0,0,1,.53,1.41,4.12,4.12,0,0,1-.1,1.7,4.17,4.17,0,0,1-.72,1.54,3.62,3.62,0,0,1-1.14,1,3.67,3.67,0,0,1-1.42.43,4.72,4.72,0,0,1-3-.76,3.69,3.69,0,0,1-1-1.05,3.64,3.64,0,0,1-.53-1.41,4.18,4.18,0,0,1,.1-1.7,4.14,4.14,0,0,1,.72-1.54,3.65,3.65,0,0,1,1.14-1,3.76,3.76,0,0,1,1.42-.43A4.44,4.44,0,0,1,368.92,247.85Zm1.45,4.43a2,2,0,0,0,0-.9,1.82,1.82,0,0,0-.34-0.75,2.24,2.24,0,0,0-.67-0.57,4,4,0,0,0-1.94-.49,2.22,2.22,0,0,0-.86.18,1.84,1.84,0,0,0-.66.5,2.2,2.2,0,0,0-.43,1.71,1.85,1.85,0,0,0,.34.74,2.2,2.2,0,0,0,.67.57,4,4,0,0,0,1.94.49,2.22,2.22,0,0,0,.86-0.18,1.84,1.84,0,0,0,.66-0.49A2,2,0,0,0,370.37,252.28Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M366.27,242a6.47,6.47,0,0,1,.41-1.15,4.06,4.06,0,0,1,.58-0.93l1.28,0.61a5.08,5.08,0,0,0-.42.71,4.13,4.13,0,0,0-.31.84,2.68,2.68,0,0,0,.14,2,2.91,2.91,0,0,0,3.63.92,2.68,2.68,0,0,0,1.06-1.66,4.07,4.07,0,0,0,.12-0.88,5.08,5.08,0,0,0,0-.83l1.42,0.07a4.09,4.09,0,0,1,.07,1.1A6.56,6.56,0,0,1,374,244a5,5,0,0,1-.73,1.67,3.62,3.62,0,0,1-1.13,1.05,3.5,3.5,0,0,1-1.42.45,4.93,4.93,0,0,1-3.1-.79,3.49,3.49,0,0,1-1-1.07,3.6,3.6,0,0,1-.49-1.46A4.93,4.93,0,0,1,366.27,242Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M374.48,235.54l-0.73,2.86,1.33,0.83-0.48,1.9-6.77-4.68,0.51-2,8.19-.89-0.48,1.9Zm-2.07,2,0.49-1.92-3.4.15Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M370.48,226l1.46,0.37-0.51,2,6,1.53L377,231.74l-6-1.53-0.51,2-1.46-.37Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M376.25,218.93a4.44,4.44,0,0,1,1.43.64,3.74,3.74,0,0,1,1,1.06,3.64,3.64,0,0,1,.53,1.41,4.12,4.12,0,0,1-.1,1.7,4.17,4.17,0,0,1-.72,1.54,3.62,3.62,0,0,1-1.14,1,3.67,3.67,0,0,1-1.42.43,4.72,4.72,0,0,1-3-.76,3.69,3.69,0,0,1-1-1.05,3.64,3.64,0,0,1-.53-1.41,4.18,4.18,0,0,1,.1-1.7,4.14,4.14,0,0,1,.72-1.54,3.65,3.65,0,0,1,1.14-1,3.76,3.76,0,0,1,1.42-.43A4.44,4.44,0,0,1,376.25,218.93Zm1.45,4.43a2,2,0,0,0,0-.9,1.82,1.82,0,0,0-.34-0.75,2.24,2.24,0,0,0-.67-0.57,4,4,0,0,0-1.94-.49,2.22,2.22,0,0,0-.86.18,1.84,1.84,0,0,0-.66.5,2.2,2.2,0,0,0-.43,1.71,1.85,1.85,0,0,0,.34.74,2.2,2.2,0,0,0,.67.57,4,4,0,0,0,1.94.49,2.22,2.22,0,0,0,.86-0.18,1.84,1.84,0,0,0,.66-0.49A2,2,0,0,0,377.7,223.36Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M376.59,211.84a2.2,2.2,0,0,1,1.21.75,2.68,2.68,0,0,1,.56,1.39l3.57-1.71-0.58,2.27-3.11,1.4-0.09.34,2.59,0.66-0.47,1.84-7.48-1.9,0.6-2.38a5.57,5.57,0,0,1,.47-1.25,3.35,3.35,0,0,1,.7-0.92A2.09,2.09,0,0,1,376.59,211.84Zm-0.42,1.78a0.84,0.84,0,0,0-.85.2,2.17,2.17,0,0,0-.52,1l-0.13.53,2.1,0.53,0.11-.45a2.05,2.05,0,0,0,0-1.23A1,1,0,0,0,376.18,213.62Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="plannings-rdv">
		<path class="cls-7" d="M382.14,524.83L419,607.57a222.17,222.17,0,0,0,75-56.66l-69.12-58A131.12,131.12,0,0,1,382.14,524.83Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M423.45,522.35a2.25,2.25,0,0,1-1,.45,2.4,2.4,0,0,1-1.09-.1,3.53,3.53,0,0,1-1.07-.58,5.22,5.22,0,0,1-1-1l-0.12-.16-2.09,1.56L415.94,521l6.18-4.61,1.33,1.78a5.7,5.7,0,0,1,.69,1.17,3.21,3.21,0,0,1,.27,1.13,2.17,2.17,0,0,1-.21,1A2.39,2.39,0,0,1,423.45,522.35Zm-1.1-1.46q0.85-.64-0.05-1.85l-0.2-.27-1.75,1.31,0.19,0.25a1.86,1.86,0,0,0,.93.74A1,1,0,0,0,422.34,520.89Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M427.13,523.08l-5,3.71,1.7,2.28-1.21.9-2.83-3.8,6.18-4.61Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M427.4,533.84l-1.77-2.37-1.48.51L423,530.42l7.9-2.31,1.25,1.67-4.47,6.92-1.17-1.57ZM427.13,531l1.18,1.59,2-2.74Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M431.2,541.43l1.78-5.3-3.69,2.75-1.09-1.47,6.18-4.61,1.09,1.47-1.78,5.3,3.69-2.75,1.09,1.47-6.18,4.61Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M436.19,548.12l1.78-5.3-3.69,2.75-1.1-1.47,6.18-4.61,1.1,1.47-1.78,5.3,3.69-2.75,1.1,1.47-6.18,4.61Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M439.36,552.36l-1.14-1.52,6.18-4.61,1.14,1.52Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M443.29,557.64l1.78-5.3-3.69,2.75-1.09-1.47,6.18-4.61,1.09,1.47-1.78,5.3,3.69-2.75,1.09,1.47-6.18,4.61Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M453.91,558.72A6,6,0,0,1,455,561l-1.3.65a5.62,5.62,0,0,0-.93-1.91,3.4,3.4,0,0,0-.74-0.75,2,2,0,0,0-.85-0.35,2.1,2.1,0,0,0-1,.08,3.46,3.46,0,0,0-1,.56,2.43,2.43,0,0,0-1.08,1.59,2.56,2.56,0,0,0,.62,1.86,3.06,3.06,0,0,0,.34.38,2.38,2.38,0,0,0,.32.27l1.73-1.29,1,1.36-2.55,1.9a5.32,5.32,0,0,1-1.08-.78,8,8,0,0,1-1.09-1.19,5,5,0,0,1-.81-1.56,3.73,3.73,0,0,1-.14-1.51A3.41,3.41,0,0,1,447,559a4.24,4.24,0,0,1,1.1-1.17,4.78,4.78,0,0,1,1.52-.78,3.52,3.52,0,0,1,1.53-.11,3.71,3.71,0,0,1,1.47.57A5.08,5.08,0,0,1,453.91,558.72Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M455,569.43a2.29,2.29,0,0,1-.89.41,2.24,2.24,0,0,1-1,0,2.85,2.85,0,0,1-1-.43,3.75,3.75,0,0,1-.89-0.88,6,6,0,0,1-.65-1.1,5.2,5.2,0,0,1-.38-1.19l1.39-.56a5.61,5.61,0,0,0,.27.94,4,4,0,0,0,.54,1,1.59,1.59,0,0,0,.62.53,0.62,0.62,0,0,0,.66-0.1,0.7,0.7,0,0,0,.26-0.53,3.43,3.43,0,0,0-.19-1.1,4.42,4.42,0,0,1-.21-1.91,2,2,0,0,1,.82-1.26,2.07,2.07,0,0,1,1.81-.44,3.16,3.16,0,0,1,1.69,1.28,5.42,5.42,0,0,1,.64,1.1,4,4,0,0,1,.31,1l-1.34.59a4.62,4.62,0,0,0-.27-0.9,4,4,0,0,0-.49-0.87,1.41,1.41,0,0,0-.53-0.46,0.52,0.52,0,0,0-.55.09,0.74,0.74,0,0,0-.19.2,0.56,0.56,0,0,0-.08.27,1.53,1.53,0,0,0,0,.4c0,0.16.08,0.36,0.14,0.59s0.16,0.67.21,1a3.32,3.32,0,0,1,0,.9,2.23,2.23,0,0,1-.24.79A2,2,0,0,1,455,569.43Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M408.46,525.39a2.19,2.19,0,0,1-1.35.46,2.68,2.68,0,0,1-1.43-.44l-0.92,3.85-1.4-1.88,0.88-3.3-0.21-.28-2.14,1.6-1.14-1.52,6.18-4.61,1.47,2a5.58,5.58,0,0,1,.68,1.16,3.37,3.37,0,0,1,.28,1.12A2.1,2.1,0,0,1,408.46,525.39Zm-1.12-1.45a0.84,0.84,0,0,0,.37-0.79,2.17,2.17,0,0,0-.48-1.06l-0.33-.44-1.74,1.3,0.28,0.37a2,2,0,0,0,1,.78A1,1,0,0,0,407.34,523.95Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M411.37,525.2l2.88,3.85L413,530l-1.74-2.33-1.24.93,1.66,2.22-1.21.9-1.66-2.22-1.32,1,1.76,2.36-1.21.9-2.9-3.88Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M411.87,538.77l1.78-5.3L410,536.21l-1.09-1.47,6.18-4.61,1.09,1.47-1.78,5.3,3.69-2.75,1.09,1.47L413,540.23Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M420,536.83l1.41,1.88a5.77,5.77,0,0,1,.89,1.67,4.45,4.45,0,0,1,.23,1.64,3.61,3.61,0,0,1-.42,1.48,3.63,3.63,0,0,1-4,1.83,4.43,4.43,0,0,1-1.51-.69,5.77,5.77,0,0,1-1.35-1.33l-1.41-1.88Zm-3.5,5.69a3.25,3.25,0,0,0,.76.75,2.38,2.38,0,0,0,.86.38,2,2,0,0,0,.9,0,2.75,2.75,0,0,0,1.59-1.19,2,2,0,0,0,.27-0.86,2.4,2.4,0,0,0-.12-0.93,3.25,3.25,0,0,0-.5-0.94l-0.34-.46-3.77,2.81Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M424.85,543.26l2.88,3.85-1.21.9-1.74-2.33-1.24.93,1.66,2.22-1.21.9-1.66-2.22-1.32,1,1.76,2.36-1.21.9-2.9-3.88Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M428.46,548.11l3.23,4.33-1.21.9-5.79.1,2.09,2.8-1.21.9-3.35-4.49,1.21-.9,5.79-.1-2-2.64Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M428.52,556.07l1.19-.89,1.9,2.54-1.19.89Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M431.09,564.53l-1.28-1.71,4.62-6.7,1.19,1.6-3.7,4.86,5.71-2.17,1.19,1.6Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M440.62,570.85a4.44,4.44,0,0,1-1.4.71,3.75,3.75,0,0,1-1.48.14,3.63,3.63,0,0,1-1.43-.47,4.55,4.55,0,0,1-2-2.68,3.61,3.61,0,0,1,0-1.51,3.67,3.67,0,0,1,.56-1.37,4.73,4.73,0,0,1,2.48-1.85,3.69,3.69,0,0,1,1.47-.15,3.62,3.62,0,0,1,1.43.48,4.55,4.55,0,0,1,2,2.68,3.64,3.64,0,0,1,0,1.5,3.77,3.77,0,0,1-.56,1.38A4.44,4.44,0,0,1,440.62,570.85Zm-4.35-1.67a2,2,0,0,0,.67.59,1.81,1.81,0,0,0,.8.21,2.2,2.2,0,0,0,.86-0.16,4,4,0,0,0,1.61-1.2,2.19,2.19,0,0,0,.4-0.78,1.82,1.82,0,0,0,0-.82,2.19,2.19,0,0,0-1.06-1.42,1.84,1.84,0,0,0-.79-0.2,2.2,2.2,0,0,0-.86.16,4,4,0,0,0-1.61,1.2,2.22,2.22,0,0,0-.4.78,1.82,1.82,0,0,0,0,.82A2,2,0,0,0,436.27,569.18Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M447.16,573.15l1.14,1.52-4,3a2.74,2.74,0,0,1-2.23.59,4.12,4.12,0,0,1-2.88-3.85,2.74,2.74,0,0,1,1.2-2l4-3,1.14,1.52-4,3a1.37,1.37,0,0,0-.6.88,1.67,1.67,0,0,0,1.22,1.63,1.36,1.36,0,0,0,1-.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M447.87,583.16a2.29,2.29,0,0,1-.89.41,2.24,2.24,0,0,1-1,0,2.85,2.85,0,0,1-1-.43,3.75,3.75,0,0,1-.89-0.88,6,6,0,0,1-.65-1.1,5.19,5.19,0,0,1-.38-1.19l1.39-.56a5.56,5.56,0,0,0,.27.94,4,4,0,0,0,.54,1,1.59,1.59,0,0,0,.62.53,0.62,0.62,0,0,0,.66-0.1,0.69,0.69,0,0,0,.26-0.53,3.43,3.43,0,0,0-.19-1.1,4.42,4.42,0,0,1-.21-1.91,2,2,0,0,1,.82-1.26,2.07,2.07,0,0,1,1.81-.44,3.16,3.16,0,0,1,1.69,1.28,5.43,5.43,0,0,1,.64,1.1,4.07,4.07,0,0,1,.31,1l-1.34.59a4.64,4.64,0,0,0-.27-0.9,4,4,0,0,0-.49-0.87,1.41,1.41,0,0,0-.53-0.46,0.52,0.52,0,0,0-.55.08,0.74,0.74,0,0,0-.19.2,0.58,0.58,0,0,0-.08.27,1.56,1.56,0,0,0,0,.4q0,0.24.14,0.59,0.13,0.51.21,1a3.32,3.32,0,0,1,0,.9,2.23,2.23,0,0,1-.24.79A2,2,0,0,1,447.87,583.16Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="click-collect">
		<path class="cls-7" d="M497.31,547a220.52,220.52,0,0,0,42.86-83.45l-87.24-21.88A130.36,130.36,0,0,1,428.22,489Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M476.33,469.8a6.47,6.47,0,0,1,1,.66,4,4,0,0,1,.78.77l-0.88,1.11a5.09,5.09,0,0,0-.6-0.57,4.12,4.12,0,0,0-.74-0.49A2.68,2.68,0,0,0,474,471a2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.06,4.06,0,0,0,.83.32,5.09,5.09,0,0,0,.81.15l-0.39,1.37a4.07,4.07,0,0,1-1.08-.18,6.55,6.55,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.61,3.61,0,0,1-.77-1.33,3.5,3.5,0,0,1-.13-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.53-.15A4.94,4.94,0,0,1,476.33,469.8Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M481.05,472.38l-2.83,5.53,2.53,1.3-0.69,1.34-4.22-2.16,3.52-6.87Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M482.66,481.87L481,481l3.52-6.87,1.69,0.87Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M491,477.32a6.47,6.47,0,0,1,1,.66,4.06,4.06,0,0,1,.78.77l-0.88,1.11a5.08,5.08,0,0,0-.6-0.57,4.13,4.13,0,0,0-.74-0.49,2.68,2.68,0,0,0-1.94-.3,2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.07,4.07,0,0,0,.83.32,5.08,5.08,0,0,0,.81.15l-0.39,1.37a4.09,4.09,0,0,1-1.08-.18,6.56,6.56,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.62,3.62,0,0,1-.77-1.33,3.5,3.5,0,0,1-.12-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.54-.15A4.93,4.93,0,0,1,491,477.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M490.51,485.9L494,479l1.69,0.87L494,483.19l3.85-2.18,1.91,1-4.06,2.2,0.7,4.75-2-1-0.48-4.49-1.72,3.36Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M457.08,484.53q-0.61-1-1.07-1.77a4,4,0,0,1-1.63.13,4.48,4.48,0,0,1-1.37-.44,3.94,3.94,0,0,1-.89-0.6,2.51,2.51,0,0,1-.55-0.71,1.68,1.68,0,0,1-.19-0.79,1.89,1.89,0,0,1,.23-0.85A1.6,1.6,0,0,1,452,479a2.06,2.06,0,0,1,.52-0.32,3.52,3.52,0,0,1,.63-0.2l0.7-.13a4.74,4.74,0,0,1-.31-1.17,1.84,1.84,0,0,1,.21-1.07,2,2,0,0,1,.51-0.64,1.79,1.79,0,0,1,.72-0.36,2.22,2.22,0,0,1,.88,0,3.38,3.38,0,0,1,1,.34,3.13,3.13,0,0,1,.76.53,2.18,2.18,0,0,1,.47.64,1.68,1.68,0,0,1,.16.71,1.6,1.6,0,0,1-.19.73,1.75,1.75,0,0,1-.38.51,2.32,2.32,0,0,1-.51.35,3.47,3.47,0,0,1-.59.24l-0.63.18,0.39,0.8,0.4,0.76q0.36-.19.76-0.46l0.85-.58,0.88,1.23q-0.48.35-.91,0.63a9.37,9.37,0,0,1-.84.49l0.92,1.55Zm-3.28-3.22a2.12,2.12,0,0,0,1.51.15c-0.14-.27-0.3-0.57-0.46-0.9s-0.3-.63-0.43-0.92a3.67,3.67,0,0,0-.68.19,0.78,0.78,0,0,0-.42.38,0.73,0.73,0,0,0-.06.59A1,1,0,0,0,453.79,481.31Zm1.47-4.46a1,1,0,0,0-.1.56,2.4,2.4,0,0,0,.17.66,5.48,5.48,0,0,0,.81-0.21,0.84,0.84,0,0,0,.48-0.42,0.53,0.53,0,0,0,0-.48,0.8,0.8,0,0,0-.38-0.38,0.81,0.81,0,0,0-.57-0.09A0.64,0.64,0,0,0,455.26,476.86Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M467.87,481.08a6.47,6.47,0,0,1,1,.66,4.06,4.06,0,0,1,.78.77l-0.88,1.11a5.08,5.08,0,0,0-.6-0.57,4.13,4.13,0,0,0-.74-0.49,2.68,2.68,0,0,0-1.94-.3,2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.07,4.07,0,0,0,.83.32,5.08,5.08,0,0,0,.81.15l-0.39,1.37a4.09,4.09,0,0,1-1.08-.18,6.56,6.56,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.62,3.62,0,0,1-.77-1.33,3.5,3.5,0,0,1-.12-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.54-.15A4.93,4.93,0,0,1,467.87,481.08Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M475.77,489.63a4.44,4.44,0,0,1-.94,1.25,3.75,3.75,0,0,1-1.27.78,3.63,3.63,0,0,1-1.49.21,4.55,4.55,0,0,1-3-1.53,3.61,3.61,0,0,1-.71-1.33,3.67,3.67,0,0,1-.1-1.48,4.73,4.73,0,0,1,1.41-2.75A3.69,3.69,0,0,1,471,484a3.62,3.62,0,0,1,1.5-.2,4.55,4.55,0,0,1,3,1.53,3.64,3.64,0,0,1,.7,1.33,3.77,3.77,0,0,1,.11,1.48A4.44,4.44,0,0,1,475.77,489.63Zm-4.64.42a2,2,0,0,0,.87.24,1.81,1.81,0,0,0,.81-0.16,2.2,2.2,0,0,0,.7-0.52,4,4,0,0,0,.91-1.78,2.19,2.19,0,0,0,0-.88,1.82,1.82,0,0,0-.34-0.75,2.19,2.19,0,0,0-1.57-.81,1.84,1.84,0,0,0-.8.17,2.2,2.2,0,0,0-.7.52,4,4,0,0,0-.91,1.78,2.22,2.22,0,0,0,0,.88,1.82,1.82,0,0,0,.33.75A2,2,0,0,0,471.13,490Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M480.25,487.59l-2.83,5.53,2.53,1.3-0.69,1.34L475,493.58l3.52-6.87Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M485.32,490.19l-2.83,5.53L485,497l-0.69,1.34-4.22-2.16,3.52-6.87Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M488.7,491.92l4.28,2.19-0.69,1.34-2.59-1.33L489,495.51l2.46,1.26-0.69,1.34-2.46-1.26-0.75,1.46,2.62,1.34L489.49,501l-4.31-2.21Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M497.61,496.33a6.47,6.47,0,0,1,1,.66,4.06,4.06,0,0,1,.78.77l-0.88,1.11a5.08,5.08,0,0,0-.6-0.57,4.13,4.13,0,0,0-.74-0.49,2.68,2.68,0,0,0-1.94-.3,2.91,2.91,0,0,0-1.71,3.34,2.68,2.68,0,0,0,1.38,1.4,4.07,4.07,0,0,0,.83.32,5.08,5.08,0,0,0,.81.15l-0.39,1.37a4.09,4.09,0,0,1-1.08-.18,6.56,6.56,0,0,1-1.13-.45,5,5,0,0,1-1.46-1.09,3.62,3.62,0,0,1-.77-1.33,3.5,3.5,0,0,1-.12-1.48,4.93,4.93,0,0,1,1.46-2.84,3.49,3.49,0,0,1,1.28-.76,3.6,3.6,0,0,1,1.54-.15A4.93,4.93,0,0,1,497.61,496.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M505.49,500.53l-0.69,1.34-1.85-.95-2.83,5.52-1.69-.87,2.83-5.52-1.84-.95,0.69-1.34Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="bon-plan">
		<path class="cls-7" d="M542.38,359.34a220.11,220.11,0,0,0-39.61-85.64l-70.47,55a130.12,130.12,0,0,1,22.4,49.23Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M469.38,342.58a1.83,1.83,0,0,1,.22.93,2,2,0,0,1-.28.91,3.36,3.36,0,0,1-.73.85,5.86,5.86,0,0,1-1.13.75l-2.13,1.1-3.55-6.85,2.46-1.27a4.12,4.12,0,0,1,1.06-.39,2.61,2.61,0,0,1,.91,0,1.76,1.76,0,0,1,.72.27,1.44,1.44,0,0,1,.49.55,1.75,1.75,0,0,1,.19,1.23,2.42,2.42,0,0,1-.71,1.15,3.07,3.07,0,0,1,.75-0.14,2.14,2.14,0,0,1,.69.07,1.65,1.65,0,0,1,.59.3A1.74,1.74,0,0,1,469.38,342.58Zm-4-.67a1.34,1.34,0,0,0,.61-0.58,0.76,0.76,0,0,0,0-.72,0.69,0.69,0,0,0-.51-0.39,1.23,1.23,0,0,0-.77.17l-0.58.3,0.77,1.48Zm2.31,1.41a0.75,0.75,0,0,0-.62-0.43,1.8,1.8,0,0,0-1,.25l-0.56.29,0.83,1.6,0.6-.31a1.46,1.46,0,0,0,.76-0.69A0.84,0.84,0,0,0,467.64,343.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M476.28,337.1a4.44,4.44,0,0,1,.48,1.5,3.76,3.76,0,0,1-.1,1.48,3.64,3.64,0,0,1-.7,1.33,4.13,4.13,0,0,1-1.33,1.05,4.18,4.18,0,0,1-1.63.49,3.63,3.63,0,0,1-1.5-.2,3.69,3.69,0,0,1-1.26-.77,4.72,4.72,0,0,1-1.42-2.75,3.69,3.69,0,0,1,.1-1.48,3.63,3.63,0,0,1,.7-1.34,4.18,4.18,0,0,1,1.34-1.05,4.13,4.13,0,0,1,1.63-.48,3.64,3.64,0,0,1,1.49.2,3.76,3.76,0,0,1,1.27.77A4.44,4.44,0,0,1,476.28,337.1Zm-2.35,4a2,2,0,0,0,.7-0.57,1.82,1.82,0,0,0,.33-0.75,2.21,2.21,0,0,0,0-.88,4,4,0,0,0-.92-1.78,2.21,2.21,0,0,0-.71-0.52,1.82,1.82,0,0,0-.81-0.16,2.2,2.2,0,0,0-1.57.81,1.83,1.83,0,0,0-.33.75,2.22,2.22,0,0,0,0,.88,4,4,0,0,0,.92,1.78,2.21,2.21,0,0,0,.71.52,1.83,1.83,0,0,0,.8.16A2,2,0,0,0,473.93,341.12Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M483.53,337.69l-4.95-2.62,2.12,4.08-1.62.84-3.55-6.85,1.62-.84,4.95,2.62L480,330.84l1.62-.84,3.55,6.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M491.27,327.82a2.25,2.25,0,0,1,.27,1.1,2.4,2.4,0,0,1-.28,1.06,3.51,3.51,0,0,1-.75,1,5.23,5.23,0,0,1-1.14.79l-0.17.09,1.2,2.31-1.69.87-3.55-6.85,2-1a5.73,5.73,0,0,1,1.27-.49,3.19,3.19,0,0,1,1.16-.08,2.15,2.15,0,0,1,1,.37A2.39,2.39,0,0,1,491.27,327.82Zm-1.62.85q-0.49-.95-1.84-0.25l-0.3.15,1,1.94,0.28-.14a1.86,1.86,0,0,0,.88-0.79A1,1,0,0,0,489.65,328.67Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M492.59,324.3l2.86,5.51,2.52-1.31,0.69,1.34L494.45,332l-3.55-6.85Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M503.25,325.78l-2.62,1.36,0.27,1.55-1.74.9-1-8.17,1.85-1,6.1,5.53-1.74.9Zm-2.88-.2,1.76-.91-2.37-2.44Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M511.35,323.27l-4.95-2.62,2.12,4.08-1.62.84-3.55-6.85,1.62-.84,4.95,2.62-2.12-4.08,1.62-.84,3.55,6.85Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="avis-client">
		<path class="cls-7" d="M329.14,627.82a219.62,219.62,0,0,0,85.23-18.17l-36.88-82.76A130.24,130.24,0,0,1,329.14,537v90.86Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M365.91,569.86l-0.48-2.91-1.55-.23-0.32-1.93,8.08,1.59,0.34,2.06-7.15,4.08-0.32-1.93Zm1.09-2.68,0.32,2,3.05-1.5Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M365.51,576.65l-0.34-2.1,7.19-3.82,0.32,2-5.52,2.61,6.07,0.71,0.32,2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M366.37,581.89L366.06,580l7.61-1.25,0.31,1.88Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M369.71,588.06a2.27,2.27,0,0,1-1,0,2.24,2.24,0,0,1-.86-0.45,2.84,2.84,0,0,1-.66-0.83,3.72,3.72,0,0,1-.39-1.19,6,6,0,0,1-.07-1.28A5.2,5.2,0,0,1,367,583l1.49,0.14a5.57,5.57,0,0,0-.19,1,4,4,0,0,0,0,1.11,1.59,1.59,0,0,0,.31.76,0.62,0.62,0,0,0,.63.21,0.69,0.69,0,0,0,.48-0.35,3.41,3.41,0,0,0,.34-1.06,4.42,4.42,0,0,1,.7-1.79,2,2,0,0,1,1.3-.74,2.07,2.07,0,0,1,1.81.45,3.16,3.16,0,0,1,.91,1.91,5.39,5.39,0,0,1,.06,1.27,4.05,4.05,0,0,1-.21,1.07l-1.46-.09a4.61,4.61,0,0,0,.17-0.93,4,4,0,0,0,0-1,1.42,1.42,0,0,0-.25-0.66,0.52,0.52,0,0,0-.53-0.18,0.76,0.76,0,0,0-.27.09,0.59,0.59,0,0,0-.19.2,1.58,1.58,0,0,0-.15.37q-0.07.23-.15,0.59-0.12.51-.26,1a3.34,3.34,0,0,1-.38.81,2.2,2.2,0,0,1-.57.59A2,2,0,0,1,369.71,588.06Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M357.27,563.73a6.51,6.51,0,0,1,.08,1.21,4,4,0,0,1-.16,1.09l-1.42,0a5.17,5.17,0,0,0,.11-0.82,4.11,4.11,0,0,0,0-.89,2.68,2.68,0,0,0-.91-1.74,2.91,2.91,0,0,0-3.7.61,2.68,2.68,0,0,0-.31,1.94,4.11,4.11,0,0,0,.24.86,5.12,5.12,0,0,0,.36.75l-1.33.5a4,4,0,0,1-.5-1,6.51,6.51,0,0,1-.31-1.18,5,5,0,0,1,0-1.82,3.62,3.62,0,0,1,.61-1.41,3.5,3.5,0,0,1,1.12-1,4.93,4.93,0,0,1,3.16-.52,3.5,3.5,0,0,1,1.37.57,3.63,3.63,0,0,1,1,1.15A5,5,0,0,1,357.27,563.73Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M358,569.05l-6.13,1,0.46,2.8-1.49.24-0.77-4.68,7.61-1.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M351.31,576L351,574.11l7.61-1.25,0.31,1.88Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M359.18,576.28L360,581l-1.49.24L358,578.4l-1.53.25,0.45,2.73-1.49.24L355,578.89l-1.62.27,0.48,2.9-1.49.24-0.78-4.78Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M353.37,588.55l4-3.89-4.54.74-0.3-1.81,7.61-1.25,0.3,1.81-4,3.89,4.54-.74,0.3,1.81-7.61,1.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M362.41,596l-1.49.24-0.34-2-6.13,1-0.31-1.88,6.13-1-0.34-2,1.49-.24Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="mini-site">
		<path class="cls-7" d="M429.51,325.05l70-56.44a222.42,222.42,0,0,0-74.26-58.7l-37.64,80.43A131.29,131.29,0,0,1,429.51,325.05Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M426,288.79l-1.19,1.48-3.47-.79,3.34,3-1.21,1.5-5.85-5.48,1.11-1.38,5.25,1.27-2.36-4.86,1.11-1.38,6.6,4.54-1.21,1.51-3.64-2.62Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M431.42,284.1l-1.23,1.53-6.23-5,1.23-1.53Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M435.69,278.78l-5.78-.41,3.71,3-1.19,1.48-6.23-5,1.19-1.48,5.78,0.41-3.71-3,1.19-1.48,6.23,5Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M439.13,274.51L437.89,276l-6.23-5,1.23-1.53Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M438.21,271.92l-1.2-1,2.06-2.56,1.2,1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M444.38,264.17a2.34,2.34,0,0,1,.89,1.76,3,3,0,0,1-.18,1.09,3.87,3.87,0,0,1-.65,1.12,6.14,6.14,0,0,1-.94.94,5.36,5.36,0,0,1-1.09.69l-0.92-1.25a5.79,5.79,0,0,0,.87-0.52,4.14,4.14,0,0,0,.83-0.79,1.65,1.65,0,0,0,.38-0.76,0.64,0.64,0,0,0-.27-0.63,0.72,0.72,0,0,0-.6-0.12,3.55,3.55,0,0,0-1.05.47,4.58,4.58,0,0,1-1.87.7,2,2,0,0,1-1.47-.49,2.15,2.15,0,0,1-.91-1.71,3.27,3.27,0,0,1,.84-2,5.58,5.58,0,0,1,.94-0.92,4.22,4.22,0,0,1,1-.58l0.94,1.19a4.87,4.87,0,0,0-.83.51,4.18,4.18,0,0,0-.74.72,1.47,1.47,0,0,0-.33.65,0.53,0.53,0,0,0,.23.53,0.77,0.77,0,0,0,.25.14,0.61,0.61,0,0,0,.29,0,1.6,1.6,0,0,0,.39-0.14l0.56-.29q0.47-.26.93-0.47a3.44,3.44,0,0,1,.89-0.27,2.29,2.29,0,0,1,.85,0A2,2,0,0,1,444.38,264.17Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M448.21,263.22L447,264.75l-6.23-5,1.23-1.53Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M446.54,252.54l1.21,1-1.35,1.67,5,4-1.23,1.53-5-4-1.35,1.67-1.21-1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M447.14,251.8l3.12-3.88,1.21,1-1.89,2.35,1.25,1,1.8-2.23,1.21,1-1.8,2.23,1.33,1.07,1.91-2.37,1.21,1-3.14,3.91Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="wallet-espace-client">
		<path class="cls-7" d="M454.2,436.75l87.22,21.87a221.35,221.35,0,0,0,2-94.3l-87.71,18.59A131.71,131.71,0,0,1,454.2,436.75Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M486.26,402.85l1.19-5.07,1.85,0-2.1,7.3-1.84,0-1.43-5.52-1.54,5.48-1.84,0-1.95-7.35,1.85,0,1.08,5.09,1.42-5.07,2,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M493.94,403.75l-2.8,0-0.47,1.41-1.86,0,2.8-7.3,2,0,2.65,7.35-1.86,0Zm-2.33-1.46,1.88,0-0.9-3.1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M499,397.9l-0.06,5.9,2.69,0,0,1.43-4.5,0,0.08-7.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M504.38,398l-0.06,5.9,2.69,0v1.43l-4.5,0,0.08-7.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M508,398l4.57,0v1.43l-2.76,0,0,1.47,2.63,0v1.43l-2.63,0,0,1.56,2.79,0v1.43l-4.6,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M519,398.11v1.43l-2,0-0.06,5.9-1.8,0,0.06-5.9-2,0V398Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M528.7,405.67q-0.93-.56-1.66-1.05a3.8,3.8,0,0,1-1.33.8,4.26,4.26,0,0,1-1.36.21,3.73,3.73,0,0,1-1-.14,2.39,2.39,0,0,1-.77-0.37,1.59,1.59,0,0,1-.49-0.59,1.8,1.8,0,0,1-.17-0.82,1.5,1.5,0,0,1,.12-0.58,1.93,1.93,0,0,1,.31-0.49,3.35,3.35,0,0,1,.45-0.44l0.54-.41a4.53,4.53,0,0,1-.76-0.86,1.75,1.75,0,0,1-.28-1,1.89,1.89,0,0,1,.16-0.76,1.75,1.75,0,0,1,.46-0.61,2.12,2.12,0,0,1,.74-0.39,3.2,3.2,0,0,1,1-.13,3,3,0,0,1,.87.13,2,2,0,0,1,.67.35,1.59,1.59,0,0,1,.43.54,1.52,1.52,0,0,1,.15.7,1.68,1.68,0,0,1-.11.59,2.22,2.22,0,0,1-.29.51,3.45,3.45,0,0,1-.41.45l-0.46.42,0.67,0.51,0.66,0.48q0.22-.32.45-0.71l0.47-.85,1.27,0.67q-0.26.5-.5,0.92a8.77,8.77,0,0,1-.5.77l1.44,0.93Zm-4.16-1.34a2,2,0,0,0,1.35-.51l-0.77-.57-0.76-.6a3.5,3.5,0,0,0-.49.45,0.74,0.74,0,0,0-.2.5,0.69,0.69,0,0,0,.2.53A0.93,0.93,0,0,0,524.54,404.33Zm-0.65-4.41a1,1,0,0,0,.15.52,2.3,2.3,0,0,0,.42.49,5.29,5.29,0,0,0,.59-0.52,0.81,0.81,0,0,0,.23-0.56,0.51,0.51,0,0,0-.19-0.42,0.86,0.86,0,0,0-1,0A0.61,0.61,0,0,0,523.9,399.92Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M467,410.76l4.57,0,0,1.43-2.76,0v1.47l2.63,0,0,1.43-2.63,0,0,1.56,2.79,0,0,1.43-4.6,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M477.6,416a2.17,2.17,0,0,1-.2.91,2.13,2.13,0,0,1-.56.73,2.7,2.7,0,0,1-.89.49,3.51,3.51,0,0,1-1.18.17,5.73,5.73,0,0,1-1.21-.14,5,5,0,0,1-1.12-.4l0.38-1.37a5.28,5.28,0,0,0,.87.33,3.82,3.82,0,0,0,1,.15,1.49,1.49,0,0,0,.76-0.16,0.59,0.59,0,0,0,.3-0.55,0.66,0.66,0,0,0-.25-0.5,3.24,3.24,0,0,0-.94-0.49,4.21,4.21,0,0,1-1.56-.95,1.87,1.87,0,0,1-.48-1.34,2,2,0,0,1,.71-1.62,3,3,0,0,1,1.94-.54,5,5,0,0,1,1.2.15,3.85,3.85,0,0,1,1,.37l-0.32,1.35a4.36,4.36,0,0,0-.84-0.31,3.77,3.77,0,0,0-.94-0.13,1.35,1.35,0,0,0-.66.13,0.49,0.49,0,0,0-.25.47,0.69,0.69,0,0,0,0,.26,0.53,0.53,0,0,0,.16.21,1.44,1.44,0,0,0,.32.2l0.53,0.24q0.46,0.19.87,0.41a3.14,3.14,0,0,1,.7.49,2.08,2.08,0,0,1,.46.63A1.87,1.87,0,0,1,477.6,416Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M484,413.33a2.14,2.14,0,0,1-.26,1,2.29,2.29,0,0,1-.71.77,3.34,3.34,0,0,1-1.05.47,5,5,0,0,1-1.31.15h-0.19l0,2.47-1.8,0,0.08-7.33,2.11,0a5.36,5.36,0,0,1,1.28.16,3.05,3.05,0,0,1,1,.45,2,2,0,0,1,.65.75A2.27,2.27,0,0,1,484,413.33Zm-1.74,0q0-1-1.43-1h-0.32l0,2.08h0.3a1.76,1.76,0,0,0,1.09-.27A0.92,0.92,0,0,0,482.21,413.32Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M488.78,416.9l-2.81,0-0.46,1.41-1.86,0,2.8-7.3,2,0,2.65,7.35-1.86,0Zm-2.33-1.45,1.88,0-0.9-3.1Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M495.35,410.93a6.15,6.15,0,0,1,1.15.12,3.82,3.82,0,0,1,1,.33l-0.28,1.32a4.92,4.92,0,0,0-.75-0.23,3.89,3.89,0,0,0-.84-0.1,2.54,2.54,0,0,0-1.78.57,2.76,2.76,0,0,0,0,3.56,2.55,2.55,0,0,0,1.77.6,3.88,3.88,0,0,0,.84-0.08,4.83,4.83,0,0,0,.76-0.22l0.25,1.33a3.81,3.81,0,0,1-1,.31,6.12,6.12,0,0,1-1.15.1,4.71,4.71,0,0,1-1.7-.3,3.43,3.43,0,0,1-1.22-.81,3.31,3.31,0,0,1-.74-1.21,4.67,4.67,0,0,1,0-3,3.31,3.31,0,0,1,.76-1.19,3.43,3.43,0,0,1,1.24-.78A4.69,4.69,0,0,1,495.35,410.93Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M498.65,411.1l4.56,0v1.43l-2.76,0,0,1.47,2.63,0v1.43l-2.63,0,0,1.56,2.79,0v1.43l-4.6,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M510.45,411.09a6.15,6.15,0,0,1,1.15.12,3.83,3.83,0,0,1,1,.33l-0.28,1.32a5,5,0,0,0-.75-0.23,3.88,3.88,0,0,0-.84-0.1,2.54,2.54,0,0,0-1.78.57,2.76,2.76,0,0,0,0,3.56,2.54,2.54,0,0,0,1.76.6,3.88,3.88,0,0,0,.84-0.08,4.88,4.88,0,0,0,.76-0.22l0.25,1.33a3.82,3.82,0,0,1-1,.31,6.15,6.15,0,0,1-1.15.1,4.7,4.7,0,0,1-1.7-.3,3.43,3.43,0,0,1-1.22-.81,3.31,3.31,0,0,1-.74-1.21,4.67,4.67,0,0,1,0-3,3.32,3.32,0,0,1,.76-1.19,3.42,3.42,0,0,1,1.24-.78A4.69,4.69,0,0,1,510.45,411.09Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M515.56,411.27l-0.06,5.9,2.7,0,0,1.43-4.5,0,0.08-7.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M520.95,418.66l-1.8,0,0.08-7.33,1.8,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M522.51,411.35l4.57,0,0,1.43-2.76,0v1.47l2.63,0,0,1.43-2.63,0,0,1.56,2.79,0,0,1.43-4.6,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M533,418.78l-3-4.4,0,4.37-1.74,0,0.08-7.33,1.74,0,3,4.4,0-4.37,1.74,0-0.08,7.33Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M541.46,411.54l0,1.43-2,0-0.06,5.9-1.8,0,0.06-5.9-2,0,0-1.43Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="communication-multi-canal">
		<path class="cls-6" d="M324.05,186a219.53,219.53,0,0,0-105.69,28.36l44.91,77.79a130,130,0,0,1,60.78-16.56V186Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M274.61,202.49a5,5,0,0,1,.24.92,3.19,3.19,0,0,1,0,.85l-1.09.16a3.91,3.91,0,0,0,0-.64,3.21,3.21,0,0,0-.16-0.67,2.08,2.08,0,0,0-.94-1.2,2.26,2.26,0,0,0-2.74,1,2.08,2.08,0,0,0,0,1.53,3.13,3.13,0,0,0,.31.62,3.88,3.88,0,0,0,.38.52l-0.94.57a3.13,3.13,0,0,1-.52-0.68,5,5,0,0,1-.4-0.85,3.84,3.84,0,0,1-.25-1.39,2.81,2.81,0,0,1,.27-1.17,2.7,2.7,0,0,1,.71-0.91,3.84,3.84,0,0,1,2.34-.85,2.71,2.71,0,0,1,1.13.24,2.82,2.82,0,0,1,1,.73A3.85,3.85,0,0,1,274.61,202.49Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M274.58,211.53a3.47,3.47,0,0,1-1.2.21,2.94,2.94,0,0,1-1.13-.23,2.85,2.85,0,0,1-1-.67,3.52,3.52,0,0,1-.88-2.44,2.79,2.79,0,0,1,.3-1.13,2.84,2.84,0,0,1,.72-0.89,3.67,3.67,0,0,1,2.26-.82,2.87,2.87,0,0,1,1.13.22,2.82,2.82,0,0,1,1,.68,3.52,3.52,0,0,1,.88,2.44,2.82,2.82,0,0,1-.3,1.13,2.93,2.93,0,0,1-.72.9A3.45,3.45,0,0,1,274.58,211.53Zm-2.86-2.22a1.55,1.55,0,0,0,.37.59,1.4,1.4,0,0,0,.55.33,1.73,1.73,0,0,0,.68.08,3.11,3.11,0,0,0,1.46-.53,1.74,1.74,0,0,0,.47-0.49,1.42,1.42,0,0,0,.21-0.61,1.71,1.71,0,0,0-.47-1.29,1.42,1.42,0,0,0-.54-0.33,1.7,1.7,0,0,0-.68-0.08,2.86,2.86,0,0,0-.77.17,2.89,2.89,0,0,0-.7.36,1.72,1.72,0,0,0-.47.49,1.42,1.42,0,0,0-.21.6A1.54,1.54,0,0,0,271.72,209.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M274.32,216.84l-0.48-1.34,1.86-1.92-3.22,1L272,213.2l5.79-1.62,0.45,1.25-2.86,2.87,4,0.38,0.45,1.25-5.48,2.46-0.49-1.36,3.1-1.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M277,224.18l-0.48-1.34,1.86-1.92-3.22,1-0.49-1.36,5.79-1.62,0.45,1.25L278,223l4,0.38,0.45,1.25L277,227.13l-0.49-1.36,3.1-1.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M284.22,229.37l0.5,1.39-3.6,1.3a2.13,2.13,0,0,1-1.79-.06,3.19,3.19,0,0,1-1.27-3.51,2.13,2.13,0,0,1,1.34-1.2L283,226l0.5,1.39-3.65,1.32a1.06,1.06,0,0,0-.64.52,1.3,1.3,0,0,0,.54,1.49,1.06,1.06,0,0,0,.83,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M280.8,237.54l2.52-3.54L280,235.22l-0.48-1.34,5.64-2,0.48,1.34-2.52,3.54,3.36-1.22,0.48,1.34-5.64,2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M282.2,241.41L281.7,240l5.64-2,0.5,1.39Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M289.35,243.25a5.1,5.1,0,0,1,.24.91,3.21,3.21,0,0,1,0,.85l-1.09.17a3.87,3.87,0,0,0,0-.64,3.21,3.21,0,0,0-.16-0.67,2.08,2.08,0,0,0-.94-1.2,2.26,2.26,0,0,0-2.74,1,2.08,2.08,0,0,0,0,1.53,3.16,3.16,0,0,0,.31.62,3.87,3.87,0,0,0,.38.52l-0.95.57a3.17,3.17,0,0,1-.52-0.68,5.07,5.07,0,0,1-.4-0.85,3.85,3.85,0,0,1-.25-1.39,2.79,2.79,0,0,1,.27-1.17,2.73,2.73,0,0,1,.71-0.91,3.84,3.84,0,0,1,2.34-.85,2.74,2.74,0,0,1,1.13.24,2.8,2.8,0,0,1,1,.73A3.83,3.83,0,0,1,289.35,243.25Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M286.9,251l-0.78-2.16-1.22,0-0.52-1.43,6.4,0.06,0.55,1.52-4.88,4.14-0.52-1.43Zm0.45-2.2,0.52,1.45,2.12-1.58Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M293.63,255.39l-1.1.4L292,254.28l-4.54,1.64-0.5-1.39,4.54-1.64-0.55-1.52,1.1-.4Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M288.75,259.53l-0.5-1.39,5.64-2,0.5,1.39Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M294,265.15a3.44,3.44,0,0,1-1.2.21,2.91,2.91,0,0,1-1.13-.23,2.82,2.82,0,0,1-1-.67,3.53,3.53,0,0,1-.88-2.44,2.8,2.8,0,0,1,.3-1.13,2.85,2.85,0,0,1,.72-0.89,3.66,3.66,0,0,1,2.26-.82,2.85,2.85,0,0,1,1.13.22,2.8,2.8,0,0,1,1,.68,3.25,3.25,0,0,1,.68,1.14,3.21,3.21,0,0,1,.21,1.3,2.82,2.82,0,0,1-.3,1.13,2.92,2.92,0,0,1-.72.9A3.46,3.46,0,0,1,294,265.15Zm-2.86-2.22a1.55,1.55,0,0,0,.37.59,1.44,1.44,0,0,0,.55.33,1.73,1.73,0,0,0,.68.07,3.11,3.11,0,0,0,1.46-.53,1.73,1.73,0,0,0,.47-0.49,1.44,1.44,0,0,0,.21-0.61,1.56,1.56,0,0,0-.1-0.69,1.55,1.55,0,0,0-.37-0.6,1.43,1.43,0,0,0-.54-0.33,1.73,1.73,0,0,0-.68-0.08,3.12,3.12,0,0,0-1.46.53,1.73,1.73,0,0,0-.47.49,1.42,1.42,0,0,0-.21.6A1.54,1.54,0,0,0,291.11,262.93Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M292.78,270.67l2.52-3.54-3.36,1.22L291.46,267l5.64-2,0.48,1.34-2.52,3.54,3.36-1.22L298.9,270l-5.64,2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M263,217.18l-0.48-1.34,1.86-1.92-3.22,1-0.49-1.36,5.79-1.62,0.45,1.25L264,216l4,0.38,0.45,1.25L263,220.14l-0.49-1.36,3.1-1.31Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M270.2,222.37l0.5,1.39-3.61,1.3a2.13,2.13,0,0,1-1.79-.06,3.19,3.19,0,0,1-1.27-3.51,2.12,2.12,0,0,1,1.34-1.2L269,219l0.5,1.39-3.65,1.32a1.06,1.06,0,0,0-.64.52,1.3,1.3,0,0,0,.54,1.49,1.06,1.06,0,0,0,.83,0Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M271.59,226.23l-4.54,1.64,0.75,2.07-1.1.4-1.25-3.46,5.64-2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M273.9,232.61l-1.1.4-0.55-1.51-4.54,1.64-0.5-1.39,4.54-1.64-0.55-1.51,1.1-.4Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M269,236.75l-0.5-1.39,5.64-2,0.5,1.39Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M271,237.17l1.08-.39,0.84,2.32-1.08.39Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M277.32,241.75a5.06,5.06,0,0,1,.24.92,3.14,3.14,0,0,1,0,.85l-1.09.16a3.87,3.87,0,0,0,0-.64,3.16,3.16,0,0,0-.16-0.67,2.09,2.09,0,0,0-.94-1.2,2.26,2.26,0,0,0-2.74,1,2.08,2.08,0,0,0,0,1.53,3.21,3.21,0,0,0,.31.62,3.85,3.85,0,0,0,.38.52l-0.95.57a3.18,3.18,0,0,1-.52-0.68,5,5,0,0,1-.4-0.85,3.85,3.85,0,0,1-.25-1.39,2.8,2.8,0,0,1,.27-1.17,2.71,2.71,0,0,1,.71-0.91,3.83,3.83,0,0,1,2.34-.84,2.68,2.68,0,0,1,1.13.24,2.79,2.79,0,0,1,1,.73A3.83,3.83,0,0,1,277.32,241.75Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M274.87,249.5l-0.78-2.16-1.22,0L272.36,246l6.4,0.06,0.55,1.52-4.88,4.14-0.52-1.43Zm0.45-2.2,0.52,1.45,2.12-1.58Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M276,256l2.52-3.54-3.36,1.22-0.48-1.34,5.64-2,0.48,1.34-2.52,3.54,3.36-1.21,0.48,1.34-5.64,2Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M279.22,261.52l-0.78-2.16-1.22,0L276.71,258l6.4,0.06,0.55,1.52-4.88,4.14-0.52-1.43Zm0.45-2.2,0.52,1.45,2.12-1.58Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M285.15,263.7l-4.54,1.64,0.75,2.07-1.1.4L279,264.36l5.64-2Z" transform="translate(-25.28 -104.81)" />
	</g>
	<g class="selector" id="cartes-fidelite">
		<path class="cls-6" d="M105.88,416.59A219.45,219.45,0,0,0,134,515.13l78.71-45.44a130,130,0,0,1-16.28-57.83Z" transform="translate(-25.28 -104.81)" />
		<path class="cls-3" d="M114,369.5l-2.4-7.9l5-1.5l0.5,1.5l-3,0.9l0.5,1.8l2.9-0.9l0.5,1.5l-2.9,0.9l0.9,3L114,369.5z"/>
		<path class="cls-3" d="M122.1,367l-1.9,0.6l-2.4-7.9l1.9-0.6L122.1,367z"/>
		<path class="cls-3" d="M121.3,358.6l2.4-0.7c0.7-0.2,1.4-0.3,2-0.3c0.6,0,1.2,0.1,1.7,0.4c0.5,0.2,1,0.5,1.3,1
		c0.4,0.4,0.7,0.9,0.8,1.5c0.2,0.6,0.2,1.1,0.1,1.7c-0.1,0.6-0.3,1.1-0.6,1.5c-0.3,0.5-0.7,0.9-1.2,1.3c-0.5,0.4-1.1,0.7-1.8,0.9
		l-2.4,0.7L121.3,358.6z M125.8,364.2c0.4-0.1,0.7-0.3,1-0.5c0.3-0.2,0.5-0.5,0.7-0.7c0.2-0.3,0.3-0.6,0.3-0.9c0-0.3,0-0.7-0.1-1.1
		c-0.1-0.4-0.3-0.7-0.5-1c-0.2-0.3-0.5-0.5-0.8-0.6c-0.3-0.1-0.6-0.2-1-0.2c-0.4,0-0.7,0-1.1,0.2l-0.6,0.2l1.5,4.8L125.8,364.2z"/>
		<path class="cls-3" d="M129.5,356.1l4.9-1.5l0.5,1.5l-3,0.9l0.5,1.6l2.8-0.9l0.5,1.5l-2.8,0.9l0.5,1.7l3-0.9l0.5,1.5l-5,1.5
		L129.5,356.1z M130.7,355l0.4-1.8l2.2-0.7l-1.1,1.9L130.7,355z"/>
		<path class="cls-3" d="M137.8,353.6l1.9,6.4l2.9-0.9l0.5,1.5l-4.8,1.5l-2.4-7.9L137.8,353.6z"/>
		<path class="cls-3" d="M146.1,359.7l-1.9,0.6l-2.4-7.9l1.9-0.6L146.1,359.7z"/>
		<path class="cls-3" d="M150.8,349.7l0.5,1.5l-2.1,0.6l1.9,6.4l-1.9,0.6l-1.9-6.4l-2.1,0.6l-0.5-1.5L150.8,349.7z"/>
		<path class="cls-3" d="M151.8,349.4l4.9-1.5l0.5,1.5l-3,0.9l0.5,1.6l2.8-0.9l0.5,1.5l-2.8,0.9l0.5,1.7l3-0.9l0.5,1.5l-5,1.5
		L151.8,349.4z M152.9,348.3l0.4-1.8l2.2-0.7l-1.1,2L152.9,348.3z"/>
		<path class="cls-3" d="M116.7,343.8c0.4-0.1,0.8-0.2,1.2-0.2c0.4,0,0.8,0,1.1,0l0.1,1.4c-0.3,0-0.6,0-0.8,0c-0.3,0-0.6,0.1-0.9,0.1
		c-0.7,0.2-1.3,0.6-1.7,1.1c-0.6,1.3-0.1,2.9,1.1,3.7c0.6,0.3,1.3,0.3,2,0c0.3-0.1,0.6-0.2,0.8-0.3c0.2-0.1,0.5-0.3,0.7-0.5l0.7,1.4
		c-0.3,0.2-0.6,0.5-0.9,0.6c-0.4,0.2-0.8,0.3-1.2,0.4c-0.6,0.2-1.2,0.3-1.8,0.2c-0.5,0-1-0.2-1.5-0.5c-0.4-0.3-0.8-0.6-1.1-1
		c-0.6-0.9-1-2-0.9-3.1c0-0.5,0.2-1,0.4-1.5c0.2-0.5,0.6-0.9,1-1.2C115.5,344.3,116,344,116.7,343.8z"/>
		<path class="cls-3" d="M126.6,347.7l-2.9,0.8l0,1.6l-1.9,0.6l0.6-8.4l2-0.6l5,6.8l-1.9,0.6L126.6,347.7z M123.8,347l2-0.6l-1.9-2.9
		L123.8,347z"/>
		<path class="cls-3" d="M134.5,341.5c0.2,0.5,0.1,1,0,1.4c-0.2,0.5-0.5,0.9-0.9,1.2l3.3,2.3l-2.3,0.7l-2.8-2l-0.3,0.1l0.8,2.6
		l-1.9,0.5l-2.2-7.6l2.4-0.7c0.4-0.1,0.9-0.2,1.4-0.2c0.4,0,0.8,0,1.2,0.1c0.3,0.1,0.7,0.3,0.9,0.5
		C134.2,340.7,134.4,341.1,134.5,341.5z M132.7,342c-0.1-0.3-0.3-0.6-0.6-0.6c-0.4-0.1-0.8-0.1-1.2,0.1l-0.5,0.2l0.6,2.1l0.5-0.1
		c0.4-0.1,0.8-0.3,1.1-0.6C132.8,342.7,132.8,342.3,132.7,342L132.7,342z"/>
		<path class="cls-3" d="M140.6,337.1l0.4,1.5l-2,0.6l1.7,6.1l-1.9,0.5l-1.7-6.1l-2,0.6l-0.4-1.5L140.6,337.1z"/>
		<path class="cls-3" d="M141.5,336.9l4.7-1.4l0.4,1.5l-2.9,0.8l0.4,1.5l2.7-0.8l0.4,1.5l-2.7,0.8l0.5,1.6l2.9-0.8l0.4,1.5l-4.7,1.4
		L141.5,336.9z"/>	</g>
		';
	}


	return '
	<div class="boule">
		<div class="boule-img left wow">
			<img src="'.get_template_directory_uri().$img_left.'" alt="" />
		</div>
		<div class="side-orange">
			'.$orange_text.'
		</div>
		<div class="boule-svg ">
			<svg id="cloud" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 603.55 602.89" width="100%" height="700px">
				<defs>
					<style>
						.cls-1 {
							fill: #2d3690;
						}

						.cls-2 {
							fill: #514949;
						}

						.cls-3 {
							fill: #fff;
						}

						.cls-4 {
							fill: #f0413e;
						}

						.cls-6,
						.cls-7 {
							opacity: 0.7;
						}

						.cls-5 {
							fill: #00aea4;
						}

						.cls-6 {
							fill: #00aea4;
						}

						.cls-7 {
							fill: #ff2a00;
						}
					</style>
				</defs>
				<title>01_masterschema_LaBoule</title>
				<path class="cls-1" d="M413.63,389.08l-0.33.07h0Z" transform="translate(-25.28 -104.81)" />
				<text x="-25.28" y="-104.81" />
				'.$schemat.'
			</svg>
		</div>
		<div class="side-green">
			'.$green_text.'
		</div>
		<div class="boule-img right wow">
			<img src="'.get_template_directory_uri().$img_right.'" alt="" />
		</div>
	</div>
	';
}






