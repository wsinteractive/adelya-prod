<?php
// [bartag foo="foo-value"]
add_shortcode( 'mise_en_avant', 'mise_en_avant_shortcode' );
function mise_en_avant_shortcode( $atts, $content='') {
	extract( shortcode_atts( array(), $atts ) );

	$output = "";

	$output = "<div class='bloc-mise-en-avant mise-en-avant-same'>";
		$output .= "<div class='bloc-mise-en-avant-content'>".$content."</div>";
	$output .= "</div>";


	return $output;
}
