<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_mise_en_avant' );
function add_mise_en_avant() {

	vc_map( array(
		"name" => "Bloc orange",
		"base" => "mise_en_avant",
		"class" => "",
		"icon" => get_template_directory_uri()."/visual-composer/images/bloc_orange.png",
		"params" => array(
				array(
					"type" => "textarea_html",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Contenu", "my-text-domain" ),
					"param_name" => "content",
				)
			)
		)
	);
}