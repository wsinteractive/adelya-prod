<?php

require get_template_directory() . '/visual-composer/shortcodes/temoignages/temoignages-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/temoignages/temoignages-template.php';

require get_template_directory() . '/visual-composer/shortcodes/slider/slider-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/slider/slider-template.php';

require get_template_directory() . '/visual-composer/shortcodes/footerdirectlink/footerdirectlink-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/footerdirectlink/footerdirectlink-template.php';

require get_template_directory() . '/visual-composer/shortcodes/footerlinksocialnetwork/footerlinksocialnetwork-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/footerlinksocialnetwork/footerlinksocialnetwork-template.php';

/*require get_template_directory() . '/visual-composer/shortcodes/subnav/subnav-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/subnav/subnav-template.php';*/

require get_template_directory() . '/visual-composer/shortcodes/title/title-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/title/title-template.php';

require get_template_directory() . '/visual-composer/shortcodes/newsbloc/newsbloc-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/newsbloc/newsbloc-template.php';

require get_template_directory() . '/visual-composer/shortcodes/clients/clients-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/clients/clients-template.php';

require get_template_directory() . '/visual-composer/shortcodes/clients/clients-categorie-listing-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/clients/clients-categorie-listing-template.php';

require get_template_directory() . '/visual-composer/shortcodes/blocaccesdirect/blocaccesdirect-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/blocaccesdirect/blocaccesdirect-template.php';

require get_template_directory() . '/visual-composer/shortcodes/blocsousnavigation/blocsousnavigation-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/blocsousnavigation/blocsousnavigation-template.php';

require get_template_directory() . '/visual-composer/shortcodes/bloccommentcamarche/bloccommentcamarche-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/bloccommentcamarche/bloccommentcamarche-template.php';

require get_template_directory() . '/visual-composer/shortcodes/miseenavant/miseenavant-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/miseenavant/miseenavant-template.php';

require get_template_directory() . '/visual-composer/shortcodes/blocdecontact/blocdecontact-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/blocdecontact/blocdecontact-template.php';

require get_template_directory() . '/visual-composer/shortcodes/blocgmap/blocgmap-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/blocgmap/blocgmap-template.php';

require get_template_directory() . '/visual-composer/shortcodes/partenaires/partenaires-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/partenaires/partenaires-template.php';

require get_template_directory() . '/visual-composer/shortcodes/partenaires/partenaires-categorie-listing-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/partenaires/partenaires-categorie-listing-template.php';

require get_template_directory() . '/visual-composer/shortcodes/imgtext/imgtext-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/imgtext/imgtext-template.php';

require get_template_directory() . '/visual-composer/shortcodes/calltoaction/calltoaction-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/calltoaction/calltoaction-template.php';

require get_template_directory() . '/visual-composer/shortcodes/boule/boule-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/boule/boule-template.php';



require get_template_directory() . '/visual-composer/shortcodes/articles/articles-slider-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/articles/articles-slider-template.php';

require get_template_directory() . '/visual-composer/shortcodes/articles/articles-list-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/articles/articles-list-template.php';

require get_template_directory() . '/visual-composer/shortcodes/authors/authors-list-settings.php';
require get_template_directory() . '/visual-composer/shortcodes/authors/authors-list-template.php';
