<?php
/**** ajout du shortcode temoignages *********/
add_action( 'vc_before_init', 'add_calltoaction_elem' );
function add_calltoaction_elem() {

	vc_map( array(
		"name" => "Bouton de contact",
		"base" => "calltoaction",
		"class" => "",
		"params" => array(
				array(
					"type" => "vc_link",
					"holder" => "div",
					"heading" => __( "Lien", "my-text-domain" ),
					"param_name" => "link",
				),
				array(
					"type" => "textfield",
					"holder" => "div",
					"class" => "",
					"heading" => __( "Sous Titre", "my-text-domain" ),
					"param_name" => "text",
				)
			)
		)
	);
}