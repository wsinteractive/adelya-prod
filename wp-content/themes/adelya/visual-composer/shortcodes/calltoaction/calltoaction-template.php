<?php
// [bartag foo="foo-value"]
add_shortcode( 'calltoaction', 'calltoaction_shortcode' );
function calltoaction_shortcode( $atts ) {
	global $post;
	extract( shortcode_atts( array(
		'link' => '',
		'text' => '',
		), $atts ) );


	$link_block = vc_build_link($link);
	$text_content = (!empty($text)) ? "<span class='calltoaction--text'>".$text."</span>" : '';


	return "<div class='calltoaction'>
				<a href='".$link_block['url']."' alt='".$link_block['alt']."' target='".$link_block['target']."'>
					<span class='icon icon-rappeler2'></span>
					${text_content}
				</a>
			</div>";
}
