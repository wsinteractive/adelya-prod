<?php
/*
Plugin Name: Custom Menu ws-interactive
Plugin URL: 
Description: A little plugin to custom WordPress menus
Version: 0.1
Author: David Mottet
Author URI: 
Contributors: 
Text Domain: 
Domain Path: 
*/

class rc_ws_custom_menu {

	/*--------------------------------------------*
	 * Constructor
	 *--------------------------------------------*/

	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {

		// add custom menu fields to menu
		add_filter( 'wp_setup_nav_menu_item', array( $this, 'rc_scm_add_custom_nav_fields' ) );

		// save menu custom fields
		add_action( 'wp_update_nav_menu_item', array( $this, 'rc_scm_update_custom_nav_fields'), 10, 3 );

		// edit menu walker
		add_filter( 'wp_edit_nav_menu_walker', array( $this, 'rc_scm_edit_walker'), 10, 2 );
	
	} // end constructor

	/* All functions will be placed here */

	/**
	 * Add custom fields to $item nav object
	 * in order to be used in custom Walker
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function rc_scm_add_custom_nav_fields( $menu_item ) {

		if(!empty($menu_item->ID)){
			$menu_item->linktype = get_post_meta( $menu_item->ID, '_menu_item_linktype', true );
			$menu_item->linktypecolor = get_post_meta( $menu_item->ID, '_menu_item_linktypecolor', true );
		}
		return $menu_item;

	}

	/**
	 * Save menu custom fields
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function rc_scm_update_custom_nav_fields( $menu_id, $menu_item_db_id, $args ) {

		// Check if element is properly sent
		print_r( is_array( $_REQUEST['menu-item-linktype']));
		if ( is_array( $_REQUEST['menu-item-linktype']) ) {
			$linktype_value = $_REQUEST['menu-item-linktype'][$menu_item_db_id];
			update_post_meta( $menu_item_db_id, '_menu_item_linktype', $linktype_value );
		}
		print_r( is_array( $_REQUEST['menu-item-linktypecolor']));
		if ( is_array( $_REQUEST['menu-item-linktypecolor']) ) {
			$linktypecolor_value = $_REQUEST['menu-item-linktypecolor'][$menu_item_db_id];
			update_post_meta( $menu_item_db_id, '_menu_item_linktypecolor', $linktypecolor_value );
		}
	}

	/**
	 * Define new Walker edit
	 *
	 * @access      public
	 * @since       1.0 
	 * @return      void
	*/
	function rc_scm_edit_walker($walker,$menu_id) {

		return 'Walker_Nav_Menu_Edit_Custom';

	}

}

// instantiate plugin's class
$GLOBALS['sweet_custom_menu'] = new rc_ws_custom_menu();

include_once( 'edit_custom_walker.php' );
include_once( 'custom_walker.php' );

?>